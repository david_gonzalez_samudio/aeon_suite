# README #

### What is this repository for? ###

#AEON: an Energy Efficiency Inspection Support Framework for Android Developers

##It is experimental and requires Android's SDK (ADB must be accessible from command line) and special apps, [Trepn Manager](https://bitbucket.org/david_gonzalez_samudio/aeon_suite/downloads/TrepManager.apk) to communicate the plugin with the Android device alongside [Trepn](https://play.google.com/store/apps/details?id=com.quicinc.trepn&hl=en).

* An abstract of what we are about

Android developers improving their apps often conduct an energy
efficiency inspection — a peer-reviewed process that determines
if a product wastes energy by following programming and performance
criteria. However, they find the required body of knowledge
scattered and support tools disconnected, and, therefore, conduct an
error-prone and time-consuming inspection.
To address such problem this paper introduces AEON, a novel
approach that automatically detects energy inefficiencies in Android
apps and supports developers correcting them. Our contributions
are: (1) a hierarchy of energy-efficient criteria, consisting of structural
and temporal specifications that formalize Android API usage
energy-wise; (2) an energy-driven model checker, which verifies
such specifications on Android-tailored state-transition graphs; (3)
an inspection support framework design that supports developers
verifying, refactoring and profiling such inefficiencies; (4) an empirical
evaluation in 600 apps, with 152 being inefficient; (5) and an
implementation extending the IntelliJ framework, consisting of an
API and a support tool.

Keywords: Software Inspection Support, Mobile Energy Efficiency,
Software Model Checking, Software Experimentation.

The code will be available publically after our paper's publication. 
Available upon request for researchers.

* Version 0.7.0
* [Git it!](git@bitbucket.org:david_gonzalez_samudio/aeon_suite.git)

We'll keep you posted =]

### How do I get set up? ###
We have the following dependecies and a configuration to follow.
##Dependencies
 Android Studio and SDK (ADB executable from console)
 [Trepn Manager](https://bitbucket.org/david_gonzalez_samudio/aeon_suite/downloads/TrepManager.apk)
 [Trepn](https://play.google.com/store/apps/details?id=com.quicinc.trepn&hl=en).
 An Android device using a compatible Qualcomm Snapdragon chipset.

### Connect a compatible device via USB
Check if your device is compatible [here](https://www.qualcomm.com/products/snapdragon/devices/all). 
Most recent devices use the Snapdragon chipsets that allow power measurements by the Trepn profiler. Otherwise, power measurements will not be recorded.

###Test your device connection to ABD
Be sure that your device communicates with ADB. A common problem is the need fro USB drivers, check your Android SDK Manager(type android on a CLI window) and check if the USB drivers are installed ( using the manager GUI, they should be checked in the extras folder).

###Choose your poison:
 0 .A. In your IDE(IntelliJ IDEA or Android Studio), Main Menu > File > Settings > Plugins > Type aeon in the search input and hit enter> Select AEON, install and apply > Restart your IDE.

 0 .B. Download our stable version from the [IntelliJ Plugin Repository](https://goo.gl/MXZbX5) (Recommended for developers).

 0 .C. Nightly build from IntelliJ Plugin Project. Follow the next steps (Recommended for researchers).

##Configuration

###Add your Android SDK to your console commands:

 1 .W. Windows:

 Open Settings > Search "environment" > Select "Edit your system environment variables" > add your Android SDK folder, its tools/ and platform-tools/ folders to PATH

 1 .L. Linux:
 
```sh
export ANDROID_HOME=<your_android_sdk_path>
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
```

###Install the "magic" apps
 2 . Install [Trepn](https://play.google.com/store/apps/details?id=com.quicinc.trepn&hl=en). It is the profiler we connect with.

 3 . Install [Trepn Manager](https://bitbucket.org/david_gonzalez_samudio/aeon_suite/downloads/TrepManager.apk). This is our custom app to communicate with the plugin with your device, and start the and stop profiling sessions from the plugin.

###Look for the AEON Panel
 4 . On your IntelliJ IDEA/Android Studio, look for our panel, it should be in your tool buttons (where Maven and Gradle are).

If you cannot see them, it may be that the tool buttons panel is hidden. Go to your main menu > View > Tool Buttons. Enable the Tools Buttons so you can see AEON's icon in the tool buttons' panel.

##How do I use AEON?
We are all about inspecting apps for energy problems. Please, take a look:
###Energy Inspection Process
####Look for energy faults and checks in your gutter or on AEON's panel:

![screenshot_15663.png](https://bitbucket.org/repo/KApyLp/images/1372536996-screenshot_15663.png)
####Assess for faults by connecting a device and "Run your app" from our panel.
Click in the gutter icons to inject energy breakpoints. You will see some new code being added to your class. They enable communication with Trepn.

Then run your app by clicking "Start Profiling" button on the "Energy Profiling" tab.

####Once your app is running, disconnect the USB cable, AEON will initiate profiling after that.
You will see the normal execution of your application building and installation in your device. Once your app is up and running, the Trepn Manager app will setup the Trepn service and your profile session be recorded.
####Do your tests on the app
Interact with your app, so the code that was point out by the energy faults gets executed. You may want to do it more than once, the more you test, the more evidence you will get about the fault.
####Reconnect the device
The plugin is waiting for your device to reconnect, as soon as the device is reconnected. This means that the Trepn manager will finish the profiling session and send it to the plugin. The plugin the will render the results.

![AEON_Framework_UserInterface.png](https://bitbucket.org/repo/KApyLp/images/1382290511-AEON_Framework_UserInterface.png)

####Analyze the results
In the "Energy Profiling" tab, the results will be plotted as XY charts. You can then evaluate if your code was consuming more energy that you expected.

![AEON_Framework_UserInterface-DIGS_CT15.png](https://bitbucket.org/repo/KApyLp/images/2179387158-AEON_Framework_UserInterface-DIGS_CT15.png)

####Have.Fun.Repeat

###Known Issues
I haven't updated some interactions with the IntelliJ API 13+, that is why gutter icons are rendered more that once.

### Contribution guidelines ###
* How to run tests
* Deployment instructions
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: David Gonzalez, concatTheStrings("d" + "gonza" + "10") at gmu dot edu
* Other community or team contact