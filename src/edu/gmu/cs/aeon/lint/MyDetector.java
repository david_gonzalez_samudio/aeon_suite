package edu.gmu.cs.aeon.lint;

/**
 * Created by DavidIgnacio on 4/17/2015.
 */
import java.util.Collection;
import java.util.Collections;

import com.android.tools.lint.detector.api.*;
import org.w3c.dom.Element;

public class MyDetector extends ResourceXmlDetector {
    public static final Issue ISSUE = Issue.create(
            "MyId",
            "My brief summary of the issue",
            "My summary of what the detector looks for",
            "My longer explanation of the issue",
            Category.CORRECTNESS, 6, Severity.WARNING,
            new Implementation(MyDetector.class, Scope.RESOURCE_FILE_SCOPE));

    @Override
    public Collection<String> getApplicableElements() {
        return Collections.singletonList(
                "com.google.io.demo.MyCustomView");
    }

    @Override
    public void visitElement(XmlContext context, Element element) {
        if (!element.hasAttributeNS(
                "http://schemas.android.com/apk/res/com.google.io.demo",
                "exampleString")) {
            context.report(ISSUE, element, context.getLocation(element),
                    "Missing required attribute 'exampleString'", null);
        }
    }
}