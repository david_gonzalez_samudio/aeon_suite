package edu.gmu.cs.aeon.lint;

/**
 * Created by DavidIgnacio on 4/17/2015.
 */
import java.util.Arrays;
import java.util.List;

import com.android.tools.lint.client.api.IssueRegistry;
import com.android.tools.lint.detector.api.Issue;

public class MyIssueRegistry extends IssueRegistry {
    public MyIssueRegistry() {
    }

    @Override
    public List<Issue> getIssues() {
        return Arrays.asList(
                MyDetector.ISSUE
        );
    }
}