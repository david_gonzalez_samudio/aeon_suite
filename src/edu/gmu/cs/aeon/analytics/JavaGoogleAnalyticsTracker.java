package edu.gmu.cs.aeon.analytics;

import com.brsanthu.googleanalytics.AppViewHit;
import com.brsanthu.googleanalytics.GoogleAnalytics;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.plugin.view.configuration.AEONConstants;


/**
 * Created by DavidIgnacio on 11/25/2014.
 */
public class JavaGoogleAnalyticsTracker implements AnalyticsTracker.TrackerInterface, AEONConstants {
    private GoogleAnalytics jGoogleAnalyticsTracker;
    private String userID="";
    private String trackingID="";
    public static final String PLUGIN_ACTION_IDE_OPEN = "PLUGIN_ACTION_IDE_OPEN";

    protected JavaGoogleAnalyticsTracker(String userID, String trackingID) {
        jGoogleAnalyticsTracker = new GoogleAnalytics("UA-12345678-1");
        this.userID=userID;
        this.trackingID=trackingID;
    }

    @Override
    public void trackFocusPoint(String focusPointName) {


              jGoogleAnalyticsTracker.postAsync(new AppViewHit(PLUGIN_ID,PLUGIN_VERSION, focusPointName).trackingId(trackingID).userId(userID));
    }
    @Override
    public void postEventHit(Report report) {

        jGoogleAnalyticsTracker.postAsync( new com.brsanthu.googleanalytics.EventHit(report.analysisCategory.name(), report.predictionType.name(),report.reason, report.isBad?1:0).trackingId(trackingID).userId(userID));
    }
}
