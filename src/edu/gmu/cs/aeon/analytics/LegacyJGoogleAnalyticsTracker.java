package edu.gmu.cs.aeon.analytics;

import com.boxysystems.jgoogleanalytics.FocusPoint;
import com.boxysystems.jgoogleanalytics.JGoogleAnalyticsTracker;
import com.boxysystems.jgoogleanalytics.LoggingAdapter;
import edu.gmu.cs.aeon.core.analysis.report.Report;

/**
 * Created by DavidIgnacio on 11/25/2014.
 */
public class LegacyJGoogleAnalyticsTracker implements AnalyticsTracker.TrackerInterface {
    protected JGoogleAnalyticsTracker jGoogleAnalyticsTracker;

    protected LegacyJGoogleAnalyticsTracker() {
        jGoogleAnalyticsTracker = new JGoogleAnalyticsTracker("AEON", "0.0.1", "UA-56700348-1");
        LoggingAdapter loggingAdapter = new LoggingAdapter() {
            @Override
            public void logError(String s) {
                System.out.println("JGoogleAnalyticsTracker.Error:: " + s);
            }

            @Override
            public void logMessage(String s) {
                System.out.println("JGoogleAnalyticsTracker.Log:: " + s);
            }
        };
        jGoogleAnalyticsTracker.setLoggingAdapter(loggingAdapter);
    }

    public void trackFocusPoint(String focusPointName) {
        jGoogleAnalyticsTracker.trackAsynchronously(new FocusPoint(focusPointName));
    }

    @Override
    public void postEventHit(Report report) {

    }
}