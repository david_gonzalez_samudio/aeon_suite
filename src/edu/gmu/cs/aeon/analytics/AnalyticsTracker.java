package edu.gmu.cs.aeon.analytics;

import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.plugin.controller.application.Configuration;
import org.jetbrains.annotations.NotNull;

/**
 * Created by DavidIgnacio on 11/13/2014.
 * proxy for JGoogleAnalyticsTracker
 */
public class AnalyticsTracker {
    private static TrackerInterface trackerInterface;

    public static
    @NotNull
    TrackerInterface getTracker() {
        if (!Configuration.getInstance().isTrackingEnabled()) {
            return new EMPTY_TRACKER();
        }
//        boolean isLegacy = Configuration.getInstance().isLegacyEnabled();
//        if (trackerInterface != null) {
//            return trackerInterface;
//        }
//
//        if (isLegacy) {
//            trackerInterface = new LegacyJGoogleAnalyticsTracker();
//        } else {
        String idval=Configuration.getInstance().TRACKING_ID;
            trackerInterface = new JavaGoogleAnalyticsTracker(idval, idval+"_"+System.nanoTime());// session value
//        }

        return trackerInterface;
    }

    public interface TrackerInterface {
        public void trackFocusPoint(String focusPointName);

        void postEventHit(Report report);
    }

    public static final class EMPTY_TRACKER implements TrackerInterface {
        @Override
        public void trackFocusPoint(String focusPointName) {

        }

        @Override
        public void postEventHit(Report report) {

        }
    }

}
