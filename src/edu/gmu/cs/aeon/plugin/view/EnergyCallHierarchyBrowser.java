package edu.gmu.cs.aeon.plugin.view;

import com.intellij.ide.IdeBundle;
import com.intellij.ide.hierarchy.HierarchyTreeStructure;
import com.intellij.ide.hierarchy.call.CallHierarchyBrowser;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.ui.PopupHandler;
import edu.gmu.cs.aeon.plugin.model.EnergyCalleeMethodsTreeStructure;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.Map;

/**
 * Created by DavidIgnacio on 4/18/2015.
 */
public class EnergyCallHierarchyBrowser extends CallHierarchyBrowser {
    @SuppressWarnings({"UnresolvedPropertyKey"})
    public static final String ENERGY_CALLEE_TYPE = IdeBundle.message("title.hierarchy.energy.callees.of");
    @SuppressWarnings({"UnresolvedPropertyKey"})
    private static final String ENERGY_CALLEE_TYPE_ACTION = IdeBundle.message("action.energy.caller.methods.hierarchy");
    public EnergyCallHierarchyBrowser(Project project, PsiMethod method) {
        super(project, method);

    }
    @Override
    protected void createTrees(@NotNull final Map<String, JTree> type2TreeMap) {
        super.createTrees(type2TreeMap);
        ActionGroup group = (ActionGroup) ActionManager.getInstance().getAction(IdeActions.GROUP_CALL_HIERARCHY_POPUP);
        final JTree tree1 = createTree(false);
        PopupHandler.installPopupHandler(tree1, group, ActionPlaces.CALL_HIERARCHY_VIEW_POPUP, ActionManager.getInstance());
        final BaseOnThisMethodAction baseOnThisMethodAction = new BaseOnThisMethodAction();
        baseOnThisMethodAction
                .registerCustomShortcutSet(ActionManager.getInstance().getAction(IdeActions.ACTION_CALL_HIERARCHY).getShortcutSet(), tree1);
        type2TreeMap.put(ENERGY_CALLEE_TYPE, tree1);

    }


    public void changeToEnergyView(){
        changeView(ENERGY_CALLEE_TYPE);
      //  myCurrentViewType=ENERGY_CALLEE_TYPE;
    }

    @Override
    protected void prependActions(@NotNull DefaultActionGroup actionGroup) {
        actionGroup.add(new ChangeViewTypeActionBase(ENERGY_CALLEE_TYPE_ACTION,
                ENERGY_CALLEE_TYPE_ACTION,
                EnergyIcons.ENERGY_MARKER_ENERGY_INFO, ENERGY_CALLEE_TYPE));
        super.prependActions(actionGroup);
    }

    @Override
    protected HierarchyTreeStructure createHierarchyTreeStructure(@NotNull final String typeName, @NotNull final PsiElement psiElement) {

        if (ENERGY_CALLEE_TYPE.equals(typeName)) {
            return new EnergyCalleeMethodsTreeStructure(myProject, (PsiMethod) psiElement, getCurrentScopeType());
        } else {
            return super.createHierarchyTreeStructure(typeName, psiElement);
        }
    }

    private class ChangeViewTypeActionBase extends ToggleAction {
        private final String myTypeName;

        public ChangeViewTypeActionBase(final String shortDescription, final String longDescription, final Icon icon, String typeName) {
            super(shortDescription, longDescription, icon);
            myTypeName = typeName;
        }

        @Override
        public final boolean isSelected(final AnActionEvent event) {
            return myTypeName.equals(myCurrentViewType);
        }

        @Override
        public final void setSelected(final AnActionEvent event, final boolean flag) {
            if (flag) {
//        setWaitCursor();
                // invokeLater is called to update state of button before long tree building operation
                ApplicationManager.getApplication().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        changeView(myTypeName);
                    }
                });
            }
        }

        @Override
        public final void update(final AnActionEvent event) {
            super.update(event);
            setEnabled(isValid());
        }
    }
}
