package edu.gmu.cs.aeon.plugin.view;

import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Created by DavidIgnacio on 10/20/2014.
 */
public interface Showable {
    @NotNull
    public Icon getIcon();

    public int getId();

    @NotNull
    public PsiElement getElement();

    @NotNull
    public String getText();
}
