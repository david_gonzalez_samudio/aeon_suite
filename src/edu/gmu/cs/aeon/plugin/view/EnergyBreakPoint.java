package edu.gmu.cs.aeon.plugin.view;

import com.intellij.codeInsight.daemon.GutterIconNavigationHandler;
import com.intellij.ide.hierarchy.call.JavaCallHierarchyProvider;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.openapi.editor.markup.RangeHighlighter;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import edu.gmu.cs.aeon.core.BaseCodeGenerator;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.util.Vector;

public class EnergyBreakPoint extends GutterIconRenderer implements Showable, GutterIconNavigationHandler<PsiElement> {

    public static String START_METHOD_NAME = "energyBreakPointStart";
    public static String END_METHOD_NAME = "energyBreakPointEnd";

    private Icon myIcon;
    private PsiElement myPsiElement;
    private RangeHighlighter rangeHighlighter;
    private PsiElement method;

    public EnergyBreakPoint(PsiElement element) {

        myPsiElement = element;
        asDisabled();

    }


    @Override
    public int getId() {
        return hashCode();
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return myIcon;
    }

    @Override
    public boolean isNavigateAction() {

        return true;

    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (!(o instanceof EnergyBreakPoint))
            return false;
        EnergyBreakPoint other = (EnergyBreakPoint) o;
        if (myPsiElement.equals(other.myPsiElement)) {
            return true;
        }

        return false;
    }


    @Override
    public int hashCode() {
        return 777 + myPsiElement.hashCode();
    }

    @Override
    public PsiElement getElement() {
        return myPsiElement;

    }

    @NotNull
    @Override
    public String getText() {
        return null;
    }

    public boolean isEnabled() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_START) || myIcon.equals(EnergyIcons.ENERGY_MARKER_START_COMPONENT) || myIcon.equals(EnergyIcons.ENERGY_MARKER_STOP) || myIcon.equals(EnergyIcons.ENERGY_MARKER_REFACTOR)) {
            return true;
        }
        return false;
    }

    public boolean isDisabled() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_DISABLED) || myIcon.equals(EnergyIcons.ENERGY_MARKER_LINKER)) {
            return true;
        }
        return false;
    }

    public boolean isStartComponent() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_START_COMPONENT)) {
            return true;
        }
        return false;
    }

    public boolean isStart() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_START)) {
            return true;
        }
        return false;
    }

    public boolean isStop() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_STOP)) {
            return true;
        }
        return false;
    }

    public void asStart() {
        myIcon = EnergyIcons.ENERGY_MARKER_START;
    }

    public void asStartComponent() {
        myIcon = EnergyIcons.ENERGY_MARKER_START_COMPONENT;
    }

    public void asStop() {
        myIcon = EnergyIcons.ENERGY_MARKER_STOP;

    }

    public void asEnabled() {
        myIcon = EnergyIcons.ENERGY_MARKER_REFACTOR;
    }

    public void asDisabled() {
        myIcon = EnergyIcons.ENERGY_MARKER_DISABLED;
    }

    public void asLinker() {
        myIcon = EnergyIcons.ENERGY_MARKER_LINKER;
    }
    public void asEnergyInfo() {
        myIcon = EnergyIcons.ENERGY_MARKER_ENERGY_INFO;
    }
    public void asWakeLock() {
        myIcon = EnergyIcons.ENERGY_MARKER_WAKE_LOCK;
    }
    public void asLocation() {
        myIcon = EnergyIcons.ENERGY_MARKER_LOCATION;
    }
    public void asGoodFix() {
        myIcon = EnergyIcons.ENERGY_MARKER_GOOD_FIX;
    }
    public void asHierarchy(PsiMethod method) {
        this.method=method;
        myIcon = EnergyIcons.ENERGY_MARKER_HIERARCHY;
    }

    @Override
    public AnAction getClickAction() {
        return new EnergyBreakPointAction();
    }


    @Override
    public void navigate(MouseEvent mouseEvent, PsiElement psiElement) {
//        System.out.println("MouseEvent: clicks " + mouseEvent.getClickCount() + "," + mouseEvent.paramString() + ", element" + psiElement.getText() + ", affecting:" + myPsiElement.getText());

        if (mouseEvent.getClickCount() > 1) {
            return;
        }
        if (method instanceof PsiMethod){
            PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(myPsiElement);
            if(isCallHierarchy()){
                //System.out.print("Clicking "+method.getText());
                new CallHierarchyGenerator(cls, method).execute();
                return;
            }
        }

        if (myPsiElement instanceof PsiStatement||method instanceof PsiMethod){
            navigateHandler();
        }
    }

    public void nonBlockingNavigationHandler() {

        if (isStart()) {
            ApplicationManager.getApplication().invokeLater(new Runnable() {
                                                                @Override
                                                                public void run() {

                                                                    final PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(myPsiElement);

                                                                    boolean isInActivity = AndroidUtilFacade.isClassSubclassOfActivity(cls);
                                                                    if (!isInActivity)
                                                                        return;

                                                                    ApplicationManager.getApplication().runWriteAction(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            new EnergyManagerGenerator(cls, Action.GENERATE_ACTION)
                                                                                    .generateAction();

                                                                            new EnergyManagerGenerator(cls, Action.START_ACTION)
                                                                                    .startAction();

                                                                        }
                                                                    });
                                                                }
                                                            }
            );


        }


    }

    public void navigateHandler() {

        if (myPsiElement == null) {
            return;
        }
        if (!myPsiElement.isValid())
            return;

        PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(myPsiElement);

        if (isDisabled() || isStartComponent()) {

            if (cls == null)
                return;

            boolean isInActivity = AndroidUtilFacade.isClassSubclassOfActivity(cls);
            if (!isInActivity)
                return;

            new EnergyManagerGenerator(cls, Action.GENERATE_ACTION)
                    .execute();

            new EnergyManagerGenerator(cls, Action.START_ACTION)
                    .execute();


        } else {
            if (isStart()) {
                new EnergyManagerGenerator(cls, Action.END_ACTION)
                        .execute();


            } else {
                if (isStop()) {
                    new EnergyManagerGenerator(cls, Action.REMOVE_ACTION)
                            .execute();

                }else{

                }
            }

        }
    }

    private boolean isCallHierarchy() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_HIERARCHY)) {
            return true;
        }
        return false;

    }

    public static enum Action {REMOVE_ACTION, END_ACTION, START_ACTION, GENERATE_ACTION}

    private class EnergyBreakPointAction extends AnAction {
        @Override
        public void actionPerformed(AnActionEvent anActionEvent) {
            navigateHandler();
        }
    }

    public class CallHierarchyGenerator extends BaseCodeGenerator {
        public static final String COMMAND_NAME = "Energy Manager";
        private final PsiElement method;
        private Vector<PsiElement> toRemove;
        private Vector<PsiElement> toRedo;

        public CallHierarchyGenerator(final PsiClass psiClass, PsiElement method) {
            super(psiClass, COMMAND_NAME);
            this.method = method;

        }
        @Override
        public void generate() {
           // new JavaCallHierarchyProvider().createHierarchyBrowser(method);
           // DoMy Stuff
          //  new JavaCallHierarchyProvider().browserActivated((new JavaCallHierarchyProvider().createHierarchyBrowser(method)));
        }

        @Override
        public void unGenerate() {

        }
    }


    public class EnergyManagerGenerator extends BaseCodeGenerator {
        public static final String COMMAND_NAME = "Energy Manager";
        private final Action action;
        private Vector<PsiElement> toRemove;
        private Vector<PsiElement> toRedo;

        public EnergyManagerGenerator(final PsiClass psiClass, Action action) {
            super(psiClass, COMMAND_NAME);
            this.action = action;
        }



// todo extract local variables from loop, add as params tuple to allow smooth migration and result tuple to continue the code in the onResult new method with taskId

        @Override
        public void generate() {
            switch (action) {
                case GENERATE_ACTION:
                    generateAction();
                    break;
                case START_ACTION:
                    startAction();
                    break;
                case END_ACTION:
                    endAction();
                    break;
                case REMOVE_ACTION:
                    removeAction();
                    break;
            }

        }

        public void removeAction() {
            myPsiElement.delete();
        }

        public void endAction() {
            if (myPsiElement != null && myPsiElement.isValid() && myPsiElement.getParent() != null) {
                PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(myPsiElement.getProject());
                myPsiElement.getParent().addBefore(elementFactory.createStatementFromText(END_METHOD_NAME + "();", null), myPsiElement);
                myPsiElement.delete();

            }
        }

        public void startAction() {
            PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(myPsiElement);
            PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(myPsiElement.getProject());
            final int currentElementLineNumber = FileDocumentManager.getInstance().getDocument(myPsiElement.getContainingFile().getVirtualFile()).getLineNumber(myPsiElement.getTextOffset());
            PsiMethod psiMethod = IntelliJUtilFacade.lookUpForContainingMethod(myPsiElement);
            String methodName = "";
            if (psiMethod != null) {
                methodName = "." + psiMethod.getName();
            }
            String EMSName = cls.getQualifiedName() + methodName + "." + currentElementLineNumber;
            PsiStatement psiStatement = elementFactory.createStatementFromText(START_METHOD_NAME + "(" + Math.abs(EMSName.hashCode()) + ", \"" + EMSName + "\");", null);
            if (myPsiElement.getParent() != null && myPsiElement.getParent() != null && myPsiElement.getParent().isValid())
                myPsiElement.getParent().addBefore(psiStatement, myPsiElement);
        }

        public void generateAction() {
            PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(myPsiElement);
            PsiMethod[] methods = cls.getMethods();
            if (methods == null)
                return;

            boolean isInActivity = AndroidUtilFacade.isClassSubclassOfActivity(cls);

            if (!isInActivity) {
                // MessageDialogBuilder.yesNo("Error adding methods for Energy profiling", "Please invoke in a class extending Activity.").show();
                return;
            }


            toRemove = new Vector<PsiElement>();
            toRedo = new Vector<PsiElement>();

            PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(prj);

            String EBSmethodBodyText = "{        final Intent stateUpdate = new Intent(\"com.quicinc.Trepn.UpdateAppState\");\n" +
                    "        stateUpdate.putExtra(\"com.quicinc.Trepn.UpdateAppState.Value\", stateId);\n" +
                    "        stateUpdate.putExtra(\"com.quicinc.Trepn.UpdateAppState.Value.Desc\", stateDescription);\n" +
                    "        sendBroadcast(stateUpdate);" +
                    "}";
            final PsiMethod EBSeBreakMethod = elementFactory.createMethodFromText("public void " + START_METHOD_NAME + "(final int stateId, final String stateDescription) " + EBSmethodBodyText, null);


            String EBEmethodBodyText = "{       final Intent stateUpdate = new Intent(\"com.quicinc.Trepn.UpdateAppState\");\n" +
                    "        stateUpdate.putExtra(\"com.quicinc.Trepn.UpdateAppState.Value\", 0);\n" +
                    "        sendBroadcast(stateUpdate);" +
                    "}";

            final PsiCodeBlock EBEeBreakMethodBody = elementFactory.createCodeBlockFromText(EBEmethodBodyText, cls);
            final PsiMethod EBEeBreakMethod = elementFactory.createMethod(END_METHOD_NAME, PsiType.VOID);

            EBEeBreakMethod.getBody().replace(EBEeBreakMethodBody);
            boolean addStartMethod = true;
            boolean addEndMethod = true;
            for (PsiMethod method : methods) {
                if (method.getName().equals(EBSeBreakMethod.getName()))
                    addStartMethod = false;
                else if (method.getName().equals(EBEeBreakMethod.getName()))
                    addEndMethod = false;
            }
            if (addStartMethod) {
                PsiElement added = cls.add(EBSeBreakMethod);
                toRemove.add(added);
                toRemove.add(added.add(JavaPsiFacade.getInstance(cls.getProject()).getElementFactory().createCommentFromText(String.format("// Generated  %s method", EBSeBreakMethod.getName()), null)));
            }


            if (addEndMethod) {
                PsiElement added = cls.add(EBEeBreakMethod);
                toRemove.add(added);
                toRemove.add(added.add(JavaPsiFacade.getInstance(cls.getProject()).getElementFactory().createCommentFromText(String.format("// Generated  %s method", EBEeBreakMethod.getName()), null)));
            }


            if (addStartMethod || addEndMethod) {
                IntelliJUtilFacade.addImportStatementToPsiElement(myPsiElement, "android.content.Intent");
                JavaCodeStyleManager.getInstance(prj).shortenClassReferences(cls);

            }


        }

        @Override
        public void unGenerate() {

            if (toRemove != null && toRemove.size() > 0) {
                PsiElement redoHere = toRemove.get(0);
                for (PsiElement element : toRedo) {
                    redoHere = redoHere.addAfter(element, redoHere);
                }

                for (PsiElement element : toRemove) {
                    element.delete();
                }
            }

            toRemove = null;
            toRedo = null;
        }
    }
}