package edu.gmu.cs.aeon.plugin.view;

import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBPanel;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.components.JBTabbedPane;
import edu.gmu.cs.aeon.plugin.model.ApplicationStateRecord;
import edu.gmu.cs.aeon.trepn.core.dataset.ITrepnSeriesBuilder;
import edu.gmu.cs.aeon.trepn.core.dataset.TrepnSeriesBuilder;
import edu.gmu.cs.aeon.trepn.core.dataset.TrepnStat;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.sql.*;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Vector;

//import javax.swing.*;


public class EnergyDataPlotPanel extends JBTabbedPane {

    private static final long serialVersionUID = 1L;

    static {
        // set a theme using the new shadow generator feature available in
        // 1.0.14 - for backwards compatibility it is not enabled by default
        ChartFactory.setChartTheme(new StandardChartTheme("JFree/Shadow",
                true));
    }

    String trepnDatafilePath;
    Connection connection;
    private java.util.List<ApplicationStateRecord> applicationStateRecordList;
    //private LegendItemCollection originalLegendItemCollection = null;
    private XYToolTipGenerator toolTipGenerator;

    // int offset = 0;


    public EnergyDataPlotPanel() {
        System.out.println("*** EnergyDataPlotPanel constructor");
        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                "Profiling results",  // title
                "Timestamp",             // x-axis label
                "Units",   // y-axis label
                new XYSeriesCollection(),            // data
                true,               // create legend?
                true,               // generate tooltips?
                false               // generate URLs?
        );
        ChartPanel panel = new ChartPanel(chart);
        panel.setFillZoomRectangle(true);
        panel.setMouseWheelEnabled(true);
        JBScrollPane jsb = new JBScrollPane(panel);
        this.addTab("Default", jsb);

    }

    /**
     * Creates a chart.
     *
     * @param dataset a dataset.
     * @return A chart.
     */
    private static JFreeChart createChart(XYDataset dataset) {
        System.out.println("*** createChart");

        /*JFreeChart chart = ChartFactory.createXYAreaChart(
                "Profiling results",  // title
                "Timestamp",             // x-axis label
                "Units",   // y-axis label
                dataset,            // data
                org.jfree.chart.plot.PlotOrientation.VERTICAL,
                true,               // create legend?
                true,               // generate tooltips?
                false               // generate URLs?
        );
        */
        String chartTitle = "Profiling results";
        String chartUnit = "Units";
        if (dataset.getSeriesCount() == 1) { //plotting only one XYSeries
            String key = (String) dataset.getSeriesKey(0);
            TrepnStat trepnStat = TrepnStat.valueOf(key);
            //chartTitle = "Profiling result for " + TrepnStatToUIStringConverter.getUIString(trepnStat);
            chartTitle = "Profiling result for " + key.replaceAll("_", " ");
            chartUnit = trepnStat.getUnits().toString();
        }
        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                chartTitle,  // title
                "Timestamp",             // x-axis label
                chartUnit,   // y-axis label
                dataset,            // data
                true,               // create legend?
                true,               // generate tooltips?
                false               // generate URLs?
        );
        chart.setBorderVisible(true);
        chart.setBackgroundPaint(JBColor.white);

        XYPlot plot = (XYPlot) chart.getPlot();
        //plot.setBackgroundPaint(JBColor.lightGray);
        plot.setBackgroundPaint(JBColor.WHITE);
        plot.setDomainGridlinePaint(JBColor.white);
        plot.setRangeGridlinePaint(JBColor.white);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);

        XYItemRenderer r = plot.getRenderer();
        XYLineAndShapeRenderer renderer = null;
        if (r instanceof XYLineAndShapeRenderer) {
            renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(true);
            renderer.setBaseShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
        }
        /*
        // tooltip generator
        XYToolTipGenerator toolTipGenerator = new XYToolTipGenerator() {
            @Override
            public String generateToolTip(XYDataset dataset, int series, int item) {
                StringBuffer sb = new StringBuffer();
                Number x = dataset.getX(series, item);
                Number y = dataset.getY(series, item);
                System.out.println("dataset = "+dataset+" series="+series+" item="+item+" x="+x+" y="+y);
                String htmlStr;
                if (x.intValue() < 200){ //this number need to be changed based on the range of onCreate
                    htmlStr = "<html><p style='color:#ff0000;'>onCreate</p></html>";
                }
                else{
                    htmlStr = "<html><p style='color:#ff0000;'>onPause</p></html>";
                }
                sb.append(htmlStr);
                return sb.toString();
            }
        };

        renderer.setSeriesToolTipGenerator(0, toolTipGenerator);
        */

        plot.setRenderer(renderer);
        //////////////// end code for adding tooltip to the data series


//        DateAxis axis = (DateAxis) plot.getDomainAxis();
        plot.getRangeAxis().setAutoTickUnitSelection(true);

//        XYPlot plot = (XYPlot)chart.getPlot();
        plot.setDomainAxis(0, new NumberAxis());
        NumberAxis axis = (NumberAxis) plot.getDomainAxis();
        axis.setNumberFormatOverride(new NumberFormat() {

            @Override
            public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {

                return new StringBuffer(String.format("%.0f", number));
            }

            @Override
            public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
                return new StringBuffer(String.format("%9.0f", (float) number));
            }

            @Override
            public Number parse(String source, ParsePosition parsePosition) {
                return null;
            }
        });
        axis.setAutoRange(true);
        axis.setAutoRangeIncludesZero(false);
        plot.setForegroundAlpha(0.5f);
        // axis.setMinimumDate(new Date(System.nanoTime()));
        //axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        return chart;

    }

    //    public EnergyDataPlotPanel(String title, String trepnDatafilePath) {
//        this.trepnDatafilePath = trepnDatafilePath;
//
//        openConnection();
//        ChartPanel chartPanel =  createChartPanel();
//        //chartPanel.setPreferredSize(new Dimension(500, 270));
//        this.addTab(trepnDatafilePath, chartPanel);
//}
    public void addEnergyDataPlotPanelTab(String title, String trepnDatafilePath) {
        //this line for testing only
        //trepnDatafilePath = "C:\\Users\\Mahmoud\\.IdeaIC13\\system\\plugins-sandbox\\plugins\\AEON\\_trepn\\logs\\Trepn_2014.11.04_111849.db";
        System.out.println("*** addEnergyDataPlotPanelTab title = " + title + " trepnDatafilePath=" + trepnDatafilePath);
        this.trepnDatafilePath = trepnDatafilePath;
        reloadData();
        JBPanel mainPanel = new JBPanel(); // main panel inside the scroll panel, contains all ChartPanel
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setSize(getWidth(), getHeight());
        //create ChartPanels equal to the number of measured data points
        XYSeriesCollection dataset = (XYSeriesCollection) createDataset();
        for (int i = 0; i < dataset.getSeriesCount(); i++) {
            XYSeries series = dataset.getSeries(i);
            XYSeriesCollection singleDataset = new XYSeriesCollection(); //contains only on eXYSeries
            singleDataset.addSeries(series);
            ChartPanel singleChartPanel = createChartPanel(singleDataset);
            //singleChartPanel.setMinimumDrawHeight(100);
            //singleChartPanel.setMaximumDrawWidth(150);
            mainPanel.add(singleChartPanel);
        }


        //ChartPanel chartPanel = createChartPanel(dataset);
        //ChartPanel chartPanel2 = createChartPanel(dataset);
        //mainPanel.add(chartPanel);
        //mainPanel.add(chartPanel2);

        JBScrollPane jsb = new JBScrollPane(mainPanel);
        //JBScrollPane jsb = new JBScrollPane(chartPanel);


        int defaultIndex = this.indexOfTab("Default");
        if (defaultIndex >= 0)
            this.removeTabAt(defaultIndex);
        String tabTitle = trepnDatafilePath.substring(trepnDatafilePath.lastIndexOf(File.separatorChar) + 1, trepnDatafilePath.lastIndexOf('.'));
        //setfocus
        this.addTab(tabTitle, jsb);
        //this.setTabPlacement(this.getTabCount());
    }

    public boolean isClosed() {
        boolean retval = true;
        if (this.connection != null) {
            try {
                retval = this.connection.isClosed();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return retval;
    }

    public void close() {
        closeConnection();
    }

    public void reloadData(

    ) {
        closeConnection();
        openConnection();
    }

    private void openConnection() {
        if (connection != null)
            return;
        try {
            Class.forName("org.sqlite.JDBC");
            try {
                this.connection = DriverManager.getConnection("jdbc:sqlite:" + this.trepnDatafilePath.replace("\\", "/"));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return;
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }

    private void closeConnection() {
        if (this.connection != null) {
            try {
                this.connection.close();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                this.connection = null;
            }
        }
    }

    private XYDataset createDataset() {

        TrepnSeriesBuilder iTrepnSeriesBuilder = new TrepnSeriesBuilder();
//        XYSeries xySeries=((ITrepnSeriesBuilder)iTrepnSeriesBuilder).createTrepnXYSeries(connection, TrepnStat.BATTERY_POWER_EPM, null);
        XYSeriesCollection xySeriesCollection = ((ITrepnSeriesBuilder) iTrepnSeriesBuilder).createTrepnXYSeriesCollection(connection, null);
        System.out.println("*** createDataset number of series in xySeriesCollection = " + xySeriesCollection.getSeriesCount());

        try {
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);
            ResultSet resultSet = statement.executeQuery("select value,description,start_time,end_time from application_state order by start_time");
            applicationStateRecordList = new Vector<ApplicationStateRecord>();
            ApplicationStateRecord record = null;
            String description = "";
            int start_time = 0;
            int end_time = 0;
            int last_end_time = 0;
            while (resultSet.next()) {
                /* get the description
                if the desc is null then
                    update the end time of the last record
                else
                    create new record
                 */
                description = resultSet.getString("description");
                System.out.println("description :" + description);
                if (description == null && applicationStateRecordList.size() > 0) {
                    record = applicationStateRecordList.get(applicationStateRecordList.size() - 1); //update last record
                    end_time = Math.max(Math.max(resultSet.getInt("end_time"), record.getEnd_time()), record.getStart_time());
                        /*if (end_time % 100 != 0) {
                            end_time = end_time - (end_time % 100) ;
                        }*/
                    record.setEnd_time(end_time);
                } else if (description != null && description.length() > 0) {
                    start_time = (resultSet.getInt("start_time") - resultSet.getInt("start_time") % 100);
                    end_time = resultSet.getInt("end_time");
                    //end_time = end_time - (end_time % 100) ;
                    record = new ApplicationStateRecord(resultSet.getInt("value"), description, start_time, end_time);
                    last_end_time = record.getEnd_time();
                    applicationStateRecordList.add(record);
                }
            }
            record = applicationStateRecordList.get(applicationStateRecordList.size() - 1); //update last record
            record.setEnd_time(last_end_time + 100);
            System.out.println("---------------------------------------");
            System.out.println(applicationStateRecordList);
            System.out.println("---------------------------------------");
        } catch (SQLException e) {
            System.out.println("::: WARNING: caught SQLException  : " + e.getMessage());
        } catch (Exception e2) {
            System.out.println("::: WARNING: caught Exception  : " + e2.getMessage());
        }
        System.out.println(" applicationStateRecordList = " + applicationStateRecordList.size());

        toolTipGenerator = new XYToolTipGenerator() {
            @Override
            public String generateToolTip(XYDataset dataset, int series, int item) {
                StringBuffer sb = new StringBuffer();
                Number x = dataset.getX(series, item);
                Number y = dataset.getY(series, item);
                //System.out.println(" series="+series+" item="+item+" x="+x+" y="+y);
                String htmlStr = "<html><p style='color:#ff0000;'>Series 1:</p>" +
                        String.format("%.2f <br />", x.doubleValue()) +
                        String.format("%.2f</html>", y.doubleValue());
                htmlStr = "<html><p style='color:#ff0000;'>" + getApplicationStateDescription(x.intValue()) + " <b>[" + y.intValue() + "]</b>" + "</p></html>";
                sb.append(htmlStr);
                return sb.toString();
            }
        };


//        XYSeriesCollection xySeriesCollection=((ITrepnSeriesBuilder)iTrepnSeriesBuilder).createAppFilteredTrepnXYSeriesCollection(connection,null,  offset);
//        offset=iTrepnSeriesBuilder.getOffset();

//        String outfilename = trepnDatafilePath+System.nanoTime()+".csv";
//        boolean xySeriesC=((CSVExportable)iTrepnSeriesBuilder).toCSVFile(connection, TrepnStat.BATTERY_POWER_EPM, null, outfilename );
//        System.out.println(
//                "]]]] Exported file: " + outfilename+" Created?"+xySeriesC
//        );

        return xySeriesCollection;
    }

    private String getApplicationStateDescription(int timestamp) {
        /*for (ApplicationStateRecord record : applicationStateRecordList){
            if (timestamp>=record.getStart_time() && timestamp<=record.getEnd_time()){
                return record.getDescription();
            }
        }
        */
        ApplicationStateRecord record = null;
        for (int i = applicationStateRecordList.size() - 1; i >= 0; i--) {
            record = applicationStateRecordList.get(i);
            if (timestamp >= record.getStart_time() && timestamp <= record.getEnd_time()) {
                return record.getDescription();
            }
        }

        return "";
    }


    public ChartPanel createChartPanel(final XYDataset dataset) {
        //final XYDataset dataset = createDataset();
        //JFreeChart chart = createChart(createDataset());
        JFreeChart chart = createChart(dataset);
        ChartPanel panel = new ChartPanel(chart);
        try {

            /*XYToolTipGenerator toolTipGenerator = new XYToolTipGenerator() {
                @Override
                public String generateToolTip(XYDataset dataset, int series, int item) {
                    StringBuffer sb = new StringBuffer();
                    Number x = dataset.getX(series, item);
                    Number y = dataset.getY(series, item);
                    System.out.println("************ dataset = "+dataset+" series="+series+" item="+item+" x="+x+" y="+y);
                    String htmlStr = "<html><p style='color:#ff0000;'>Series 1:</p>" +
                            String.format("%.2f <br />", x.doubleValue()) +
                            String.format("%.2f</html>", y.doubleValue());
                    if (x.intValue() < 10){
                        htmlStr = "<html><p style='color:#ff0000;'>onCreate</p></html>";
                    }
                    else{
                        htmlStr = "<html><p style='color:#ff0000;'>onPause</p></html>";
                    }
                    sb.append(htmlStr);
                    return sb.toString();
                }
            };
            */
            //set the trepn's color for each series
            XYSeriesCollection xySeriesCollection = (XYSeriesCollection) dataset;
            for (int i = 0; i < xySeriesCollection.getSeriesCount(); i++) {
                final XYPlot plot = (XYPlot) chart.getPlot();
                final XYItemRenderer renderer = plot.getRenderer();
                // get the color from the description
                String seriesDescription = xySeriesCollection.getSeries(i).getDescription();
                System.out.println("*** series i = " + i + " color rgb = " + seriesDescription);
                Color seriesColor = new Color(Integer.parseInt(seriesDescription));
                renderer.setSeriesPaint(i, seriesColor); // i will work on that after the presentation Nov, 6
                //renderer.getLegendItem(i,i)

                //add tooltip
                renderer.setSeriesToolTipGenerator(0, toolTipGenerator);
                plot.setRenderer(renderer);
            }
        } catch (Exception e) {
            //if for any reason the color couldn't be interpreted, accept the default color
            e.printStackTrace();
        }

       /* final LegendItemCollection originalLegendItemCollection = renderer.getLegendItems();


        panel.addChartMouseListener(new ChartMouseListener() {
            @Override
            public void chartMouseClicked(ChartMouseEvent chartMouseEvent) {

                ChartEntity e = chartMouseEvent.getEntity();
                if (e instanceof LegendItemEntity) {
                    LegendItemEntity entity = (LegendItemEntity) e;
                    Comparable seriesKey = entity.getSeriesKey();
                    for (int i = 0; i < dataset.getSeriesCount(); i++) {
                        if ((dataset.getSeriesKey(i).equals(seriesKey))) {
                            if (renderer.getSeriesVisible(i) == Boolean.FALSE) {
                                renderer.setSeriesStroke(i, new BasicStroke(1.5f));
                                renderer.setSeriesVisible(i, true);
                                originalLegendItemCollection.get(i).setLabelPaint(Color.black);
                            } else {
                                renderer.setSeriesStroke(i, new BasicStroke(1.5f));
                                renderer.setSeriesVisible(i, false);
                                originalLegendItemCollection.get(i).setLabelPaint(Color.red);
                            }
                        }
                        plot.setFixedLegendItems(originalLegendItemCollection);
                    }

                }
            }

            @Override
            public void chartMouseMoved(ChartMouseEvent chartMouseEvent) {

            }
        });*/


        panel.setFillZoomRectangle(true);
        panel.setMouseWheelEnabled(true);
        return panel;
    }


}