package edu.gmu.cs.aeon.plugin.view;


/*
 IDEA PsiViewer Plugin
 Copyright (C) 2002 Andrew J. Armstrong

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 Author:
 Andrew J. Armstrong <andrew_armstrong@bigpond.com>
*/


import com.intellij.codeInsight.daemon.DaemonCodeAnalyzer;
import com.intellij.openapi.application.AccessToken;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.project.ProjectManagerListener;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.ui.JBSplitter;
import com.intellij.ui.components.*;
//import com.intellij.ui.components.panels.VerticalLayout;
import edu.gmu.cs.aeon.analytics.AnalyticsTracker;
import edu.gmu.cs.aeon.analytics.JavaGoogleAnalyticsTracker;
import edu.gmu.cs.aeon.compatibility.UICompatibilityManager;
import edu.gmu.cs.aeon.core.analysis.report.ReportManager;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.core.utils.Helpers;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;
import edu.gmu.cs.aeon.plugin.controller.AEONProjectComponent;
import edu.gmu.cs.aeon.plugin.model.PsiViewerTreeModel;
import edu.gmu.cs.aeon.plugin.view.configuration.AEONConstants;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import edu.gmu.cs.aeon.trepn.common.ControlStatus;
import edu.gmu.cs.aeon.trepn.common.OnStatusUpdateListener;
import edu.gmu.cs.aeon.trepn.common.TrepnGeneralValues;
import edu.gmu.cs.aeon.trepn.control.DeviceManager;
import edu.gmu.cs.aeon.trepn.control.OnControlActionRunListener;
import edu.gmu.cs.aeon.trepn.control.OnDatafileChangedListener;
import edu.gmu.cs.aeon.trepn.control.actions.*;
import edu.gmu.cs.aeon.trepn.core.TrepnFilenameGenerator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * A JPanel that holds a toolbar, a tree view, and a property sheet.
 */
// TODO should be a project component. Move from PsiViewerProjectcomponent the initialization to here

public class AEONPanel extends JPanel implements Runnable, AEONConstants, OnStatusUpdateListener, OnControlActionRunListener, OnDatafileChangedListener {
    private static final Logger LOG = Logger.getInstance("edu.gmu.cs.edu.gmu.cs.aeon.plugin.view.AEONPanel");
    private static final String CARET_MOVED = "caret moved";
    private static final String TREE_SELECTION_CHANGED = "tree selection changed";
    private static Vector<HashMap<PsiStatement, EnergyBreakPoint>> statements;
    private static OnDatafileChangedListener _this;
    private final Project _project;
    private final ViewerTreeSelectionListener _treeSelectionListener;
    private final EditorCaretMover _caretMover;
    private final EditorPsiElementHighlighter _highlighter;
    private final AEONProjectComponent _projectComponent;
    private final PropertySheetHeaderRenderer _propertyHeaderRenderer =
            new PropertySheetHeaderRenderer(Helpers.getIcon(AEONConstants.ICON_PSI),
                    SwingConstants.LEFT,
                    BorderFactory.createEtchedBorder());
    private final PropertySheetHeaderRenderer _valueHeaderRenderer =
            new PropertySheetHeaderRenderer(Helpers.getIcon(AEONConstants.ICON_PSI),
                    SwingConstants.LEFT,
                    BorderFactory.createEtchedBorder());
    TrepnFilenameGenerator trepnFilenameGenerator = null;
    JButton button1 = new JButton();
    boolean isPlotted = false;
    JFileChooser fc;
    HashMap<String, HashMap<String, String>> appMap;
    //private boolean isWorking;
    private String _actionTitle;
    private PsiViewerTree _tree;
    private PsiViewerTreeModel _model;
    private PsiElement _rootElement; // The root element of the tree
    private PsiElement _selectedElement; // The currently selected element in the tree
    private PropertySheetPanel _propertyPanel;
    private ToolWindow _toolWindow;
    private JBSplitter _JBsplitter;
    private boolean inSetSelectedElement = false;
    private EnergyDataPlotPanel _energyDataPlotPanel;

    public static class Options {

        public static boolean isTurnOffInstanceOn=false;
        public  static JToggleButton jbRadioButtonIsTurnOffInstanceOn=new JToggleButton();
        public static boolean isEnergyAnalysisOn=true;
        public  static JToggleButton jbRadioButtonIsEnergyAnalysisOn=new JToggleButton();
        public static boolean isWakelockAnalysisOn=true;
        public  static JToggleButton jbRadioButtonIsWakelockAnalysisOn=new JToggleButton();

        public static boolean isLocationAnalysisOn=true;
        public  static JToggleButton jbRadioButtonIsLocationAnalysisOn=new JToggleButton();
       public static boolean isFinalizeAnalysisOn=true;
       public  static JToggleButton jbRadioButtonIsFinalizeAnalysisOn=new JToggleButton();
    }
    public void prepareAndroidUtilFacade(@NotNull final Project project){
        new AndroidUtilFacade().mapAndroidCallbacks(project);
        AndroidUtilFacade.prepareThreadClasses(project);
        AndroidUtilFacade.IS_PREPARED=true;
    }

    public AEONPanel(AEONProjectComponent projectComponent) {
        _this = this;
        _projectComponent = projectComponent;
        _project = projectComponent.getProject();
        _caretMover = new EditorCaretMover(_projectComponent.getProject());
        _highlighter = new EditorPsiElementHighlighter(_project);
        _model = new PsiViewerTreeModel(_projectComponent);
        _treeSelectionListener = new ViewerTreeSelectionListener();
        //  this.isWorking = false;


        ApplicationManager.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                Editor editor = (FileEditorManager.getInstance(_project)).getSelectedTextEditor();
                if (editor != null && editor.getMarkupModel() != null)
                    editor.getMarkupModel().removeAllHighlighters();
            }
        });

        buildGUI();


        ProjectManager.getInstance().addProjectManagerListener(new ProjectManagerListener() {
            @Override
            public void projectOpened(Project project) {
                if(!AndroidUtilFacade.IS_PREPARED){
                    prepareAndroidUtilFacade(_project);
                    populateAndroidData();
                    AnalyticsTracker.getTracker().trackFocusPoint(JavaGoogleAnalyticsTracker.PLUGIN_ACTION_IDE_OPEN);
                }

            }

            @Override
            public boolean canCloseProject(Project project) {
                return false;
            }

            @Override
            public void projectClosed(Project project) {

            }

            @Override
            public void projectClosing(Project project) {

            }
        });
    }
    public void populateAndroidData(){
        StringBuffer data=new StringBuffer();

        data.append("MANIFEST");
        data.append("\n");
        Map<String, AndroidUtilFacade.ComponentType> map=AndroidUtilFacade.getComponentsFromManifestOfProject(_project);
        for (String key:map.keySet()){
            data.append(key);
            data.append(" --> ");
            data.append(map.get(key).name());
            data.append("\n");
        }
        data.append("\n\n LAYOUT");
        data.append("\n");
        for (String path:AndroidUtilFacade.getPossibleLayoutRootElements(_project)){
            data.append(path);
            data.append("\n");
        }
        dataPane.setText(data.toString());
//        androidStructurePane.add(dataPane);
//        panTree.addTab("Android Structure", EnergyIcons.ENERGY_MARKER_HIERARCHY, androidStructurePane);

    }

    private static void debug(String message) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(message);
        }
    }

    public static OnDatafileChangedListener getLatestInstance() {
        return _this;
    }

    public void selectRootElement(PsiElement element, String actionTitle) {
        _actionTitle = actionTitle;
        setRootElement(element);
    }

    public void refreshRootElement() {
        selectRootElement(getRootElement(), _actionTitle);
    }

    private void showRootElement() {
        getToolWindow().setTitle(_actionTitle + " " + getRootElement());
        resetTree();
    }

    private void resetTree() {
        //todo, make dynamic


            _tree.getSelectionModel().removeTreeSelectionListener(_treeSelectionListener);

            Enumeration expandedDescendants = null;
            TreePath path = null;
            if (_model.getRoot() != null) {
                expandedDescendants = _tree.getExpandedDescendants(new TreePath(_model.getRoot()));
                path = _tree.getSelectionModel().getSelectionPath();
            }

            _model = new PsiViewerTreeModel(_projectComponent);
        if(_projectComponent.isEnergyBreakPointView()) {
            _model.disableEnergyView(PsiViewerTreeModel.EnergyView.ENERGY_MEASUREMENT_POINT);
            _model.enableEnergyView(PsiViewerTreeModel.EnergyView.ENERGY_BREAK_POINT);
        }else{
            if(_projectComponent.isEnergyCallGraphView()) {
                _model.disableEnergyView(PsiViewerTreeModel.EnergyView.ENERGY_BREAK_POINT);
                _model.enableEnergyView(PsiViewerTreeModel.EnergyView.ENERGY_MEASUREMENT_POINT);
            }
        }
            _model.setRoot(getRootElement());
            _tree.setModel(_model);
            if (expandedDescendants != null)
                while (expandedDescendants.hasMoreElements()) {
                    TreePath treePath = (TreePath) expandedDescendants.nextElement();
                    _tree.expandPath(treePath);
                }
            _tree.setSelectionPath(path);
            _tree.scrollPathToVisible(path);
            _tree.scrollPathToVisible(path);

            _tree.getSelectionModel().addTreeSelectionListener(_treeSelectionListener);

    }

    public void showProperties(boolean showProperties) {
//        _JBsplitter.getSecondComponent().setVisible(showProperties);
//      updatePropertySheet();
        //TODO check this change: visibility
        _JBsplitter.getFirstComponent().setVisible(showProperties);
        _JBsplitter.getFirstComponent().validate();

    }

    private ToolWindow getToolWindow() {
        return _toolWindow;
    }

    public void setToolWindow(ToolWindow toolWindow) {
        _toolWindow = toolWindow;
    }
    JBTabbedPane panTree;
    JBPanel energyTree;
    JBScrollPane energyTreeScroll;
    JTextArea dataPane;
    private void buildGUI() {

        setLayout(new BorderLayout());
        panTree = new JBTabbedPane();

        _tree = new PsiViewerTree(_model);
        _tree.getSelectionModel().addTreeSelectionListener(_treeSelectionListener);

        ActionMap actionMap = _tree.getActionMap();
        actionMap.put("EditSource", new AbstractAction("EditSource") {
            public void actionPerformed(ActionEvent e) {
                debug("key typed " + e);
                if (getSelectedElement() == null) return;
                Editor editor = _caretMover.openInEditor(getSelectedElement());
                selectElementAtCaret(editor, TREE_SELECTION_CHANGED);
                editor.getContentComponent().requestFocus();
            }
        });
        InputMap inputMap = _tree.getInputMap();
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0, true), "EditSource");

        _propertyPanel = new PropertySheetPanel();
        _energyDataPlotPanel = new EnergyDataPlotPanel();
        _JBsplitter = new JBSplitter(true, 1.f, .1f, .1f);

//todo WHERE THE HELL THE PROPERTIES VIEW WENT
        JBTabbedPane pan = new JBTabbedPane();

        _JBsplitter.setFirstComponent(new JBScrollPane(_propertyPanel));
        _JBsplitter.getFirstComponent().setVisible(true);
        _JBsplitter.getFirstComponent().validate();


        _energyDataPlotPanel = new EnergyDataPlotPanel();
        _JBsplitter.setSecondComponent(_energyDataPlotPanel);

        //_JBsplitter.setSecondComponent(_energyDataPlotPanel);

        JBSplitter jSplitPaneExtra = new JBSplitter(true, .3f, .1f, .5f);


        button1 = new JButton();
        button1.setText("Start Profiling");
        JButton button2 = new JButton();
        button2.setText("Batch Load");
        JButton button3 = new JButton();
        button3.setText("Batch Analysis");
        JButton button4 = new JButton();
        button4.setText("GreenDroid Scan");
        JButton button5 = new JButton();
        button5.setText("Current Project Analysis");
//        JButton button6 = new JButton();
//        button6.setText("Resolve Android Callbacks");
//        button6.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//            }
//        });

        _propertyPanel.add(button1);

        JBPanel analysisPanel = new JBPanel();
        analysisPanel.add(button2);
        analysisPanel.add(button3);
        analysisPanel.add(button5);
//        analysisPanel.add(button4);
//        analysisPanel.add(button6);


        pan.addTab("Batch Analysis", analysisPanel);

        pan.addTab("Energy Profiling", _JBsplitter);

        JBScrollPane tv= new JBScrollPane(_tree);
        panTree.addTab("Analysis View", EnergyIcons.ENERGY_MARKER_HIERARCHY, tv);


        energyTree= new JBPanel();
        energyTreeScroll= new JBScrollPane(energyTree);
        panTree.addTab("Energy Call Graph", EnergyIcons.ENERGY_MARKER_ENERGY_INFO, energyTreeScroll);
        JBPanel androidStructurePane=new JBPanel();
        dataPane=new JTextArea();



        JBPanel optionsPanel = new JBPanel();
        UICompatibilityManager.addVerticalLayout(optionsPanel);

        panTree.addTab("Analysis Options", EnergyIcons.ENERGY_SETTINGS, new JBScrollPane(optionsPanel));
        panTree.setSelectedIndex(0);

        Options.jbRadioButtonIsTurnOffInstanceOn.setText("Turn off PC after analysis Disabled");
        Options.jbRadioButtonIsTurnOffInstanceOn.setSelected(false);
        Options.jbRadioButtonIsTurnOffInstanceOn.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                AbstractButton abstractButton = (AbstractButton) e
                        .getSource();
                ButtonModel buttonModel = abstractButton.getModel();
                boolean pressed = buttonModel.isSelected();
                if (pressed) {
                    Options.jbRadioButtonIsTurnOffInstanceOn.setText("Turn off PC after analysis Enabled");
                    Options.isTurnOffInstanceOn = true;
                } else {
                    Options.jbRadioButtonIsTurnOffInstanceOn.setText("Turn off PC after analysis Disabled");
                    Options.isTurnOffInstanceOn = false;
                }
                DaemonCodeAnalyzer.getInstance(_project).restart();
            }
        });
        optionsPanel.add(Options.jbRadioButtonIsTurnOffInstanceOn);

        Options.jbRadioButtonIsEnergyAnalysisOn.setText("Energy Estimates Enabled");
        Options.jbRadioButtonIsEnergyAnalysisOn.setSelected(true);
        Options.jbRadioButtonIsEnergyAnalysisOn.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                AbstractButton abstractButton = (AbstractButton) e
                        .getSource();
                ButtonModel buttonModel = abstractButton.getModel();
                boolean pressed = buttonModel.isSelected();
                if (pressed) {
                    Options.jbRadioButtonIsEnergyAnalysisOn.setText("Energy Estimates Enabled");
                    Options.isEnergyAnalysisOn = true;
                } else {
                    Options.jbRadioButtonIsEnergyAnalysisOn.setText("Energy Estimates Disabled");
                    Options.isEnergyAnalysisOn = false;
                }
                DaemonCodeAnalyzer.getInstance(_project).restart();
            }
        });
        optionsPanel.add(Options.jbRadioButtonIsEnergyAnalysisOn);



        Options.jbRadioButtonIsWakelockAnalysisOn.setText("WakeLock Enabled");
        Options.jbRadioButtonIsWakelockAnalysisOn.setSelected(true);
        Options.jbRadioButtonIsWakelockAnalysisOn.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                AbstractButton abstractButton = (AbstractButton) e
                        .getSource();
                ButtonModel buttonModel = abstractButton.getModel();
                boolean pressed = buttonModel.isSelected();
                if (pressed) {
                    Options.jbRadioButtonIsWakelockAnalysisOn.setText("WakeLock Enabled");
                    Options.isWakelockAnalysisOn = true;
                } else {
                    Options.jbRadioButtonIsWakelockAnalysisOn.setText("WakeLock Disabled");
                    Options.isWakelockAnalysisOn = false;
                }
                DaemonCodeAnalyzer.getInstance(_project).restart();
            }
        });
        optionsPanel.add(Options.jbRadioButtonIsWakelockAnalysisOn);

        Options.jbRadioButtonIsLocationAnalysisOn.setText("Location Enabled");
        Options.jbRadioButtonIsLocationAnalysisOn.setSelected(true);
        Options.jbRadioButtonIsLocationAnalysisOn.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                AbstractButton abstractButton = (AbstractButton) e
                        .getSource();
                ButtonModel buttonModel = abstractButton.getModel();
                boolean pressed = buttonModel.isSelected();
                if(pressed){
                    Options.jbRadioButtonIsLocationAnalysisOn.setText("Location Enabled");
                    Options.isLocationAnalysisOn = true;
                }
                else{
                    Options.jbRadioButtonIsLocationAnalysisOn.setText("Location Disabled");
                    Options.isLocationAnalysisOn = false;
                }
                DaemonCodeAnalyzer.getInstance(_project).restart();
            }
        });
        optionsPanel.add( Options.jbRadioButtonIsLocationAnalysisOn);



        Options.jbRadioButtonIsFinalizeAnalysisOn.setText("Variable Finalization Enabled");
        Options.jbRadioButtonIsFinalizeAnalysisOn.setSelected(true);
        Options.jbRadioButtonIsFinalizeAnalysisOn.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                AbstractButton abstractButton = (AbstractButton) e
                        .getSource();
                ButtonModel buttonModel = abstractButton.getModel();
                boolean pressed = buttonModel.isSelected();
                if(pressed){
                    Options.jbRadioButtonIsFinalizeAnalysisOn.setText("Variable Finalization Enabled");
                    Options.isFinalizeAnalysisOn = true;
                }
                else{
                    Options.jbRadioButtonIsFinalizeAnalysisOn.setText("Variable Finalization Disabled");
                    Options.isFinalizeAnalysisOn = false;
                }
                DaemonCodeAnalyzer.getInstance(_project).restart();
            }
        });
        optionsPanel.add( Options.jbRadioButtonIsFinalizeAnalysisOn);

        //panTree.addTab("Greedy Path", new JBScrollPane(_tree));

        jSplitPaneExtra.setFirstComponent(panTree);
        jSplitPaneExtra.setSecondComponent(pan);

        add(jSplitPaneExtra);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e1) {
                System.out.println("Start Profiling Clicked");

                isPlotted = false;
                runProjectAction();

                trepnFilenameGenerator = new TrepnFilenameGenerator();
                ControlStatus controlStatus = new ControlStatus(AEONPanel.this);
                DeviceManager ds = new DeviceManager();
                new startService(AEONPanel.this, trepnFilenameGenerator, true, true).run();

                //todo ticker telling to unplug to start profiling

            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e1) {
                new BatchManager().invokeLoader();
            }
        });

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e1) {

                ProgressManager.getInstance().run(new Task.Backgroundable(_project, "Batch files Analysis") {
                    public void run(@NotNull final ProgressIndicator progressIndicator) {
                        progressIndicator.setIndeterminate(false);

                        ApplicationManager.getApplication().runReadAction(new Runnable() {
                            @Override
                            public void run() {
                                new BatchManager(progressIndicator).invokeAnalysis(appMap);

                            }
                        });


                    }
                });


            }
        });

        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e1) {

                ProgressManager.getInstance().run(new Task.Backgroundable(_project, "Current project Analysis") {
                    public void run(@NotNull final ProgressIndicator progressIndicator) {
                        progressIndicator.setIndeterminate(false);
                        AccessToken token = null;
                        ApplicationManager.getApplication().runReadAction(new Runnable() {
                            @Override
                            public void run() {
                                new BatchManager(progressIndicator).invokeAnalysis(null);
                            }
                        });

                    }
                });


            }
        });

//
//        button4.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e1) {
//                System.out.println("Analysis Clicked");
////                if (appMap!=null) {     }else{
////                    int resp=MessageDialogBuilder.yesNo("No Batch files loaded","Load batch files first, then proceed to this step").show();
////                    if(resp==OptionsMessageDialog.OK_EXIT_CODE){
////
////                    }
////                }
//
//                // do whatever you need to do
//                ProgressManager.getInstance().run(new Task.Backgroundable(_project, "Soot Analysis") {
//                    public void run(@NotNull ProgressIndicator progressIndicator) {
//                        progressIndicator.setIndeterminate(false);
//                        AccessToken token = null;
//                        try {
//                            token = ApplicationManager.getApplication().acquireReadActionLock();
//                            // ProjectRootManager.getInstance(_project).getFileIndex()
//                            long initTime = System.nanoTime();
////                            GreenDroid.runGreenDroid();
////
//
//                            long endTime = System.nanoTime();
//
//                            long seconds = TimeUnit.NANOSECONDS.toSeconds(endTime - initTime);
//                            long minutes = TimeUnit.SECONDS.toMinutes(seconds);
//                            long remainSeconds = seconds - TimeUnit.MINUTES.toSeconds(minutes);
//                            //   System.out.println("SummaryReport Created successfully as" + output+"\nBatch analysis time: " + String.format("%d", minutes) + " minutes and " + String.format("%d", remainSeconds) + " seconds.");
//                            final String message = "GreenDroid analysis time: " + String.format("%d", minutes) + " minutes and " + String.format("%d", remainSeconds) + " seconds.";
//                            showMessage(message, _propertyPanel);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//
//                        } finally {
//                            token.finish();
//                        }
//
//
//                    }
//                });
//
//
//            }
//        });



//        jSplitPaneExtra.validate();

    }

    public void showMessage(final String message, final Component component) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JBPopupFactory.getInstance().createMessage(message).showUnderneathOf(component);
            }
        });
    }

    public void changeButtonTitle(final String s, final boolean isEnabled) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                button1.setText(s);
                button1.setEnabled(isEnabled);
            }
        });

    }

    public void run() {
    }

    public void showPlot() {
        if (isPlotted) return;
        isPlotted = true;
        System.out.println("db file: " + TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR + trepnFilenameGenerator.getLastGeneratedFilename());


        _energyDataPlotPanel.addEnergyDataPlotPanelTab("Energy Analysis", TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR + trepnFilenameGenerator.getLastGeneratedFilename());


    }

    @Override
    public void onStatusUpdate(ControlStatus.ControlState paramControlState, String paramString) {
        String controlState = paramControlState.name();
        System.out.println("Panel onStatusUpdate: " + paramString + ",  CS:" + controlState);
        if (trepnFilenameGenerator != null && trepnFilenameGenerator.getLastGeneratedFilename() != null)
            System.out.println(new File(TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR + trepnFilenameGenerator.getLastGeneratedFilename()).exists() + "FILE " + TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR + trepnFilenameGenerator.getLastGeneratedFilename());
        if (controlState.equals("ACTION_DONE") && new File(TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR + trepnFilenameGenerator.getLastGeneratedFilename()).exists()) {
            System.out.println("Plotting");
            showPlot();
        }
        System.out.println("STATUS UPDATE: " + controlState + " (" + paramString + ")");

        if (controlState.equals("STARTING_TREPN")) {

            changeButtonTitle("Detach the Device to profile", false);
            waitForDeviceReconnection();
        }
        if (controlState.equals("PROCESSING_CHARTS")) {
            DeviceManager ds = new DeviceManager();
            new stopService(ds, AEONPanel.this, AEONPanel.this, trepnFilenameGenerator).run();
            changeButtonTitle("Start Profiling", true);
        }
    }

    public void runProjectAction() {
        try {
            Robot robot = new Robot();


            // Simulate a key press of shif+f10 : run Project
            robot.keyPress(KeyEvent.VK_SHIFT);
            robot.keyPress(KeyEvent.VK_F10);
            robot.keyRelease(KeyEvent.VK_SHIFT);
            robot.keyRelease(KeyEvent.VK_F10);

        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    private void waitForDeviceReconnection() {

        new Thread() {
            public void run() {
                DeviceManager ds = new DeviceManager();
                int eventCount = 0;
                boolean waitForDevice = true;
                while (waitForDevice) {

                    try {

                        Thread.sleep(1000);

                    } catch (Exception localException) {
                        localException.printStackTrace();
                    }
                    String deviceString = ds.getSelectedDeviceString();
                    if (deviceString.equals("NONE")) {
                        eventCount = 1;

                        changeButtonTitle("Waiting for reattachment of the device", false);
                    } else {
                        if (eventCount == 1) {
                            waitForDevice = false;
                            ControlStatus controlStatus = new ControlStatus(AEONPanel.this);
                            new stopService(new DeviceManager(), AEONPanel.this, AEONPanel.this, trepnFilenameGenerator).run();
                            new FileOnDeviceTransfer(new DeviceManager(), AEONPanel.this, AEONPanel.this, trepnFilenameGenerator).run();
                            ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.PROCESSING_CHARTS);
                        }
                    }


                }
            }
        }.start();
    }

    @Override
    public void onControlActionRun(ControlAction paramControlAction) {
        System.out.println("Panel onControlActionRun: " + paramControlAction.getAdbDeviceList() + "");
    }

    @Override
    public void onControlActionRunError(String paramString, ControlAction paramControlAction) {

        System.out.println("Panel onControlActionRunError: " + paramString + "");
    }

    @Override
    public void onDatafileChanged(String paramString) {
        System.out.println("Panel onDatafileChanged: " + paramString);

    }

    public void applyWhitespaceFilter() {
        showRootElement();
    }

    public void applyHighlighting() {
        _highlighter.highlightElement(getSelectedElement());
    }

    public void removeHighlighting() {
        _highlighter.removeHighlight();
    }

    private PsiElement getSelectedElement() {
        return _selectedElement;
    }

    private void setSelectedElement(PsiElement element, String reason) {
        if (inSetSelectedElement)
            return;

        try {
            debug("selection changed to " + element + " due to " + reason);
            inSetSelectedElement = true;
            _selectedElement = element;
//            updatePropertySheet();
            if (reason.equals(TREE_SELECTION_CHANGED))
                changeTreeSelection();
            applyHighlighting();
            if (reason.equals(CARET_MOVED) && element != null)
                moveEditorCaret();
        } finally {
            inSetSelectedElement = false;
        }
    }

    public void updatePropertySheet() {
        if (!_projectComponent.isShowProperties())
            return;
        _propertyPanel.setTarget(_selectedElement);
        _propertyPanel.getTable().getTableHeader().setReorderingAllowed(false);

        _propertyHeaderRenderer.setIconForElement(_selectedElement);
        _propertyPanel.getTable().getColumnModel().getColumn(0).setHeaderRenderer(_propertyHeaderRenderer);
        _propertyPanel.getTable().getColumnModel().getColumn(1).setHeaderRenderer(_valueHeaderRenderer);

//        if (_selectedElement != null)
//            _splitPane.setDividerLocation(_projectComponent.getSplitDividerLocation());
    }

    private void changeTreeSelection() {
        TreePath path = getPath(getSelectedElement());
        _tree.expandPath(path);
        _tree.scrollPathToVisible(path);
        _tree.setSelectionPath(path);
    }

    private TreePath getPath(PsiElement element) {
        if (element == null) return null;
//        //todo critical path formation in tree
//        if (statements == null) {
//            statements = EnergyLineMarkerProvider.statements;
//        }
//        if (!(element instanceof PsiStatement))
//            return null;

//        if(EnergyLineMarkerProvider.getEBP((PsiStatement)element)!=null)
//            return null;

        LinkedList<PsiElement> list = new LinkedList<PsiElement>();
        while (element != null && element != _rootElement) {
            list.addFirst(element);
            element = element.getParent();
        }
        if (element != null)
            list.addFirst(element);
        TreePath treePath = new TreePath(list.toArray());
        debug("root=" + _rootElement + ", treePath=" + treePath);
        return treePath;
    }

    private void moveEditorCaret() {
        if (_projectComponent.isAutoScrollToSource()) {
            LOG.debug("moving editor caret");
            _caretMover.moveEditorCaret(getSelectedElement());
        }
    }

    public PsiElement getRootElement() {
        return _rootElement;
    }

    private void setRootElement(PsiElement rootElement) {
        _rootElement = rootElement;
        showRootElement();
    }

    public void selectElementAtCaret() {
        selectElementAtCaret(FileEditorManager.getInstance(_project).getSelectedTextEditor(), null);
    }
    PsiMethod currentMethod=null;
    public void selectElementAtCaret(@Nullable Editor editor, @Nullable String changeSource) {



        if (editor == null) /* Vince Mallet (21 Oct 2003) */ {
            debug("selectElementAtCaret: Can't select element, editor is null");
            return;
        }

        PsiFile psiFile = PsiDocumentManager.getInstance(_project).getPsiFile(editor.getDocument());

        PsiElement elementAtCaret = null;
        if (psiFile != null) {
            elementAtCaret = psiFile.findElementAt(editor.getCaretModel().getOffset());
            if (elementAtCaret != null && elementAtCaret.getParent() != null) {
                if (elementAtCaret.getParent().getChildren().length == 0)
                    elementAtCaret = elementAtCaret.getParent();
            }
        }
if(elementAtCaret != null){
    PsiMethod psiMethod=IntelliJUtilFacade.lookUpForContainingMethod(elementAtCaret);
    if(psiMethod!=null&&currentMethod!=psiMethod){
        currentMethod=psiMethod;
        if(energyTree.getComponentCount()>0){
            energyTree.remove(0);
        }
        EnergyCallHierarchyBrowser hierarchyBrowser=new EnergyCallHierarchyBrowser(psiMethod.getProject(),psiMethod);
        hierarchyBrowser.changeToEnergyView();
//        hierarchyBrowser.addHierarchyListener(new HierarchyListener() {
//            @Override
//            public void hierarchyChanged(HierarchyEvent e) {
//                System.out.println("YOOOOOO "+e.getSource().toString());
//            }
//        });
//        new CalleeMethodsTreeStructure(psiMethod.getProject(), psiMethod, HierarchyBrowserBaseEx.SCOPE_PROJECT).;
//        TreeStructureUtil.
       JComponent component= hierarchyBrowser.getComponent();

       // energyTreeScroll.setLayout(new BorderLayout());
//        energyTree.setSize(energyTreeScroll.getSize());
//        energyTree.validate();
//       // component.setLayout(new BorderLayout());
//       component.setSize(energyTreeScroll.getSize());
        energyTree.add(component);
//        component.revalidate();
       // energyTree.setPreferredSize(energyTree.getParent().getPreferredSize());
       energyTreeScroll.validate();
    }
}
        if (elementAtCaret != null && elementAtCaret != getSelectedElement()) {
            debug("new element at caret " + elementAtCaret + ", current root=" + getRootElement());
            if (!PsiTreeUtil.isAncestor(getRootElement(), elementAtCaret, false)) {
                selectRootElement(psiFile, TITLE_PREFIX_CURRENT_FILE);
            }
            //check for null

            setSelectedElement(elementAtCaret, changeSource == null ? AEONPanel.CARET_MOVED : changeSource);
        }
    }

    private class ViewerTreeSelectionListener implements TreeSelectionListener {
        public void valueChanged(TreeSelectionEvent e) {
            setSelectedElement((PsiElement) _tree.getLastSelectedPathComponent(),
                    AEONPanel.TREE_SELECTION_CHANGED);
        }
    }

    public class BatchManager {
        private ProgressIndicator progressIndicator;

        public BatchManager(ProgressIndicator progressIndicator) {
            this.progressIndicator = progressIndicator;
        }
        public BatchManager() {

        }

        public void invokeLoader() {
           // PluginPsiUtil.checkIfAndroidPluginIsInstalled();
            fc = new JFileChooser("Select batch folder to load");
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fc.setAcceptAllFileFilterUsed(false);
            File startFile=new File("C:\\Users\\DavidIgnacio\\Desktop\\Soot\\app_repo\\test");
            if(!startFile.exists()){
                startFile=new File("D:\\Repositories");
            }
            if(!startFile.exists()){
                startFile=new File(_project.getBasePath());
            }
            fc.setCurrentDirectory(startFile);

            fc.addChoosableFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    if (f.isDirectory())
                        return true;
                    return false;
                }

                @Override
                public String getDescription() {
                    return "Directories only";
                }
            });
            String where=IntelliJUtilFacade.getSourceRootPathForCopying(_project);
            int returnVal = fc.showDialog(_propertyPanel, "Copy in "+(where!=null?where:"N/A"));


            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                if (!file.isDirectory()) {
                    file = file.getParentFile();
                }
//                System.out.println("Opening: " + file.getAbsolutePath() + ".");
                final String from = file.getAbsolutePath();
                ProgressManager.getInstance().run(new Task.Backgroundable(_project, "Batch files processing: Copying files from directory " + from + " to src folder in this project:" + _project.getName()) {
                    public void run(@NotNull ProgressIndicator progressIndicator) {


                        // Set the progress bar percentage and text
                        progressIndicator.setIndeterminate(false);
                        long initTime = System.nanoTime();
                        String batchLoadStatus="Successfully";
                        try {
                            appMap = ReportManager.applicationsBatchLoad(from, progressIndicator, this.getProject());
                        }catch (Exception e){
                            batchLoadStatus="Unsuccessfully ["+e.toString()+"]";
                        }
                        finally {
                            long endTime = System.nanoTime();

                            long seconds = TimeUnit.NANOSECONDS.toSeconds(endTime - initTime);
                            long minutes = TimeUnit.SECONDS.toMinutes(seconds);
                            long remainSeconds = seconds - TimeUnit.MINUTES.toSeconds(minutes);

                            final String message = "\nBatch processing done in " + String.format("%d", minutes) + " minutes and " + String.format("%d", remainSeconds) + " seconds. Status: "+batchLoadStatus;
                            showMessage(message, _propertyPanel);
                            ReportManager.writeToFile(_project.getBasePath(), "AEON_Analysis.log",message, true);
                            DaemonCodeAnalyzer.getInstance(_project).restart();

                        }

                    }
                });

            } else {
             //   System.out.println("Open command cancelled by user.");
            }

        }

        private void turnOffInstance(){
            try {
                Process p = Runtime.getRuntime().exec("shutdown -s");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void invokeAnalysis(HashMap<String, HashMap<String, String>> appMap) {
            if(appMap==null){

                final String message = "Please do Batch Load first";
                showMessage(message, _propertyPanel);
                return;
            }
            long initTime = System.nanoTime();
            String batchAnalysisStatus="Successfully";
            String output="N/A";
            try {
                output = ReportManager.applicationsBatchAnalysis(appMap, _project, progressIndicator);
            }catch (Exception e){
                batchAnalysisStatus="Unsuccessfully ["+e.toString()+"]";
            }
            finally {
                long endTime = System.nanoTime();

                long seconds = TimeUnit.NANOSECONDS.toSeconds(endTime - initTime);
                long minutes = TimeUnit.SECONDS.toMinutes(seconds);
                long remainSeconds = seconds - TimeUnit.MINUTES.toSeconds(minutes);
                //   System.out.println("SummaryReport Created successfully as" + output+"\nBatch analysis time: " + String.format("%d", minutes) + " minutes and " + String.format("%d", remainSeconds) + " seconds.");
                final String message = "\nReport generated as " + output + ".\n Batch analysis done in " + String.format("%d", minutes) + " minutes and " + String.format("%d", remainSeconds) + " seconds. Status: "+batchAnalysisStatus;
                ReportManager.writeToFile(_project.getBasePath(), "AEON_Analysis.log", message, true);
                showMessage(message, _propertyPanel);

                if(Options.isTurnOffInstanceOn){
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    ReportManager.writeToFile(_project.getBasePath(), "AEON_Analysis.log", "\nShutting down System after analysis at  "+ dateFormat.format(date), true);
                    turnOffInstance();
                }

            }
        }
    }
}