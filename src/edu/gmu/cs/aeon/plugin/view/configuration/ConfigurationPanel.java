package edu.gmu.cs.aeon.plugin.view.configuration;

/**
 * Created by DavidIgnacio on 7/10/2014.
 */
/*
    IDEA Plugin
    Copyright (C) 2002 Andrew J. Armstrong

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Author:
	Andrew J. Armstrong <andrew_armstrong@bigpond.com>
*/


import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPanel;
import com.intellij.ui.components.JBTabbedPane;
import edu.gmu.cs.aeon.core.utils.Helpers;

import javax.swing.*;
import java.awt.*;

public class ConfigurationPanel extends JBPanel {
    private JBCheckBox _pluginEnabled;
    private JBCheckBox _legacyEnabled;
    private JBCheckBox _trackingEnabled;
    private JBLabel _trackingIDLabel;
    private JColorChooser _colorChooser;
    private AlphaChooserPanel _alphaChooser;

    public ConfigurationPanel() {

        buildGUI();
    }

    private void buildGUI() {
        _alphaChooser = new AlphaChooserPanel();
        _colorChooser = new JColorChooser();
        _colorChooser.addChooserPanel(_alphaChooser);

        _pluginEnabled = new JBCheckBox("Enable AEON Plugin");
        _pluginEnabled.setToolTipText("Enable/disable the PsiViewer tool window");

        _legacyEnabled = new JBCheckBox("Enable AEON Google Analytics Legacy support");
        _legacyEnabled.setToolTipText("Enable/disable Google Analytics Legacy support: Only if you have Google Analytics below version 2.0 ");

        _trackingEnabled = new JBCheckBox("Enable AEON Google Analytics tracking");
        _trackingEnabled.setToolTipText("Enable/disable Google Analytics tracking features. WE DON'T COLLECT USER SENSITIVE INFO");
        _trackingIDLabel = new JBLabel("TRACKING_ID");

        setLayout(new BorderLayout());

        JBPanel topPane = new JBPanel();

//
        topPane.setBorder(BorderFactory.createEtchedBorder());
        topPane.add(_pluginEnabled);
      //  topPane.add(_legacyEnabled);
        topPane.add(_trackingEnabled);
        topPane.add(_trackingIDLabel);
        topPane.setLayout(new BoxLayout(topPane, BoxLayout.Y_AXIS));

        JBTabbedPane tabbedPane = new JBTabbedPane(JTabbedPane.TOP);
        tabbedPane.setPreferredSize(new Dimension(400, 600));
        tabbedPane.insertTab("Highlighter",
                Helpers.getIcon(AEONConstants.ICON_TOGGLE_HIGHLIGHT),
                _colorChooser,
                "Set highlighter color",
                0);

        add(topPane, BorderLayout.NORTH);
        add(tabbedPane, BorderLayout.CENTER);
    }

    public boolean isPluginEnabled() {
        return _pluginEnabled.isSelected();
    }

    public void setPluginEnabled(boolean enabled) {
        _pluginEnabled.setSelected(enabled);
    }

    public boolean isLegacyEnabled() {
        return _legacyEnabled.isSelected();
    }

    public void setLegacyEnabled(boolean enabled) {
        _legacyEnabled.setSelected(enabled);
    }

    public boolean isTrackingEnabled() {
        return _trackingEnabled.isSelected();
    }

    public void setTrackingEnabled(boolean enabled) {
        _trackingEnabled.setSelected(enabled);
    }

    public String getTrackingIDLabel() {
        return _trackingIDLabel.getText();
    }

    public void setTrackingIDLabel(String text) {
        _trackingIDLabel.setText(text);
    }

    public Color getHighlightColor() {
        return _colorChooser.getSelectionModel().getSelectedColor();
    }

    public void setHighlightColor(Color color) {
        _alphaChooser.setAlpha(color.getAlpha());
        _colorChooser.setColor(color);
        _colorChooser.getSelectionModel().setSelectedColor(color);
//        _alphaChooser.updateUI();
//        _colorChooser.updateUI();
//        getComponent(1).invalidate();
    }

}
