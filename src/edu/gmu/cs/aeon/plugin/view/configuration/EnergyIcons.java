package edu.gmu.cs.aeon.plugin.view.configuration;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * Created by DavidIgnacio on 9/16/2014.
 * Cache class for plugin icons
 */
public class EnergyIcons {
    public static final Icon ENERGY_MARKER_DISABLED = IconLoader.getIcon("/icons/energy_marker_disabled.png");
    public static final Icon ENERGY_MARKER_START = IconLoader.getIcon("/icons/energy_marker_start.png");
    public static final Icon ENERGY_MARKER_START_COMPONENT = IconLoader.getIcon("/icons/energy_marker_start_component.png");
    public static final Icon ENERGY_MARKER_STOP = IconLoader.getIcon("/icons/energy_marker_stop.png");
    public static final Icon ENERGY_MARKER_REFACTOR = IconLoader.getIcon("/icons/energy_marker_refactor.png");
    public static final Icon ENERGY_MARKER_LINKER = IconLoader.getIcon("/icons/energy_marker_linker.png");
    public static final Icon ENERGY_MARKER_WARNING = IconLoader.getIcon("/icons/energy_marker_warning.png");
    public static final Icon ENERGY_MARKER_WARNING_MODERATE = IconLoader.getIcon("/icons/energy_marker_warning_circle_red.png");
    public static final Icon ENERGY_MARKER_WARNING_CRITICAL = IconLoader.getIcon("/icons/energy_marker_warning_circle_wine.png");
    public static final Icon ENERGY_MARKER_WARNING_LOOP_CASE = IconLoader.getIcon("/icons/energy_marker_warning_triangle_orange.png");
    public static final Icon ENERGY_MARKER_WARNING_JAVA_CASE = IconLoader.getIcon("/icons/energy_marker_warning_triangle_blue.png");
    public static final Icon ENERGY_MARKER_WARNING_RESOURCE_CASE = IconLoader.getIcon("/icons/energy_marker_warning_triangle_yellow.png");
    public static final Icon ENERGY_MARKER_WAKE_LOCK = IconLoader.getIcon("/icons/wake_lock.png");
    public static final Icon ENERGY_MARKER_ENERGY_INFO = IconLoader.getIcon("/icons/Icon_energy_16x16.png");
    public static final Icon ENERGY_MARKER_GOOD_FIX = IconLoader.getIcon("/icons/good_fix.png");
    public static final Icon ENERGY_MARKER_LOCATION = IconLoader.getIcon("/icons/location.png");
    public static final Icon ENERGY_MARKER_HIERARCHY= IconLoader.getIcon("/icons/hierarchy.gif");
    public static final Icon ENERGY_SETTINGS= IconLoader.getIcon("/icons/settings.png");


}
