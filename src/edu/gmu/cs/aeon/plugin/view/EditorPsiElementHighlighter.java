package edu.gmu.cs.aeon.plugin.view;

/**
 * Created by DavidIgnacio on 7/10/2014.
 */
/*
 *  Copyright (c) 2002 Sabre, Inc. All rights reserved.
 */


import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.markup.HighlighterTargetArea;
import com.intellij.openapi.editor.markup.RangeHighlighter;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiWhiteSpace;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;
import edu.gmu.cs.aeon.plugin.controller.AEONProjectComponent;
import edu.gmu.cs.aeon.plugin.controller.application.Configuration;
import edu.gmu.cs.aeon.plugin.view.configuration.AEONConstants;


class EditorPsiElementHighlighter {
    private static final Logger LOG = Logger.getInstance("idea.plugin.psiviewer.view.Highlighter");

    private final Project _project;
    private final TextAttributes _textAttributes;
    private RangeHighlighter _highlighter;
    private Editor _editor;

    public EditorPsiElementHighlighter(Project project) {
        _project = project;
        _textAttributes = Configuration.getInstance().getTextAttributes();
    }

    private static void debug(String message) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(message);
        }
    }

    public void highlightElement(final PsiElement psiElement) {
        ApplicationManager.getApplication().runReadAction(new Runnable() {
            public void run() {
                apply(psiElement);
            }
        });
    }

    public void removeHighlight() {
        ApplicationManager.getApplication().runReadAction(new Runnable() {
            public void run() {
                remove();
            }
        });
    }

    private void apply(PsiElement element) {
        remove();

        _editor = FileEditorManager.getInstance(_project).getSelectedTextEditor();
        if (_editor == null) {
            debug("no editor => no need to highlight");
            return;
        }

        if (element instanceof PsiWhiteSpace && isWhiteSpaceFiltered())
            return;

        if (isHighlightOn() && isElementInEditor(_editor, element)) {
            TextRange textRange = element.getTextRange();
            debug("Adding highlighting for " + textRange);
            _highlighter = _editor.getMarkupModel().addRangeHighlighter(textRange.getStartOffset(),
                    textRange.getEndOffset(),
                    AEONConstants.PSIVIEWER_HIGHLIGHT_LAYER,
                    _textAttributes,
                    HighlighterTargetArea.EXACT_RANGE);
        }
    }

    private void remove() {
        if (_highlighter != null && _highlighter.isValid()) {
            debug("Removing highlighter for " + _highlighter);
            _editor.getMarkupModel().removeHighlighter(_highlighter);
            _highlighter = null;
        }
    }

    private boolean isWhiteSpaceFiltered() {
        return AEONProjectComponent.getInstance(_project).isFilterWhitespace();
    }

    private boolean isHighlightOn() {
        return AEONProjectComponent.getInstance(_project).isHighlighted();
    }

    private boolean isElementInEditor(Editor editor, PsiElement psiElement) {
        if (psiElement == null || PluginPsiUtil.getContainingFile(psiElement) == null) return false;
        VirtualFile elementFile = psiElement.getContainingFile().getVirtualFile();
        if (elementFile == null)
            return false;   // 20050826
        VirtualFile editorFile = FileDocumentManager.getInstance().getFile(editor.getDocument());
        return elementFile.equals(editorFile);
    }
}
