package edu.gmu.cs.aeon.plugin.view;

import com.intellij.codeInsight.daemon.GutterIconNavigationHandler;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.markup.EffectType;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageDialogBuilder;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import edu.gmu.cs.aeon.core.BaseCodeGenerator;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Vector;

public class EnergyLineMarker extends GutterIconRenderer implements Showable, GutterIconNavigationHandler<PsiElement> {


    private Icon myIcon;
    private PsiElement myPsiElement;
    private Refactorer myRefactorer;

    public EnergyLineMarker(@NotNull PsiElement element, @NotNull Refactorer myRefactorer) {

        myPsiElement = element;
        this.myRefactorer = myRefactorer;
        asRefactor();

    }


    @Override
    public int getId() {
        return hashCode();
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return myIcon;
    }

    @Override
    public boolean isNavigateAction() {

        return true;

    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (!(o instanceof EnergyLineMarker))
            return false;
        EnergyLineMarker other = (EnergyLineMarker) o;
        if (myPsiElement.equals(other.myPsiElement)) {
            return true;
        }

        return false;
    }


    @Override
    public int hashCode() {
        return 777 + myPsiElement.hashCode();
    }

    @Override
    public PsiElement getElement() {
        return myPsiElement;

    }

    @NotNull
    @Override
    public String getText() {
        return "";
    }

    public boolean isEnabled() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_START) || myIcon.equals(EnergyIcons.ENERGY_MARKER_STOP) || myIcon.equals(EnergyIcons.ENERGY_MARKER_REFACTOR)) {
            return true;
        }
        return false;
    }

    public boolean isDisabled() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_DISABLED) || myIcon.equals(EnergyIcons.ENERGY_MARKER_LINKER)) {
            return true;
        }
        return false;
    }

    public boolean isRefactor() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_REFACTOR)) {
            return true;
        }
        return false;
    }

    public boolean isStop() {
        if (myIcon.equals(EnergyIcons.ENERGY_MARKER_STOP)) {
            return true;
        }
        return false;
    }

    public void asRefactor() {
        myIcon = EnergyIcons.ENERGY_MARKER_REFACTOR;
    }

    public void asStop() {
        myIcon = EnergyIcons.ENERGY_MARKER_STOP;

    }

    public void doHighlight() {
        new RefactorerHandler(myPsiElement, Action.HIGHLIGHT_ACTION).generate();
    }

    public void asDisabled() {
        myIcon = EnergyIcons.ENERGY_MARKER_DISABLED;
    }

    public void asLinker() {
        myIcon = EnergyIcons.ENERGY_MARKER_LINKER;
    }

    @Override
    public AnAction getClickAction() {
        return new EnergyBreakPointAction();
    }


    @Override
    public void navigate(MouseEvent mouseEvent, PsiElement psiElement) {
//        System.out.println("MouseEvent: clicks " + mouseEvent.getClickCount() + "," + mouseEvent.paramString() + ", element" + psiElement.getText() + ", affecting:" + myPsiElement.getText());

        if (mouseEvent.getClickCount() > 1) {
            return;
        }
//        if(myPsiElement instanceof PsiStatement)
        navigateHandler();
    }

    public void navigateHandler() {
        if (myPsiElement == null) {
            return;
        }
        if (!myPsiElement.isValid())
            return;

        PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(myPsiElement);
//        PsiElement previewRefactorElementElement=myRefactorer.previewRefactorElement(myPsiElement);

        PsiElement previewRefactorElementElement = myPsiElement;
        if (previewRefactorElementElement == null)
            return;
        int response = MessageDialogBuilder.yesNoCancel("", previewRefactorElementElement.getText()).show();
        System.out.println("response:" + response);
        if (response == 0) {
            myRefactorer.refactorElement(myPsiElement);
            asDisabled();
        }

    }

    public static enum Action {HIGHLIGHT_ACTION}

    private class EnergyBreakPointAction extends AnAction {
        @Override
        public void actionPerformed(AnActionEvent anActionEvent) {
            navigateHandler();
        }
    }

    public class RefactorerHandler extends BaseCodeGenerator {
        public static final String COMMAND_NAME = "Energy Manager";
        private final Action action;
        private Vector<PsiElement> toRemove;
        private Vector<PsiElement> toRedo;

        public RefactorerHandler(final PsiElement psiClass, Action action) {
            super(psiClass, COMMAND_NAME);
            this.action = action;
        }

        public void highlightElement(final PsiElement element) {

            int lineNum = PsiDocumentManager.getInstance(element.getProject()).getCachedDocument(element.getContainingFile()).getLineNumber(element.getTextRange().getStartOffset());
            final TextAttributes textattributes = new TextAttributes(null, Color.DARK_GRAY, null, EffectType.LINE_UNDERSCORE, Font.PLAIN);
            final Project project = element.getProject();
            final FileEditorManager editorManager = FileEditorManager.getInstance(project);
            final Editor editor = editorManager.getSelectedTextEditor();
            //   editor.getMarkupModel().addLineHighlighter(lineNum, HighlighterLayer.CARET_ROW, textattributes);


        }


// todo extract local variables from loop, add as params tuple to allow smooth migration and result tuple to continue the code in the onResult new method with taskId

        @Override
        public void generate() {
            switch (action) {
                case HIGHLIGHT_ACTION:
                    highlightElement(myPsiElement);
                    break;

            }

        }


        @Override
        public void unGenerate() {

            if (toRemove != null && toRemove.size() > 0) {
                PsiElement redoHere = toRemove.get(0);
                for (PsiElement element : toRedo) {
                    redoHere = redoHere.addAfter(element, redoHere);
                }

                for (PsiElement element : toRemove) {
                    element.delete();
                }
            }

            toRemove = null;
            toRedo = null;
        }
    }
}