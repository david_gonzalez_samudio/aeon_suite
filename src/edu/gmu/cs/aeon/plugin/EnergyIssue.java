package edu.gmu.cs.aeon.plugin;

import org.jetbrains.annotations.NotNull;

/**
 * Created by DavidIgnacio on 10/24/2014.
 */
public interface EnergyIssue {
    @NotNull
    public String getId();

    @NotNull
    public String getBriefSummary();

    @NotNull
    public String getSummary();

    @NotNull
    public String getExplanation();

    public Priority getPriority();

    public Severity getSeverity();

    public Category getCategory();

    public enum Priority {P1, P2, P3, P4, P5, P6, P7, P8, P9, P10}

    public enum Severity {SUGGESTION, WARNING, ERROR, FATAL}

    ;

    public enum Category {ENERGY_CONSUMING, CORRECTNESS, USABILITY, SECURITY, ACCESSIBILITY, PERFORMANCE, I18N}

    ;
}
