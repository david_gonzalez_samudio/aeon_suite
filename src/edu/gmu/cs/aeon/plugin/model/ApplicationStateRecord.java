package edu.gmu.cs.aeon.plugin.model;

/**
 * Created by Mahmoud on 12/31/2014.
 */
public class ApplicationStateRecord {
    private int value;
    private String description;
    private int start_time;
    private int end_time;

    public ApplicationStateRecord(int value, String description, int start_time, int end_time) {
        this.value = value;
        this.description = description;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public int getStart_time() {
        return start_time;
    }

    public int getEnd_time() {
        return end_time;
    }

    public void setEnd_time(int end_time) {
        this.end_time = end_time;
    }

    @Override
    public String toString() {
        String record = this.description + " : " + this.start_time + " - " + this.end_time;
        return record;
    }
}
