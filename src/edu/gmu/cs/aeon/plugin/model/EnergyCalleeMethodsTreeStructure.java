package edu.gmu.cs.aeon.plugin.model;

import com.intellij.ide.hierarchy.HierarchyTreeStructure;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiMethod;

/**
 * Created by DavidIgnacio on 4/18/2015.
 */
/*
 * Copyright 2000-2013 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import com.intellij.ide.hierarchy.HierarchyNodeDescriptor;
import com.intellij.psi.*;
import com.intellij.psi.search.searches.OverridingMethodsSearch;
import com.intellij.util.ArrayUtil;
import com.intellij.util.containers.HashMap;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public final class EnergyCalleeMethodsTreeStructure extends HierarchyTreeStructure {
    private final String myScopeType;

    /**
     * Should be called in read action
     */
    public EnergyCalleeMethodsTreeStructure(final Project project, final PsiMethod method, final String scopeType) {
        super(project, new EnergyCallHierarchyNodeDescriptor(project, null, method, true, false));
        myScopeType = scopeType;
    }

    @NotNull
    protected final Object[] buildChildren(@NotNull final HierarchyNodeDescriptor descriptor) {
        if(((EnergyCallHierarchyNodeDescriptor)descriptor).type== EnergyCallHierarchyNodeDescriptor.Type.LOOP_BLOCK||((EnergyCallHierarchyNodeDescriptor)descriptor).type== EnergyCallHierarchyNodeDescriptor.Type.THEN_BRANCH||((EnergyCallHierarchyNodeDescriptor)descriptor).type== EnergyCallHierarchyNodeDescriptor.Type.ELSE_BRANCH){
           final PsiElement branch=((EnergyCallHierarchyNodeDescriptor) descriptor).getTargetElement();
            final ArrayList<PsiElement> psiElements = new ArrayList<PsiElement>();
            visitor(branch, psiElements);
            final PsiMethod baseMethod = (PsiMethod) ((EnergyCallHierarchyNodeDescriptor) getBaseDescriptor()).getTargetElement();
            final PsiClass baseClass = baseMethod.getContainingClass();

            final HashMap<PsiElement, EnergyCallHierarchyNodeDescriptor> psiElementToDescriptorMap = new HashMap<PsiElement, EnergyCallHierarchyNodeDescriptor>();

            final ArrayList<EnergyCallHierarchyNodeDescriptor> result = new ArrayList<EnergyCallHierarchyNodeDescriptor>();

            for (final PsiElement calledElement : psiElements) {
                if (calledElement instanceof PsiMethod) {
                    final PsiMethod calledMethod = (PsiMethod) calledElement;
                    if (!isInScope(baseClass, calledMethod, myScopeType)) continue;

                    EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMap.get(calledMethod);
                    if (d == null) {
                        d = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, calledMethod, false, false);
                        psiElementToDescriptorMap.put(calledMethod, d);
                        result.add(d);
                    } else {
                        d.incrementUsageCount();
                    }
                } else {
                    if (calledElement instanceof PsiIfStatement) {
                        final PsiIfStatement psiIfStatement = (PsiIfStatement) calledElement;
                        if (!isInScope(baseClass, psiIfStatement, myScopeType)) continue;

                        EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMap.get(psiIfStatement);
                        if (d == null) {
                            d = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, psiIfStatement, false, false);
                            psiElementToDescriptorMap.put(psiIfStatement, d);
                            result.add(d);
                        } else {
                            d.incrementUsageCount();
                        }
                    }else{
                        if (calledElement instanceof PsiLoopStatement) {
                            final PsiLoopStatement psiIfStatement = (PsiLoopStatement) calledElement;
                            if (!isInScope(baseClass, psiIfStatement, myScopeType)) continue;

                            EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMap.get(psiIfStatement);
                            if (d == null) {
                                d = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, psiIfStatement, false, false);
                                psiElementToDescriptorMap.put(psiIfStatement, d);
                                result.add(d);
                            } else {
                                d.incrementUsageCount();
                            }
                        } else {

                        }
                    }
                }
            }
            return ArrayUtil.toObjectArray(result);
        }
        final PsiElement enclosingElement = ((EnergyCallHierarchyNodeDescriptor)descriptor).getEnclosingElement();
        if(enclosingElement instanceof PsiIfStatement){
            final PsiIfStatement ifStatement = (PsiIfStatement) enclosingElement;
            final ArrayList<EnergyCallHierarchyNodeDescriptor> result = new ArrayList<EnergyCallHierarchyNodeDescriptor>();
            //     new JavaCallReferenceProcessor(enclosingElement.getReference(), new JavaCallHierarchyData()).process()



//            EnergyCallHierarchyNodeDescriptor IfDescriptor = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, ifStatement, false, false);
//            IfDescriptor.type= EnergyCallHierarchyNodeDescriptor.Type.IF_BLOCK;
//            result.add(IfDescriptor);


            final PsiElement body = ifStatement.getThenBranch();
//                final ArrayList<PsiElement> psiElements = new ArrayList<PsiElement>();
            EnergyCallHierarchyNodeDescriptor IfThenDescriptor =null;
            if (body != null) {
                IfThenDescriptor = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, body, false, false);
                IfThenDescriptor.type= EnergyCallHierarchyNodeDescriptor.Type.THEN_BRANCH;
                result.add(IfThenDescriptor);
                //visitor(body, psiElements);
            }

//                final PsiElement basePsiElement = ((EnergyCallHierarchyNodeDescriptor) getBaseDescriptor()).getTargetElement();
//                final PsiClass baseClass = IntelliJUtilFacade.lookUpForContainingClass(basePsiElement);
//                if(baseClass==null) {
//                    return ArrayUtil.EMPTY_OBJECT_ARRAY;
//                }
//
//                final HashMap<PsiElement, EnergyCallHierarchyNodeDescriptor> psiElementToDescriptorMap = new HashMap<PsiElement, EnergyCallHierarchyNodeDescriptor>();
//
//
//
//                for (final PsiElement calledElement : psiElements) {
//                    if (calledElement instanceof PsiMethod) {
//                        final PsiMethod calledMethod = (PsiMethod) calledElement;
//                        if (!isInScope(baseClass, calledMethod, myScopeType)) continue;
//
//                        EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMap.get(calledMethod);
//                        if (d == null) {
//                            d = new EnergyCallHierarchyNodeDescriptor(myProject, IfThenDescriptor, calledMethod, false, false);
//                            d.type= EnergyCallHierarchyNodeDescriptor.Type.THEN_BRANCH;
//                            psiElementToDescriptorMap.put(calledMethod, d);
//                            result.add(d);
//                        } else {
//                            d.incrementUsageCount();
//                        }
//                    } else {
//                        if (calledElement instanceof PsiIfStatement) {
//                            final PsiIfStatement psiIfStatement = (PsiIfStatement) calledElement;
//                            if (!isInScope(baseClass, psiIfStatement, myScopeType)) continue;
//
//                            EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMap.get(psiIfStatement);
//                            if (d == null) {
//                                d = new EnergyCallHierarchyNodeDescriptor(myProject, IfThenDescriptor, psiIfStatement, false, false);
//                                psiElementToDescriptorMap.put(psiIfStatement, d);
//                               // result.add(d);
//                            } else {
//                                d.incrementUsageCount();
//                            }
//                        } else {
//
//                        }
//
//                    }
//                }

            final PsiElement bodyElse = ifStatement.getElseBranch();
//                final ArrayList<PsiElement> psiElementsElse = new ArrayList<PsiElement>();
            EnergyCallHierarchyNodeDescriptor IfElseDescriptor =null;
            if (bodyElse != null) {
                IfElseDescriptor = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, bodyElse, false, false);
                IfElseDescriptor.type= EnergyCallHierarchyNodeDescriptor.Type.ELSE_BRANCH;
                result.add(IfElseDescriptor);
                //  visitor(bodyElse, psiElementsElse);
            }
//                return result.toArray();
//
////                final PsiElement basePsiElement = ((EnergyCallHierarchyNodeDescriptor) getBaseDescriptor()).getTargetElement();
////                final PsiClass baseClass = IntelliJUtilFacade.lookUpForContainingClass(basePsiElement);
//                if(baseClass==null) {
//                    return ArrayUtil.EMPTY_OBJECT_ARRAY;
//                }
//
//                final HashMap<PsiElement, EnergyCallHierarchyNodeDescriptor> psiElementToDescriptorMapElse = new HashMap<PsiElement, EnergyCallHierarchyNodeDescriptor>();
//
//
//
//                for (final PsiElement calledElement : psiElementsElse) {
//                    if (calledElement instanceof PsiMethod) {
//                        final PsiMethod calledMethod = (PsiMethod) calledElement;
//                        if (!isInScope(baseClass, calledMethod, myScopeType)) continue;
//
//                        EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMapElse.get(calledMethod);
//                        if (d == null) {
//                            d = new EnergyCallHierarchyNodeDescriptor(myProject, IfElseDescriptor, calledMethod, false, false);
//                            psiElementToDescriptorMapElse.put(calledMethod, d);
//                            result.add(d);
//                        } else {
//                            d.incrementUsageCount();
//                        }
//                    } else {
//                        if (calledElement instanceof PsiIfStatement) {
//                            final PsiIfStatement psiIfStatement = (PsiIfStatement) calledElement;
//                            if (!isInScope(baseClass, psiIfStatement, myScopeType)) continue;
//
//                            EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMapElse.get(psiIfStatement);
//                            if (d == null) {
//                                d = new EnergyCallHierarchyNodeDescriptor(myProject, IfElseDescriptor, psiIfStatement, false, false);
//                                psiElementToDescriptorMapElse.put(psiIfStatement, d);
//                                result.add(d);
//                            } else {
//                                d.incrementUsageCount();
//                            }
//                        } else {
//
//                        }
//
//                    }
//                }

//                // also add overriding methods as children
//                final PsiMethod[] overridingMethods = OverridingMethodsSearch.search(method, true).toArray(PsiMethod.EMPTY_ARRAY);
//                for (final PsiMethod overridingMethod : overridingMethods) {
//                    if (!isInScope(baseClass, overridingMethod, myScopeType)) continue;
//                    final EnergyCallHierarchyNodeDescriptor node = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, overridingMethod, false, false);
//                    if (!result.contains(node)) result.add(node);
//                }

/*
    // show method implementations in EJB Class
    final PsiMethod[] ejbImplementations = EjbUtil.findEjbImplementations(method, null);
    for (int i = 0; i < ejbImplementations.length; i++) {
      PsiMethod ejbImplementation = ejbImplementations[i];
      result.add(new CallHierarchyNodeDescriptor(myProject, descriptor, ejbImplementation, false));
    }
*/
            return ArrayUtil.toObjectArray(result);

        }else{
           // if()
        }


//        if (!(enclosingElement instanceof PsiMethod)&&!(enclosingElement instanceof PsiIfStatement)) {
//            return ArrayUtil.EMPTY_OBJECT_ARRAY;
//        }
        if(enclosingElement instanceof PsiMethod) {
            final PsiMethod method = (PsiMethod) enclosingElement;
            //     new JavaCallReferenceProcessor(enclosingElement.getReference(), new JavaCallHierarchyData()).process()
            final ArrayList<PsiElement> psiElements = new ArrayList<PsiElement>();

            final PsiCodeBlock body = method.getBody();
            if (body != null) {
                visitor(body, psiElements);
            }

            final PsiMethod baseMethod = (PsiMethod) ((EnergyCallHierarchyNodeDescriptor) getBaseDescriptor()).getTargetElement();
            final PsiClass baseClass = baseMethod.getContainingClass();

            final HashMap<PsiElement, EnergyCallHierarchyNodeDescriptor> psiElementToDescriptorMap = new HashMap<PsiElement, EnergyCallHierarchyNodeDescriptor>();

            final ArrayList<EnergyCallHierarchyNodeDescriptor> result = new ArrayList<EnergyCallHierarchyNodeDescriptor>();

            for (final PsiElement calledElement : psiElements) {
                if (calledElement instanceof PsiMethod) {
                    final PsiMethod calledMethod = (PsiMethod) calledElement;
                    if (!isInScope(baseClass, calledMethod, myScopeType)) continue;

                    EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMap.get(calledMethod);
                    if (d == null) {
                        d = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, calledMethod, false, false);
                        psiElementToDescriptorMap.put(calledMethod, d);
                        result.add(d);
                    } else {
                        d.incrementUsageCount();
                    }
                } else {
                    if (calledElement instanceof PsiIfStatement) {
                        final PsiIfStatement psiIfStatement = (PsiIfStatement) calledElement;
                        if (!isInScope(baseClass, psiIfStatement, myScopeType)) continue;

                        EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMap.get(psiIfStatement);
                        if (d == null) {
                            d = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, psiIfStatement, false, false);
                            psiElementToDescriptorMap.put(psiIfStatement, d);
                            result.add(d);
                        } else {
                            d.incrementUsageCount();
                        }
                    } else {
                        if (calledElement instanceof PsiLoopStatement) {
                            final PsiLoopStatement psiIfStatement = (PsiLoopStatement) calledElement;
                            if (!isInScope(baseClass, psiIfStatement, myScopeType)) continue;

                            EnergyCallHierarchyNodeDescriptor d = psiElementToDescriptorMap.get(psiIfStatement);
                            if (d == null) {
                                d = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, psiIfStatement, false, false);
                                psiElementToDescriptorMap.put(psiIfStatement, d);
                                result.add(d);
                            } else {
                                d.incrementUsageCount();
                            }
                        } else {

                        }

                    }

                }
            }

            // also add overriding methods as children
            final PsiMethod[] overridingMethods = OverridingMethodsSearch.search(method, true).toArray(PsiMethod.EMPTY_ARRAY);
            for (final PsiMethod overridingMethod : overridingMethods) {
                if (!isInScope(baseClass, overridingMethod, myScopeType)) continue;
                final EnergyCallHierarchyNodeDescriptor node = new EnergyCallHierarchyNodeDescriptor(myProject, descriptor, overridingMethod, false, false);
                if (!result.contains(node)) result.add(node);
            }

/*
    // show method implementations in EJB Class
    final PsiMethod[] ejbImplementations = EjbUtil.findEjbImplementations(method, null);
    for (int i = 0; i < ejbImplementations.length; i++) {
      PsiMethod ejbImplementation = ejbImplementations[i];
      result.add(new CallHierarchyNodeDescriptor(myProject, descriptor, ejbImplementation, false));
    }
*/
            return ArrayUtil.toObjectArray(result);
        }else{
            return ArrayUtil.EMPTY_OBJECT_ARRAY;
        }


    }




    private static void visitor(final PsiElement element, final ArrayList<PsiElement> methods) {
        final PsiElement[] children = element.getChildren();
        for (final PsiElement child : children) {

            if (child instanceof PsiMethodCallExpression) {
                visitor(child, methods);
                final PsiMethodCallExpression callExpression = (PsiMethodCallExpression)child;
                final PsiReferenceExpression methodExpression = callExpression.getMethodExpression();
                final PsiMethod method = (PsiMethod)methodExpression.resolve();
                if (method != null) {
                    methods.add(method);
                }
            }
            else {
                if (child instanceof PsiNewExpression) {
                    visitor(child, methods);
                    final PsiNewExpression newExpression = (PsiNewExpression) child;
                    final PsiMethod method = newExpression.resolveConstructor();
                    if (method != null) {
                        methods.add(method);
                    }
                }else{
                    if(child instanceof PsiIfStatement){
                        PsiIfStatement psiIfStatement=(PsiIfStatement)child;
                        methods.add(psiIfStatement);
//                        if(psiIfStatement.getThenBranch()!=null){
//                        methods.add(psiIfStatement);
//                        }
//                        if(psiIfStatement.getElseBranch()!=null){
//                            methods.add(psiIfStatement);
//                        }

                    }else{
                        if(child instanceof PsiLoopStatement){
                            PsiLoopStatement psiIfStatement=(PsiLoopStatement)child;
                            methods.add(psiIfStatement);
//                        if(psiIfStatement.getThenBranch()!=null){
//                        methods.add(psiIfStatement);
//                        }
//                        if(psiIfStatement.getElseBranch()!=null){
//                            methods.add(psiIfStatement);
//                        }

                        }else{
                            visitor(child, methods);
                        }
                    }
                }
            }
        }
    }
}
