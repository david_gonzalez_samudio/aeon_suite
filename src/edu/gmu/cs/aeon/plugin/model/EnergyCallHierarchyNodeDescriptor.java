package edu.gmu.cs.aeon.plugin.model;

/**
 * Created by DavidIgnacio on 4/18/2015.
 */
/*
 * Copyright 2000-2013 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


        import com.intellij.codeInsight.highlighting.HighlightManager;
        import com.intellij.icons.AllIcons;
        import com.intellij.ide.IdeBundle;
        import com.intellij.ide.hierarchy.HierarchyNodeDescriptor;
        import com.intellij.openapi.editor.Editor;
        import com.intellij.openapi.editor.colors.EditorColors;
        import com.intellij.openapi.editor.colors.EditorColorsManager;
        import com.intellij.openapi.editor.markup.RangeHighlighter;
        import com.intellij.openapi.editor.markup.TextAttributes;
        import com.intellij.openapi.fileEditor.FileEditorManager;
        import com.intellij.openapi.project.Project;
        import com.intellij.openapi.roots.ui.util.CompositeAppearance;
        import com.intellij.openapi.util.Comparing;
        import com.intellij.openapi.util.Iconable;
        import com.intellij.openapi.util.TextRange;
        import com.intellij.pom.Navigatable;
        import com.intellij.psi.*;
        import com.intellij.psi.presentation.java.ClassPresentationUtil;
        import com.intellij.psi.util.*;
        import com.intellij.ui.LayeredIcon;
        import edu.gmu.cs.aeon.core.analysis.estimation.MethodEnergyCalculator;
        import edu.gmu.cs.aeon.knowledge.energy.EnergyKnowledgeManager;
        import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
        import org.jetbrains.annotations.NotNull;

        import javax.swing.*;
        import java.awt.*;
        import java.util.ArrayList;
        import java.util.List;

public final class EnergyCallHierarchyNodeDescriptor extends HierarchyNodeDescriptor implements Navigatable {
    private int myUsageCount = 1;
    private final List<PsiReference> myReferences = new ArrayList<PsiReference>();
    private final boolean myNavigateToReference;
    private double energyEstimate=0;
    Type type = Type.METHOD;
    //same as callgraph
    public  enum Type {
        API, METHOD, SWITCH_BLOCK, SWITCH_BRANCH, IF_BLOCK , THEN_BRANCH, ELSE_BRANCH, LOOP_BLOCK, TRY_BLOCK, TRY_BRANCH, CATCH_BRANCH, ROOT, FINALLY_BRANCH
    }

    public EnergyCallHierarchyNodeDescriptor(@NotNull Project project,
                                       final HierarchyNodeDescriptor parentDescriptor,
                                       @NotNull PsiElement element,
                                       final boolean isBase,
                                       final boolean navigateToReference) {
        super(project, parentDescriptor, element, isBase);
        myNavigateToReference = navigateToReference;
        if(element instanceof PsiMethod){

            final PsiMethod method = (PsiMethod)element;
            final PsiClass containingClass = method.getContainingClass();
            String className = "";
            if (containingClass != null) {
                className=containingClass.getQualifiedName();
            }
            final String methodName = method.getName();
             Double est = EnergyKnowledgeManager.energyGreedyAPIs.get(className + "." + methodName);
            if (est != null) {
                 type=Type.API;
                energyEstimate=est;
            }else{
                energyEstimate= MethodEnergyCalculator.doEnergyAnalysisReport(element).finalEstimate;
                if(energyEstimate>0) {
                    type = Type.API;
                }else{
                    type = Type.METHOD;
                }
            }
        }else{
            if(element instanceof PsiIfStatement) {
                type = Type.IF_BLOCK;
            }else{
                if(element instanceof PsiLoopStatement) {
                    type = Type.LOOP_BLOCK;
                }else{

                }

            }
               energyEstimate= MethodEnergyCalculator.doEnergyAnalysisReport(element).finalEstimate;
//            }else{
//
//            }
        }
    }

    /**
     * @return PsiMethod or PsiClass or JspFile, if block a statement
     */
    public final PsiElement getEnclosingElement(){
        return getPsiElement() == null ? null : getEnclosingElement(getPsiElement(), type);
    }

    public static PsiElement getEnclosingElement(final PsiElement element, Type type){
        switch (type) {
            case API:
                return PsiTreeUtil.getNonStrictParentOfType(element, PsiMethod.class, PsiClass.class);
            case METHOD:
                return PsiTreeUtil.getNonStrictParentOfType(element, PsiMethod.class, PsiClass.class);
            case LOOP_BLOCK:
                return PsiTreeUtil.getNonStrictParentOfType(element, PsiLoopStatement.class, PsiMethod.class, PsiClass.class);
            default:
                return PsiTreeUtil.getNonStrictParentOfType(element, PsiIfStatement.class,PsiMethod.class, PsiClass.class);
        }

    }

    public final void incrementUsageCount(){
        myUsageCount++;
    }

    /**
     * Element for OpenFileDescriptor
     */
    public final PsiElement getTargetElement(){
        return getPsiElement();
    }

    public final boolean isValid(){
        final PsiElement element = getEnclosingElement();
        return element != null && element.isValid();
    }

    public final boolean update() {
        final CompositeAppearance oldText = myHighlightedText;
        final Icon oldIcon = getIcon();

        int flags = Iconable.ICON_FLAG_VISIBILITY;
        if (isMarkReadOnly()) {
            flags |= Iconable.ICON_FLAG_READ_STATUS;
        }

        boolean changes = super.update();


        myHighlightedText = new CompositeAppearance();
        TextAttributes mainTextAttributes = null;
        if (myColor != null) {
            mainTextAttributes = new TextAttributes(myColor, null, null, null, Font.PLAIN);
        }
        String estimateData = "";
        final PsiElement enclosingElement = getEnclosingElement();
       // energyEstimate= MethodEnergyCalculator.doEnergyAnalysisReport(enclosingElement).finalEstimate;
        if (energyEstimate > 0) {
            estimateData = " -- Energy[" + energyEstimate + " mAh]";
        }

        switch (type) {
            case METHOD:
                if (enclosingElement == null) {
                    final String invalidPrefix = IdeBundle.message("node.hierarchy.invalid");
                    if (!myHighlightedText.getText().startsWith(invalidPrefix)) {
                        myHighlightedText.getBeginning().addText(invalidPrefix, HierarchyNodeDescriptor.getInvalidPrefixAttributes());
                    }
                    return true;
                }

                Icon newIcon = enclosingElement.getIcon(flags);
                if (changes && myIsBase) {
                    final LayeredIcon icon = new LayeredIcon(2);
                    icon.setIcon(newIcon, 0);
                    icon.setIcon(AllIcons.Hierarchy.Base, 1, -AllIcons.Hierarchy.Base.getIconWidth() / 2, 0);
                    newIcon = icon;
                }

                setIcon(newIcon);
                break;
            case API:
                setIcon(EnergyIcons.ENERGY_MARKER_ENERGY_INFO);
                break;
            case IF_BLOCK:
                setIcon(EnergyIcons.ENERGY_MARKER_LINKER);
                break;
            case THEN_BRANCH:
//                double energyEstimate1= MethodEnergyCalculator.doEnergyAnalysisReport(getTargetElement()).finalEstimate;
//                if (energyEstimate1 > 0) {
//                    estimateData = " -- Energy[" + energyEstimate + " mAh]";
//                }
                setIcon(EnergyIcons.ENERGY_MARKER_GOOD_FIX);

                myHighlightedText.getEnding().addText("Then " + estimateData, mainTextAttributes);
                myName = myHighlightedText.getText();
                return true;
            case ELSE_BRANCH:
//                double energyEstimate2= MethodEnergyCalculator.doEnergyAnalysisReport(getTargetElement()).finalEstimate;
//                if (energyEstimate2 > 0) {
//                    estimateData = " -- Energy[" + energyEstimate + " mAh]";
//                }
                setIcon(EnergyIcons.ENERGY_MARKER_STOP);
                myHighlightedText.getEnding().addText("Else " + estimateData, mainTextAttributes);
                myName = myHighlightedText.getText();
                return true;
            case LOOP_BLOCK:
                setIcon(EnergyIcons.ENERGY_MARKER_WARNING_LOOP_CASE);
                break;

        }


        if (enclosingElement instanceof PsiIfStatement) {
            final PsiIfStatement psiIfStatement = (PsiIfStatement) enclosingElement;
            String conditionText = psiIfStatement.getCondition().getText();
            myHighlightedText.getEnding().addText(conditionText != null ? "IF (" + conditionText + ")" + estimateData : "IF (N/A)" + estimateData, mainTextAttributes);
        } else {
            if(enclosingElement instanceof PsiLoopStatement){
                final PsiLoopStatement psiLoopStatement = (PsiLoopStatement) enclosingElement;
                String conditionText = "LOOP()";
                //todo validate nulls
                if(enclosingElement instanceof PsiForeachStatement){
                    final PsiForeachStatement psiForeachStatement=(PsiForeachStatement)enclosingElement;
                    final String iterationParameter=psiForeachStatement.getIterationParameter()==null?"":psiForeachStatement.getIterationParameter().getText();
                    final String iteratedValue=psiForeachStatement.getIteratedValue()==null?"":psiForeachStatement.getIteratedValue().getText();
                    conditionText="ForEach("+iterationParameter+" : "+iteratedValue+")";
                }
                if(enclosingElement instanceof PsiForStatement){
                    final PsiForStatement psiForStatement=(PsiForStatement)enclosingElement;
                    final String initialization=psiForStatement.getInitialization()==null?";":psiForStatement.getInitialization().getText();
                    final String condition=psiForStatement.getCondition()==null?"":psiForStatement.getCondition().getText();
                    final String update=psiForStatement.getUpdate()==null?"":psiForStatement.getUpdate().getText();
                    conditionText="For("+initialization+" "+condition+" ; "+update+")";
                }
                if(enclosingElement instanceof PsiDoWhileStatement){
                    final PsiDoWhileStatement psiDoWhileStatement=(PsiDoWhileStatement)enclosingElement;
                    final String condition=psiDoWhileStatement.getCondition()==null?"":psiDoWhileStatement.getCondition().getText();
                    conditionText="DoWhile("+condition+")";
                }
                if(enclosingElement instanceof PsiWhileStatement){
                    final PsiWhileStatement psiWhileStatement=(PsiWhileStatement)enclosingElement;
                    final String condition=psiWhileStatement.getCondition()==null?"":psiWhileStatement.getCondition().getText();
                    conditionText="While("+condition+")";
                }
                myHighlightedText.getEnding().addText(conditionText  + estimateData, mainTextAttributes);
            }else{
            if (enclosingElement instanceof PsiMethod) {
                if (enclosingElement instanceof SyntheticElement) {
                    PsiFile file = enclosingElement.getContainingFile();
                    myHighlightedText.getEnding().addText(file != null ? file.getName() : IdeBundle.message("node.call.hierarchy.unknown.jsp"), mainTextAttributes);
                } else {
                    final PsiMethod method = (PsiMethod) enclosingElement;
                    final StringBuilder buffer = new StringBuilder(128);
                    final PsiClass containingClass = method.getContainingClass();
                    String className = "";
                    if (containingClass != null) {
                        className = containingClass.getQualifiedName();
                        buffer.append(ClassPresentationUtil.getNameForClass(containingClass, false));
                        buffer.append('.');
                    }
                    final String methodText = PsiFormatUtil.formatMethod(
                            method,
                            PsiSubstitutor.EMPTY, PsiFormatUtilBase.SHOW_NAME | PsiFormatUtilBase.SHOW_PARAMETERS,
                            PsiFormatUtilBase.SHOW_TYPE
                    );
                    final String methodName = method.getName();

                    buffer.append(methodText);


                    if (energyEstimate > 0) {
                        buffer.append(" -- Energy[" + energyEstimate + " mAh]");
                    }

                    myHighlightedText.getEnding().addText(buffer.toString(), mainTextAttributes);
                }
            } else if (FileTypeUtils.isInServerPageFile(enclosingElement) && enclosingElement instanceof PsiFile) {
                final PsiFile file = PsiUtilCore.getTemplateLanguageFile(enclosingElement);
                myHighlightedText.getEnding().addText(file.getName(), mainTextAttributes);
            } else {
                if(enclosingElement instanceof PsiClass) {
                    myHighlightedText.getEnding().addText(ClassPresentationUtil.getNameForClass((PsiClass) enclosingElement, false), mainTextAttributes);
                }
            }
        }
    }
        if (myUsageCount > 1) {
            myHighlightedText.getEnding().addText(IdeBundle.message("node.call.hierarchy.N.usages", myUsageCount), HierarchyNodeDescriptor.getUsageCountPrefixAttributes());
        }
//        if (!(FileTypeUtils.isInServerPageFile(enclosingElement) && enclosingElement instanceof PsiFile)) {
//            final PsiClass containingClass = enclosingElement instanceof PsiMethod
//                    ? ((PsiMethod)enclosingElement).getContainingClass()
//                    : (PsiClass)enclosingElement;
//            if (containingClass != null) {
//                final String packageName = JavaHierarchyUtil.getPackageName(containingClass);
//                //myHighlightedText.getEnding().addText("  (" + packageName + ")", HierarchyNodeDescriptor.getPackageNameAttributes());
//            }
//        }
        myName = myHighlightedText.getText();

        if (
                !Comparing.equal(myHighlightedText, oldText) ||
                        !Comparing.equal(getIcon(), oldIcon)
                ){
            changes = true;
        }
        return changes;
    }

    public void addReference(final PsiReference reference) {
        myReferences.add(reference);
    }

    public boolean hasReference(PsiReference reference) {
        return myReferences.contains(reference);
    }

    public void navigate(boolean requestFocus) {
        if (!myNavigateToReference) {
            if (getPsiElement() instanceof Navigatable && ((Navigatable)getPsiElement()).canNavigate()) {
                ((Navigatable)getPsiElement()).navigate(requestFocus);
            }
            return;
        }

        final PsiReference firstReference = myReferences.get(0);
        final PsiElement element = firstReference.getElement();
        if (element == null) return;
        final PsiElement callElement = element.getParent();
        if (callElement instanceof Navigatable && ((Navigatable)callElement).canNavigate()) {
            ((Navigatable)callElement).navigate(requestFocus);
        } else {
            final PsiFile psiFile = callElement.getContainingFile();
            if (psiFile == null || psiFile.getVirtualFile() == null) return;
            FileEditorManager.getInstance(myProject).openFile(psiFile.getVirtualFile(), requestFocus);
        }

        Editor editor = PsiUtilBase.findEditor(callElement);

        if (editor != null) {

            HighlightManager highlightManager = HighlightManager.getInstance(myProject);
            EditorColorsManager colorManager = EditorColorsManager.getInstance();
            TextAttributes attributes = colorManager.getGlobalScheme().getAttributes(EditorColors.SEARCH_RESULT_ATTRIBUTES);
            ArrayList<RangeHighlighter> highlighters = new ArrayList<RangeHighlighter>();
            for (PsiReference psiReference : myReferences) {
                final PsiElement eachElement = psiReference.getElement();
                if (eachElement != null) {
                    final PsiElement eachMethodCall = eachElement.getParent();
                    if (eachMethodCall != null) {
                        final TextRange textRange = eachMethodCall.getTextRange();
                        highlightManager.addRangeHighlight(editor, textRange.getStartOffset(), textRange.getEndOffset(), attributes, false, highlighters);
                    }
                }
            }
        }
    }

    public boolean canNavigate() {
        if (!myNavigateToReference) {
            return getPsiElement() instanceof Navigatable && ((Navigatable) getPsiElement()).canNavigate();
        }
        if (myReferences.isEmpty()) return false;
        final PsiReference firstReference = myReferences.get(0);
        final PsiElement callElement = firstReference.getElement().getParent();
        if (callElement == null || !callElement.isValid()) return false;
        if (!(callElement instanceof Navigatable) || !((Navigatable)callElement).canNavigate()) {
            final PsiFile psiFile = callElement.getContainingFile();
            if (psiFile == null) return false;
        }
        return true;
    }

    public boolean canNavigateToSource() {
        return canNavigate();
    }
}
