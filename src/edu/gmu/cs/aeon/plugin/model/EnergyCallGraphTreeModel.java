package edu.gmu.cs.aeon.plugin.model;

/**
 * Created by DavidIgnacio on 7/10/2014.
 */
/*
    IDEA PsiViewer Plugin
    Copyright (C) 2002 Andrew J. Armstrong

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Author:
	Andrew J. Armstrong <andrew_armstrong@bigpond.com>
*/

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiWhiteSpace;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;
import edu.gmu.cs.aeon.knowledge.energy.EnergyKnowledgeManager;
import edu.gmu.cs.aeon.plugin.controller.AEONProjectComponent;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EnergyCallGraphTreeModel implements TreeModel {
    private final AEONProjectComponent _projectComponent;

    private PsiElement _root;
    private Report _rootReport;
    public enum EnergyView{
        ENERGY_BREAK_POINT, ENERGY_MEASUREMENT_POINT, ENERGY_CALL_GRAPH, PROJECT_VIEW
    }
    public final Set<EnergyView> enabledEnergyViews=new HashSet<EnergyView>();

    public EnergyCallGraphTreeModel(AEONProjectComponent projectComponent) {
        _projectComponent = projectComponent;
        enabledEnergyViews.add(EnergyView.ENERGY_CALL_GRAPH);
    }
    public void enableEnergyView(EnergyView energyView){
        enabledEnergyViews.add(energyView);
    }
    public void disableEnergyView(EnergyView energyView){
        enabledEnergyViews.remove(energyView);
    }

    public Object getRoot() {
        return _root;
    }

    public void setRoot(Report rootReport) {
        _rootReport=rootReport;
        _root = rootReport.cause;
    }

    public void setRoot(PsiElement root) {
        _root = root;
    }

    public Object getChild(Object parent, int index) {
        PsiElement psi = (PsiElement) parent;
        List<PsiElement> children = getFilteredChildren(psi);
        return children.get(index);
    }

    public int getChildCount(Object parent) {
        PsiElement psi = (PsiElement) parent;
        return getFilteredChildren(psi).size();
    }

    public boolean isLeaf(Object node) {
        PsiElement psi = (PsiElement) node;
        return getFilteredChildren(psi).size() == 0;
    }

    private List<PsiElement> getFilteredChildren(PsiElement psi) {
        final List<PsiElement> filteredChildren = new ArrayList<PsiElement>();

//        if (psi instanceof PsiLanguageInjectionHost) {
//            InjectedLanguageUtil.enumerate(psi, psi.getContainingFile(), false, new PsiLanguageInjectionHost.InjectedPsiVisitor() {
//                @Override
//                public void visit(@NotNull final PsiFile injectedPsi, @NotNull final List<PsiLanguageInjectionHost.Shred> places) {
//                    if (injectedPsi.isValid()) filteredChildren.add(injectedPsi);
//                }
//            });
//
//            return filteredChildren;
//        }

        for (PsiElement e = psi.getFirstChild(); e != null; e = e.getNextSibling()) {
            if (isValid(e)) {
                if(enabledEnergyViews.contains(EnergyView.ENERGY_BREAK_POINT)) {

                    if (isValidPath(e) && filterIsToShow(e)) {
                        filteredChildren.add(e);
                    }
                }
                if(enabledEnergyViews.contains(EnergyView.ENERGY_MEASUREMENT_POINT)) {
                    if (isEnergyPath(e)) {
                        filteredChildren.add(e);
                    }

                }

            }
        }


        return filteredChildren;
    }

    public boolean filterIsToShow(PsiElement psiElement) {
        return true;

    }

    private boolean isEnergyPath(PsiElement psiElement) {

        if (psiElement instanceof PsiMethodCallExpression) {
            PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) psiElement;
            String className = IntelliJUtilFacade.getMethodCallExpressionClassName(psiMethodCallExpression);
            String methodName = IntelliJUtilFacade.getMethodCallExpressionMethodName(psiMethodCallExpression);
            if (EnergyKnowledgeManager.energyGreedyAPIs.get(className + "." + methodName) != null) {
                return true;
            }
        }
        PsiElement e = psiElement.getFirstChild();
        boolean isStatementPath = false;
        while (e != null) {

            isStatementPath = isStatementPath || isEnergyPath(e);

            e = e.getNextSibling();
        }
        return isStatementPath;
    }

    private boolean isValid(PsiElement psiElement) {
        return !_projectComponent.isFilterWhitespace() || !(psiElement instanceof PsiWhiteSpace);
    }
//
//    private boolean isValidEnergyPath(PsiElement psiElement) {
//        if (psiElement instanceof PsiStatement) {
//            EnergyBreakPoint toValidate = EnergyLineMarkerProvider.getEBP(psiElement);
//            if (toValidate != null && toValidate.isEnabled())
//                return true;
//        }
//
//        PsiElement e = psiElement.getFirstChild();
//        boolean isStatementPath = false;
//        while (e != null) {
//
//            isStatementPath = isStatementPath || isValidEnergyPath(e);
//
//            e = e.getNextSibling();
//        }
//        return isStatementPath;
//    }

    private boolean isValidPath(PsiElement psiElement) {
        if (psiElement instanceof PsiMethodCallExpression) {

            return true;
        }

        PsiElement e = psiElement.getFirstChild();
        boolean isStatementPath = false;
        while (e != null) {

            isStatementPath = isStatementPath || isValidPath(e);

            e = e.getNextSibling();
        }
        return isStatementPath;
    }

    public int getIndexOfChild(Object parent, Object child) {
        PsiElement psiParent = (PsiElement) parent;
        List<PsiElement> psiChildren = getFilteredChildren(psiParent);

        return psiChildren.indexOf((PsiElement) child);
    }

    public void valueForPathChanged(TreePath path, Object newValue) {
    }

    public synchronized void addTreeModelListener(TreeModelListener l) {
    }

    public synchronized void removeTreeModelListener(TreeModelListener l) {
    }


}
