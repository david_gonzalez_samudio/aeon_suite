package edu.gmu.cs.aeon.plugin;

/**
 * Created by DavidIgnacio on 5/4/2014.
 */

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import edu.gmu.cs.aeon.core.analysis.redundantresolution.RedundantOperationInLoop;
import edu.gmu.cs.aeon.core.analysis.resources.greedyloop.HttpClientInLoop;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.plugin.view.EnergyBreakPoint;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

public class EnergyAnnotator implements Annotator {

    public static Set<PsiElement> foundAsyncTask = new HashSet<PsiElement>();
    public static Set<PsiElement> foundHttpClient = new HashSet<PsiElement>();
    public static Set<PsiElement> foundRedundantOperationInLoop = new HashSet<PsiElement>();
    public static Set<PsiElement> foundRedundantEncapsulation = new HashSet<PsiElement>();


    @Override
    public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {
        annotateEnergyCode(element, holder);
        // collectStateMakers(element, holder);
    }

    public void collectStateMakers(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {
        {
            if (!(element instanceof PsiMethod))
                return;

            if (!AndroidUtilFacade.isAndroidComponentStateMethod((PsiMethod) element) && !AndroidUtilFacade.methodLooksLikeImplementsUISomethingSomethingListener((PsiMethod) element))
                return;
            PsiElement ref = null;
            try {
                ref = ((PsiMethod) element).getBody().getStatements()[0];//todo fix super. case
            } catch (NullPointerException npe) {
                return;
            } catch (IndexOutOfBoundsException ioobe) {
                return;
            }

//            if (ref == null)
//                return;
//            System.out.println("sib " + ref.getText());
            //todo what if method is empty... nothing useful

            EnergyBreakPoint ebp = new EnergyBreakPoint(ref);
            ebp.navigateHandler();

        }
    }

    public void annotateEnergyCode(PsiElement element, AnnotationHolder holder) {
//        AsyncTaskGenerator asyncTaskGenerator=new AsyncTaskGenerator();
//        PsiElement loopStatement=asyncTaskGenerator(element, holder);
//        if(loopStatement!=null)
//            foundHttpClient.add(loopStatement);

        HttpClientInLoop clientInLoop = new HttpClientInLoop();
        PsiElement loopStatement = clientInLoop.annotateUseCase(element, holder);
        if (loopStatement != null)
            foundHttpClient.add(loopStatement);

        Refactorer redundantOperationInLoop = new RedundantOperationInLoop();
        if (redundantOperationInLoop.isUseCase(element)) {

            PsiElement ref = redundantOperationInLoop.annotateUseCase(element, holder);
        }

//        RedundantOperationInLoop redundantVariableInLoop=new RedundantOperationInLoop(null);
//        PsiElement toAnnotate= redundantVariableInLoop.annotateUseCase(element, holder);
//        if(toAnnotate!=null){
//            foundRedundantOperationInLoop.add(toAnnotate);
//        }
//        RedundantEncapsulation redundantEncapsulation=new RedundantEncapsulation(null);
//        toAnnotate= redundantEncapsulation.annotateUseCase(element, holder);
//        if(toAnnotate!=null){
//            foundRedundantEncapsulation.add(toAnnotate);
//            PsiClass[] classes = JavaPsiFacade.getInstance(element.getProject()).findClasses("UseCase001.java",new EverythingGlobalScope(element.getProject()));
//            for(PsiClass pc:classes)
//            System.out.println(pc.getName()+" >>"+pc.getQualifiedName());
//
//
//        }
    }
}

//    public static final TextAttributesKey SEPARATOR = TextAttributesKey.createTextAttributesKey("SIMPLE_SEPARATOR", new TextAttributes(Color.GREEN, Color.DARK_GRAY, null, null, Font.BOLD));

//    public void annotateLiteralExpression(PsiElement element, AnnotationHolder holder) {
//        if (element instanceof PsiLiteralExpression) {
//            PsiLiteralExpression literalExpression = (PsiLiteralExpression) element;
//            if (literalExpression.getValue() == null || !(literalExpression.getValue() instanceof String))
//                return;
//            String value = (String) literalExpression.getValue();
//            if (value.startsWith("simple:")) {
//                Project project = element.getProject();
//                List<String> properties = new ArrayList<String>(2);
//                properties.add("Zing");
//                if (properties.size() == 1 && value.contains("Zing")) {
//                    TextRange range = new TextRange(element.getTextRange().getStartOffset() + 7,
//                            element.getTextRange().getEndOffset());
//                    Annotation annotation = holder.createInfoAnnotation(range, null);
//                    annotation.setTextAttributes(DefaultLanguageHighlighterColors.LINE_COMMENT);
//                } else if (properties.size() == 0) {
//                    TextRange range = new TextRange(element.getTextRange().getStartOffset() + 8,
//                            element.getTextRange().getEndOffset());
//                    holder.createErrorAnnotation(range, "Unresolved property");
//                }
//            }
//        }
//    }






