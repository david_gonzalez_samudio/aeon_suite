package edu.gmu.cs.aeon.plugin;

import com.intellij.codeInsight.daemon.GutterIconNavigationHandler;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.NotNullLazyValue;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.util.Function;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by DavidIgnacio on 9/26/2014.
 */
public class EnergyBreakPointInfo<P> extends RelatedItemLineMarkerInfo implements ActionListener {


    public EnergyBreakPointInfo(@NotNull PsiElement element, @NotNull TextRange range, Icon icon, int updatePass, @Nullable Function tooltipProvider, @Nullable GutterIconNavigationHandler navHandler, GutterIconRenderer.Alignment alignment, @NotNull NotNullLazyValue targets) {
        super(element, range, icon, updatePass, tooltipProvider, navHandler, alignment, targets);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new Messages().showYesNoCancelDialog("Clicked", "EBP", this.myIcon);
    }

    public RelatedItemLineMarkerInfo<PsiElement> createLineMarkerInfo(@NotNull PsiElement element, NavigationGutterIconBuilder navigationGutterIconBuilder, String tooltip) {
//
//            NotNullLazyValue<Collection<? extends GotoRelatedItem>> gotoTargets = new NotNullLazyValue<Collection<? extends GotoRelatedItem>>() {
//                  @NotNull
//                  @Override
//                  protected Collection<? extends GotoRelatedItem> compute() {
//                       if (myGotoRelatedItemProvider != null) {
//                              return ContainerUtil.concat(myTargets.getValue(), myGotoRelatedItemProvider);
//                            }
//                       return Collections.emptyList();
//                      }
//                };
//         GutterIconRenderer x= new GutterIconRenderer();
//           return new EnergyBreakPoint<PsiElement>(element, element.getTextRange(), navigationGutterIconBuilder, Pass.UPDATE_OVERRIDEN_MARKERS,
//                                                                    tooltip == null ? null : new ConstantFunction<PsiElement, String>(tooltip),
//                                                                    renderer.isNavigateAction() ? renderer : null, renderer.getAlignment(),
//                                                                     gotoTargets);
        return null;
    }

}
