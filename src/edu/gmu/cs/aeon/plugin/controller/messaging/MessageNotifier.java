package edu.gmu.cs.aeon.plugin.controller.messaging;

import com.intellij.lang.java.parser.DeclarationParser;

/**
 * Created by DavidIgnacio on 9/28/2014.
 */
public interface MessageNotifier {


    void beforeAction(DeclarationParser.Context context);

    void afterAction(DeclarationParser.Context context);
}
