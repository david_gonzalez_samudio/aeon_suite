package edu.gmu.cs.aeon.plugin.controller.messaging;

import com.intellij.lang.java.parser.DeclarationParser;
import com.intellij.util.messages.MessageBus;
import com.intellij.util.messages.Topic;

/**
 * Created by DavidIgnacio on 9/28/2014.
 */
public class ChangeActionNotifier {
    Topic<MessageNotifier> CHANGE_ACTION_TOPIC = Topic.create("MessageNotifier", MessageNotifier.class);
    private MessageBus myBus;

    public void init(MessageBus bus) {
        myBus = bus;
        bus.connect().subscribe(CHANGE_ACTION_TOPIC, new MessageNotifier() {
            @Override
            public void beforeAction(DeclarationParser.Context context) {
                // Process 'before action' event.
            }

            @Override
            public void afterAction(DeclarationParser.Context context) {
                // Process 'after action' event.
            }
        });
    }

    public void doChange(DeclarationParser.Context context) {
        MessageNotifier publisher = myBus.syncPublisher(CHANGE_ACTION_TOPIC);
        publisher.beforeAction(context);
        try {
            // Do action
            // ...
        } finally {
            publisher.afterAction(context);
        }
    }
}
