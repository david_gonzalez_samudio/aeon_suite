package edu.gmu.cs.aeon.plugin.controller.application;

/**
 * Created by DavidIgnacio on 7/10/2014.
 */
/*
    IDEA Plugin
    Copyright (C) 2002 Andrew J. Armstrong

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Author:
	Andrew J. Armstrong <andrew_armstrong@bigpond.com>
*/


import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import edu.gmu.cs.aeon.core.utils.Helpers;
import edu.gmu.cs.aeon.plugin.controller.AEONProjectComponent;
import edu.gmu.cs.aeon.plugin.view.configuration.ConfigurationPanel;
import edu.gmu.cs.aeon.plugin.view.configuration.AEONConstants;

import javax.swing.*;
import java.awt.*;


public class Configuration implements ApplicationComponent, AEONConstants, Configurable {


    public String HIGHLIGHT_COLOR = DEFAULT_HIGHLIGHT_COLOR;
    public boolean PLUGIN_ENABLED = true;
    public boolean LEGACY_ENABLED = false;
    public boolean TRACKING_ENABLED = true;
    public String TRACKING_ID = "TRACKING_ID_"+System.nanoTime();
    private final TextAttributes _textAttributes = new TextAttributes();

    private ConfigurationPanel _panel;

    public Configuration() {
        load();
    }

    private static void enableToolWindows(boolean enableToolWindows) {
        Project[] projects = ProjectManager.getInstance().getOpenProjects();
        for (Project project : projects) {
            AEONProjectComponent pc = AEONProjectComponent.getInstance(project);
            if (enableToolWindows) pc.initToolWindow();
            else
                pc.unregisterToolWindow();
        }
    }

    public static Configuration getInstance() {
        return (Configuration) ApplicationManager.getApplication().getComponent(Configuration.class);
    }

    public void initComponent() {
        getTextAttributes().setBackgroundColor(getHighlightColor());
    }

    public void disposeComponent() {
    }

    public String getComponentName() {
        return PLUGIN_NAME + "." + CONFIGURATION_COMPONENT_NAME;
    }

    public String getDisplayName() {
        return PLUGIN_NAME;
    }


    public Icon getIcon() {
        return Helpers.getIcon(ICON_CONFIGURATION);
    }

    public String getHelpTopic() {
        return "AEON Configuration Setup";
    }

    public JComponent createComponent() {
        _panel = new ConfigurationPanel();
      //  load();
        return _panel;
    }

    public boolean isModified() {
        if (_panel.isPluginEnabled() ^ PLUGIN_ENABLED) {
            return true;
        }
        if (_panel.isLegacyEnabled() ^ LEGACY_ENABLED) {
            return true;
        }
        if (_panel.isTrackingEnabled() ^ TRACKING_ENABLED) {
//            TRACKING_ID="TRACKING_ID_"+System.nanoTime();
//            _panel.setTrackingIDLabel(TRACKING_ID);
            return true;
        }

        if (!Helpers.encodeColor(_panel.getHighlightColor()).equals(HIGHLIGHT_COLOR)) {
            return true;
        }

        return false;
    }

    /**
     * Save the settings from the configuration panel
     */
    public void apply() throws ConfigurationException {
        store();
        if (PLUGIN_ENABLED ^ _panel.isPluginEnabled())  // If plugin-enabled state has changed...
            enableToolWindows(_panel.isPluginEnabled());
        getTextAttributes().setBackgroundColor(getHighlightColor());
        _panel.setHighlightColor(getHighlightColor());

    }

    public void reset() {
        HIGHLIGHT_COLOR = DEFAULT_HIGHLIGHT_COLOR;
        PLUGIN_ENABLED = true;
        LEGACY_ENABLED = false;
        TRACKING_ENABLED = true;
        // TRACKING_ID="TRACKING_ID";
        _panel.setPluginEnabled(PLUGIN_ENABLED);
        _panel.setLegacyEnabled(LEGACY_ENABLED);
        _panel.setTrackingEnabled(TRACKING_ENABLED);
        _panel.setTrackingIDLabel(TRACKING_ID);
        _panel.setHighlightColor(getHighlightColor());

    }

    public void load() {
        PropertiesComponent propertiesComponent = PropertiesComponent.getInstance(ProjectManager.getInstance().getDefaultProject());

        HIGHLIGHT_COLOR = propertiesComponent.getValue(PLUGIN_ID + "_HIGHLIGHT_COLOR", DEFAULT_HIGHLIGHT_COLOR);
       PLUGIN_ENABLED = propertiesComponent.getBoolean(PLUGIN_ID + "_PLUGIN_ENABLED", true);
      //  PLUGIN_ENABLED=true;
        LEGACY_ENABLED = propertiesComponent.getBoolean(PLUGIN_ID + "_LEGACY_ENABLED", false);
        TRACKING_ENABLED = propertiesComponent.getBoolean(PLUGIN_ID + "_TRACKING_ENABLED", true);
        TRACKING_ID = propertiesComponent.getValue(PLUGIN_ID + "_TRACKING_ID", "TRACKING_ID_" + System.nanoTime());


        if (_panel == null) {
            return;
        }

        _panel.setPluginEnabled(PLUGIN_ENABLED);
        _panel.setLegacyEnabled(LEGACY_ENABLED);
        _panel.setTrackingEnabled(TRACKING_ENABLED);
        _panel.setTrackingIDLabel(TRACKING_ID);
        _panel.setHighlightColor(getHighlightColor());
        initComponent();

    }

    public void store() {
        if (_panel == null) {
            return;
        }
        PLUGIN_ENABLED = _panel.isPluginEnabled();
        LEGACY_ENABLED = _panel.isLegacyEnabled();
        TRACKING_ENABLED = _panel.isTrackingEnabled();
        TRACKING_ID = _panel.getTrackingIDLabel();
        HIGHLIGHT_COLOR = Helpers.encodeColor(_panel.getHighlightColor());
        PropertiesComponent propertiesComponent = PropertiesComponent.getInstance(ProjectManager.getInstance().getDefaultProject());
        propertiesComponent.setValue(PLUGIN_ID + "_HIGHLIGHT_COLOR", HIGHLIGHT_COLOR);
        propertiesComponent.setValue(PLUGIN_ID + "_PLUGIN_ENABLED", String.valueOf(PLUGIN_ENABLED));
        propertiesComponent.setValue(PLUGIN_ID + "_LEGACY_ENABLED", String.valueOf(LEGACY_ENABLED));
        propertiesComponent.setValue(PLUGIN_ID + "_TRACKING_ENABLED", String.valueOf(TRACKING_ENABLED));
        propertiesComponent.setValue(PLUGIN_ID + "_TRACKING_ID", TRACKING_ID);
        initComponent();
    }

    public void disposeUIResources() {
        _panel = null;
    }

    public boolean isPluginEnabled() {
        return PLUGIN_ENABLED;
    }

    public boolean isLegacyEnabled() {
        return LEGACY_ENABLED;
    }

    public boolean isTrackingEnabled() {
        return TRACKING_ENABLED;
    }

    private Color getHighlightColor() {
        return Helpers.parseColor(HIGHLIGHT_COLOR);
    }

    public TextAttributes getTextAttributes() {
        return _textAttributes;
    }

}
