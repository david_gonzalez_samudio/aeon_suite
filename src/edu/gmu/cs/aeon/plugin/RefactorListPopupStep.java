package edu.gmu.cs.aeon.plugin;


/**
 * Created by DavidIgnacio on 4/14/2014.
 */

import com.intellij.codeInsight.CodeInsightUtil;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorModificationUtil;
import com.intellij.openapi.ui.popup.PopupStep;
import com.intellij.openapi.ui.popup.util.BaseListPopupStep;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.Nullable;

public class RefactorListPopupStep extends BaseListPopupStep {
    protected PsiElement psiElement;
    protected Editor editor;
    protected PsiClass psiClass;

    public RefactorListPopupStep(@Nullable String aTitle, Object[] aValues, PsiElement psiElement, Editor editor, PsiClass psiClass) {
        super(aTitle, aValues);

        this.psiElement = psiElement;
        this.editor = editor;
        this.psiClass = psiClass;
    }

    @Override
    public PopupStep onChosen(final Object selectedValue, boolean finalChoice) {
        if (!finalChoice)
            return super.onChosen(selectedValue, finalChoice);

        if (!CodeInsightUtil.preparePsiElementsForWrite(psiElement)) {
            return super.onChosen(selectedValue, finalChoice);
        }

        new WriteCommandAction.Simple(psiClass.getProject(), psiClass.getContainingFile()) {

            @Override
            protected void run() throws Throwable {
                EditorModificationUtil.insertStringAtCaret(editor, (String) selectedValue + editor.getSelectionModel().getSelectedText(true) + ")", true, true);
                editor.getSelectionModel().selectWordAtCaret(true);
                EditorModificationUtil.deleteSelectedText(editor);

            }

        }.execute();


        return super.onChosen(selectedValue, finalChoice);
    }
}
