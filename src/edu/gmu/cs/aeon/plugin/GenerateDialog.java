package edu.gmu.cs.aeon.plugin;

/**
 * Created by DavidIgnacio on 4/14/2014.
 */

import com.intellij.ide.util.DefaultPsiElementCellRenderer;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.LabeledComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiImportStatement;
import com.intellij.psi.PsiMethod;
import com.intellij.ui.CollectionListModel;
import com.intellij.ui.ToolbarDecorator;
import com.intellij.ui.components.JBList;
import edu.gmu.cs.aeon.core.AndroidAPIEnergy;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.List;

/**
 * Class to handle the Dialog when GenerateAction is called
 */
public class GenerateDialog extends DialogWrapper {
    private final String dialogDescription = "Energy Refactoring";
    private final String dialogTitle = "Select elements to be included during Energy Refactoring";
    private LabeledComponent<JPanel> labeledComponent;
    private CollectionListModel<PsiMethod> filteredMethods;
    private CollectionListModel<?> collectionListModel;

    public GenerateDialog(PsiClass psiClass, String type) {
        super(psiClass.getProject());
        setTitle(dialogTitle);
        //changed from allFields to just the local scope
        List<PsiImportStatement> psiImportStatementFilteredList = AndroidAPIEnergy.getPsiImportStatements(psiClass);

        collectionListModel = new CollectionListModel<PsiImportStatement>(psiImportStatementFilteredList);
        JBList jbList = new JBList(collectionListModel);

        jbList.setCellRenderer(new DefaultPsiElementCellRenderer());
        ToolbarDecorator decorator = ToolbarDecorator.createDecorator(jbList);
        decorator.disableAddAction();
        JPanel panel = decorator.createPanel();
        labeledComponent = LabeledComponent.create(panel, dialogDescription);
        init();
    }


    public GenerateDialog(PsiClass psiClass) {
        super(psiClass.getProject());
        setTitle(dialogTitle);
        //changed from allFields to just the local scope
        filteredMethods = new CollectionListModel<PsiMethod>(psiClass.getMethods());
        JList fieldList = new JBList(filteredMethods);
        fieldList.setCellRenderer(new DefaultPsiElementCellRenderer());
        ToolbarDecorator decorator = ToolbarDecorator.createDecorator(fieldList);
        decorator.disableAddAction();
        JPanel panel = decorator.createPanel();
        labeledComponent = LabeledComponent.create(panel, dialogDescription);

        init();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return labeledComponent;
    }

    public List<PsiMethod> getMethods() {
        return filteredMethods.getItems();
    }

    public List<?> getImports() {
        return collectionListModel.getItems();
    }
}