package edu.gmu.cs.aeon.plugin.actions;

/**
 * Created by DavidIgnacio on 4/14/2014.
 */

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.ui.popup.ListPopup;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.source.tree.ElementType;
import com.intellij.psi.util.PsiTreeUtil;
import edu.gmu.cs.aeon.plugin.RefactorListPopupStep;

import java.util.ArrayList;
import java.util.List;

public class RefactorAction extends AnAction {
    public void actionPerformed(AnActionEvent e) {

        PsiClass psiClass = getPsiClassFromContext(e);
        Editor editor = e.getData(PlatformDataKeys.EDITOR);
        if (editor == null)
            return;

        PsiElement psiElement = getPsiElement(e, editor);

        refactorCommand(psiClass, psiElement, editor);

    }

    private PsiClass getPsiClassFromContext(AnActionEvent e) {
        PsiFile psiFile = e.getData(LangDataKeys.PSI_FILE);
        Editor editor = e.getData(PlatformDataKeys.EDITOR);
        if (psiFile == null || editor == null) {
            return null;
        }
        int offset = editor.getCaretModel().getOffset();
        PsiElement elementAt = psiFile.findElementAt(offset);
        PsiClass psiClass = PsiTreeUtil.getParentOfType(elementAt, PsiClass.class);
        return psiClass;
    }

    public void refactorCommand(final PsiClass psiClass, final PsiElement psiElement, final Editor editor) {
        new WriteCommandAction.Simple(psiClass.getProject(), psiClass.getContainingFile()) {

            @Override
            protected void run() throws Throwable {
                String originalWord = psiElement.getText();


                List<String> synonyms = new ArrayList<String>();
                synonyms.add("@AE(");

                ListPopup listPopup = JBPopupFactory.getInstance().createListPopup(
                        new RefactorListPopupStep("AEON", synonyms.toArray(), psiElement, editor, psiClass)
                );

                listPopup.showInBestPositionFor(editor);

            }

        }.execute();
    }

    @Override
    public void update(AnActionEvent e) {
        Editor editor = e.getData(PlatformDataKeys.EDITOR);
        PsiElement psiElement = getPsiElement(e, editor);
        e.getPresentation().setEnabled(psiElement != null);
    }

    private PsiElement getPsiElement(AnActionEvent e, Editor editor) {
        PsiFile psiFile = e.getData(LangDataKeys.PSI_FILE);

        if (psiFile == null || editor == null) {
            return null;
        }

        int offset = editor.getCaretModel().getOffset();
        PsiElement elementAt = psiFile.findElementAt(offset);

        if (elementAt == null || elementAt.getNode() == null || elementAt.getNode().getElementType() != ElementType.IDENTIFIER) {
            return null;
        }

        System.out.println("offset: " + offset);
        System.out.println("psi element text: " + elementAt.getText());
        System.out.println("psi element language: " + elementAt.getLanguage().getDisplayName());
        System.out.println("psi element nodetype: " + elementAt.getNode().getElementType());

        return elementAt;
    }

}