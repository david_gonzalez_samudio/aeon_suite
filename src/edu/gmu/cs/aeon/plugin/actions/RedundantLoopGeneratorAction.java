package edu.gmu.cs.aeon.plugin.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiFile;
import com.intellij.psi.util.PsiUtilBase;
import edu.gmu.cs.aeon.core.BaseAndroidCodeGenerateAction;
import edu.gmu.cs.aeon.plugin.PluginHelper;
import org.jetbrains.annotations.NotNull;

/**
 * Created by DavidIgnacio on 5/5/2014.
 */
public class RedundantLoopGeneratorAction extends BaseAndroidCodeGenerateAction {
    public RedundantLoopGeneratorAction() {
        super(null);
    }

    @Override
    protected boolean isValidForFile(@NotNull final Project project, @NotNull final Editor editor, @NotNull final PsiFile file) {
        return super.isValidForFile(project, editor, file);
    }

    @Override
    protected boolean isValidForClass(@NotNull final PsiClass targetClass) {
        return super.isValidForClass(targetClass);
    }

    @Override
    public void actionPerformed(final AnActionEvent e) {
        // super.actionPerformed(e);

        PsiClass cls = PluginHelper.getPsiClassFromContext(e);

//        if (!EnergyAnnotator.foundRedundantOperationInLoop.isEmpty()) {
//
//            RedundantOperationInLoop rv=new RedundantOperationInLoop(cls);
//            for (PsiElement d:EnergyAnnotator.foundRedundantOperationInLoop) {
//                rv.refactorElement(d);
//            }
//
//
//        } else {
//            //todo error balloon - no views
////        }
//        new EnergyManagerGenerator(cls)
//                .execute();
    }

    @Override
    public void actionPerformedImpl(@NotNull final Project project, final Editor editor) {
        final PsiFile f = PsiUtilBase.getPsiFileInEditor(editor, project);
        if (f == null) return;
        PsiClass cls = getTargetClass(editor, f);
//todo change the call
//        new EnergyManagerGenerator(cls)
//                .execute();

    }

    @Override
    public void update(AnActionEvent e) {
        PsiClass psiClass = PluginHelper.getPsiClassFromContext(e);
        e.getPresentation().setEnabled(psiClass != null);
    }
}
