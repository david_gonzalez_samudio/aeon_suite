package edu.gmu.cs.aeon.plugin.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import edu.gmu.cs.aeon.core.AndroidAPIEnergy;
import edu.gmu.cs.aeon.plugin.GenerateDialog;
import edu.gmu.cs.aeon.plugin.PluginHelper;

import java.util.List;

/**
 * Created by DavidIgnacio on 4/14/2014.
 */
public class GenerateAction extends AnAction {

    public void actionPerformed(AnActionEvent e) {
//        PsiClass psiClass = PluginHelper.getPsiClassFromContext(e);
//        GenerateDialog dlg = new GenerateDialog(psiClass);
//        dlg.show();
//        if (dlg.isOK()) {
//            generateChanges(psiClass, dlg.getMethods());
//        }
        PsiClass psiClass = PluginHelper.getPsiClassFromContext(e);
        GenerateDialog dlg = new GenerateDialog(psiClass, null);
        dlg.show();
        if (dlg.isOK()) {
            refactorImports(psiClass, dlg.getImports());
        }
    }

    public void generateChanges(final PsiClass psiClass, final List<PsiMethod> methods) {
        new WriteCommandAction.Simple(psiClass.getProject(), psiClass.getContainingFile()) {

            @Override
            protected void run() throws Throwable {
                generateDirectFields(psiClass, methods);
                //  generateImplementsComparable(psiClass);


            }

        }.execute();
    }

    public void refactorImports(final PsiClass psiClass, final List<?> psiElements) {
        new WriteCommandAction.Simple(psiClass.getProject(), psiClass.getContainingFile()) {

            @Override
            protected void run() throws Throwable {
                // AndroidAPIEnergy.refactorImportsToProxy(psiClass, (List<PsiImportStatement>)psiElements);
                //todo
                AndroidAPIEnergy.proxyRefactoring(psiClass);
//                AndroidAPIEnergy.refactoringAPItoProxies(psiClass);
            }

        }.execute();
    }

    private void generateDirectFields(PsiClass psiClass, List<PsiMethod> methods) {
        String psiClassName = psiClass.getName();
        StringBuilder builder = new StringBuilder("\n\n*Generated Code\n\n");
        PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(psiClass.getProject());
        for (PsiMethod method : methods) {
            String methodName = method.getName();
            String methodText = method.getText();
            String methodType = method.getReturnType().getCanonicalText(true);
            // example: public int methodName;
            builder.append("\npublic " + methodType + " " + methodName + ";");
            builder.append("\n//" + methodText + "\n");

            PsiField psiField = elementFactory.createField(methodName, method.getReturnType());

            PsiElement psiElement = psiClass.add(psiField);
            JavaCodeStyleManager.getInstance(psiClass.getProject()).shortenClassReferences(psiElement);
            // psiClass.replace()
        }
        builder.append("\n\n" +
                " End Generated Code\n" +
                "\n");
        // PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(psiClass.getProject());
//      PsiComment psiCodeComment = elementFactory.createCommentFromText(builder.toString(), psiClass);
//      PsiElement psiElement = psiClass.add(psiCodeComment);
//        JavaCodeStyleManager.getInstance(psiClass.getProject()).shortenClassReferences(psiElement);

    }


    @Override
    public void update(AnActionEvent e) {
        PsiClass psiClass = PluginHelper.getPsiClassFromContext(e);
        e.getPresentation().setEnabled(psiClass != null);
    }
}
