package edu.gmu.cs.aeon.plugin;

import com.intellij.codeHighlighting.Pass;
import com.intellij.codeInsight.daemon.GutterIconNavigationHandler;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.navigation.GotoRelatedItem;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NotNullLazyValue;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.psi.*;
import com.intellij.util.ConstantFunction;
import edu.gmu.cs.aeon.core.analysis.Analyzer;
import edu.gmu.cs.aeon.core.analysis.MethodAnalyzer;
import edu.gmu.cs.aeon.core.analysis.estimation.MethodEnergyCalculator;
import edu.gmu.cs.aeon.core.analysis.estimation.PsiElementEnergyReport;
import edu.gmu.cs.aeon.core.analysis.greedyloop.GreedyCallsInLoopAnalyzer;
import edu.gmu.cs.aeon.core.analysis.redundantresolution.RedundantOperationInLoop;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.core.analysis.resources.cast.OnProvideAssistDataListenerAPIAnalyzer;
import edu.gmu.cs.aeon.core.analysis.resources.greedyloop.HttpClientInLoop;
import edu.gmu.cs.aeon.core.analysis.resources.greedyloop.URLConnectionInLoop;
import edu.gmu.cs.aeon.core.analysis.resources.location.LocationAPIAnalyzer;
import edu.gmu.cs.aeon.core.analysis.resources.powermanager.WakeLockAnalyzer;
import edu.gmu.cs.aeon.core.analysis.smells.VariableFinalizerRefactor;
import edu.gmu.cs.aeon.core.callgraph.AEONCallGraph;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.plugin.view.EnergyBreakPoint;
import edu.gmu.cs.aeon.plugin.view.EnergyLineMarker;
import edu.gmu.cs.aeon.plugin.view.AEONPanel;
import edu.gmu.cs.aeon.trepn.control.OnDatafileChangedListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.*;

public class EnergyLineMarkerProvider extends RelatedItemLineMarkerProvider {

    public static Vector<HashMap<PsiStatement, EnergyBreakPoint>> statements;
    public static Vector<HashMap<PsiElement, EnergyLineMarker>> detectedIssues;
    public static HashMap<PsiElement, EnergyBreakPoint> energyBreakPoints;
    public static HashMap<PsiElement, EnergyBreakPoint> energyMeasurePoints;
    private static OnDatafileChangedListener onDatafileChangedListener;

    static {
        onDatafileChangedListener = AEONPanel.getLatestInstance();
        detectedIssues = new Vector<HashMap<PsiElement, EnergyLineMarker>>();
        statements = new Vector<HashMap<PsiStatement, EnergyBreakPoint>>();
        energyBreakPoints = new HashMap<PsiElement, EnergyBreakPoint>();
        energyMeasurePoints = new HashMap<PsiElement, EnergyBreakPoint>();

    }

    //    @Nullable
//    public static EnergyBreakPoint getEBP(PsiStatement psiStatement) {
//
//        for (HashMap<PsiStatement, EnergyBreakPoint> statementTuple : statements) {
//            for (Object element : statementTuple.keySet()) {
//                PsiStatement found = (PsiStatement) element;
//                try {
//                    final int currentElementLineNumber = FileDocumentManager.getInstance().getDocument(psiStatement.getContainingFile().getVirtualFile()).getLineNumber(psiStatement.getTextOffset());
//                    final int currentFoundElementLineNumber = FileDocumentManager.getInstance().getDocument(found.getContainingFile().getVirtualFile()).getLineNumber(found.getTextOffset());
//                    if (psiStatement.getContainingFile().equals(found.getContainingFile())) {
//                        if (currentElementLineNumber == currentFoundElementLineNumber) {
//                            return statementTuple.get(found);
//                        }
//                    } else {
//                        return null;
//                    }
//                } catch (NullPointerException npe) {
//                    return null;
//                } catch (IllegalArgumentException iae) {
//                    return null;
//                }
//            }
//        }
//        return null;
//    }
    NotNullLazyValue<Collection<? extends GotoRelatedItem>> gotoTargets = new NotNullLazyValue<Collection<? extends GotoRelatedItem>>() {


        @NotNull
        @Override
        protected Collection<? extends GotoRelatedItem> compute() {
//            if (myGotoRelatedItemProvider != null) {
//                return ContainerUtil.concat(myTargets.getValue(), myGotoRelatedItemProvider);
//            }
            return Collections.emptyList();
        }
    };
    private Editor editor;

    @Nullable
    public static EnergyBreakPoint getEBP(PsiElement psiStatement) {
        if (energyBreakPoints.containsKey(psiStatement)) {
            // if(energyBreakPoints.get(psiStatement).)
            return energyBreakPoints.get(psiStatement);
        }

        return null;
        //todo check for memory leaks when the app is being used fo too much and the invalids elements are still referenced here
    }

    //    @Nullable
//    public static EnergyActionMarker getEnergyMarker(PsiElement psiElement) {
//
//        for (HashMap<PsiElement, EnergyActionMarker> statementTuple : detectedIssues) {
//            for (Object element : statementTuple.keySet()) {
//                PsiElement found = (PsiElement) element;
//                try {
//                    final int currentElementLineNumber = FileDocumentManager.getInstance().getDocument(psiElement.getContainingFile().getVirtualFile()).getLineNumber(psiElement.getTextOffset());
//                    final int currentFoundElementLineNumber = FileDocumentManager.getInstance().getDocument(found.getContainingFile().getVirtualFile()).getLineNumber(found.getTextOffset());
//                    if (psiElement.getContainingFile().equals(found.getContainingFile())) {
//                        if (currentElementLineNumber == currentFoundElementLineNumber) {
//                            return statementTuple.get(found);
//                        }
//                    } else {
//                        return null;
//                    }
//                } catch (NullPointerException npe) {
//                    return null;
//                }
//            }
//        }
//
//        return null;
//    }


    //todo callhierarachy
//    File file=new File(element.getContainingFile().getVirtualFile().getCanonicalPath());
//    List<File> files=new ArrayList<File>();
//    files.add(file);
//
//    try {
//        ApplicationManager.getApplication().runReadAction(new Runnable() {
//            @Override
//            public void run() {
//                if(!showingCallHierarchy) {
//                    new CallHierarchyBrowser(psiMethod1.getProject(), psiMethod1);
//                }
//                showingCallHierarchy=true;
//            }
//        });
//
//        //  new LintCliClient().run(new MyIssueRegistry(), files);
//    }catch (Exception e){}
     static boolean showingCallHierarchy=false;
    public void collectEnergyMeasurementMakers(@NotNull final PsiElement element, Collection<? super RelatedItemLineMarkerInfo> result) {
        if (!(element instanceof PsiMethod))
            return;
        final PsiMethod psiMethod1 = (PsiMethod) element;
//        MethodEnergyCalculator methodEnergyCalculator = new MethodEnergyCalculator();

//        psiMethod1.accept(methodEnergyCalculator);
        //System.out.println();
//        double calc=MethodEnergyCalculator.calculate(methodEnergyCalculator);
         PsiElementEnergyReport psiElementEnergyReport =MethodEnergyCalculator.doEnergyAnalysisReport(psiMethod1);

        if(psiElementEnergyReport.finalEstimate>0) {


            EnergyBreakPoint ebp = new EnergyBreakPoint(psiMethod1);
            ebp.asEnergyInfo();
            RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(psiMethod1, psiMethod1.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                    new ConstantFunction<PsiElement, String>(psiElementEnergyReport.getText()),
                    ebp.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) ebp : null, ebp.getAlignment(),
                    gotoTargets);
                if(result!=null&&relatedItemLineMarkerInfo!=null) {
                    result.add(relatedItemLineMarkerInfo);
                    energyMeasurePoints.put(psiMethod1, ebp);
                }
        }// todo, color based measurements
    }

    public void collectReportMakers(@NotNull final PsiElement element, Collection<? super RelatedItemLineMarkerInfo> result) {
        if (!(element instanceof PsiMethod))
            return;
        PsiMethod psiMethod1 = (PsiMethod) element;

        //System.out.println();
        for (Report report : MethodAnalyzer.analyzeMethodAndCollectReports(psiMethod1)) {
            EnergyBreakPoint ebp = new EnergyBreakPoint(psiMethod1);
            ebp.asLinker();
            RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(psiMethod1, psiMethod1.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                    new ConstantFunction<PsiElement, String>(report.getReportData()),
                    ebp.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) ebp : null, ebp.getAlignment(),
                    gotoTargets);
            result.add(relatedItemLineMarkerInfo);
           // energyBreakPoints.put(psiMethod1, ebp);
        }
    }
    public void collectHierarchyMakers(@NotNull final PsiElement element, Collection<? super RelatedItemLineMarkerInfo> result) {
        if (!(element instanceof PsiMethod))
            return;
        PsiMethod psiMethod1 = (PsiMethod) element;
//        EnergyBreakPoint ebp = new EnergyBreakPoint(psiMethod1);
//        ebp.asHierarchy(psiMethod1);
//        String out="";
//        for (VirtualFile url: ModuleRootManager.getInstance(ModuleUtil.findModuleForPsiElement(psiMethod1)).getSourceRoots()){
//            out+=url.getCanonicalPath()+"\n";
//        }
//        RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(psiMethod1, psiMethod1.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
//                new ConstantFunction<PsiElement, String>(out),
//                ebp.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) ebp : null, ebp.getAlignment(),
//                gotoTargets);
//        result.add(relatedItemLineMarkerInfo);
        Collection<PsiElementEnergyReport> paths=MethodEnergyCalculator.doMethodEnergyAnalysisByPaths(psiMethod1);
        StringBuffer pathText=new StringBuffer();
        int pathCount=0;
        for(PsiElementEnergyReport path:paths){
            pathCount++;
            pathText.append("Path "+pathCount+" : \n"+path.getPathText()+"\n");
        }

        if(pathCount>0) {

            EnergyBreakPoint ebp = new EnergyBreakPoint(psiMethod1);
            ebp.asHierarchy(psiMethod1);
            RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(psiMethod1, psiMethod1.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                    new ConstantFunction<PsiElement, String>(pathText.toString()),
                    ebp.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) ebp : null, ebp.getAlignment(),
                    gotoTargets);
            result.add(relatedItemLineMarkerInfo);
        }
           // energyBreakPoints.put(psiMethod1, ebp);

    }
    public void collectEnergyLineMakers(@NotNull final PsiElement element, Collection<? super RelatedItemLineMarkerInfo> result) {

//        if(element instanceof PsiClass){
//            PsiClass psiClass = (PsiClass) element;
//            System.out.println("!!!!!!!!!!!!REPORT "+ ReportManager.getReport(psiClass).getReportData());
//        }
        Refactorer redundantOperationInLoop = new RedundantOperationInLoop();
        if (redundantOperationInLoop.isUseCase(element)) {


            // PsiElement ref=redundantOperationInLoop.annotateUseCase(element, null);

            EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, redundantOperationInLoop);
            // energyLineMarker.doHighlight();
            RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(element, element.getTextRange(), redundantOperationInLoop.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                    new ConstantFunction<PsiElement, String>("RedundantOperationInLoop"),
                    energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                    gotoTargets);

            //energyBreakPoints.put(element, ebp);
            result.add(relatedItemLineMarkerInfo);
        }

        if(AEONPanel.Options.isLocationAnalysisOn) {

            LocationAPIAnalyzer locationProviderAnalyzer = new LocationAPIAnalyzer();
            locationProviderAnalyzer.isUseCase(element);

            for (Report report : locationProviderAnalyzer.getReports(null)) {
                EnergyBreakPoint ebp = new EnergyBreakPoint(element);
                ebp.asLocation();
                report.isBad = true;
                EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, redundantOperationInLoop);
                // energyLineMarker.doHighlight();
                RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(report.cause, report.cause.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                        new ConstantFunction<PsiElement, String>(report.getReportDataForUI()),
                        energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                        gotoTargets);

                //energyBreakPoints.put(element, ebp);
                result.add(relatedItemLineMarkerInfo);
            }
            for (Report report : locationProviderAnalyzer.okReports) {
                EnergyBreakPoint ebp = new EnergyBreakPoint(element);
                ebp.asGoodFix();
                report.isBad = false;
                EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, redundantOperationInLoop);
                // energyLineMarker.doHighlight();
                RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(report.cause, report.cause.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                        new ConstantFunction<PsiElement, String>(report.getReportDataForUI()),
                        energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                        gotoTargets);

                //energyBreakPoints.put(element, ebp);
                result.add(relatedItemLineMarkerInfo);
            }
        }

        if(AEONPanel.Options.isWakelockAnalysisOn) {
            WakeLockAnalyzer wakeLockAnalyzer = new WakeLockAnalyzer();
            wakeLockAnalyzer.mode = Analyzer.Mode.UI;
            wakeLockAnalyzer.isUseCase(element);

            for (Report report : wakeLockAnalyzer.getReports(null)) {
                EnergyBreakPoint ebp = new EnergyBreakPoint(element);
                ebp.asWakeLock();
                report.isBad = true;
                EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, redundantOperationInLoop);
                // energyLineMarker.doHighlight();
                RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(report.cause, report.cause.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                        new ConstantFunction<PsiElement, String>(report.getReportDataForUI()),
                        energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                        gotoTargets);

                //energyBreakPoints.put(element, ebp);
                result.add(relatedItemLineMarkerInfo);
            }
            for (Report report : wakeLockAnalyzer.okReports) {
                EnergyBreakPoint ebp = new EnergyBreakPoint(element);
                ebp.asGoodFix();
                report.isBad = false;
                EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, redundantOperationInLoop);
                // energyLineMarker.doHighlight();
                RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(report.cause, report.cause.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                        new ConstantFunction<PsiElement, String>(report.getReportDataForUI()),
                        energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                        gotoTargets);

                //energyBreakPoints.put(element, ebp);
                result.add(relatedItemLineMarkerInfo);
            }


        }

        Refactorer urlConnectionInLoop = new URLConnectionInLoop();
        if (urlConnectionInLoop.isUseCase(element)) {


            // PsiElement ref=redundantOperationInLoop.annotateUseCase(element, null);

            EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, urlConnectionInLoop);
            // energyLineMarker.doHighlight();
            RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(element, element.getTextRange(), urlConnectionInLoop.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                    new ConstantFunction<PsiElement, String>("urlConnectionInLoop"),
                    energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                    gotoTargets);

            //energyBreakPoints.put(element, ebp);
            result.add(relatedItemLineMarkerInfo);
        }
        Refactorer httpClientInLoop = new HttpClientInLoop();
        if (httpClientInLoop.isUseCase(element)) {


            // PsiElement ref=redundantOperationInLoop.annotateUseCase(element, null);

            EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, httpClientInLoop);
            // energyLineMarker.doHighlight();
            RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(element, element.getTextRange(), httpClientInLoop.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                    new ConstantFunction<PsiElement, String>("httpClientInLoop"),
                    energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                    gotoTargets);

            //energyBreakPoints.put(element, ebp);
            result.add(relatedItemLineMarkerInfo);
        }

        if(AEONPanel.Options.isFinalizeAnalysisOn) {
            Refactorer variableFinalizerRefactor = new VariableFinalizerRefactor();
            if (variableFinalizerRefactor.isUseCase(element)) {


                // PsiElement ref=redundantOperationInLoop.annotateUseCase(element, null);

                EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, variableFinalizerRefactor);
                // energyLineMarker.doHighlight();
                RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(element, element.getTextRange(), variableFinalizerRefactor.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                        new ConstantFunction<PsiElement, String>("variableFinalizerRefactor"),
                        energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                        gotoTargets);

                //energyBreakPoints.put(element, ebp);
                result.add(relatedItemLineMarkerInfo);
            }
        }
        Refactorer onProvideAssistDataListenerAPIAnalyzer = new OnProvideAssistDataListenerAPIAnalyzer();
        if (onProvideAssistDataListenerAPIAnalyzer.isUseCase(element)) {


            // PsiElement ref=redundantOperationInLoop.annotateUseCase(element, null);

            EnergyLineMarker energyLineMarker = new EnergyLineMarker(element, onProvideAssistDataListenerAPIAnalyzer);
            // energyLineMarker.doHighlight();
            RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(element, element.getTextRange(), onProvideAssistDataListenerAPIAnalyzer.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                    new ConstantFunction<PsiElement, String>("variableFinalizerRefactor"),
                    energyLineMarker.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) energyLineMarker : null, energyLineMarker.getAlignment(),
                    gotoTargets);

            //energyBreakPoints.put(element, ebp);
            result.add(relatedItemLineMarkerInfo);
        }
    }


    public void collectStateMakers(@NotNull final PsiElement element, Collection<? super RelatedItemLineMarkerInfo> result) {
        {
            if (!(element instanceof PsiMethod))
                return;

            if (AndroidUtilFacade.isAndroidComponentStateMethod((PsiMethod) element) || AndroidUtilFacade.methodLooksLikeImplementsUISomethingSomethingListener((PsiMethod) element)) {

                PsiElement ref = null;
                try {
                    ref = ((PsiMethod) element).getBody().getStatements()[0];//todo fix super. case
                } catch (NullPointerException npe) {
                    return;
                } catch (IndexOutOfBoundsException ioobe) {
                    return;
                }

//            if (ref == null)
//                return;
//            System.out.println("sib " + ref.getText());
                //todo what if method is empty... nothing useful

                EnergyBreakPoint ebp = new EnergyBreakPoint(ref);
                ebp.asStartComponent();
                //  ebp.nonBlockingNavigationHandler();

                RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(ref, ref.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                        new ConstantFunction<PsiElement, String>("Component State Tracing"),
                        ebp.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) ebp : null, ebp.getAlignment(),
                        gotoTargets);

                //energyBreakPoints.put(element, ebp);
                result.add(relatedItemLineMarkerInfo);
            }
        }
    }

    protected void collectNavigationMarkers(@NotNull final PsiElement element, Collection<? super RelatedItemLineMarkerInfo> result) {

//        if(element instanceof PsiMethod){
//            AEONCallGraph.buildCallGraphAndGetPaths(element);
//        }

        if (element instanceof PsiClass|| element instanceof PsiAnonymousClass) {
            int greedycount = 0;
           StringBuffer textgreedy = new StringBuffer();
            GreedyCallsInLoopAnalyzer greedyAnalyzer = new GreedyCallsInLoopAnalyzer();
            greedyAnalyzer.doAnalysis(element);
            Vector<Report> greedyreports = greedyAnalyzer.faultReports;

            for (Report report1 : greedyreports) {
                greedycount++;
                textgreedy.append( "Greedy ICall " + greedycount + ": " + report1.getReportData() + "\n");
            }
            if (greedycount > 0) {

               System.out.println("-----------Report Greedy:\n"+textgreedy.toString()+"\n-----End Report Greedy:\n");


            }

        }
        //todo, change views of markers in options and don't overlap
//        if(element instanceof PsiClass){
////            DaemonCodeAnalyzer.getInstance(element.getProject()).restart(element.getContainingFile());
//            result=new ArrayList<RelatedItemLineMarkerInfo>();
//        }
//        collectHierarchyMakers(element,result);
       collectEnergyLineMakers(element, result);
        if(AEONPanel.Options.isEnergyAnalysisOn) {
            collectEnergyMeasurementMakers(element, result);
        }
        collectStateMakers(element, result);
        PsiElement psiStatement = IntelliJUtilFacade.lookUpForContainingStatement(element);
        if (psiStatement == null) {
            psiStatement = element;
        }
        EnergyBreakPoint ebp = new EnergyBreakPoint(psiStatement);

        boolean addMarker = false;
        if (element instanceof PsiMethodCallExpression) {
            PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) element;
            //  String caller = (psiMethodCallExpression.getMethodExpression().getFirstChild() == null || psiMethodCallExpression.getMethodExpression().getFirstChild().getText().equals("")) ? "this" : psiMethodCallExpression.getMethodExpression().getFirstChild().getText();
            String methodName = psiMethodCallExpression.getMethodExpression().getReferenceName();
            // System.out.println("Method Call Expression: " + psiMethodCallExpression.getText() + " >> arguments: " + psiMethodCallExpression.getArgumentList().getText() + ", method name: " + methodName + ", caller: " + caller + ", returns: " + psiMethodCallExpression.getType().getCanonicalText());

            if (methodName.equals("energyBreakPointStart")) {
                ebp.asStart();
                addMarker = true;
            } else {
                if (methodName.equals("energyBreakPointEnd")) {
                    ebp.asStop();
                    addMarker = true;

                }
            }

        } else {
            if (element instanceof PsiStatement) {
                addMarker = true;

            } else {
                //return;
            }
        }

        for (Object o : result.toArray()) {
            if (o instanceof RelatedItemLineMarkerInfo) {
                RelatedItemLineMarkerInfo relatedItemLineMarkerInfo1 = (RelatedItemLineMarkerInfo) o;
                if (relatedItemLineMarkerInfo1.getElement() instanceof PsiStatement) {
                    if (psiStatement.equals(relatedItemLineMarkerInfo1.getElement()))
                        return;
                }
            }

        }

        if (!addMarker)
            return;

        String tooltip = psiStatement.getText();
        RelatedItemLineMarkerInfo relatedItemLineMarkerInfo = new RelatedItemLineMarkerInfo<PsiElement>(psiStatement, psiStatement.getTextRange(), ebp.getIcon(), Pass.UPDATE_OVERRIDEN_MARKERS,
                tooltip == null ? null : new ConstantFunction<PsiElement, String>(tooltip),
                ebp.isNavigateAction() ? (GutterIconNavigationHandler<PsiElement>) ebp : null, ebp.getAlignment(),
                gotoTargets);

        energyBreakPoints.put(psiStatement, ebp);
        result.add(relatedItemLineMarkerInfo);

    }


}
