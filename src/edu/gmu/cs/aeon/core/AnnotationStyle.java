package edu.gmu.cs.aeon.core;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.editor.markup.TextAttributes;

import java.awt.*;

/**
 * Created by DavidIgnacio on 6/1/2014.
 */
public enum AnnotationStyle {
    CRITICAL(TextAttributesKey.createTextAttributesKey("ENERGY_CRITICAL", new TextAttributes(null, Color.red, null, null, Font.BOLD))),
    MODERATE(TextAttributesKey.createTextAttributesKey("ENERGY_MODERATE", new TextAttributes(null, Color.orange, null, null, Font.BOLD))),
    SUGGESTION(TextAttributesKey.createTextAttributesKey("ENERGY_SUGGESTION", new TextAttributes(null, Color.lightGray, null, null, Font.BOLD)));

    private final TextAttributesKey value;

    private AnnotationStyle(TextAttributesKey annotationStyle) {
        value = annotationStyle;
    }

    public TextAttributesKey getTextAttributesKey() {
        return value;
    }
}
