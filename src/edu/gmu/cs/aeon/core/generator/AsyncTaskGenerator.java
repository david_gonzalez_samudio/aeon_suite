package edu.gmu.cs.aeon.core.generator;

/**
 * Created by DavidIgnacio on 5/5/2014.
 */

import com.intellij.codeInsight.actions.OptimizeImportsProcessor;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import edu.gmu.cs.aeon.core.BaseCodeGenerator;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;

public class AsyncTaskGenerator extends BaseCodeGenerator {

    public static final String COMMAND_NAME = "Generate AsyncTask code";
    public String params = "Void";
    public String progress = "Void";
    public String result = "Void";
    public String body = "return null;";
    public PsiElement toRemove;
    private String clsName;

    public AsyncTaskGenerator(final PsiElement psiElement) {
        super(psiElement, COMMAND_NAME);
    }
// todo extract local variables from loop, add as params tuple to allow smooth migration and result tuple to continue the code in the onResult new method with taskId

    @Override
    public void generate() {
        StringBuilder classStr = new StringBuilder();
        PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(_myPsiElement);
        body = _myPsiElement.getText() + "\nreturn null;";
        clsName = cls.getName();

        classStr.append(String.format("protected %s doInBackground(%s... params) {\n%s\n}\n",
                result,
                params,
                body));

        //create
        PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(prj);
        final PsiClass on = elementFactory.createClassFromText(classStr.toString(), null);
        final PsiClass viewHolderClass = elementFactory.createClassFromText(classStr.toString(), null);
        viewHolderClass.setName(String.format("My%sTask", clsName));
        PsiReferenceList extendsList = viewHolderClass.getExtendsList();

        extendsList.add(elementFactory.createReferenceFromText(String.format("android.os.AsyncTask<%s, %s, %s> ", params, progress, result), cls));
        PsiModifierList modifierList = viewHolderClass.getModifierList();
        assert modifierList != null : "no modifiers";
        modifierList.setModifierProperty(PsiModifier.PRIVATE, true);
        //add and reformat
        PsiElement newClass = IntelliJUtilFacade.lookUpForContainingClass(_myPsiElement).add(viewHolderClass);
        JavaCodeStyleManager.getInstance(prj).shortenClassReferences(newClass);
        new OptimizeImportsProcessor(prj, _myPsiElement.getContainingFile())
                .runWithoutProgress();

//        //todo rename inplace
//        RefactoringFactory.getInstance(prj)
//                .createRename(newClass, null, true, false)
//                .run();
        PsiCodeFragment p;
        toRemove = _myPsiElement;
//        List<PsiVariable> list=RefactoringUtil.collectReferencedVariables(_myPsiElement);
//        System.out.println("----------- VAR REF INIT");
//        for (PsiVariable variable:list){
//            if((variable.hasInitializer()&&variable instanceof PsiLocalVariable)){
//                if(!variable.getInitializer().getContext().equals(_myPsiElement.getContext()))
//                    System.out.println(variable.getType() + " " + variable.getName()+"  init "+ variable.getInitializer());}
//
//
//        }
//        System.out.println("----------- VAR REF END");
        if (_myPsiElement != null && toRemove != null)
            toRemove.replace(JavaPsiFacade.getInstance(_myPsiElement.getProject()).getElementFactory().createStatementFromText(String.format("new My%sTask().execute((%s) %s); // Moved to My%sTask inner class ", clsName, params, "null", clsName), cls));
        toRemove = null;
    }

    @Override
    public void unGenerate() {

    }


}