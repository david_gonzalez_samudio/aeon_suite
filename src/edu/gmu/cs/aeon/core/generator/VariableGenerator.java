package edu.gmu.cs.aeon.core.generator;

/**
 * Created by DavidIgnacio on 5/5/2014.
 */

import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;
import edu.gmu.cs.aeon.core.BaseCodeGenerator;
import edu.gmu.cs.aeon.core.analysis.smells.VariableFinalizerRefactor;

public class VariableGenerator extends BaseCodeGenerator {

    public static final String COMMAND_NAME = "Generate AsyncTask code";
    public PsiElement toRemove;
    PsiType type;
    PsiElement where;
    String name;
    PsiExpression value;

    public VariableGenerator(PsiType type, PsiElement where, String name, PsiExpression value) {
        super(where, COMMAND_NAME);
        this.type = type;
        this.where = where;
        this.name = name;
        this.value = value;
    }


// todo extract local variables from loop, add as params tuple to allow smooth migration and result tuple to continue the code in the onResult new method with taskId

    @Override
    public void generate() {

        //create
        PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(where.getProject());
        PsiDeclarationStatement psiDeclarationStatement = elementFactory.createVariableDeclarationStatement(name, type, value);
        for (PsiElement psiElement : psiDeclarationStatement.getDeclaredElements()) {
            new VariableFinalizerRefactor().refactorElement(psiElement);
        }

//        newClass= where.getPrevSibling().add(psiDeclarationStatement);
//        newClass= newClass.getParent().addAfter(EBEpsiStatement, where);


        where.getParent().addBefore(psiDeclarationStatement, where).add(JavaPsiFacade.getInstance(_myPsiElement.getProject()).getElementFactory().createCommentFromText(String.format("// Moved  %s call out of the loop to local variable %s ", value.getText(), name), _myPsiElement));

        // JavaCodeStyleManager.getInstance(prj).shortenClassReferences(newClass);

//        new OptimizeImportsProcessor(prj, cls.getContainingFile())
//                .runWithoutProgress();

        //todo rename inplace

        PsiVariable replacement = (PsiVariable) psiDeclarationStatement.getDeclaredElements()[0];
        PsiUtil.setModifierProperty(replacement, PsiModifier.FINAL, false);
        if (value != null && replacement != null)
            value.replace(replacement.getNameIdentifier());

        toRemove = null;


    }

    @Override
    public void unGenerate() {

    }

}