package edu.gmu.cs.aeon.core.generator;

/**
 * Created by DavidIgnacio on 5/5/2014.
 */

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiReference;
import edu.gmu.cs.aeon.core.BaseCodeGenerator;

public class DecapsulationGenerator extends BaseCodeGenerator {

    public static final String COMMAND_NAME = "Generate AsyncTask code";
    public PsiElement toRemove;
    PsiReference toReplace;
    PsiExpression replacement;

    public DecapsulationGenerator(PsiClass cls, PsiReference toReplace, PsiExpression replacement) {
        super(cls, COMMAND_NAME);
        this.toReplace = toReplace;
        this.replacement = replacement;

    }
// todo extract local variables from loop, add as params tuple to allow smooth migration and result tuple to continue the code in the onResult new method with taskId

    @Override
    public void generate() {

        //create
        //   PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(prj);

        //    PsiUtil.setModifierProperty(replacement, PsiModifier.FINAL, false);
        if (toReplace != null && replacement != null)
            toReplace.getElement().getParent().replace(replacement);

        toRemove = null;
    }

    @Override
    public void unGenerate() {

    }

}