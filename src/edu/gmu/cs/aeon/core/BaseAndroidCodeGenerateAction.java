package edu.gmu.cs.aeon.core;


import com.intellij.codeInsight.CodeInsightActionHandler;
import com.intellij.codeInsight.generation.actions.BaseGenerateAction;
import com.intellij.psi.PsiAnonymousClass;
import com.intellij.psi.PsiClass;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import org.jetbrains.annotations.NotNull;

/**
 * User: anatoly
 * Date: 09.07.13
 * Time: 16:09
 * src/ru/korniltsev/intellij/android/generate/BaseAndroidGenerateCodeAction.java
 */
public abstract class BaseAndroidCodeGenerateAction extends BaseGenerateAction {
    public BaseAndroidCodeGenerateAction(final CodeInsightActionHandler handler) {
        super(handler);
    }

    @Override
    protected boolean isValidForClass(@NotNull final PsiClass targetClass) {
        boolean isValidForAndroid = AndroidUtilFacade.findSdk() != null
                && !(targetClass instanceof PsiAnonymousClass);
        return super.isValidForClass(targetClass)
                && isValidForAndroid;
    }
}