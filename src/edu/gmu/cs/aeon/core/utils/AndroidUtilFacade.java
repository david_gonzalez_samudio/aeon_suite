package edu.gmu.cs.aeon.core.utils;

/**
 * Created by DavidIgnacio on 5/5/2014.
 * from src/ru/korniltsev/intellij/android/utils/MyAndroidUtils.java
 */

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.projectRoots.ProjectJdkTable;
import com.intellij.openapi.projectRoots.Sdk;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.EverythingGlobalScope;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.ArrayUtil;
import edu.gmu.cs.aeon.core.analysis.report.AnalysisCategory;
import edu.gmu.cs.aeon.core.analysis.report.Fault;
import edu.gmu.cs.aeon.core.analysis.report.PredictionType;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import org.jetbrains.android.dom.layout.AndroidLayoutUtil;
import org.jetbrains.android.dom.manifest.*;
import org.jetbrains.android.facet.AndroidFacet;
import org.jetbrains.android.facet.AndroidRootUtil;
import org.jetbrains.android.util.AndroidUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

//import com.android.tools.idea.gradle.IdeaAndroidProject;

public class AndroidUtilFacade {
    public static final HashMap<String,  HashMap<MethodCase,Set<String>>> androidComponentStateMethods;
    private static final ArrayList<UnifiedStateMachineRow> componentsUnifiedStateMachine;
    private static final Map<String, Set<String>> resourceReleasingCalls;
    private static final Map<String, Set<String>> resourceAcquiringCalls;
    public static boolean IS_PREPARED = false;
    //    private static boolean mapComponentClassesAndResourceMethods(@NotNull final Project p) {
//        if(method2ClassShouldReleaseResource!=null){
//            return false;
//        }
//        method2ClassShouldReleaseResource=new HashMap<String, PsiClass[]>();
//        method2ClassShouldAcquireResource=new HashMap<String, PsiClass[]>();
//
//        PsiClass[] result1 = findActivityClasses(p);
//
//        PsiClass[] result2 = findAppWidgetProviderClasses(p);
//        PsiClass[] result3 = findDreamServiceClasses(p);
//        PsiClass[] result4 = findContentProviderClasses(p);
//        PsiClass[] result5 = findServiceClasses(p);
//        PsiClass[] result6 = findBroadcastReceiverClasses(p);
//
//        method2ClassShouldReleaseResource.put("onPause",result1);
//        method2ClassShouldReleaseResource.put("onDestroy", ArrayUtil.mergeArrays(result3, result5));
//        method2ClassShouldReleaseResource.put("onHandleIntent",result5);
//        PsiClass[] classes = new PsiClass[result1.length + result2.length + result3.length + result4.length+result5.length+result6.length];
//        int index = 0;
//        for (PsiClass psiClass : result1) {
//            classes[index++] = psiClass;
//        }
//        for (PsiClass psiClass : result2) {
//            classes[index++] = psiClass;
//        }
//        for (PsiClass psiClass : result3) {
//            classes[index++] = psiClass;
//        }
//        for (PsiClass psiClass : result4) {
//            classes[index++] = psiClass;
//        }
//        for (PsiClass psiClass : result5) {
//            classes[index++] = psiClass;
//        }
//        for (PsiClass psiClass : result6) {
//            classes[index++] = psiClass;
//        }
//
//    return true;
//    }
//    public static String mapResourceDefiningCall2ComponentStatesResource="/data/";
//    public static String mapResourceAcquiringCall2ComponentStatesResource="data/";
//    public static String mapResourceReleasingCall2ComponentStatesResource="data/";
//    private static Map<String, Set<String>> mapResourceDefiningCall2ComponentStates;
//    private static Map<String, Set<String>> mapResourceAcquiringCall2ComponentStates;
//    private static Map<String, Set<String>> mapResourceReleasingCall2ComponentStates;
    public static String mapAndroidCallbacksResource = "/data/AndroidCallbacks.txt";
    public static Map<String, Set<Report>> expectedResults;
    public static Map<PsiClass, Set<PsiMethod>> androidCallbacksMap;
    public static Set<String> methodCallIgnoreList;
    //    static {
//        new AndroidUtilFacade().mapAllResourcesCalls2ComponentsStates();
//    }
//    private  void mapAllResourcesCalls2ComponentsStates(){
//            mapResourceDefiningCall2ComponentStates=new HashMap<String, Set<String>>();
//            mapResourcesCalls2ComponentsStates(mapResourceDefiningCall2ComponentStatesResource, mapResourceDefiningCall2ComponentStates);
//    }
//
//    private void mapResourcesCalls2ComponentsStates(String resource, Map<String, Set<String>> mapResourceReleasingCall2ComponentStates) {//
//
//        ClassLoader classLoader = getClass().getClassLoader();
//
//        Map<String, java.util.List<Double[]>> tmp = new HashMap<String, java.util.List<Double[]>>();
//        try {
//            File file = new File(classLoader.getResource(resource).getFile());
//            BufferedReader reader = new BufferedReader(new FileReader(file));
//
//            String line = reader.readLine();
//            while ((line = reader.readLine()) != null) {
//                String[] apiVal = line.split(",");
//                String api = apiVal[0];
//
//            }
//            reader.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
    private static String expectedResultsResource = "/expectedResults/expectedResults.csv";
    private static Set<PsiClass> androidThreadClassesAndInterfaces;

    static{
        componentsUnifiedStateMachine=new ArrayList<UnifiedStateMachineRow>();
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Activity.onCreate", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Activity.onStart", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Activity.onRestart", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Activity.onResume", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Activity.onPause", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Activity.onStop", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Activity.onDestroy", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));

        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Service.onCreate", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Service.onStart", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Service.onStartCommand", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Service.onBind", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.Service.onDestroy", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));

        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onCreate", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onStart", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onStartCommand", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onBind", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onDestroy", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onAttachedToWindow", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onDreamingStarted", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onDreamingStopped", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.service.dreams.DreamService.onDetachedFromWindow", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));


        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.IntentService.onCreate", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.IntentService.onStart", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.IntentService.onStartCommand", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.IntentService.onBind", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.IntentService.onDestroy", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.IntentService.onHandleIntent", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.app.IntentService.onHandleIntent", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));


        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.content.ContentProvider.onCreate", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.content.ContentProvider.onTrimMemory", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.content.ContentProvider.onTrimMemory", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.content.ContentProvider.shutdown", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));


        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.content.BroadcastReceiver.onReceive", UnifiedStateMachineRow.ComponentsUnifiedState.ACTIVE));
        componentsUnifiedStateMachine.add(new UnifiedStateMachineRow("android.content.BroadcastReceiver.onReceive", UnifiedStateMachineRow.ComponentsUnifiedState.IDLE));

    }

    static {
        androidComponentStateMethods = new HashMap<String, HashMap<MethodCase, Set<String>>>();// if not here the reformat will NPE
        loadActivitiesData();
        loadServiceData();
        loadDreamServiceData();
        loadBroadCastReceiverData();

        resourceReleasingCalls = new HashMap<String, Set<String>>();
        mapResourceReleasingCalls();
        resourceAcquiringCalls = new HashMap<String, Set<String>>();
        mapResourceAcquiringCalls();

//        androidEventDrivenMethods = new HashMap<String, Set<String>>();
//
//
//
//
//        androidEventDrivenMethods.put("android.view.GestureDetector.OnDoubleTapListener",
//                new HashSet<String>(){{add("onSingleTapConfirmed");add("onDoubleTap");add("onDoubleTapEvent");}});
//        androidEventDrivenMethods.put("android.view.GestureDetector.OnGestureListener",
//                new HashSet<String>(){{add("onDown");add("onFling");add("onLongPress");add("onScroll");add("onShowPress");add("onSingleTapUp")}});
//        androidEventDrivenMethods.put("android.view.MenuItem.OnMenuItemClickListener",
//                new HashSet<String>(){{add("onMenuItemClick");}});
//        androidEventDrivenMethods.put("android.view.View.OnKeyListener",
//                new HashSet<String>(){{add("onKey");}});
//        androidEventDrivenMethods.put("android.view.View.OnClickListener",
//                new HashSet<String>(){{add("onClick");}});
//        androidEventDrivenMethods.put("android.view.View.OnLongClickListener",
//                new HashSet<String>(){{add("onLongClick");}});
//        androidEventDrivenMethods.put("android.view.View.OnGenericMotionListener",
//                new HashSet<String>(){{add("onGenericMotion");}});
//        androidEventDrivenMethods.put("android.view.View.OnDragListener",
//                new HashSet<String>(){{add("onDrag");}});
//        androidEventDrivenMethods.put("android.view.View.OnHoverListener",
//                new HashSet<String>(){{add("onHover");}});
//        androidEventDrivenMethods.put("android.view.View.OnDragListener",
//                new HashSet<String>(){{add("onDrag");}});
//        androidEventDrivenMethods.put("",
//                new HashSet<String>(){{add("");}});
//        androidEventDrivenMethods.put("",
//                new HashSet<String>(){{add("");}});
//        androidEventDrivenMethods.put("",
//                new HashSet<String>(){{add("");}});

    }

    static {
        methodCallIgnoreList = new HashSet<String>();
        methodCallIgnoreList.add("android.util.Log.d");
        methodCallIgnoreList.add("android.util.Log.e");
        methodCallIgnoreList.add("android.util.Log.i");
        methodCallIgnoreList.add("android.util.Log.getStackTraceString");
        methodCallIgnoreList.add("android.util.Log.isLoggable");
        methodCallIgnoreList.add("android.util.Log.println");
        methodCallIgnoreList.add("android.util.Log.v");
        methodCallIgnoreList.add("android.util.Log.wtf");
        methodCallIgnoreList.add("android.os.PowerManager.WakeLock.isHeld");
        methodCallIgnoreList.add("android.os.PowerManager.newWakeLock");
//        methodCallIgnoreList.add("java.io.PrintStream.print");
//        methodCallIgnoreList.add("java.io.PrintStream.println");
    }

    @NotNull
    public static Set<UnifiedStateMachineRow.ComponentsUnifiedState> getUnifiedStateMachineStates(@Nullable String qualifiedComponentNameAndCallback){
        Set<UnifiedStateMachineRow.ComponentsUnifiedState> states=new HashSet<UnifiedStateMachineRow.ComponentsUnifiedState>();
        for (UnifiedStateMachineRow row:componentsUnifiedStateMachine){
            if(row.qualifiedComponentNameAndCallback.equals(qualifiedComponentNameAndCallback)){
                states.add(row.unifiedState);
            }
        }
        return states;
    }

    @NotNull
    public static Set<String> getUnifiedStateMachineActivityMethods(@NotNull Set<UnifiedStateMachineRow.ComponentsUnifiedState> componentsUnifiedStates){
        Set<String> activityMethods=new HashSet<String>();
        for (UnifiedStateMachineRow row:componentsUnifiedStateMachine){
            for(UnifiedStateMachineRow.ComponentsUnifiedState componentsUnifiedState:componentsUnifiedStates){
//                System.out.println("ACT "+row.unifiedState.name()+" "+row.qualifiedComponentNameAndCallback+" in "+componentsUnifiedState.name());
                if(row.unifiedState==componentsUnifiedState &&row.qualifiedComponentNameAndCallback.startsWith("android.app.Activity")){
                                    System.out.println("ACT "+row.unifiedState.name()+" "+row.qualifiedComponentNameAndCallback+" in "+componentsUnifiedState.name());

                    activityMethods.add(row.qualifiedComponentNameAndCallback);
                }
            }

        }
        return activityMethods;
    }

    @NotNull
    public static Set<String> translateComponentMethodToActivityMethod(@Nullable PsiMethod psiMethod){
        Set<String> allComponentMethods=new HashSet<String>();
        if(psiMethod==null){
            return allComponentMethods;
        }
        PsiClass containingClass=psiMethod.getContainingClass();
        if(containingClass==null){
            return allComponentMethods;
        }
        containingClass=getAndroidComponentClass(containingClass);
        if(containingClass==null){
            return allComponentMethods;
        }
      //  System.out.println("TRANSL " + containingClass.getQualifiedName() + "." + psiMethod.getName());

        Set<UnifiedStateMachineRow.ComponentsUnifiedState> componentStates=getUnifiedStateMachineStates(containingClass.getQualifiedName() + "." + psiMethod.getName());


         allComponentMethods=getUnifiedStateMachineActivityMethods(componentStates);

//        if(allComponentMethods.contains(psiMethod.getName())){
//            return true
//        }
        return allComponentMethods;
    }

    private static void mapResourceReleasingCalls(){
        HashSet<String> onPause=new HashSet<String>();
        onPause.add("onPause");
        HashSet<String> onPauseOnStop=new HashSet<String>();
        onPauseOnStop.add("onPause");
        onPauseOnStop.add("onStop");
        HashSet<String> onDestroy=new HashSet<String>();
        onDestroy.add("onDestroy");
        resourceReleasingCalls.put("android.media.AudioManager.abandonAudioFocus", onPause);
        resourceReleasingCalls.put("android.media.AudioRecord.release", onPauseOnStop);
        resourceReleasingCalls.put("android.media.MediaPlayer.release", onPauseOnStop);
        resourceReleasingCalls.put("android.media.MediaPlayer.stop", onPauseOnStop);
        resourceReleasingCalls.put("android.hardware.Camera.unlock", onPause);
        resourceReleasingCalls.put("android.hardware.Camera.release", onPause);
        resourceReleasingCalls.put("android.hardware.Camera.stopFaceDetection", onPause);
        resourceReleasingCalls.put("android.hardware.Camera.stopPreview", onPause);
        resourceReleasingCalls.put("android.hardware.SensorManager.unregisterListener", onPause);
//       handled differently, needs context sensitivity, wakelock too
//        resourceReleasingCalls.put("android.location.LocationManager.removeUpdates", onPause);
        resourceReleasingCalls.put("android.os.Vibrator.cancel", onDestroy);
        resourceReleasingCalls.put("android.net.wifi.WifiManager.disableNetwork", onDestroy);
        resourceReleasingCalls.put("android.support.v4.content.WakefulBroadcastReceiver.completeWakefulIntent", onPause);//startWakefulService

    }

    public static boolean isResourceReleasingCallInExpectedActivityLifeCycleCallback(String methodCall, String rootMethodName){
        Set<String> activityLifeCycleCallback=resourceReleasingCalls.get(methodCall);
        return activityLifeCycleCallback != null && activityLifeCycleCallback.contains(rootMethodName);
    }

    public static boolean isResourceReleasingCall(String methodCall){
        return resourceReleasingCalls.containsKey(methodCall);
    }

    public static boolean isResourceAcquiringCall(String methodCall){
        return resourceAcquiringCalls.containsKey(methodCall);
    }

    public static boolean isResourceAcquiringCallInExpectedActivityLifeCycleCallback(String methodCall, String rootMethodName){
        Set<String> activityLifeCycleCallback=resourceAcquiringCalls.get(methodCall);
        return activityLifeCycleCallback != null && activityLifeCycleCallback.contains(rootMethodName);
    }

    private static void mapResourceAcquiringCalls(){

        HashSet<String> onResume=new HashSet<String>();
        onResume.add("onResume");
        HashSet<String> onCreateOnResume=new HashSet<String>();
        onCreateOnResume.add("onCreate");
        onCreateOnResume.add("onResume");
        HashSet<String> onCreate=new HashSet<String>();
        onCreate.add("onCreate");
        resourceAcquiringCalls.put("android.media.AudioManager.requestAudioFocus", onResume);
        resourceAcquiringCalls.put("android.media.AudioRecord.AudioRecord", onResume);
        resourceAcquiringCalls.put("android.media.MediaPlayer.MediaPlayer", onCreate);
        resourceAcquiringCalls.put("android.media.MediaPlayer.create", onCreate);
        resourceAcquiringCalls.put("android.media.MediaPlayer.start", onResume);
        resourceAcquiringCalls.put("android.hardware.Camera.lock", onResume);
        resourceAcquiringCalls.put("android.hardware.Camera.open", onResume);
        resourceAcquiringCalls.put("android.hardware.Camera.startFaceDetection", onResume);
        resourceAcquiringCalls.put("android.hardware.Camera.startPreview", onResume);
        resourceAcquiringCalls.put("android.hardware.SensorManager.registerListener", onResume);
//       handled differently, needs context sensitivity, wakelock too
//        resourceReleasingCalls.put("android.location.LocationManager.removeUpdates", onPause);
       // resourceAcquiringCalls.put("android.os.Vibrator.vibrate", onCreateOnResume);
        resourceAcquiringCalls.put("android.net.wifi.WifiManager.enableNetwork", onCreateOnResume);
        resourceAcquiringCalls.put("android.support.v4.content.WakefulBroadcastReceiver.startWakefulIntent", onCreateOnResume);//startWakefulService

    }
//    private static void loadData(){
//
//    }
//    private static void loadData(){
//
//    }
//    private static void loadData(){

    private static void loadActivitiesData(){
        String[] qualifiedNameForActivities = getActivityClassesQualifiedNames();


        HashMap<MethodCase,Set<String>> activityData=new HashMap<MethodCase, Set<String>>();
        Set<String> activityStateMethodsAll = new HashSet<String>();
        activityStateMethodsAll.add("onCreate");
        activityStateMethodsAll.add("onStart");
        activityStateMethodsAll.add("onRestart");
        activityStateMethodsAll.add("onResume");
        activityStateMethodsAll.add("onPause");
        activityStateMethodsAll.add("onStop");
        activityStateMethodsAll.add("onDestroy");
        activityData.put(MethodCase.ALL, activityStateMethodsAll);
        Set<String> activityStateMethodsWakelockRelease = new HashSet<String>();
        activityStateMethodsWakelockRelease.add("onPause");
        activityData.put(MethodCase.RESOURCE_RELEASE, activityStateMethodsWakelockRelease);
       // activityData.put(MethodCase.LOCATION_REMOVE, activityStateMethodsWakelockRelease);
        Set<String> activityStateMethodsWakelockAcquire = new HashSet<String>();
        activityStateMethodsWakelockAcquire.add("onResume");
        activityData.put(MethodCase.RESOURCE_ACQUIRE, activityStateMethodsWakelockAcquire);
      //  activityData.put(MethodCase.LOCATION_REQUEST, activityStateMethodsWakelockAcquire);


        for (String qualifiedNameForActivity : qualifiedNameForActivities) {
            androidComponentStateMethods.put(qualifiedNameForActivity, activityData);
        }
    }

    private static void loadServiceData(){
        HashMap<MethodCase,Set<String>> serviceData=new HashMap<MethodCase, Set<String>>();

        String qualifiedNameForService = "android.app.Service";
        Set<String> serviceStateMethodsAll = new HashSet<String>();
        serviceStateMethodsAll.add("onCreate");
        serviceStateMethodsAll.add("onStartCommand");
        serviceStateMethodsAll.add("onDestroy");
        serviceData.put(MethodCase.ALL, serviceStateMethodsAll);
        Set<String> serviceStateMethodsA = new HashSet<String>();

        serviceStateMethodsA.add("onStartCommand");


        serviceData.put(MethodCase.RESOURCE_ACQUIRE, serviceStateMethodsA);
       // serviceData.put(MethodCase.LOCATION_REQUEST, serviceStateMethodsA);
        Set<String> serviceStateMethodsR = new HashSet<String>();

        serviceStateMethodsR.add("onDestroy");

        serviceData.put(MethodCase.RESOURCE_RELEASE, serviceStateMethodsR);
      //  serviceData.put(MethodCase.LOCATION_REMOVE, serviceStateMethodsR);

        androidComponentStateMethods.put(qualifiedNameForService, serviceData);

    }

    public static boolean methodLooksLikeImplementsUISomethingSomethingListener(final PsiMethod psiMethod){
        return (psiMethod.getName().startsWith("on")&&classLooksLikeImplementsUISomethingSomethingListener(psiMethod.getContainingClass()));
    }

    public static boolean methodLooksLikeImplementsSomethingSomethingListener(final PsiMethod psiMethod){
        return (psiMethod.getName().startsWith("on")&&classLooksLikeImplementsSomethingSomethingListener(psiMethod.getContainingClass()));
    }

    public static boolean classLooksLikeImplementsUISomethingSomethingListener(final PsiClass cls) {
        try {
            PsiClassType[] implementsListTypes = cls.getImplementsListTypes();
            for (PsiClassType implementsListType : implementsListTypes) {
                PsiClass resolved = implementsListType.resolve();
                if (resolved != null) {
                    if (resolved.getQualifiedName().startsWith("android.view.")) {
                        return true;
                    } else {
                        PsiClassType[] extendsListTypes = resolved.getExtendsListTypes();
                        for (PsiClassType extendsListType : extendsListTypes) {
                            PsiClass resolved2 = extendsListType.resolve();
                            if (resolved2.getQualifiedName().startsWith("android.view.")) {
                                return true;
                            }
                        }

                    }
                }
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public static boolean classLooksLikeImplementsSomethingSomethingListener(final PsiClass cls) {
        try {
            PsiClassType[] implementsListTypes = cls.getImplementsListTypes();
            for (PsiClassType implementsListType : implementsListTypes) {
                PsiClass resolved = implementsListType.resolve();
                if (resolved != null) {
                    if (resolved.getQualifiedName().endsWith("Listener")) {
                        return true;
                    } else {
                        PsiClassType[] extendsListTypes = resolved.getExtendsListTypes();
                        for (PsiClassType extendsListType : extendsListTypes) {
                            PsiClass resolved2 = extendsListType.resolve();
                            if (resolved2.getQualifiedName().endsWith("Listener")) {
                                return true;
                            }
                        }

                    }
                }
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    private static void loadBroadCastReceiverData(){
        String qualifiedNameForBroadCastReceiver = " android.content.BroadcastReceiver";
        Set<String> broadcastReceiverStateMethods = new HashSet<String>();
        broadcastReceiverStateMethods.add("onReceive");

        HashMap<MethodCase,Set<String>> bcData=new HashMap<MethodCase, Set<String>>();
        bcData.put(MethodCase.ALL, broadcastReceiverStateMethods);
        bcData.put(MethodCase.RESOURCE_RELEASE, broadcastReceiverStateMethods);
        bcData.put(MethodCase.RESOURCE_ACQUIRE, broadcastReceiverStateMethods);
      //  bcData.put(MethodCase.LOCATION_REMOVE, broadcastReceiverStateMethods);
      //  bcData.put(MethodCase.LOCATION_REQUEST, broadcastReceiverStateMethods);

        androidComponentStateMethods.put(qualifiedNameForBroadCastReceiver, bcData);

        String qualifiedNameForContentProvider = " android.content.ContentProvider";
        Set<String> contentProviderStateMethods = new HashSet<String>();
        contentProviderStateMethods.add("onCreate");
        HashMap<MethodCase,Set<String>> cpData=new HashMap<MethodCase, Set<String>>();
        cpData.put(MethodCase.ALL, broadcastReceiverStateMethods);
        cpData.put(MethodCase.RESOURCE_RELEASE, contentProviderStateMethods);
        cpData.put(MethodCase.RESOURCE_ACQUIRE, contentProviderStateMethods);
       // cpData.put(MethodCase.LOCATION_REMOVE, contentProviderStateMethods);
       // cpData.put(MethodCase.LOCATION_REQUEST, contentProviderStateMethods);
        androidComponentStateMethods.put(qualifiedNameForContentProvider, cpData);

    }

//    }
    private static void loadDreamServiceData(){
        HashMap<MethodCase,Set<String>> serviceData=new HashMap<MethodCase, Set<String>>();
        String qualifiedNameForService = "android.service.dreams.DreamService";
        Set<String> serviceStateMethodsAll = new HashSet<String>();
        serviceStateMethodsAll.add("onDreamingStarted");
        serviceStateMethodsAll.add("onDreamingStopped");
        serviceStateMethodsAll.add("onDestroy");
        serviceData.put(MethodCase.ALL, serviceStateMethodsAll);
        Set<String> serviceStateMethodsA = new HashSet<String>();

        serviceStateMethodsA.add("onDreamingStarted");


        serviceData.put(MethodCase.RESOURCE_ACQUIRE, serviceStateMethodsA);
      //  serviceData.put(MethodCase.LOCATION_REQUEST, serviceStateMethodsA);
        Set<String> serviceStateMethodsR = new HashSet<String>();

        serviceStateMethodsR.add("onDreamingStopped");
        serviceData.put(MethodCase.RESOURCE_RELEASE, serviceStateMethodsR);
       // serviceData.put(MethodCase.LOCATION_REMOVE, serviceStateMethodsR);

        androidComponentStateMethods.put(qualifiedNameForService, serviceData);
    }

    @Nullable
    public static Sdk findSdk() {
        Sdk[] allJdks = ProjectJdkTable.getInstance().getAllJdks();
        for (Sdk s : allJdks) {
            if (s.getSdkType().getName().toLowerCase().contains("android")) {
                return s;
            }
        }
        return null;
    }

    //static   Set<String> componentReleaseRecommended;
//    static{
//        componentReleaseRecommended=new HashSet<String>();
//        componentReleaseRecommended.add("onPause");
//        componentReleaseRecommended.add("onDayDreamStopped");
//        componentReleaseRecommended.add("onPause");
//        componentReleaseRecommended.add("onPause");
//        componentReleaseRecommended.add("onPause");
//        componentReleaseRecommended.add("onPause");
//        componentReleaseRecommended.add("onPause");
//    }
    public static boolean isMethodCaseOfAndroidComponent(PsiMethod psiMethod, MethodCase methodCase){
        PsiClass component=getAndroidComponentClass(psiMethod.getContainingClass());
        if(component!=null){
            HashMap<MethodCase, Set<String>> methods=androidComponentStateMethods.get(component.getQualifiedName());
            if(methods!=null){
                Set<String> finalMethods=methods.get(methodCase);
                if(finalMethods!=null&&finalMethods.contains(psiMethod.getName())){
                        return true;
                }
            }
       }
        return false;

    }

    public static boolean isClassSubclassOfAdapter(final PsiClass cls) {
        PsiClass adapterClass = findAdapterClass(cls.getProject());
        return !(adapterClass == null || !cls.isInheritor(adapterClass, true));
    }

    private static PsiClass findAdapterClass(final Project project) {
        final PsiClass adapterClass;
        JavaPsiFacade facade = JavaPsiFacade.getInstance(project);
        adapterClass = facade.findClass("android.widget.Adapter", new EverythingGlobalScope(project));
        return adapterClass;
    }

    public static boolean implementsParcelable(final PsiClass cls) {
        PsiClassType[] implementsListTypes = cls.getImplementsListTypes();
        for (PsiClassType implementsListType : implementsListTypes) {
            PsiClass resolved = implementsListType.resolve();
            if (resolved != null && "android.os.Parcelable".equals(resolved.getQualifiedName())) {
                return true;
            }
        }
        return false;
    }

    public static PsiClass findParcelableClass(@NotNull final Project p) {
        return JavaPsiFacade.getInstance(p)
                .findClass("android.os.Parcelable", new EverythingGlobalScope(p));
    }

    public static boolean implementsViewOnClickListener(@NotNull final PsiClass cls) {
        PsiClassType[] implementsListTypes = cls.getImplementsListTypes();
        for (PsiClassType implementsListType : implementsListTypes) {
            PsiClass resolved = implementsListType.resolve();
            if (resolved != null && "android.view.View.OnClickListener".equals(resolved.getQualifiedName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean implementsLocationListener(@NotNull final PsiClass cls) {
        PsiClassType[] implementsListTypes = cls.getImplementsListTypes();
        for (PsiClassType implementsListType : implementsListTypes) {
            PsiClass resolved = implementsListType.resolve();
            if (resolved != null && "android.location.LocationListener".equals(resolved.getQualifiedName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean implementsOnProvideAssistDataListener(@NotNull final PsiClass cls) {
        PsiClassType[] implementsListTypes = cls.getImplementsListTypes();
        for (PsiClassType implementsListType : implementsListTypes) {
            PsiClass resolved = implementsListType.resolve();
            if (resolved != null && "android.app.Application.OnProvideAssistDataListener".equals(resolved.getQualifiedName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isClassSubclassOfActivity(final PsiClass cls) {
        PsiClass[] activityClasses = findActivityClasses(cls.getProject());
        if (activityClasses == null) {
            return false;
        } else {
            for (PsiClass activityClass : activityClasses) {
                if (cls.isInheritor(activityClass, true)) {
                    return true;
                }
            }
            return false;
        }
    }

    //todo include fragemnt
    public static boolean extendsActivity(final PsiClass cls) {
        PsiClassType[] extendsListTypes = cls.getExtendsListTypes();
        for (PsiClassType extendsListType : extendsListTypes) {
            PsiClass resolved = extendsListType.resolve();
            if (resolved != null && ("android.app.Activity".equals(resolved.getQualifiedName()) || "android.support.v4.app.FragmentActivity".equals(resolved.getQualifiedName()) || "android.support.v7.app.ActionBarActivity".equals(resolved.getQualifiedName()))) {
                return true;
            }
        }
        return false;
    }

    public static boolean extendsNativeActivity(final PsiClass cls) {
        PsiClassType[] extendsListTypes = cls.getExtendsListTypes();
        for (PsiClassType extendsListType : extendsListTypes) {
            PsiClass resolved = extendsListType.resolve();
            if (resolved != null && ("android.app.NativeActivity".equals(resolved.getQualifiedName()))) {
                return true;
            }
        }
        return false;
    }
//   private static  HashMap<String, PsiClass[]> method2ClassShouldReleaseResource;
//
//    private static  HashMap<String, PsiClass[]> method2ClassShouldAcquireResource;
//
//    public static boolean isMethodCaseOfAndroidComponentShouldReleaseResource(PsiMethod psiMethod){
//        PsiClass component=getAndroidComponentClass(psiMethod.getContainingClass());
//        if(component!=null){
//            HashMap<MethodCase, Set<String>> methods=androidComponentStateMethods.get(component.getQualifiedName());
//            if(methods!=null){
//                Set<String> finalMethods=methods.get(methodCase);
//                if(finalMethods!=null&&finalMethods.contains(psiMethod.getName())){
//                    return true;
//                }
//            }
//        }
//        return false;
//
//    }

    public static String[] getActivityClassesQualifiedNames() {
        return new String[]{"android.app.Activity", "android.support.v4.app.FragmentActivity", "android.support.v7.app.ActionBarActivity","android.app.Fragment"};
    }

    public static
    @Nullable
    PsiFile getLayoutXMLFileFromCaret(@NotNull Editor e, @NotNull PsiFile f) {
        int offset = e.getCaretModel().getOffset();
        PsiElement candidate1 = f.findElementAt(offset);
        PsiElement candidate2 = f.findElementAt(offset - 1);

        PsiFile ret = findXmlResource(candidate1);
        if (ret != null) {
            return ret;
        }
        return findXmlResource(candidate2);
    }

    private static
    @Nullable
    PsiFile findXmlResource(PsiElement elementAt) {
        if (elementAt == null) {
            return null;
        }
        if (!(elementAt instanceof PsiIdentifier)) {
            return null;
        }
        PsiElement rLayout = elementAt.getParent().getFirstChild();
        if (rLayout == null) {
            return null;
        }
        if (!"R.layout".equals(rLayout.getText())) {
            return null;
        }
        Project prj = elementAt.getProject();
        String name = String.format("%s.xml", elementAt.getText());
        PsiFile[] foundFiles = FilenameIndex
                .getFilesByName(prj, name, new EverythingGlobalScope(prj));
        if (foundFiles.length <= 0) {
            return null;
        }
        return foundFiles[0];
    }

    public static
    @NotNull
    List<AndroidView> getIDsFromXML(@NotNull PsiFile f) {
        final ArrayList<AndroidView> ret = new ArrayList<AndroidView>();
        f.accept(new XmlRecursiveElementVisitor() {
            @Override
            public void visitElement(final PsiElement element) {
                super.visitElement(element);
                if (element instanceof XmlTag) {
                    XmlTag t = (XmlTag) element;
                    XmlAttribute id = t.getAttribute("android:id", null);
                    if (id == null) {
                        return;
                    }
                    final String val = id.getValue();
                    if (val == null) {
                        return;
                    }
                    ret.add(new AndroidView(val, t.getName()));

                }

            }
        });
        return ret;
    }

    public static boolean isClassSubclassOfComponent(PsiClass cls) {
        if (cls == null)
            return false;
        PsiClass[] activityClasses = findComponentClasses(cls.getProject());
        if (activityClasses == null) {
            return false;
        } else {
            for (PsiClass activityClass : activityClasses) {
                if (cls.isInheritor(activityClass, true)) {
                    return true;
                }
            }
            return false;
        }
    }

    @Nullable
    public static PsiClass  getAndroidComponentClass(PsiClass cls) {
        if (cls == null)
            return null;
        PsiClass[] activityClasses = findComponentClasses(cls.getProject());
        if (activityClasses == null) {
            return null;
        } else {
            for (PsiClass activityClass : activityClasses) {
                if (cls.isInheritor(activityClass, true)) {
                    return activityClass;
                }
            }
            return null;
        }
    }

    public static boolean isAndroidCallbackMethod(final PsiMethod psiMethod){
        PsiClass resolvedClass=getAndroidCallbackClass(psiMethod.getContainingClass());
        if(resolvedClass!=null){
            for(PsiMethod resolvedClassMethod:androidCallbacksMap.get(resolvedClass)) {
                if (psiMethod.getName().equals(resolvedClassMethod.getName())) {
                    return true;
                }
            }
        }
        return false;
    }
    public static PsiClass getClassIfIsAndroidCallbackMethod(final PsiMethod psiMethod){
        PsiClass resolvedClass=getAndroidCallbackClass(psiMethod.getContainingClass());
        if(resolvedClass!=null){
            for(PsiMethod resolvedClassMethod:androidCallbacksMap.get(resolvedClass)) {
                if (psiMethod.getName().equals(resolvedClassMethod.getName())) {
                    return resolvedClass;
                }
            }
        }
        return null;
    }
    public static boolean isAndroidCallbackClass(final PsiClass cls) {
        try {
            PsiClassType[] implementsListTypes = cls.getImplementsListTypes();
            for (PsiClassType implementsListType : implementsListTypes) {
                PsiClass resolved = implementsListType.resolve();
                if (resolved != null) {
                    if (androidCallbacksMap.containsKey(resolved)) {
                        return true;
                    } else {
                        PsiClassType[] extendsListTypes = resolved.getExtendsListTypes();
                        for (PsiClassType extendsListType : extendsListTypes) {
                            PsiClass resolved2 = extendsListType.resolve();
                            if (androidCallbacksMap.containsKey(resolved2)) {
                                return true;
                            }
                        }

                    }
                }
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }
    public static boolean areClassRelated(final PsiClass cls, final Set<PsiClass> toCompareWith) {


        try {

            for (PsiClass compareWith : toCompareWith) {

                if (cls.isInheritor(compareWith, true)) {
                    return true;
                }
            }

            PsiClassType[] relatedListTypes = ArrayUtil.mergeArrays(cls.getImplementsListTypes(), cls.getExtendsListTypes());
            for (PsiClassType relatedListType : relatedListTypes) {
                PsiClass resolved = relatedListType.resolve();
                if (resolved != null) {
                    if (toCompareWith.contains(resolved)) {
                        return true;
                    } else {
                        for (PsiClass compareWith : toCompareWith) {

                            if (resolved.isInheritor(compareWith, true)) {
                                return true;
                            }
                        }

                    }
                }
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }
    public static PsiClass getClassRelated(final PsiClass cls, final Set<PsiClass> toCompareWith) {


        try {

            for (PsiClass compareWith : toCompareWith) {

                if (cls.isInheritor(compareWith, true)) {
                    return compareWith;
                }
            }

            PsiClassType[] relatedListTypes = ArrayUtil.mergeArrays(cls.getImplementsListTypes(), cls.getExtendsListTypes());
            for (PsiClassType relatedListType : relatedListTypes) {
                PsiClass resolved = relatedListType.resolve();
                if (resolved != null) {
                    if (toCompareWith.contains(resolved)) {
                        return resolved;
                    } else {
                        for (PsiClass compareWith : toCompareWith) {

                            if (resolved.isInheritor(compareWith, true)) {
                                return compareWith;
                            }
                        }

                    }
                }
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }
    public static PsiClass getAndroidCallbackClass(final PsiClass cls) {
        try {
            PsiClassType[] implementsListTypes = cls.getImplementsListTypes();
            for (PsiClassType implementsListType : implementsListTypes) {
                PsiClass resolved = implementsListType.resolve();
                if (resolved != null) {
                    if (androidCallbacksMap.containsKey(resolved)) {
                        return resolved;
                    } else {
                        PsiClassType[] extendsListTypes = resolved.getExtendsListTypes();
                        for (PsiClassType extendsListType : extendsListTypes) {
                            PsiClass resolved2 = extendsListType.resolve();
                            if (androidCallbacksMap.containsKey(resolved2)) {
                                return resolved2;
                            }
                        }

                    }
                }
            }
        }catch (Exception e){

        }
        return null;
    }

    //beware of default
    private static PsiClass findClassInProject(Project p,String qualifiedClassName) {

        return JavaPsiFacade.getInstance(p)
                .findClass(qualifiedClassName, new EverythingGlobalScope(p));
    }


    private static PsiClass[] findComponentClasses(@NotNull final Project p) {
        PsiClass[] result1 = findActivityClasses(p);
        PsiClass[] result2 = findAppWidgetProviderClasses(p);
        PsiClass[] result3 = findDreamServiceClasses(p);
        PsiClass[] result4 = findContentProviderClasses(p);
        PsiClass[] result5 = findServiceClasses(p);
        PsiClass[] result6 = findBroadcastReceiverClasses(p);


        PsiClass[] classes = new PsiClass[result1.length + result2.length + result3.length + result4.length+result5.length+result6.length];
        int index = 0;
        for (PsiClass psiClass : result1) {
            classes[index++] = psiClass;
        }
        for (PsiClass psiClass : result2) {
            classes[index++] = psiClass;
        }
        for (PsiClass psiClass : result3) {
            classes[index++] = psiClass;
        }
        for (PsiClass psiClass : result4) {
            classes[index++] = psiClass;
        }
        for (PsiClass psiClass : result5) {
            classes[index++] = psiClass;
        }
        for (PsiClass psiClass : result6) {
            classes[index++] = psiClass;
        }

        return classes;
    }

    public static boolean isMethodCaseOfAndroidThreadClass(PsiMethod psiMethod){
        try {
            PsiClass component = getClassRelated(psiMethod.getContainingClass(), androidThreadClassesAndInterfaces);
            if (component != null) {
                PsiMethod[] componentMethods = ArrayUtil.mergeArrays(component.getMethods(), component.getConstructors());
                if (componentMethods != null) {
                    for(PsiMethod componentMethod:componentMethods) {
                        if (psiMethod.getName().equals(componentMethod.getName())) {
                            return true;
                        }
                    }
                }
            }
        }catch (Exception e){
            return false;
        }
        return false;

    }

    public static void prepareThreadClasses(@NotNull final Project p) {

        PsiClass[] result1 = JavaPsiFacade.getInstance(p)
                .findClasses("java.lang.Thread", new EverythingGlobalScope(p));


        PsiClass[] result2 = JavaPsiFacade.getInstance(p)
                .findClasses("java.lang.Runnable", new EverythingGlobalScope(p));

        PsiClass[] result3 = JavaPsiFacade.getInstance(p)
                .findClasses("android.os.AsyncTask", new EverythingGlobalScope(p));

        PsiClass[] result4 = JavaPsiFacade.getInstance(p)
                .findClasses("java.util.TimerTask", new EverythingGlobalScope(p));

        androidThreadClassesAndInterfaces = new HashSet<PsiClass>();

        for (PsiClass psiClass : result1) {
            androidThreadClassesAndInterfaces.add(psiClass);
        }
        for (PsiClass psiClass : result2) {
            androidThreadClassesAndInterfaces.add(psiClass);
        }
        for (PsiClass psiClass : result3) {
            androidThreadClassesAndInterfaces.add(psiClass);
        }
        for (PsiClass psiClass : result4) {
            androidThreadClassesAndInterfaces.add(psiClass);
        }


    }

    private static PsiClass[] findActivityClasses(@NotNull final Project p) {
        PsiClass[] result1 = JavaPsiFacade.getInstance(p)
                .findClasses("android.app.Activity", new EverythingGlobalScope(p));
        int result1Length = result1.length;

        PsiClass[] result2 = JavaPsiFacade.getInstance(p)
                .findClasses("android.support.v4.app.FragmentActivity", new EverythingGlobalScope(p));
        int result2Length = result2.length;
        PsiClass[] result3 = JavaPsiFacade.getInstance(p)
                .findClasses("android.support.v7.app.ActionBarActivity", new EverythingGlobalScope(p));
        int result3Length = result3.length;
        PsiClass[] result4 = JavaPsiFacade.getInstance(p)
                .findClasses("android.app.Fragment", new EverythingGlobalScope(p));
        int result4Length = result4.length;
        PsiClass[] classes = new PsiClass[result1Length + result2Length + result3Length+ result4Length];
        int index = 0;
        for (PsiClass psiClass : result1) {
            classes[index++] = psiClass;
        }
        for (PsiClass psiClass : result2) {
            classes[index++] = psiClass;
        }
        for (PsiClass psiClass : result3) {
            classes[index++] = psiClass;
        }
        for (PsiClass psiClass : result4) {
            classes[index++] = psiClass;
        }

        return classes;
    }

    private static PsiClass[] findAppWidgetProviderClasses(@NotNull final Project p) {
        return JavaPsiFacade.getInstance(p)
                .findClasses("android.appwidget.AppWidgetProvider", new EverythingGlobalScope(p));
    }

    private static PsiClass[] findContentProviderClasses(@NotNull final Project p) {
        return JavaPsiFacade.getInstance(p)
                .findClasses("android.content.ContentProvider", new EverythingGlobalScope(p));
    }

    private static PsiClass[] findDreamServiceClasses(@NotNull final Project p) {
        return JavaPsiFacade.getInstance(p)
                .findClasses("android.service.dreams.DreamService", new EverythingGlobalScope(p));
    }

    private static PsiClass[] findServiceClasses(@NotNull final Project p) {
        return JavaPsiFacade.getInstance(p)
                .findClasses("android.app.Service", new EverythingGlobalScope(p));
    }

    private static PsiClass[] findBroadcastReceiverClasses(@NotNull final Project p) {

        return JavaPsiFacade.getInstance(p)
                .findClasses("android.content.BroadcastReceiver", new EverythingGlobalScope(p));

    }

    public static boolean isAndroidComponentStateMethod(PsiMethod element) {
        if (element.getContainingClass() == null) {
            return false;
        }
        PsiClass component = getAndroidComponentClass(element.getContainingClass());
        return component != null;
    }

//    public static boolean isAndroidEventDrivenMethod(PsiMethod element) {
//        PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(element);
//        if (AndroidUtilFacade.implementsViewOnClickListener(cls)) {
//            String nameM = "";
//            PsiMethod psiMethod = null;
//
//            psiMethod = element;
//            nameM = element.getName();
////            System.out.println(nameM);
//
//
//            for (Set<String> methods : androidEventDrivenMethods.values()) {
//                if (methods.contains(nameM)) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }

    public static boolean methodCallInIgnoreList(PsiMethodCallExpression psiMethodCallExpression){
        String className = IntelliJUtilFacade.getMethodCallExpressionClassName(psiMethodCallExpression);
        String methodName = IntelliJUtilFacade.getMethodCallExpressionMethodName(psiMethodCallExpression);
        return methodCallIgnoreList.contains(className+"."+methodName);
    }

    public static Vector<Manifest> getManifests(final Project project){
        final Vector<Manifest> manifests=new Vector<Manifest>();
        for(Module module:ModuleManager.getInstance(project).getModules()) {
            final AndroidFacet facet = AndroidFacet.getInstance(module);
            if(facet!=null) {
                final VirtualFile manifestFile = AndroidRootUtil.getPrimaryManifestFile(facet);
                if(manifestFile!=null) {
                   final  Manifest manifest = AndroidUtils.loadDomElement(facet.getModule(), manifestFile, Manifest.class);
                    if(manifest!=null){
                        manifests.add(manifest);
                    }

                }
            }
        }
        return manifests;
    }

    @Nullable
    public static AndroidFacet  getAndroidFacet(final Project project){

        for(Module module:ModuleManager.getInstance(project).getSortedModules()) {
            return AndroidFacet.getInstance(module);
        }
        return null;
    }

    @NotNull
    public static List<AndroidFacet> getAndroidFacets(final @Nullable Project project){
        return  AndroidUtils.getApplicationFacets(project);
    }

    @Nullable
    public static Manifest getManifest(final Project project){

        for(Module module:ModuleManager.getInstance(project).getSortedModules()) {
            final AndroidFacet facet = AndroidFacet.getInstance(module);

                if(facet!=null) {
                    final VirtualFile manifestFile = AndroidRootUtil.getPrimaryManifestFile(facet);
                    if(manifestFile!=null) {
                        return AndroidUtils.loadDomElement(facet.getModule(), manifestFile, Manifest.class);
                    }
                }
            }
        return null;
    }

    @NotNull
    public static Map<String, ComponentType> mapComponents2TypesFromManifest(final @Nullable Manifest manifest){
        //todo, add thi on ebp generation
        //<editor-fold defaultstate="collapsed"> desc="data"

        Map<String, ComponentType> component2typeMap=new HashMap<String, ComponentType>();
       //</editor-fold>
        if(manifest==null) {
            return component2typeMap;
        }
        if(manifest.getPackage()==null){
            component2typeMap.put(manifest.getPackage().getStringValue(), ComponentType.PACKAGE);
        }
        for (Activity activity:manifest.getApplication().getActivities()){
            if(activity.getParentActivityName()==null){
                // todo, add activity hierarchy
                    component2typeMap.put(activity.getActivityClass().getStringValue(), ComponentType.MAIN_ACTIVITY);
            }else{
                component2typeMap.put(activity.getActivityClass().getStringValue(), ComponentType.MAIN_ACTIVITY);
            }
        }
        for (Service service:manifest.getApplication().getServices()){
            if(service.getServiceClass()!=null){
                component2typeMap.put(service.getServiceClass().getStringValue(), ComponentType.SERVICE);
            }
        }
        for (Receiver receiver:manifest.getApplication().getReceivers()){
            if(receiver.getReceiverClass()!=null){
                component2typeMap.put(receiver.getReceiverClass().getStringValue(), ComponentType.BROADCAST_RECEIVER);
            }
        }
        for (Provider provider:manifest.getApplication().getProviders()){
            if(provider.getProviderClass()!=null){
                component2typeMap.put(provider.getProviderClass().getStringValue(), ComponentType.CONTENT_PROVIDER);
            }
        }
        return  component2typeMap;
    }

    public static Map<String, ComponentType> getComponentsFromManifestOfProject(final Project project){
        return mapComponents2TypesFromManifest(getManifest(project));
    }

    @NotNull
    public static List<String> getPossibleLayoutRootElements(final Project project){
        if(project==null) {
            return new ArrayList<String>();
        }
        for(Module module:ModuleManager.getInstance(project).getModules()) {
            final AndroidFacet facet = AndroidFacet.getInstance(module);
            if(facet!=null) {
                 List<String> layoutsElements=AndroidLayoutUtil.getPossibleRoots(facet);
                if(layoutsElements!=null) {
                    return layoutsElements;
                }
            }
        }

        return new ArrayList<String>();
    }

    public void mapExpectedResults(final Project p) {
        expectedResults = new HashMap<String, Set<Report>>();

        ClassLoader classLoader = getClass().getClassLoader();

        try {
            File file = new File(classLoader.getResource(expectedResultsResource).getFile());
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line = null;
            int ignoreCount = 0;
            while ((line = reader.readLine()) != null && ignoreCount++ < 10) {
                // ignoring boilerplate data in file
            }
            while ((line = reader.readLine()) != null) {
                final String[] rowLine = line.split(",");


                if (rowLine.length > 6) {
                    final String appName = rowLine[0];
                    final String analysisCategory = rowLine[1];
                    final String predictionType = rowLine[2];
                    final String faultLevel = rowLine[3];
                    final String faultSolution = rowLine[4];
                    final String analysisReason = rowLine[5];
                    final String analysisDescription = rowLine[6];
                    final Report report = new Report(AnalysisCategory.valueOf(analysisCategory), PredictionType.valueOf(predictionType), null, null, analysisReason, analysisDescription, true);
                    report.faultLevel = Fault.Level.valueOf(faultLevel);
                    report.faultSolution = Fault.Solution.valueOf(faultSolution);
                    Set<Report> appReports = expectedResults.get(appName);
                    if (appReports == null) {
                        appReports = new HashSet<Report>();
                    }
                    appReports.add(report);
                    expectedResults.put(appName, appReports);
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void mapAndroidCallbacks(final Project p) {
        androidCallbacksMap = new HashMap<PsiClass, Set<PsiMethod>>();

        ClassLoader classLoader = getClass().getClassLoader();

        try {
            File file = new File(classLoader.getResource(mapAndroidCallbacksResource).getFile());
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line = null;
            while ((line = reader.readLine()) != null) {
                final String fixedLine = line.replace("$", ".");
                //  System.out.println("LINE "+fixedLine);
                PsiClass foundClass = findClassInProject(p, fixedLine);

                if (foundClass != null) {
                    HashSet<PsiMethod> foundClassMethods = new HashSet<PsiMethod>();
                    for (PsiMethod foundClassMethod : foundClass.getMethods()) {
                        //  System.out.println("AC>>>>>>>>>> "+foundClass.getQualifiedName()+"  "+foundClassMethod.getName());
                        foundClassMethods.add(foundClassMethod);
                    }

                    androidCallbacksMap.put(foundClass, foundClassMethods);
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //    public static HashMap<String, Set<String>> androidEventDrivenMethods;
    public enum MethodCase {
        ALL, RESOURCE_RELEASE, RESOURCE_ACQUIRE, RESOURCE_CREATION,
        //simplified component state machine
//    INITIATING, VISIBLE, NOT_VISIBLE, FINALIZING
    }

    public enum ComponentType {
        PACKAGE,
        MAIN_ACTIVITY,
        ACTIVITY,
        SERVICE,
        BROADCAST_RECEIVER,
        CONTENT_PROVIDER
    }
}