package edu.gmu.cs.aeon.core.utils;

/**
 * Created by DavidIgnacio on 7/10/2014.
 */
/*
 *  Copyright (c) 2002 Sabre, Inc. All rights reserved.
 */


import com.intellij.ide.plugins.PluginManager;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.extensions.PluginDescriptor;
import com.intellij.openapi.extensions.PluginId;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.TextEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.psi.util.PsiUtil;
import edu.gmu.cs.aeon.core.analysis.report.AnalysisCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Vector;


public class PluginPsiUtil {
    Key<AnalysisCategory> faultCategoryKey = new Key<AnalysisCategory>("Greedy");

    @Nullable
    private static VirtualFile getVirtualFile(Project project, PsiElement psiElement) {
        if (psiElement == null || !psiElement.isValid() || psiElement.getContainingFile() == null) {
            return null;
        }
        return psiElement.getContainingFile().getVirtualFile();
    }

    public static void search(PsiElement element) {
        ReferencesSearch.search(element).findAll().toArray();
    }

    @Nullable
    public static PsiFile getContainingFile(PsiElement psiElement) {
        if (psiElement == null || !psiElement.isValid()) {
            return null;
        }

        return psiElement.getContainingFile();
    }

    public static boolean isElementInSelectedFile(Project project, PsiElement psiElement) {
        VirtualFile elementFile = getVirtualFile(project, psiElement);
        if (elementFile == null) {
            return false;
        }

        VirtualFile[] currentEditedFiles = FileEditorManager.getInstance(project).getSelectedFiles();

        for (VirtualFile file : currentEditedFiles) {
            if (elementFile.equals(file)) {
                return true;
            }
        }
        return false;
    }



    @Nullable
    public static Editor getEditorIfSelected(Project project, PsiElement psiElement) {
        VirtualFile elementFile = getVirtualFile(project, psiElement);
        if (elementFile == null) {
            return null;
        }

        FileEditor fileEditor = FileEditorManager.getInstance(project).getSelectedEditor(elementFile);

        Editor editor = null;

        if (fileEditor != null && fileEditor instanceof TextEditor) {
            editor = ((TextEditor) fileEditor).getEditor();
        }

        return editor;
    }

    @Nullable
    public static Vector<PsiFile> getProjectPsiFiles(final Project project) {
        final Vector<PsiFile> files = new Vector<PsiFile>();

        for (final VirtualFile virtualFile : FilenameIndex.getAllFilesByExt(project, "java")) {
            ApplicationManager.getApplication().runReadAction(new Runnable() {
                @Override
                public void run() {
                    files.add(PsiManager.getInstance(project).findFile(virtualFile));
                }
            });

        }
        return files;
    }

    @NotNull
    public static Project createProject(@NotNull String projectName, @NotNull String projectFilePath) throws NullPointerException {
        Project newProject = ProjectManager.getInstance().createProject(projectName, projectFilePath);
        if (newProject == null) {
            throw new NullPointerException("Failed to create a new IDEA project " + projectName + " in path " + projectFilePath);
        }
        return newProject;
    }

    public static boolean isProjectOpen(@NotNull String projectName) {
        for (Project project : ProjectManager.getInstance().getOpenProjects()) {
            if (project.getName().equals(projectName)) {
                return true;
            }
        }
        return false;
    }

//    public static HashMap<String, HashMap<String, String>> applicationsBatchLoad(String rootFolder) {
//        HashMap<String, HashMap<String, String>> appMap = new HashMap<String, HashMap<String, String>>();
//
//        File rootFolderFile = new File(rootFolder);
//        File[] appFolders = null;
//        if (rootFolderFile.isDirectory()) {
//            appFolders = rootFolderFile.listFiles(new FileFilter() {
//                @Override
//                public boolean accept(File pathname) {
//                    if (pathname.isDirectory()) {
////                        if(pathname.canRead()) {
////                            System.out.println("Cannot read Directory " + pathname.getAbsolutePath());
//                        return true;
////                        }
//                    }
//                    return false;
//                }
//            });
//        }
//
////        for(File appfolder:appFolders){
////            String projectName=appfolder.getName();
////            String projectFilePath=appfolder.getAbsolutePath();
////            System.out.println("APP NAME "+projectName+" PATH "+projectFilePath);
////            if(!isProjectOpen(projectName)) {
////                createProject(projectName, projectFilePath);
////                try {
////                    ProjectManager.getInstance().loadAndOpenProject(projectName);
////                } catch (IOException e) {
////                    e.printStackTrace();
////                } catch (JDOMException e) {
////                    e.printStackTrace();
////                } catch (InvalidDataException e) {
////                    e.printStackTrace();
////                }
////            }
////
//////            processPsiFiles(getProjectPsiFiles(projectName));
////
//
////        }
//        if (appFolders == null) {
//            System.out.println("Cannot find Directory " + rootFolder);
//            return appMap;
//        }
//
//        for (File appfolder : appFolders) {
//            String projectName = appfolder.getName();
//            String projectFilePath = appfolder.getAbsolutePath();
//
//
//            System.out.println("APP NAME " + projectName + " PATH " + projectFilePath);
//            File[] outputFolderList = appfolder.listFiles(new FileFilter() {
//                @Override
//                public boolean accept(File pathname) {
//                    if (pathname.isDirectory() && pathname.getName().equals("output")) {
//                        return true;
//                    }
//                    return false;
//                }
//            });
//            for (File outputFolder : outputFolderList) {
//                File[] srcFolderList = outputFolder.listFiles(new FileFilter() {
//                    @Override
//                    public boolean accept(File pathname) {
//                        if (pathname.isDirectory() && pathname.getName().equals("src")) {
//                            return true;
//                        }
//                        return false;
//                    }
//                });
//                String srcFilename = ProjectManager.getInstance().getOpenProjects()[0].getBasePath() + File.separator + "src";
//                File srcFileProject = new File(srcFilename);
//                // if(srcFileProject.exists() && srcFileProject.canWrite()){
//                for (File srcFolder : srcFolderList) {
//
//                    try {
//
////                                System.out.println("copying from "+ srcFolder.toPath()+" to "+ srcFilename);
//                        HashMap<String, String> map = new HashMap<String, String>();
//
//                        copyFolder(srcFolder, srcFileProject, map);
//                        appMap.put(projectName, map);
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
////                        }else{
////                            System.out.println("fail opening " + srcFilename);
////                        }
//            }
//
//        }
//        return appMap;
//
//    }



    //    public static String applicationsBatchAnalysis(HashMap<String, HashMap<String, String>> appMap, Project project,  com.intellij.openapi.progress.ProgressIndicator progressIndicator){
//
//        for(Project project:ProjectManager.getInstance().getOpenProjects()) {
//            System.out.println("PRJ NAME "+project.getName());
//            return processPsiFiles(getProjectPsiFiles(project), appMap, project, progressIndicator);
//        }
//        return "";
//    }

    public static boolean addOnTopOfFile(String path, String fileName, String data) {
        File output = new File(path, fileName);
        System.out.println("Adding data on top of " + output.getAbsolutePath());
        if (!output.exists()) {
            try {
                output.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        try {
            RandomAccessFile f = new RandomAccessFile(output, "rw");
            f.seek(0);
            f.write(data.getBytes());
            f.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean checkIfAndroidPluginIsInstalled() {
        // System.out.println("PLUGIN IDS");
        PluginId pluginId = null;
        for (PluginDescriptor pluginDescriptor : PluginManager.getPlugins()) {
            if (pluginDescriptor.getPluginId().getIdString().equals("org.jetbrains.android")) {
                pluginId = pluginDescriptor.getPluginId();
            }
            // System.out.println(pluginDescriptor.toString() + " === " + pluginDescriptor.getPluginId().toString());
        }
        if (pluginId == null) {
            return false;
        }
        // PluginManager.getPlugin(pluginId);

        return true;

    }

    public double sScore(PsiElement node, double sScore) {
        double currentScore = 1;
        for (PsiElement edge : node.getChildren()) {
            if (edge.getUserData(faultCategoryKey).equals(AnalysisCategory.LOCATION)) {
//                if(edge.toNode.energyValue == null){
//                    sScore = sScore(edge.toNode, sScore);
//                }
//                else{
//                    sScore = sScore(edge.toNode, sScore)+(edge.toNode.energyValue*edge.toNode.getRScore());
//                }
                sScore(edge, 1);

            }
        }
        return sScore;
    }

    public void doRefactoring() {
//        JavaRefactoringFactory factory = JavaRefactoringFactory.getInstance(project);
//        JavaRenameRefactoring rename = factory.createRename(psiElement, renameString);
//        UsageInfo[] usages = rename.findUsages();
//
////alter the 'usages' array, remove all the usages that you don't want to refactor
//
//        rename.doRefactoring(usages); // modified 'usages' array
    }





}
