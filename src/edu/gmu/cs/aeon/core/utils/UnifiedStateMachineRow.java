package edu.gmu.cs.aeon.core.utils;

/**
 * Created by DavidIgnacio on 5/3/2015.
 */
public class UnifiedStateMachineRow{
    String qualifiedComponentNameAndCallback;
    ComponentsUnifiedState unifiedState;

    public UnifiedStateMachineRow(String qualifiedComponentNameAndCallback, ComponentsUnifiedState unifiedState) {
        this.qualifiedComponentNameAndCallback = qualifiedComponentNameAndCallback;
        this.unifiedState = unifiedState;
    }
    public  enum ComponentsUnifiedState{
        INIT, ACTIVE, IDLE, DONE
    }
}