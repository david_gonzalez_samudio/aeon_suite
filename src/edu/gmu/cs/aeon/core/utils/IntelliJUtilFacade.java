package edu.gmu.cs.aeon.core.utils;

import com.intellij.codeInsight.actions.OptimizeImportsProcessor;
import com.intellij.execution.Location;
import com.intellij.execution.testframework.AbstractTestProxy;
import com.intellij.lang.Language;
import com.intellij.openapi.application.AccessToken;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.impl.PsiClassImplUtil;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.PsiShortNamesCache;
import com.intellij.psi.search.searches.ClassInheritorsSearch;
import com.intellij.psi.search.searches.OverridingMethodsSearch;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.psi.util.PsiMethodUtil;
import com.intellij.psi.util.PsiSuperMethodUtil;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.psi.util.PsiUtil;
import com.intellij.refactoring.RefactoringFactory;
import com.intellij.refactoring.listeners.RefactoringEventData;
import com.intellij.refactoring.listeners.RefactoringEventListener;
import com.intellij.util.FileContentUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 5/3/2014.
 * Quick access for the PSI cookbook http://confluence.jetbrains.com/display/IDEADEV/PSI+Cookbook
 */
public class IntelliJUtilFacade {
    public static PsiStatement lookForFirstStatementInMethod(PsiMethod element) {
        PsiElement myElement = element.getBody().getFirstBodyElement();
        while (myElement != null) {
            if (myElement instanceof PsiStatement)
                return (PsiStatement) myElement;
            myElement = myElement.getNextSibling();
        }
        return null;
    }

    /**
     * How do I find a class by qualified name?
     *
     * @param project            context
     * @param classQualifiedName
     * @return
     */
    public static PsiClass findClassInProject(Project project, String classQualifiedName) {
        return (JavaPsiFacade.getInstance(project)).findClass(classQualifiedName, GlobalSearchScope.allScope(project));
    }

    /**
     * How do I find a class by short name?
     *
     * @param project   context
     * @param className
     * @return
     */
    public static PsiClass[] findClassesInProject(Project project, String className) {
        return PsiShortNamesCache.getInstance(project).getClassesByName(className, GlobalSearchScope.allScope(project));
    }

    @Nullable
    public static PsiClass getClass(Project project, String qualifiedClassName) {
        JavaPsiFacade psiFacade = JavaPsiFacade.getInstance(project);
        return psiFacade.findClass(qualifiedClassName, GlobalSearchScope.allScope(project));
    }

    public static boolean isInstanceOf(PsiClass instance, PsiClass interfaceExtendsClass) {

        String className = interfaceExtendsClass.getQualifiedName();
        if (className == null) {
            return true;
        }

        if (className.equals(instance.getQualifiedName())) {
            return true;
        }

        for (PsiClassType psiClassType : PsiClassImplUtil.getExtendsListTypes(instance)) {
            PsiClass resolve = psiClassType.resolve();
            if (resolve != null) {
                if (className.equals(resolve.getQualifiedName())) {
                    return true;
                }
            }
        }

        for (PsiClass psiInterface : PsiClassImplUtil.getInterfaces(instance)) {
            if (className.equals(psiInterface.getQualifiedName())) {
                return true;
            }
        }

        return false;
    }

    public static PsiMethod lookUpForContainingMethod(final PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            //String value = (String) myElement.getText();
            //System.out.println(myElement.getClass() + " LookUp--> " + (value == null ? "" : value));
            if (myElement instanceof PsiMethod)
                return (PsiMethod) myElement;
            myElement = myElement.getParent();

        }
        return null;
    }

    public static PsiStatement lookUpForContainingStatement(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiStatement)
                return (PsiStatement) myElement;
            myElement = myElement.getParent();
        }
        return null;
    }

    public static PsiLoopStatement lookUpForContainingLoop(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiLoopStatement)
                return (PsiLoopStatement) myElement;
            myElement = myElement.getParent();
        }
        return null;
    }

    public static PsiIfStatement lookUpForContainingIfStatement(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiIfStatement)
                return (PsiIfStatement) myElement;
            myElement = myElement.getParent();
        }
        return null;
    }

    public static PsiTryStatement lookUpForContainingTryStatement(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiTryStatement)
                return (PsiTryStatement) myElement;
            myElement = myElement.getParent();
        }
        return null;
    }
    public static PsiElement lookUpForContainingTryOrIfOrSwitchOrLoopStatement(final PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiTryStatement||myElement instanceof PsiIfStatement||myElement instanceof PsiSwitchStatement||myElement instanceof PsiLoopStatement){
                return myElement;
            }
            myElement = myElement.getParent();
        }
        return null;
    }

    public static boolean instanceOf(Object object, Class... classes) {
        if(classes != null) {
            Class[] arr$ = classes;
            int len$ = classes.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                Class c = arr$[i$];
                if(c.isInstance(object)) {
                    return true;
                }
            }
        }

        return false;
    }
    public static boolean isPsiElementInSourceRootsIntelliJ(@Nullable final PsiElement element){
        AccessToken readAccessToken=null;
        if(IS_SOURCES_ROOT_SCOPE) {
            try {
                readAccessToken = ApplicationManager.getApplication().acquireReadActionLock();
                if(ProjectRootManager.getInstance(element.getProject()).getFileIndex().getSourceRootForFile(element.getContainingFile().getVirtualFile())!=null){
                    return true;
                }else {
                    return false;
                }

            }catch (Exception e){
                return false;
            }
            finally {
                if(readAccessToken!=null){
                    readAccessToken.finish();
                }

            }
        }
        return false;
    }
    private static String sourceRootPathForCopying=null;
    public static boolean isPsiElementInSourceRoots(@Nullable final PsiElement element){
        if(sourceRootPathForCopying==null){
            sourceRootPathForCopying= getSourceRootPathForCopying(element.getProject());
        }
        try {
            if (element.getContainingFile().getVirtualFile().getCanonicalPath().startsWith(sourceRootPathForCopying)) {
                return true;
            }
        }catch(Exception e){

        }
        return false;
    }
    @Nullable
    public static String getSourceRootPathForCopying(@Nullable final Project project){
                if(project==null){
                    return null;
                }

                String androidPath=null;
                String pluginPath=null;// mostly any another project
        AccessToken readAccessToken=null;
        try {
           readAccessToken = ApplicationManager.getApplication().acquireReadActionLock();
            for (VirtualFile virtualFile : ProjectRootManager.getInstance(project).getContentSourceRoots()) {
                final String path = virtualFile.getCanonicalPath();
                if (path.indexOf("java") > path.indexOf("main") && path.indexOf("main") > path.indexOf("src")) {
                    androidPath=path;
                } else {
                    if (path.endsWith("src")) {
                        pluginPath=path;
                    }
                }

            }

        }finally {
            if(readAccessToken!=null){
                readAccessToken.finish();
            }

        }
        if(androidPath!=null){
            return androidPath;
        }
        return pluginPath;
    }
    public static final boolean IS_SOURCES_ROOT_SCOPE=true;

    public static boolean isInFinallyBlock(PsiElement element) {
        PsiElement myElement = element;
        boolean inTry = false;
        PsiCodeBlock finallyBlock = null;
        while (myElement != null) {
            if (myElement instanceof PsiTryStatement) {
                inTry = true;
                finallyBlock = ((PsiTryStatement) myElement).getFinallyBlock();
                myElement = null;
            } else {
                myElement = myElement.getParent();
            }
        }
        if (inTry) {
            System.out.print(" in try");
            PsiCodeBlock psiCodeBlock = lookUpForContainingCodeBlock(element);

            if (psiCodeBlock != null && psiCodeBlock.equals(finallyBlock)) {
                System.out.print(", in block finally");
                return true;
            }
//            if(psiCodeBlock!=null){
//                System.out.print(", in block");
//                PsiElement finallyKeyword=psiCodeBlock.getPrevSibling();
//                System.out.print(", with l-sibling "+finallyKeyword.getText());
//                if(finallyKeyword!=null && finallyKeyword instanceof PsiKeyword){
//
//                    if(((PsiKeyword)finallyKeyword).getText().equals("finally")){
//                        return true;
//                    }
//
//                }
//            }
        }
        return false;
    }

    public static String getPsiLiteralExpressionValue(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiLiteralExpression) {
                Object obj = ((PsiLiteralExpression) myElement).getValue();
                if (obj instanceof String) {
                    return (String) obj;
                }
            }
            myElement = myElement.getParent();
        }
        return null;

    }

    public static String getResolvedClassQualifiedName(@NotNull PsiElement element) {
        String qualifierClassQualifiedName = "";

        try {
            if(element instanceof PsiMethodCallExpression){
                PsiMethodCallExpression psiMethodCallExpression=(PsiMethodCallExpression)element;
               // if(psiMethodCallExpression.resolveMethod()!=null){
                    PsiType psiType= psiMethodCallExpression.resolveMethod().getReturnType();
                   // if()
                qualifierClassQualifiedName = PsiUtil.resolveClassInType(psiType).getQualifiedName();
                //}
            }else{
                qualifierClassQualifiedName = PsiUtil.resolveClassInType(PsiUtil.getTypeByPsiElement(element.getReference().resolve())).getQualifiedName();
            }


        } catch (Exception npe) {

        }
        if(qualifierClassQualifiedName!=null){
        return qualifierClassQualifiedName;
        }else{
            return "";
        }
    }

    public static boolean isNullKeyword(@NotNull PsiElement element) {
        if (element instanceof PsiLiteralExpression && element.getText().equals("null")) {
            return true;
        } else {
            return false;
        }
    }


    //    public static String getPsiLiteralExpressionValueFromStringLiteral(PsiElement element){
//        PsiElement myElement = element;
//        while (myElement != null) {
//            if (myElement instanceof PsiLiteralExpression) {
//                PsiLiteralExpression psiLiteralExpression= ((PsiLiteralExpression) myElement);
//                if(psiLiteralExpression.getType().equals("") {
//                    return (String) psiLiteralExpression.getValue();
//                }
//            }
//            myElement = myElement.getParent();
//        }
//        return null;
//
//    }

    public static PsiExpression getDeParenthesizedPsiExpression(PsiExpression psiExpressionIn) {
        PsiExpression psiExpression=psiExpressionIn;
        while(psiExpression instanceof PsiParenthesizedExpression){
            psiExpression=((PsiParenthesizedExpression)psiExpression).getExpression();
        }
        return psiExpression;
    }
    public static PsiClass lookUpForContainingClass(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiClass)
                return (PsiClass) myElement;
            myElement = myElement.getParent();
        }
        return null;
    }

    public static PsiBinaryExpression lookUpForContainingBinaryOperation(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiBinaryExpression)
                return (PsiBinaryExpression) myElement;
            myElement = myElement.getParent();
        }
        return null;
    }

    public static PsiCallExpression lookUpForContainingCallExpression(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiCallExpression)
                return (PsiCallExpression) myElement;
            myElement = myElement.getParent();
        }
        return null;
    }

    public static PsiCodeBlock lookUpForContainingCodeBlock(PsiElement element) {
        PsiElement myElement = element;
        while (myElement != null) {
            if (myElement instanceof PsiCodeBlock)
                return (PsiCodeBlock) myElement;
            myElement = myElement.getParent();
        }
        return null;
    }

    public static Vector<PsiElement> lookDownForPsiStatements(PsiElement element) {
        PsiElement[] e = new PsiElement[1];
        e[0] = element;
        return lookDownForPsiStatements(e, null);
    }

    private static Vector<PsiElement> lookDownForPsiStatements(PsiElement[] psiElements, Vector<PsiElement> result) {

        if (result == null)
            result = new Vector<PsiElement>();
        if (psiElements == null)
            return null;

        for (PsiElement current : psiElements) {
            System.out.println("DOWN: " + current.getText());
            if (current instanceof PsiStatement) {
                result.add(current);
            } else {
                lookDownForPsiStatements(current.getChildren(), result);
            }
        }
        return result;
    }

    public static boolean addImportStatementToPsiElement(PsiElement element, String classToBeImportedQualifiedName) {
        Project prj = element.getProject();
        PsiImportStatement psiImportStatement = JavaPsiFacade.getInstance(prj).getElementFactory().createImportStatement(IntelliJUtilFacade.findClassInProject(prj, classToBeImportedQualifiedName));
        if (psiImportStatement == null)
            return false;
        if (element.getLanguage().is(Language.findLanguageByID("JAVA"))) {
            try {
                ((PsiJavaFile) element.getContainingFile()).getImportList().add(psiImportStatement);
            } catch (NullPointerException npe) {
                return false;
            }
            new OptimizeImportsProcessor(prj, element.getContainingFile())
                    .runWithoutProgress();
        }
        return true;
    }

    /**
     * How do I find a file if I know its name but don't know the path?
     *
     * @param psiClass the context where to look
     * @param fileName the file to be find
     * @return
     */

    public PsiFile[] getFilesByName(@NotNull PsiClass psiClass, @NotNull String fileName) {
        return FilenameIndex.getFilesByName(psiClass.getProject(), fileName, GlobalSearchScope.projectScope(psiClass.getProject()));
    }

    /**
     * How do I find where a particular PSI element is used?
     *
     * @param psiClass
     * @return
     */
    public com.intellij.util.Query<com.intellij.psi.PsiReference> getReferences(@NotNull PsiClass psiClass) {
        return ReferencesSearch.search(psiClass);
    }

    /**
     * How do I rename a PSI element?
     *
     * @param contextPsiClass a class that is the project to refactor
     * @param psiElement      the element to be refactored
     * @param newName         the new name that psiElement will have
     */
    public com.intellij.refactoring.RenameRefactoring refactorName(@NotNull PsiClass contextPsiClass, @NotNull PsiElement psiElement, String newName) {

        return (RefactoringFactory.getInstance(contextPsiClass.getProject())).createRename(psiElement, newName, false, false);
    }

    /**
     * How can I cause the PSI for a virtual file to be rebuilt?
     */
    public void rebuildFiles() {
        FileContentUtil.reparseFiles();
    }

    /**
     * How do I find all inheritors of a class
     */
    public com.intellij.util.Query<PsiClass> getInheritors(PsiClass psiClass) {
        return ClassInheritorsSearch.search(psiClass);
    }

    /**
     * How do I find a superclass of a Java class?
     *
     * @param psiClass target class
     * @return
     */
    public PsiClass getSuperClass(@NotNull PsiClass psiClass) {
        return psiClass.getSuperClass();
    }

    /**
     * How do I get a reference to the containing package of a Java class?
     *
     * @param psiClass target class
     * @return
     */
    public PsiPackage getPackage(@NotNull PsiClass psiClass) {
        PsiJavaFile javaFile = (PsiJavaFile) psiClass.getContainingFile();
        return JavaPsiFacade.getInstance(psiClass.getProject()).findPackage(javaFile.getPackageName());
    }

    /**
     * How do I find the methods overriding a specific method?
     *
     * @param psiMethod
     * @return
     */
    public com.intellij.util.Query<PsiMethod> getOverridingMethods(PsiMethod psiMethod) {
        return OverridingMethodsSearch.search(psiMethod);
    }

    public void listenRefactoring(Object obj) {
        RefactoringEventData refactoringEventData;
        RefactoringEventListener refactoringEventListener;
        com.intellij.execution.testframework.TestStatusListener a;
//        com.intellij.psi.util.PsiUtil;
        //PsiUtilCore.getQualifiedNameAfterRename()

//        com.intellij.execution.junit2.ui.Formatters#statisticsFor
//        com.intellij.execution.testframework.AbstractTestProxy#getMagnitude
//        AbstractTestProxy
    }

    private Location getLocation(AbstractTestProxy test) {

        Project[] openProjects = ProjectManager.getInstance().getOpenProjects();


        for (Project openedProject : openProjects) {

            Location location = test.getLocation(openedProject, GlobalSearchScope.allScope(openedProject));


            if (location != null) {

                return location;

            }

        }


        return null;

    }

    public static String getMethodCallExpressionClassName(PsiMethodCallExpression psiMethodCallExpression) {
        String out = "";
        try {
            PsiType psiElement1 = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
            //PsiMethodReferenceUtil.
            if (psiElement1 != null) {
                out = PsiUtil.resolveClassInType((psiElement1)).getQualifiedName();
            }
        } catch (NullPointerException npe) {


        }catch(com.intellij.openapi.project.IndexNotReadyException inre){
            return getMethodCallExpressionClassName(psiMethodCallExpression, 0);

        }catch (Exception e){

        }

        return (out == null) ? "" : out;

    }
    private static final int INRE_RETRIES=3;
    private static String getMethodCallExpressionClassName(PsiMethodCallExpression psiMethodCallExpression, int tryCount) {
        if(tryCount>INRE_RETRIES){
            return "";
        }
        String out = "";

        try {
            PsiType psiElement1 = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
            //PsiMethodReferenceUtil.
            if (psiElement1 != null) {
                out = PsiUtil.resolveClassInType((psiElement1)).getQualifiedName();
            }
        } catch (NullPointerException npe) {

        }catch(com.intellij.openapi.project.IndexNotReadyException inre){
            return getMethodCallExpressionClassName(psiMethodCallExpression, tryCount+1);

        }

        return (out == null) ? "" : out;

    }

    public static String getMethodCallExpressionMethodName(PsiMethodCallExpression psiMethodCallExpression) {

        String out ="";
        try {
            out=psiMethodCallExpression.getMethodExpression().getReferenceName();
        }catch (com.intellij.openapi.project.IndexNotReadyException inre){
            return getMethodCallExpressionMethodName(psiMethodCallExpression, 0);
        }
        return (out == null) ? "" : out;

    }
    private static String getMethodCallExpressionMethodName(PsiMethodCallExpression psiMethodCallExpression, int tryCount) {
        if(tryCount>INRE_RETRIES){
            return "";
        }
        String out ="";
        try {
            out=psiMethodCallExpression.getMethodExpression().getReferenceName();
        }catch (com.intellij.openapi.project.IndexNotReadyException inre){
            return getMethodCallExpressionMethodName(psiMethodCallExpression, tryCount+1);
        }
        return (out == null) ? "" : out;

    }

    //    public void readManifest(Module module){
//        final AndroidFacet facet = FacetManager.getInstance(module).getFacetsByType(AndroidFacet.ID);
//        final VirtualFile manifestFile = AndroidRootUtil.getManifestFile(facet);
//        if (manifestFile == null ||
//                !ReadonlyStatusHandler.ensureFilesWritable(facet.getModule().getProject(), manifestFile)) {
//            return;
//        }
//        final Manifest manifest = AndroidUtils.loadDomElement(facet.getModule(), manifestFile, Manifest.class);
//    }


}
