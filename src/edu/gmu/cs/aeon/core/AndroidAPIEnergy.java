package edu.gmu.cs.aeon.core;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.ide.util.PackageUtil;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.search.GlobalSearchScope;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by DavidIgnacio on 4/20/2014.
 */
public final class AndroidAPIEnergy {

    final static String energyPackageQualifiedName = "edu.gmu.cs.energy";
    private static final List<String> importStatementsList = new ArrayList<String>(Arrays.asList(
            "android.location.LocationManager",
            "android.location.LocationProvider",
            "android.os.PowerManager",
            "java.lang.Exception",
            "",
            "",
            "",
            "",
            "",
            ""
    ));
    private static final List<String> energyCriticalList = new ArrayList<String>(Arrays.asList(
            "android.location.LocationManager",
            "android.location.LocationProvider",
            "android.os.PowerManager",
            "org.apache.http.client.HttpClient",
            "",
            "",
            "",
            "",
            "",
            ""
    ));
    private static final List<String> energyModerateList = new ArrayList<String>(Arrays.asList(
            "",
            "",
            "",
            "",
            "",
            ""
    ));
    private static final List<String> energySuggestionList = new ArrayList<String>(Arrays.asList(
            "",
            "",
            "",
            "",
            "",
            ""
    ));
    private static final List<String> energyLoopCriticalList = new ArrayList<String>(Arrays.asList(
            "java.lang.String",
            "android.graphics.Bitmap",
            "android.database.sqlite.SQLiteDatabase",
            "",
            "",
            ""
    ));
    private static final List<String> energyMethodCriticalList = new ArrayList<String>(Arrays.asList(
            "ArrayAdapter.notifyDataSetChanged",
            "SQLiteDatabase.openDatabase",
            "SQLiteDatabase.query",
            "ContextWrapper.deleteDatabase",
            "XML.newSerializer",
            "Bitmap.getPixel",
//            "Bitmap.setPixel",
//            "Context.bindService",
            "Activity.findViewById"
    ));

//    protected AndroidAPIEnergy(List<String> importStatementsList) {
//        this.importStatementsList = importStatementsList;
//    }

    private AndroidAPIEnergy() {
    }

    public static List<String> getImportStatementsList() {
        return importStatementsList;
    }

    public static List<PsiImportStatement> getPsiImportStatements(PsiClass psiClass) {
        PsiImportStatement[] ImportStatementArrayFromClass = ((PsiJavaFile) psiClass.getContainingFile()).getImportList().getImportStatements();
        List<PsiImportStatement> psiImportStatementFilteredList = new ArrayList<PsiImportStatement>();
        for (PsiImportStatement importStatement : ImportStatementArrayFromClass) {
            for (String allowedImport : AndroidAPIEnergy.getImportStatementsList()) {
                if (importStatement.getQualifiedName().matches(allowedImport))
                    psiImportStatementFilteredList.add(importStatement);
            }
        }
        return psiImportStatementFilteredList;
    }

    public static boolean isInEnergyCriticalList(String classQualifiedName) {
        for (String element : energyCriticalList) {
            if (element.equals(classQualifiedName))
                return true;
        }
        return false;
    }

    public static boolean isInEnergyModerateList(String classQualifiedName) {
        for (String element : energyModerateList) {
            if (element.equals(classQualifiedName))
                return true;
        }
        return false;
    }

    public static boolean isInEnergySuggestionList(String classQualifiedName) {
        for (String element : energySuggestionList) {
            if (element.equals(classQualifiedName))
                return true;
        }
        return false;
    }

    public static void refactorImportsToProxy(PsiClass psiClass, List<PsiImportStatement> importStatementList) {
        String psiClassName = psiClass.getName();
        StringBuilder builder = new StringBuilder("\n\n*Generated Code\n\n");
        PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(psiClass.getProject());
        for (PsiImportStatement importStatement : importStatementList) {
            String importStatementText = importStatement.getText();
            String importStatementQualifiedName = ((PsiImportStatement) importStatement).getQualifiedName();

            builder.append("\nimport " + importStatementText + " " + importStatementQualifiedName + ";");
            builder.append("\n");
            PsiClass ref = elementFactory.createClass("Refactored");
            // PsiImportStatement psiPackageStatement=elementFactory.createImportStatement(ref);
            //  ((PsiImportStatement)importStatement).replace(psiPackageStatement);
            PsiJavaFile javaFile = (PsiJavaFile) psiClass.getContainingFile();
            PsiPackage pkg = JavaPsiFacade.getInstance(psiClass.getProject()).findPackage(javaFile.getPackageName());

        }

        PsiImportStatement psiProxyImportStatement = elementFactory.createImportStatement(psiClass);
        PsiElement psiElement = psiClass.add(psiProxyImportStatement);
//todo use it in refactoring class generation
        JavaCodeStyleManager.getInstance(psiClass.getProject()).shortenClassReferences(psiElement);

        builder.append("\n\n" +
                " End Generated Code\n" +
                "\n");
        System.out.print(builder.toString());

    }

    public static PsiDirectory findOrCreateEnergyDirectoryForProject(PsiClass psiClass) {
        Project project = psiClass.getProject();
        PsiManager psiManager = PsiManager.getInstance(project);
        PsiDirectory baseDirectory = psiManager.findDirectory(psiClass.getContainingFile().getContainingDirectory().getVirtualFile());
        System.out.println("Proj base: " + baseDirectory.getName());
        Module module = ProjectRootManager.getInstance(project).getFileIndex().getModuleForFile(psiClass.getContainingFile().getVirtualFile());
        System.out.println("module: " + module.getName());
        PsiDirectory psiDir = PackageUtil.findOrCreateDirectoryForPackage(module, energyPackageQualifiedName, baseDirectory, false);
        PsiPackage psiPackage = JavaPsiFacade.getInstance(project).findPackage(energyPackageQualifiedName);
        if (psiPackage != null) {
            System.out.println("Package already created: " + psiPackage.getName());
            return psiDir;
        }

//        psiPackage = JavaDirectoryService.getInstance().getPackage(psiDir);
        System.out.println("Package created: " + energyPackageQualifiedName);
        return psiDir;
    }

    public static PsiClass findOrCreateEnergyClassForProject(PsiClass psiClass, String energyClassName) {
        PsiDirectory psiDir = findOrCreateEnergyDirectoryForProject(psiClass);
        return JavaDirectoryService.getInstance().createClass(psiDir, energyClassName);

    }

    public static PsiClass findOrCreateEnergyInterfaceForProject(PsiClass psiClass, String energyInterfaceName) {
        PsiDirectory psiDir = findOrCreateEnergyDirectoryForProject(psiClass);
        return JavaDirectoryService.getInstance().createInterface(psiDir, energyInterfaceName);
    }

    public static PsiClass findOrCreateEnergyClassForProject(PsiClass psiClass, String energyClassName, String contents) {

        PsiElementFactory factory = JavaPsiFacade.getInstance(psiClass.getProject()).getElementFactory();
        PsiFile element = PsiFileFactory.getInstance(psiClass.getProject()).createFileFromText(energyClassName + ".java", JavaFileType.INSTANCE, contents);
        PsiDirectory psiDir = findOrCreateEnergyDirectoryForProject(psiClass);
        PsiFile nameTokensFile = (PsiFile) psiDir.add(element);
        GlobalSearchScope scope = GlobalSearchScope.allScope(psiClass.getProject());
        PsiClass createdPsiClass = JavaPsiFacade.getInstance(psiClass.getProject()).findClass(energyPackageQualifiedName + "." + energyClassName + ".java", scope);
        return createdPsiClass;
    }

    //    public static void proxyRefactoring(PsiClass psiClass){
//        String energyClassName=psiClass.getName()+"Refactored";
//        StringBuffer contentsRefactored=new StringBuffer("package "+energyPackageQualifiedName+";");
//        contentsRefactored.append("public class "+energyClassName+"{" +
//                "int boom;" +
//                " public void zoom(int toom){" +
//                "int xoom=boom+toom;" +
//                "}" +
//                "}");
//
//    }
    public static void proxyRefactoring(PsiClass psiClass) {
        Project project = psiClass.getProject();
        PsiElementFactory factory = JavaPsiFacade.getInstance(project).getElementFactory();
        System.out.println("P Name " + project.getName() + ", fac " + (factory == null ? "null" : "ok") + ", class " + psiClass.getQualifiedName());
        String energyInterfaceName = psiClass.getName() + "ProxyInterface";
        String energyProxyName = psiClass.getName() + "Proxy";
        PsiClass proxyInterface = findOrCreateEnergyInterfaceForProject(psiClass, energyInterfaceName);
        PsiClass proxyClass = findOrCreateEnergyClassForProject(psiClass, energyProxyName);
        PsiImportStatement importStatementForSource = factory.createImportStatement(psiClass);
        ((PsiJavaFile) proxyClass.getContainingFile()).getImportList().add(importStatementForSource);

        String fieldSource = "public final " + psiClass.getName() + " wrapped;";
        PsiField newField = factory.createFieldFromText(fieldSource, null);
        proxyClass.add(newField);
        PsiMethod methodConstructor = factory.createConstructor(proxyClass.getName());
        methodConstructor.getParameterList().add(factory.createParameter(psiClass.getName().toLowerCase(), factory.createType(psiClass)));
        methodConstructor.getBody().add((factory.createStatementFromText("wrapped = " + psiClass.getName().toLowerCase() + ";", methodConstructor.getBody())));
        proxyClass.add(methodConstructor);
        for (PsiMethod psiMethod : psiClass.getMethods()) {
            String methodSourceName = psiMethod.getName() == null ? "null" : psiMethod.getName();
            PsiType methodSourceType = psiMethod.getReturnType();

            if (methodSourceType != null) {
                if (psiMethod == null) {//remove
                    System.out.println("m: " + psiMethod.getBody().getStatements().toString());
                } else {
                    System.out.println("method info: " + psiMethod.getName() + "" + psiMethod.getBody().getStatements().toString());
                    PsiMethod proxyMethod = factory.createMethod(methodSourceName, methodSourceType);
                    PsiMethod interfaceMethod = factory.createMethodFromText("public " + methodSourceType.getPresentableText() + " " + methodSourceName + "();", null);
                    PsiParameterList methodInterfaceParameterList = interfaceMethod.getParameterList();
                    PsiParameterList methodProxyParameterList = proxyMethod.getParameterList();
                    methodInterfaceParameterList.replace(psiMethod.getParameterList());
                    methodProxyParameterList.replace(psiMethod.getParameterList());
                    proxyInterface.add(interfaceMethod);
                    PsiParameter[] methodSourceParameters = psiMethod.getParameterList().getParameters();
                    String params = "";
                    for (int i = 0; i < methodSourceParameters.length; i++) {
                        PsiParameter psiParameter = methodSourceParameters[i];
                        if (i < (methodSourceParameters.length - 1))
                            params += psiParameter.getName() + ", ";
                        else
                            params += psiParameter.getName();
                    }
                    String bodyReflection = "wrapped." + methodSourceName + "(" + params + ");";
                    proxyMethod.getBody().add(factory.createStatementFromText(bodyReflection, proxyMethod.getBody()));
                    proxyClass.add(proxyMethod);
                }
            }
        }

        generateImplements(psiClass, proxyInterface.getQualifiedName());
        generateImplements(proxyClass, proxyInterface.getQualifiedName());
    }

    public static void refactoringAPItoProxies(PsiClass psiClass) {
        List<PsiImportStatement> psiImportStatementFilteredList = getPsiImportStatements(psiClass);

        Project project = psiClass.getProject();
        //  Module module = ProjectRootManager.getInstance(project).getFileIndex().getModuleForFile(project.getProjectFile()).getModuleWithDependenciesAndLibrariesScope(true);
        //GlobalSearchScope scope = GlobalSearchScope.moduleWithDependenciesAndLibrariesScope(module);
        GlobalSearchScope scope = ProjectRootManager.getInstance(project).getFileIndex().getModuleForFile(project.getProjectFile()).getModuleWithDependenciesAndLibrariesScope(true);

        for (PsiImportStatement importStatement : psiImportStatementFilteredList) {
            System.out.println("looking for: " + importStatement.getQualifiedName());
            PsiClass importPsiClass = JavaPsiFacade.getInstance(psiClass.getProject()).findClass(importStatement.getQualifiedName(), scope);
            if (importPsiClass != null) {

                System.out.println("Found: " + importStatement.getQualifiedName());
                proxyRefactoring(importPsiClass);
            }
        }
    }

    public static void reflect() {

    }

    //TODO evaluate the changes required in the implements
    public static PsiClass generateImplements(PsiClass psiClass, String interfaceToImplementQualifiedName) {
        PsiClassType[] implementsListTypes = psiClass.getImplementsListTypes();
        for (PsiClassType implementsListType : implementsListTypes) {
            PsiClass resolved = implementsListType.resolve();
            if (resolved != null && interfaceToImplementQualifiedName.equals(resolved.getQualifiedName())) {
                return psiClass;
            }
        }

        String implementsType = interfaceToImplementQualifiedName;
        PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(psiClass.getProject());
        PsiJavaCodeReferenceElement referenceElement = elementFactory.createReferenceFromText(implementsType, psiClass);
        if (psiClass.isWritable())
            psiClass.getImplementsList().add(referenceElement);
        return psiClass;
    }
}
