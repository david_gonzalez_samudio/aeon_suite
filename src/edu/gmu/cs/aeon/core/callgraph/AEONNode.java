package edu.gmu.cs.aeon.core.callgraph;

import com.intellij.psi.PsiElement;
import edu.gmu.cs.aeon.core.analysis.estimation.PsiElementEnergyReport;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/22/2015.
 */
public class AEONNode {

    public  enum Type {
         UNKNOWN, API, METHOD, SWITCH_BLOCK, SWITCH_BRANCH, IF_BLOCK , THEN_BRANCH, ELSE_BRANCH, LOOP_BLOCK, TRY_BLOCK, TRY_BRANCH, CATCH_BRANCH, ROOT, FINALLY_BRANCH, BEYOND_SOURCES, METHOD_CALL, RECURSION
    }
    Type type=Type.UNKNOWN;
    String nodeText="";
    PsiElement nodePsiElement;
    AEONNode parent;
    Vector<AEONNode> children=new Vector<AEONNode>();
    PsiElementEnergyReport energyReport=new PsiElementEnergyReport();
    protected AEONNode(){}
}
