package edu.gmu.cs.aeon.core.callgraph;

import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;
import edu.gmu.cs.aeon.knowledge.energy.EnergyKnowledgeManager;

import java.util.*;

/**
 * Created by DavidIgnacio on 4/22/2015.
 */
public class AEONCallGraph {
    public static Map<PsiElement, AEONNode> visitedElements=new HashMap<PsiElement, AEONNode>();
    public static Vector<Vector<AEONNode>> buildCallGraphAndGetPaths(PsiElement rootElement){
        AEONNode rootNode=build(rootElement);
        return getAllEnergyPaths(rootNode);
    }

   public static AEONNode build(PsiElement rootElement){
       AEONNode root= buildAndConnectNewNode(rootElement, null);//root case
       visitPsiElement(rootElement, root);
       return root;
   }
    public static Vector<Vector<AEONNode>> getAllEnergyPaths(AEONNode rootNode){
       final  Vector<Vector<AEONNode>> allPaths=new Vector<Vector<AEONNode>>();
        Vector<AEONNode> currentPath=new Vector<AEONNode>();
        buildPaths(rootNode, currentPath, allPaths);
        return allPaths;
    }

    private static void buildPaths(AEONNode currentNode, Vector<AEONNode> currentPath, Vector<Vector<AEONNode>> allPaths) {
        currentPath.add(currentNode);
        //&& currentNode.parent.children.get(currentNode.parent.children.size()-1).equals(currentNode)
        if(currentNode.children.size()==0 ){
            System.out.println("\nFound Path:");
            for(AEONNode node:currentPath){
                System.out.print(">> " + node.nodeText);
            }
            allPaths.add(currentPath);
        }
        for(AEONNode child:currentNode.children){
            Vector<AEONNode> newPath=new Vector<AEONNode>(currentPath);
//            Collections.copy(newPath, currentPath);
            buildPaths(child, newPath ,allPaths);
        }
    }

    private static AEONNode buildAndConnectNewNode(PsiElement psiElement, AEONNode parentNode){
//        if(!visitedElements.containsKey(psiElement)){
            AEONNode newAeonNode=new AEONNode();
            newAeonNode.nodePsiElement =psiElement;
            if(parentNode==null){
                parentNode=newAeonNode;
                parentNode.type= AEONNode.Type.ROOT;
                parentNode.nodeText=psiElement.getText();
                return parentNode;
            }else{
                newAeonNode.parent =parentNode;
                parentNode.children.add(newAeonNode);
            }
           // visitedElements.put(psiElement, newAeonNode);
            return newAeonNode;
//        }else{
//            return visitedElements.get(psiElement);
//        }
    }
    private static void  visitPsiElement(PsiElement psiElement, AEONNode parentNode){
        AEONNode aeonNode= buildAndConnectNewNode(psiElement, parentNode);
        Vector<AEONNode> currentNodes= processPsiElementAndBranching(aeonNode);
        for(AEONNode branchedNode:currentNodes){
            final ArrayList<PsiElement> collectedElements= new ArrayList<PsiElement>();
            collectPsiElements(branchedNode.nodePsiElement, collectedElements);
            for(PsiElement childElement:collectedElements){
                visitPsiElement(childElement, branchedNode);
            }
        }

    }
    private static boolean isCyclic(final AEONNode currentNode){
        AEONNode aeonNode=currentNode.parent;
        while(aeonNode!=null){
            if(aeonNode.nodePsiElement.equals(currentNode.nodePsiElement)){
                return true;
            }
            aeonNode=aeonNode.parent;
        }
        return false;
    }
    private static Vector<AEONNode> processPsiElementAndBranching(AEONNode parentAeonNode) {
        Vector<AEONNode> branches=new Vector<AEONNode>();
        final PsiElement psiElement=parentAeonNode.nodePsiElement;
//        if(aeonNode.type== AEONNode.Type.ROOT){
//            branches.add(aeonNode);
//            return branches;
//        }

        if(psiElement instanceof PsiMethodCallExpression||psiElement instanceof PsiNewExpression) {
            PsiMethod resolvedMethod=null;
            String className ="";
            String methodName="";
            String label="";
            if(psiElement instanceof PsiMethodCallExpression){

            final PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) psiElement;
                label=psiMethodCallExpression.getMethodExpression().getText();
                className = IntelliJUtilFacade.getMethodCallExpressionClassName(psiMethodCallExpression);
                methodName = IntelliJUtilFacade.getMethodCallExpressionMethodName(psiMethodCallExpression);
                resolvedMethod= psiMethodCallExpression.resolveMethod();
            }else{
                final PsiNewExpression newExpression = (PsiNewExpression)psiElement;
                label=newExpression.getText();
                resolvedMethod= newExpression.resolveConstructor();
                if(resolvedMethod!=null) {
                    final PsiType psiType = resolvedMethod.getReturnType();

                    if (psiType != null) {
                        className = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                    }

                    methodName = resolvedMethod.getName();
                }

            }
            parentAeonNode.nodeText = label;
            Double est = EnergyKnowledgeManager.energyGreedyAPIs.get(className + "." + methodName);
            if (est != null) {
                parentAeonNode.energyReport.finalEstimate=est;
                parentAeonNode.type= AEONNode.Type.API;

            }else{

                    if(isCyclic(parentAeonNode)){
                        parentAeonNode.type = AEONNode.Type.RECURSION;
                        parentAeonNode.nodeText = "Recursion: "+label;

                        //branches.add(RECURSION_NODE);
                    }else{
                        parentAeonNode.type = AEONNode.Type.METHOD_CALL;
                        AEONNode node= buildAndConnectNewNode(resolvedMethod, parentAeonNode);
                        branches.add(node);
                    }



            }

        }else{
            if(psiElement instanceof PsiMethod) {
                parentAeonNode.nodeText = ((PsiMethod) psiElement).getName();
                if (IntelliJUtilFacade.isPsiElementInSourceRoots(psiElement)) {
                    parentAeonNode.type = AEONNode.Type.METHOD;

                    branches.add(parentAeonNode);
                }else{
                    parentAeonNode.type = AEONNode.Type.BEYOND_SOURCES;

                }

            }else{
                    if(psiElement instanceof PsiIfStatement){
                        final PsiIfStatement psiIfStatement=(PsiIfStatement)psiElement;
                        parentAeonNode.type= AEONNode.Type.IF_BLOCK;
                        parentAeonNode.nodeText="If "+(psiIfStatement.getCondition()!=null?psiIfStatement.getCondition().getText():"");
                        if(psiIfStatement.getThenBranch()!=null) {
                            AEONNode node = buildAndConnectNewNode(psiIfStatement.getThenBranch(), parentAeonNode);
                           node.nodeText="Then Branch";
                            node.type= AEONNode.Type.THEN_BRANCH;
                            branches.add(node);
                        }
                        if(psiIfStatement.getElseBranch()!=null) {
                            AEONNode node = buildAndConnectNewNode(psiIfStatement.getElseBranch(), parentAeonNode);
                            node.nodeText="Else Branch";
                            node.type= AEONNode.Type.ELSE_BRANCH;
                            branches.add(node);
                        }
                    }else{
                        if(psiElement instanceof PsiLoopStatement){
                            parentAeonNode.type= AEONNode.Type.LOOP_BLOCK;
                            parentAeonNode.nodeText="Loop";
                            branches.add(parentAeonNode);
                        }else{
                            if(psiElement instanceof PsiTryStatement){
                                parentAeonNode.type= AEONNode.Type.TRY_BRANCH;
                                parentAeonNode.nodeText="Try";
                                branches.add(parentAeonNode);

                            }else{
                                if(psiElement instanceof PsiSwitchStatement){
                                    parentAeonNode.type= AEONNode.Type.SWITCH_BLOCK;
                                    parentAeonNode.nodeText="Switch";
                                    branches.add(parentAeonNode);

                                }else{

                                }

                            }
                        }

                    }


            }

        }
        return branches;
    }
public static final AEONNode RECURSION_NODE= new AEONNode();
    static {
        RECURSION_NODE.nodeText="Recursion";
        RECURSION_NODE.energyReport.hasRecursion=true;
    }
    public static final AEONNode LIMIT_NODE= new AEONNode();
    static {
        LIMIT_NODE.nodeText="Scope Limit";
    }

    private static void collectPsiElements(final PsiElement element, final ArrayList<PsiElement> filteredElements) {
        final PsiElement[] children = element.getChildren();
        for (final PsiElement child : children) {
            collectPsiElements(child, filteredElements);
            if (child instanceof PsiMethodCallExpression) {
//                final PsiMethodCallExpression callExpression = (PsiMethodCallExpression)child;
//                final PsiReferenceExpression methodExpression = callExpression.getMethodExpression();
//                final PsiMethod method = (PsiMethod)methodExpression.resolve();
//                if (method != null) {
//                    filteredElements.add(method);
//                }
                filteredElements.add(child);
            }
            else {
                if (child instanceof PsiNewExpression) {
//                    final PsiNewExpression newExpression = (PsiNewExpression) child;
//                    final PsiMethod method = newExpression.resolveConstructor();
//                    if (method != null) {
//                        filteredElements.add(method);
//                    }
                    filteredElements.add(child);
                }else{
                    if(child instanceof PsiIfStatement){

                        filteredElements.add(child);
                    }else{
                        if(child instanceof PsiLoopStatement){
                            filteredElements.add(child);

                        }else{
                            if(child instanceof PsiTryStatement){
                                filteredElements.add(child);

                            }else{
                                if(child instanceof PsiSwitchStatement){
                                    filteredElements.add(child);

                                }
                            }
                        }
                    }
                }
            }
        }
    }




}
