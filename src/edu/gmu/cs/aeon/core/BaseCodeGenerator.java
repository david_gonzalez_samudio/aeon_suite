package edu.gmu.cs.aeon.core;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;

/**
 * User: anatoly
 * Date: 01.07.13
 * Time: 12:13
 * src/ru/korniltsev/intellij/android/generate/BaseGenerateCodeWriter.java
 */
public abstract class BaseCodeGenerator extends WriteCommandAction.Simple {
    public Project prj;
    public PsiElement _myPsiElement;

    public BaseCodeGenerator(@NotNull final PsiElement psiElement, String commandName) {
        super(psiElement.getProject(), commandName);
        this._myPsiElement = psiElement;
        this.prj = _myPsiElement.getProject();
    }

    @Override
    protected void run() throws Throwable {
        generate();
    }

    public abstract void generate();

    public abstract void unGenerate();


}