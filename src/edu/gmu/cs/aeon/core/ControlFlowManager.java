package edu.gmu.cs.aeon.core;

import com.intellij.psi.*;
import com.intellij.psi.controlFlow.AnalysisCanceledException;
import com.intellij.psi.controlFlow.ControlFlowFactory;
import com.intellij.psi.controlFlow.ControlFlowPolicy;
import org.jetbrains.annotations.Nullable;

/**
 * Created by DavidIgnacio on 10/24/2014.
 */
public class ControlFlowManager {
    public void AnayzeForWakelocks(PsiElement psiElement) throws AnalysisCanceledException {
        ControlFlowFactory.getInstance(psiElement.getProject()).getControlFlow(psiElement, new ControlFlowPolicy() {
            @Nullable
            @Override
            public PsiVariable getUsedVariable(PsiReferenceExpression psiReferenceExpression) {
                PsiElement element = psiReferenceExpression.getQualifierExpression().getReference().getElement();
                if (element instanceof PsiVariable) {
                    return (PsiVariable) element;
                }
                return null;
            }

            @Override
            public boolean isParameterAccepted(PsiParameter psiParameter) {
                return true;
            }

            @Override
            public boolean isLocalVariableAccepted(PsiLocalVariable psiLocalVariable) {
                return true;
            }
        });
    }
}
