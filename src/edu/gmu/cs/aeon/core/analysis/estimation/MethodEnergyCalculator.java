package edu.gmu.cs.aeon.core.analysis.estimation;

import com.intellij.psi.*;
import com.intellij.util.ArrayUtil;
import edu.gmu.cs.aeon.core.analysis.report.AnalysisCategory;
import edu.gmu.cs.aeon.core.analysis.report.PredictionType;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.core.analysis.report.ReportFactory;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * Created by DavidIgnacio on 4/5/2015.
 */
public class MethodEnergyCalculator{
    public static final double IF_BLOCK_WEIGHT = 1;
    public static final double SWITCH_BLOCK_WEIGHT = 1;
    public static boolean isExploreTry = true;
    static Set<MethodEnergyCalculator> visited = new HashSet<MethodEnergyCalculator>();
    static boolean isExploreConditional = true; // explore conditional
    static boolean isExploreLoop = true; // explore conditional
    static boolean useIndirectionLevel = true; // limit depth
    static int maxIndirectionLevel = 10;
    static boolean isToExport = false;
    public static double IF_WEIGHT=0.8;
    public static double ELSE_WEIGHT=0.2;
    public static double LOOP_WEIGHT=1;
    public static double TRY_WEIGHT=0.8;
    public static double CATCH_WEIGHT=0.1;
    public static double FINALLY_WEIGHT=1;
    MethodEnergyCalculator parent;
    Vector<MethodEnergyCalculator> children = new Vector<MethodEnergyCalculator>();
    PsiElement currentElement;
    double weight=1.0;
    double estimate;
    Type type = Type.METHOD;
    int currentIndirectionLevel = 0;
static   MethodEnergyCalculator deepestLeaf;


    public static void resetVisited(){
        visited = new HashSet<MethodEnergyCalculator>();
    }

    public static PsiElementEnergyReport psiElementEnergyReport;

    public static PsiElementEnergyReport doEnergyAnalysisReport(PsiElement element){
        resetVisited();
        psiElementEnergyReport =new PsiElementEnergyReport();
        deepestLeaf=null;
        String call="";
        try {
            PsiMethod leMethod = IntelliJUtilFacade.lookUpForContainingMethod(element);


            call = leMethod.getContainingClass().getQualifiedName() + "." + leMethod.getName();
          //  System.out.print("le call  " + call);
            psiElementEnergyReport.callPath.add(call);

        }catch (Exception e){
            call=element.getText().substring(0, Math.min(20, element.getTextLength()));
          //  System.out.print("ERRORROROROROR mmethod i snot method sintzx");
            psiElementEnergyReport.callPath.add(call);
        }

        MethodEnergyCalculator root = new MethodEnergyCalculator();

        root.weight = 1;
        root.currentElement = element;

            root.parent = null;
            root.type= MethodEnergyCalculator.Type.ROOT;
            if (!MethodEnergyCalculator.visited.contains(root)) {
                MethodEnergyCalculator.visited.add(root);
            }

        psiElementEnergyReport.visitedFromThisMethod=new HashSet<MethodEnergyCalculator>();
        psiElementEnergyReport.visitedFromThisMethod.add(root);
        MethodEnergyVisitor methodEnergyVisitor = new MethodEnergyVisitor(root);
        element.accept(methodEnergyVisitor);
        psiElementEnergyReport.finalEstimate= MethodEnergyCalculator.calculateAll(root,0, 0, 0, 0);
        return psiElementEnergyReport;
    }

    public static Collection<PsiElementEnergyReport> doMethodEnergyAnalysisByPaths(PsiMethod psiMethod1){
        psiElementEnergyReport =new PsiElementEnergyReport();
        deepestLeaf=null;
        String call="";
        try {
            PsiMethod leMethod = psiMethod1;

             call = leMethod.getContainingClass().getQualifiedName() + "." + leMethod.getName();
            //  System.out.print("le call  " + call);
            psiElementEnergyReport.callPath.add(call);

        }catch (Exception e){
            System.out.print("ERRORROROROROR mmethod i snot method sintzx");
        }

        MethodEnergyCalculator root = new MethodEnergyCalculator();

        root.weight = 1;
        root.currentElement = psiMethod1;

        root.parent = null;
        root.type= MethodEnergyCalculator.Type.ROOT;
        if (!MethodEnergyCalculator.visited.contains(root)) {
            MethodEnergyCalculator.visited.add(root);
        }

        psiElementEnergyReport.visitedFromThisMethod=new HashSet<MethodEnergyCalculator>();
        psiElementEnergyReport.visitedFromThisMethod.add(root);
        MethodEnergyVisitor methodEnergyVisitor = new MethodEnergyVisitor(root);
        psiMethod1.accept(methodEnergyVisitor);
        return MethodEnergyCalculator.getAllPaths(methodEnergyVisitor.root, call);
    }
    public static Vector<Report> doClassEnergyAnalysis(PsiClass psiClass) {
        Vector<Report> out = new Vector<Report>();
        PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getMethods(), psiClass.getConstructors());

//        int faultyCount = 0;
//        int totalCount = 0;
        for (PsiMethod psiMethod1 : psiMethods) {
            PsiElementEnergyReport psiElementEnergyReport =MethodEnergyCalculator.doEnergyAnalysisReport(psiMethod1);

            if(psiElementEnergyReport.finalEstimate>0) {
                out.add(ReportFactory.buildReport(AnalysisCategory.GREEDY_API, PredictionType.ENERGY_FAULT, null,psiMethod1, psiElementEnergyReport.getText(), psiMethod1.getText(), true));
            }

        }
        return out;
    }
    @NotNull
    public  static Vector<PsiElementEnergyReport>  getAllPaths(@Nullable MethodEnergyCalculator node, String rootMethod){
        final Vector<PsiElementEnergyReport> allPathsOut=new Vector<PsiElementEnergyReport>();

        if(node!=null) {
            psiElementEnergyReport.finalEstimate=getAllPaths(node, 0, 0, 0, 0, rootMethod,allPathsOut);
        }
        allPathsOut.add(psiElementEnergyReport);
        return allPathsOut;
    }
    private static double getAllPaths(MethodEnergyCalculator node, int callStackCount, int loopCount, int ifCount, int tryCount, String callStack, final Vector<PsiElementEnergyReport> allPathsOut) {// always take the most expensive path including loops or conditions, loops metrics is only one iteration cost
        double current =0;



        if(callStackCount==0){
            current = node.estimate * node.weight;
        }else{
            if(loopCount>0||ifCount>0||tryCount>0) {
                current = node.estimate * node.weight;

            }
        }

        int loopFound=0;
        int tryFound=0;
        int callStackFound=0;
        int ifFound=0;
        String call="";
        switch(node.type){
            case API:
                return current;

            case IF_BLOCK:
                ifFound=1;
                break;
            case TRY_BLOCK:
                tryFound=1;
                break;
            case METHOD:



                    if(node.currentElement instanceof PsiMethod){

                        try {
                            PsiMethod leMethod = (PsiMethod) node.currentElement;

                            String call2 = leMethod.getContainingClass().getQualifiedName() + "." + leMethod.getName();
                            call=" > "+call2;
                           System.out.print(callStack+" le call  "+call);
                            psiElementEnergyReport.callPath.add(call);
                            callStackFound=1;
                        }catch (Exception e){
                            System.out.print("ERRORROROROROR mmethod i snot method sintzx");
                        }
                        if(psiElementEnergyReport.visitedFromThisMethod.contains(node) &&callStackCount+callStackCount>0){
                            psiElementEnergyReport.hasRecursion=true;
                            allPathsOut.add(buildMethodEnergyReport(current, callStackCount + callStackFound, loopCount + loopFound, ifCount + ifFound, tryCount + tryFound, true, callStack + call));
                            return current;
                        }
                    }else{
                        System.out.print("ERRORROROROROR mmethod i snot method");
                    }



                psiElementEnergyReport.visitedFromThisMethod.add(node);
                break;
            case LOOP_BLOCK:
                loopFound=1;
                break;
        }





        if(callStackCount + callStackFound> psiElementEnergyReport.maxCallStackDepth){
            psiElementEnergyReport.maxCallStackDepth=callStackCount + callStackFound;
            deepestLeaf=node;

        }
        if(loopCount + loopFound> psiElementEnergyReport.maxLoopComplexity){
            psiElementEnergyReport.maxLoopComplexity=loopCount + loopFound;
        }
        if(ifCount + ifFound> psiElementEnergyReport.maxIfComplexity){
            psiElementEnergyReport.maxIfComplexity=ifCount + ifFound;
        }
        if(tryCount+tryFound> psiElementEnergyReport.maxTryComplexity){
            psiElementEnergyReport.maxTryComplexity=tryCount+tryFound;
        }


        for (MethodEnergyCalculator methodEnergyCalculator : node.children) {


            if(ifFound>0) {

                for(MethodEnergyCalculator branchEnergyCalculator : methodEnergyCalculator.children) {
                    double currento = getAllPaths(branchEnergyCalculator, callStackCount + callStackFound, loopCount + loopFound, ifCount + ifFound, tryCount + tryFound, callStack+call, allPathsOut);
                    methodEnergyCalculator.estimate = psiElementEnergyReport.computeEstimates(currento,methodEnergyCalculator);

                }
                current+=methodEnergyCalculator.estimate;

            }else{
                current+= getAllPaths(methodEnergyCalculator, callStackCount + callStackFound, loopCount + loopFound, ifCount, tryCount + tryFound, callStack+call, allPathsOut);
            }

        }
        if(node.children.size()==0 &&!(callStack+call).equals("")){

            allPathsOut.add(buildMethodEnergyReport(current,callStackCount + callStackFound, loopCount + loopFound, ifCount + ifFound, tryCount + tryFound, false, callStack+call));
        }
        return current;
    }
    public static PsiElementEnergyReport buildMethodEnergyReport(double current, int callStackCount, int loopCount, int ifCount, int tryCount, boolean hasRecursion,String callStack){
        PsiElementEnergyReport psiElementEnergyReport =new PsiElementEnergyReport();
        psiElementEnergyReport.finalEstimate=current;
        psiElementEnergyReport.maxCallStackDepth=callStackCount;
        psiElementEnergyReport.maxLoopComplexity=loopCount;
        psiElementEnergyReport.maxIfComplexity=ifCount;
        psiElementEnergyReport.maxTryComplexity=tryCount;
        psiElementEnergyReport.finalCallPath=callStack;
        psiElementEnergyReport.hasRecursion=hasRecursion;
        return psiElementEnergyReport;
    }
    public static double calculateAll(MethodEnergyCalculator node, int callStackCount, int loopCount, int ifCount, int tryCount) {// always take the most expensive path including loops or conditions, loops metrics is only one iteration cost
        double current =0;




        if(callStackCount==0){
            current = node.estimate * node.weight;
        }else{
            if(loopCount>0||ifCount>0||tryCount>0) {
                current = node.estimate * node.weight;

            }
        }

        int loopFound=0;
        int tryFound=0;
        int callStackFound=0;
        int ifFound=0;
        switch(node.type){
            case API:
                return current;

            case IF_BLOCK:
                ifFound=1;
                break;
            case TRY_BLOCK:
                tryFound=1;
                break;
            case METHOD:

                if(psiElementEnergyReport.visitedFromThisMethod.contains(node)&&callStackCount+callStackFound>0){
                    psiElementEnergyReport.hasRecursion=true;
                    return current;
                }else{

                    if(node.currentElement instanceof PsiMethod){

                        try {
                            PsiMethod leMethod = (PsiMethod) node.currentElement;

                            String call = leMethod.getContainingClass().getQualifiedName() + "." + leMethod.getName();
                            //System.out.print("le call  "+call);
                            psiElementEnergyReport.callPath.add(call);
                            callStackFound=1;
                        }catch (Exception e){
                            System.out.print("ERRORROROROROR mmethod i snot method sintzx");
                        }
                    }else{
                        System.out.print("ERRORROROROROR mmethod i snot method");
                    }

                }
                psiElementEnergyReport.visitedFromThisMethod.add(node);
                break;
            case LOOP_BLOCK:
                loopFound=1;
                break;
        }





        if(callStackCount + callStackFound> psiElementEnergyReport.maxCallStackDepth){
            psiElementEnergyReport.maxCallStackDepth=callStackCount + callStackFound;
            deepestLeaf=node;

        }
        if(loopCount + loopFound> psiElementEnergyReport.maxLoopComplexity){
            psiElementEnergyReport.maxLoopComplexity=loopCount + loopFound;
        }
        if(ifCount + ifFound> psiElementEnergyReport.maxIfComplexity){
            psiElementEnergyReport.maxIfComplexity=ifCount + ifFound;
        }
        if(tryCount+tryFound> psiElementEnergyReport.maxTryComplexity){
            psiElementEnergyReport.maxTryComplexity=tryCount+tryFound;
        }


        for (MethodEnergyCalculator methodEnergyCalculator : node.children) {


            if(ifFound>0) {

                for(MethodEnergyCalculator branchEnergyCalculator : methodEnergyCalculator.children) {
                    double currento = calculateAll(branchEnergyCalculator, callStackCount + callStackFound, loopCount + loopFound, ifCount + ifFound, tryCount+tryFound);
                    methodEnergyCalculator.estimate = psiElementEnergyReport.computeEstimates(currento,methodEnergyCalculator);

                }
                current+=methodEnergyCalculator.estimate;

            }else{
                current+= calculateAll(methodEnergyCalculator, callStackCount + callStackFound, loopCount + loopFound, ifCount, tryCount+tryFound);
            }

            }
    return current;
    }
    public static String getPath(){
        if(deepestLeaf!=null){
        String out="";
        MethodEnergyCalculator par = deepestLeaf;

        while (par != null) {
            if (par.type.equals(Type.METHOD) && par.currentElement instanceof PsiMethod) {
                try {
                    PsiMethod leMethod = (PsiMethod)par.currentElement;

                    String call = leMethod.getContainingClass().getQualifiedName() + "." + leMethod.getName();
                    //System.out.print("le call  " + call);
                   // psiElementEnergyReport.callPath.add(call);
                    out+=">> "+call+"\n"+out;

                }catch (Exception e){
                    System.out.print("ERRORROROROROR mmethod i snot method sintzx");
                }


            }
            par = par.parent;
        }
        return out;
        }
        return "None";
    }

    public static double calculate(MethodEnergyCalculator node) {
        double current = node.estimate*node.weight;

        for (MethodEnergyCalculator methodEnergyCalculator : node.children) {
            current += calculate(methodEnergyCalculator);
        }
        return current;
    }
    public static double calculateLoopsOnly(MethodEnergyCalculator node, int loopCount) {//starts a 0
        double current =0;


        if(loopCount>0) {
            current = node.estimate * node.weight;

        }
        int loopFound=0;
        if(node.type==Type.LOOP_BLOCK) {
            loopFound=1;
        }

            for (MethodEnergyCalculator methodEnergyCalculator : node.children) {
                current += calculateLoopsOnly(methodEnergyCalculator, loopCount+loopFound);
            }




        return current;
    }



    @Override
    public int hashCode() {
        return 71  + ((currentElement == null) ? 0 : currentElement.hashCode());

    }



   public  enum Type {
        API, METHOD, SWITCH_BLOCK, SWITCH_BRANCH, IF_BLOCK ,IF_BRANCH, ELSE_BRANCH, LOOP_BLOCK, TRY_BLOCK, TRY_BRANCH, CATCH_BRANCH, ROOT, FINALLY_BRANCH
    }


}
