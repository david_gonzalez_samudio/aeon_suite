package edu.gmu.cs.aeon.core.analysis.estimation;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/10/2015.
 */
public class PsiElementEnergyReport {// remember to reset per method
    public int maxLoopComplexity=0;// how many loops inside loops are involved in this method call
    public int maxIfComplexity=0;// how many if inside if are involved in this method call
    public int maxTryComplexity=0;// how many catches inside catches
    public int maxCallStackDepth=0;//in case of recursion(direct or indirect), the count before it finds it
    public boolean hasRecursion=false;
    public String finalCallPath="";
    public Vector<String> callPath=new Vector<String>();
    public double finalEstimate=0;
    public enum Mode{
        MIN,// not entering ifs or loops
        AVG, // even execution of all paths
        MAX,// more expensive paths
        WEIGHT // giving weights to most probable path
    }
    public Mode mode=Mode.MAX;
//    public double maxEstimate=0;
//    public double minEstimate=0;
//    public double weightedEstimate=0;
//    public double averageEstimate=0;
    public Set<MethodEnergyCalculator> visitedFromThisMethod = new HashSet<MethodEnergyCalculator>();
    public double computeEstimates(double referenceValue, MethodEnergyCalculator nodeToCompute){
        //for the if condition
        double result=0;
        switch (mode){
            case MAX:
                if (referenceValue > nodeToCompute.estimate) {
                    result=referenceValue;
                }else{
                    result=nodeToCompute.estimate;
                }
                break;
            case MIN:
                if (referenceValue < nodeToCompute.estimate) {
                    result=referenceValue;
                }else{
                    result=nodeToCompute.estimate;
                }
                break;
            case AVG:
                result=(referenceValue + nodeToCompute.estimate)/2;
                break;
            case WEIGHT:
                result=referenceValue + nodeToCompute.estimate*nodeToCompute.weight;
                break;
        }
        return result;
    }
    public String getCallPath(){
        StringBuffer out=new StringBuffer();
        for(String call:callPath){
            out.append(" >> "+call);
        }
        return out.toString();
    }
    public boolean isCallPathRecursive(){
        boolean out=false;
        int callPathSize=callPath.size();
        for(int i=0;i<callPathSize-1;i++){
            for(int j=i+1;j<callPathSize;j++){
                if(callPath.get(i).equals(callPath.get(j))){
                    out=true;
                }

            }

        }
        return out;
    }
    public String getText(){
        StringBuffer out=new StringBuffer();
        out.append("[Method Energy Analysis, Coverage "+mode.name()+"]\n");
        out.append("\tEstimated Energy Consumption: "+finalEstimate+"\n");
        out.append("\t max O(n): "+maxLoopComplexity+"\n");
        out.append("\t max Conditional Depth: "+maxIfComplexity+"\n");
        out.append("\t max Try/Catch/Finally Depth: "+maxTryComplexity+"\n");
        out.append("\tMax Call Depth: "+maxCallStackDepth+", has Recursion? "+isCallPathRecursive()+"\n");
        out.append("\tDeepest Call path:\n "+MethodEnergyCalculator.getPath()+"\n");

        return out.toString();
    }
    public String getPathText(){
        StringBuffer out=new StringBuffer();
        out.append("[Path Energy Analysis, Coverage "+mode.name()+"]\n");
        out.append("\tCall path:\n "+finalCallPath+"\n");
        out.append("\tCall Depth: "+maxCallStackDepth+", has Recursion? "+hasRecursion+"\n");
        out.append("\tEstimated Energy Consumption: "+finalEstimate+"\n");
        out.append("\tO(n): "+maxLoopComplexity+"\n");
        out.append("\tConditional Depth: "+maxIfComplexity+"\n");
        out.append("\tTry/Catch/Finally Depth: "+maxTryComplexity+"\n");


        return out.toString();
    }
}
