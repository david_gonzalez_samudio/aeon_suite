package edu.gmu.cs.aeon.core.analysis.estimation;

import com.intellij.psi.*;
import com.intellij.psi.javadoc.PsiDocComment;
import com.intellij.psi.util.PsiUtil;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;
import edu.gmu.cs.aeon.knowledge.energy.EnergyKnowledgeManager;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by DavidIgnacio on 4/5/2015.
 */
public class MethodEnergyVisitor extends PsiRecursiveElementVisitor{
    MethodEnergyCalculator currentMethodEnergyCalculator;
    MethodEnergyCalculator root;
    public int inLoopCount=0;
    public int inConditionalCount=0;
    public int inTryCount=0;


    Set<PsiElement> visitedPsiElements=new HashSet<PsiElement>();

    public MethodEnergyVisitor(MethodEnergyCalculator root) {
        this.root=root;
        currentMethodEnergyCalculator=root;
    }

    @Override
    public void visitElement(final PsiElement element) {
        if (element instanceof PsiComment || element instanceof PsiDocComment||element instanceof PsiLiteralExpression) {
            return;
        }

         boolean isVisitElement=true;
//        if(element instanceof PsiMethod){
//            isVisitElement=false;
//
//            MethodEnergyCalculator methodCall = new MethodEnergyCalculator();
//
//            methodCall.type = MethodEnergyCalculator.Type.METHOD;
//            methodCall.parent=currentMethodEnergyCalculator;
//            methodCall.weight = 1;
//            methodCall.currentElement = element;
//            if(root==null) {
//
////                root=methodCall;
////                root.parent = null;
////                root.type= MethodEnergyCalculator.Type.ROOT;
////                if (!MethodEnergyCalculator.visited.contains(methodCall)) {
////                    MethodEnergyCalculator.visited.add(methodCall);
////                    currentMethodEnergyCalculator = methodCall;
////                    isVisitElement=false;
////                    super.visitElement(methodCall.currentElement);
////
////                }
//            }else{
//                currentMethodEnergyCalculator.children.add(methodCall);
//                MethodEnergyCalculator par=methodCall;
//                while(par!=null){
//                    if(par.currentElement.equals(root)){
//                        MethodEnergyCalculator.psiElementEnergyReport.hasRecursion=true;
//                    }
//                    par=par.parent;
//                }
//            }
//
//
//
//
//
//                currentMethodEnergyCalculator=methodCall.parent;
//            ;
//
//        }

        if(element instanceof PsiIfStatement) {
            inConditionalCount++;
            isVisitElement = false;
            PsiIfStatement castedElement = (PsiIfStatement) element;

            MethodEnergyCalculator ifBlock = new MethodEnergyCalculator();
            ifBlock.parent = currentMethodEnergyCalculator;
            ifBlock.type = MethodEnergyCalculator.Type.IF_BLOCK;
            ifBlock.weight = MethodEnergyCalculator.IF_BLOCK_WEIGHT;
            ifBlock.currentElement = castedElement;

            currentMethodEnergyCalculator.children.add(ifBlock);
//            if (!visited.contains(ifBlock)) {
//                ifBlock.currentElement.accept(ifBlock);
//            }
            MethodEnergyCalculator.visited.add(ifBlock);
            ifBlock.estimate = 0;

if(castedElement.getThenBranch()!=null){
            MethodEnergyCalculator ifBranch = new MethodEnergyCalculator();
            ifBranch.parent = ifBlock;
            ifBranch.type = MethodEnergyCalculator.Type.IF_BRANCH;
            ifBranch.weight = MethodEnergyCalculator.IF_WEIGHT;
            ifBranch.currentElement = castedElement.getThenBranch();

            ifBlock.children.add(ifBranch);
            if (!MethodEnergyCalculator.visited.contains(ifBranch)) {
                MethodEnergyCalculator.visited.add(ifBranch);
                currentMethodEnergyCalculator = ifBranch;
                super.visitElement(ifBranch.currentElement);
            }

            ifBranch.estimate = 0;

        }
if(castedElement.getElseBranch()!=null) {
    MethodEnergyCalculator elseBranch = new MethodEnergyCalculator();
    elseBranch.parent = ifBlock;
    elseBranch.type = MethodEnergyCalculator.Type.ELSE_BRANCH;
    elseBranch.weight = MethodEnergyCalculator.ELSE_WEIGHT;
    elseBranch.currentElement = castedElement.getElseBranch();


    ifBlock.children.add(elseBranch);
    if (!MethodEnergyCalculator.visited.contains(elseBranch)) {
        MethodEnergyCalculator.visited.add(elseBranch);
        currentMethodEnergyCalculator = elseBranch;
        super.visitElement(elseBranch.currentElement);
    }

    elseBranch.estimate = 0;
    currentMethodEnergyCalculator = ifBlock.parent;
}
            inConditionalCount--;
        }
        if(element instanceof PsiLoopStatement){
            inLoopCount++;
           isVisitElement=false;

            PsiLoopStatement castedElement = (PsiLoopStatement)element;
            MethodEnergyCalculator add = new MethodEnergyCalculator();
            add.parent = currentMethodEnergyCalculator;
            add.type = MethodEnergyCalculator.Type.LOOP_BLOCK;
            add.weight=MethodEnergyCalculator.LOOP_WEIGHT;
            add.currentElement = castedElement;


            currentMethodEnergyCalculator.children.add(add);
            if (!MethodEnergyCalculator.visited.contains(add)) {
                MethodEnergyCalculator.visited.add(add);
                currentMethodEnergyCalculator=add;
                super.visitElement(add.currentElement);

            }
            add.estimate = 0;
            currentMethodEnergyCalculator=add.parent;
            inLoopCount--;

        }

        if(element instanceof PsiSwitchStatement) {
            inConditionalCount++;
            isVisitElement=false;
            //pending, to branch out each case
            PsiSwitchStatement castedElement = (PsiSwitchStatement) element;

            MethodEnergyCalculator switchBlock = new MethodEnergyCalculator();
            switchBlock.parent = currentMethodEnergyCalculator;
            switchBlock.type = MethodEnergyCalculator.Type.SWITCH_BLOCK;
            switchBlock.weight = MethodEnergyCalculator.SWITCH_BLOCK_WEIGHT;
            switchBlock.currentElement = castedElement;


            currentMethodEnergyCalculator.children.add(switchBlock);
            if (!MethodEnergyCalculator.visited.contains(switchBlock)) {
                MethodEnergyCalculator.visited.add(switchBlock);
                super.visitElement(switchBlock.currentElement);
            }

            switchBlock.estimate = 0;

            currentMethodEnergyCalculator=switchBlock.parent;
            inConditionalCount--;
        }



        if(element instanceof PsiTryStatement) {
            inTryCount++;
            isVisitElement = false;
            PsiTryStatement castedElement = (PsiTryStatement) element;

            MethodEnergyCalculator tryBlock = new MethodEnergyCalculator();
            tryBlock.parent = currentMethodEnergyCalculator;
            tryBlock.type = MethodEnergyCalculator.Type.TRY_BLOCK;
            tryBlock.weight = MethodEnergyCalculator.TRY_WEIGHT;
            tryBlock.currentElement = castedElement;
            MethodEnergyCalculator.visited.add(tryBlock);
            currentMethodEnergyCalculator.children.add(tryBlock);
//            if (!visited.contains(tryBlock)) {
//                tryBlock.currentElement.accept(tryBlock);
//            }
            tryBlock.estimate = 0;

            if(castedElement.getTryBlock()!=null){

            MethodEnergyCalculator tryBranch = new MethodEnergyCalculator();
            tryBranch.parent = tryBlock;
            tryBranch.type = MethodEnergyCalculator.Type.TRY_BRANCH;
            tryBranch.weight = MethodEnergyCalculator.TRY_WEIGHT;
            tryBranch.currentElement = castedElement.getTryBlock();

            tryBlock.children.add(tryBranch);
            if (!MethodEnergyCalculator.visited.contains(tryBranch)) {
                MethodEnergyCalculator.visited.add(tryBranch);
                currentMethodEnergyCalculator = tryBranch;
                super.visitElement(tryBranch.currentElement);
            }


            tryBranch.estimate = 0;
                currentMethodEnergyCalculator=tryBlock;
        }



            if(castedElement.getCatchBlocks()!=null) {

                double prop = MethodEnergyCalculator.CATCH_WEIGHT / castedElement.getCatchBlocks().length;
                for (PsiCodeBlock catchBlock : castedElement.getCatchBlocks()) {
                    MethodEnergyCalculator catchBranch = new MethodEnergyCalculator();
                    catchBranch.parent = tryBlock;
                    catchBranch.type = MethodEnergyCalculator.Type.CATCH_BRANCH;
                    catchBranch.weight = prop;
                    catchBranch.currentElement = catchBlock;

                    tryBlock.children.add(catchBranch);
                    if (!MethodEnergyCalculator.visited.contains(catchBranch)) {
                        MethodEnergyCalculator.visited.add(catchBranch);
                        currentMethodEnergyCalculator = catchBranch;
                        super.visitElement(catchBranch.currentElement);
                    }


                    catchBranch.estimate = 0;
                }

                currentMethodEnergyCalculator = tryBlock;
            }

            if( castedElement.getFinallyBlock()!=null) {
                MethodEnergyCalculator finallyBranch = new MethodEnergyCalculator();
                finallyBranch.parent = currentMethodEnergyCalculator;
                finallyBranch.type = MethodEnergyCalculator.Type.FINALLY_BRANCH;
                finallyBranch.weight = MethodEnergyCalculator.FINALLY_WEIGHT;
                finallyBranch.currentElement = castedElement.getFinallyBlock();

                tryBlock.children.add(finallyBranch);
                if (!MethodEnergyCalculator.visited.contains(finallyBranch)) {
                    MethodEnergyCalculator.visited.add(finallyBranch);
                    currentMethodEnergyCalculator = finallyBranch;
                    super.visitElement(finallyBranch.currentElement);

                }


                finallyBranch.estimate = 0;
            }
            currentMethodEnergyCalculator=tryBlock.parent;

            inTryCount--;

        }
try {
    if (element instanceof PsiMethodCallExpression) {
        isVisitElement = false;
        visitMethodCallExpression((PsiMethodCallExpression) element);
    }
    if (element instanceof PsiNewExpression) {
        isVisitElement = false;
        visitNewExpression((PsiNewExpression) element);
    }
}catch (Exception e){
    return;

}
       if(isVisitElement){
//           if(currentMethodEnergyCalculator!=null) {
               if(!visitedPsiElements.contains(element)) {
                   visitedPsiElements.add(element);
                   super.visitElement(element);
               }
//           }
       }

    }

    private void visitMethodCallExpression(PsiMethodCallExpression psiMethodCallExpression) {
        String className = IntelliJUtilFacade.getMethodCallExpressionClassName(psiMethodCallExpression);
        String methodName = IntelliJUtilFacade.getMethodCallExpressionMethodName(psiMethodCallExpression);
        //  System.out.print(">> " + className + "." + methodName);
        Double est = EnergyKnowledgeManager.energyGreedyAPIs.get(className + "." + methodName);
        if (est != null) {
            MethodEnergyCalculator add = new MethodEnergyCalculator();
            add.parent = currentMethodEnergyCalculator;
            add.type = MethodEnergyCalculator.Type.API;
            add.estimate = est;

            currentMethodEnergyCalculator.children.add(add);
           // return;
        } else {
            PsiMethod method = psiMethodCallExpression.resolveMethod();
            if(method!=null) {
                MethodEnergyCalculator add = new MethodEnergyCalculator();
                add.parent = currentMethodEnergyCalculator;
                add.type = MethodEnergyCalculator.Type.METHOD;
                add.currentElement = method;

                currentMethodEnergyCalculator.children.add(add);
                if (!MethodEnergyCalculator.visited.contains(add)) {
                    MethodEnergyCalculator.visited.add(add);
                    currentMethodEnergyCalculator = add;
                    if(IntelliJUtilFacade.isPsiElementInSourceRoots(method)) {
                        super.visitElement(method);
                    }
                } else {
                    MethodEnergyCalculator par = add.parent;
                    while (par != null) {
                        if (par.currentElement.equals(method)) {
                            MethodEnergyCalculator.psiElementEnergyReport.hasRecursion = true;
                        }
                        par = par.parent;
                    }
                }

                add.estimate = 0;
                currentMethodEnergyCalculator = add.parent;
            }
        }

    }
    private void visitNewExpression(PsiNewExpression inPsiNewExpression) {
        final PsiNewExpression newExpression = inPsiNewExpression;
        final PsiMethod method = newExpression.resolveConstructor();
        if(method==null||method.getContainingClass()==null)
            return;
        String className =method.getContainingClass().getQualifiedName();
        final PsiType psiType = method.getReturnType();

        if (psiType != null) {
            className = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
        }

        String methodName = method.getName();
          System.out.print(">> " + className + "." + methodName);
        Double est = EnergyKnowledgeManager.energyGreedyAPIs.get(className + "." + methodName);
        if (est != null) {
            MethodEnergyCalculator add = new MethodEnergyCalculator();
            add.parent = currentMethodEnergyCalculator;
            add.type = MethodEnergyCalculator.Type.API;
            add.estimate = est;

            currentMethodEnergyCalculator.children.add(add);
            // return;
        } else {


                MethodEnergyCalculator add = new MethodEnergyCalculator();
                add.parent = currentMethodEnergyCalculator;
                add.type = MethodEnergyCalculator.Type.METHOD;
                add.currentElement = method;

                currentMethodEnergyCalculator.children.add(add);
                if (!MethodEnergyCalculator.visited.contains(add)) {
                    MethodEnergyCalculator.visited.add(add);
                    currentMethodEnergyCalculator = add;
                    if(IntelliJUtilFacade.isPsiElementInSourceRoots(method)) {
                        super.visitElement(method);
                    }
                } else {
                    MethodEnergyCalculator par = add.parent;
                    while (par != null) {
                        if (par.currentElement.equals(method)) {
                            MethodEnergyCalculator.psiElementEnergyReport.hasRecursion = true;
                        }
                        par = par.parent;
                    }
                }

                add.estimate = 0;
                currentMethodEnergyCalculator = add.parent;

        }

    }




}
