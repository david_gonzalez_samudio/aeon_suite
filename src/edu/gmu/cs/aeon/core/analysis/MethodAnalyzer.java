package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.*;
import com.intellij.psi.javadoc.PsiDocComment;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.core.analysis.report.Reportable;
import edu.gmu.cs.aeon.core.analysis.resources.powermanager.WakeLockAnalyzer;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;
import edu.gmu.cs.aeon.knowledge.energy.EnergyKnowledgeManager;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/5/2015.
 */
public class MethodAnalyzer extends PsiRecursiveElementVisitor {
    public static boolean isExploreTry = true;
    static Set<MethodAnalyzer> visited = new HashSet<MethodAnalyzer>();
    static boolean isExploreConditional = true; // explore conditional
    static boolean isExploreLoop = true; // explore conditional
    static boolean useIndirectionLevel = true; // limit depth
    static int maxIndirectionLevel = 10;
    static boolean isToExport = false;
    static Collection<Reportable> reportables;

    static {
        reportables = populateReportables();
    }

    MethodAnalyzer parent;
    Set<MethodAnalyzer> children = new HashSet<MethodAnalyzer>();
    PsiMethod method;
    double estimate;
    Type type = Type.METHOD;
    int currentIndirectionLevel = 0;
    Vector<Report> reports = new Vector<Report>();
    private String className;
    private String methodName;

    private static Collection<Reportable> populateReportables() {
        Collection<Reportable> reportables = new Vector<Reportable>();
        reportables.add((Reportable) new WakeLockAnalyzer());
        return reportables;
    }

    public static Vector<Report> analyzeMethodAndCollectReports(PsiMethod method) {
        // ReportFactory.buildReport();
        MethodAnalyzer methodEnergyCalculator = new MethodAnalyzer();
        method.accept(methodEnergyCalculator);
        return null;
    }

    public static double calculate(MethodAnalyzer node) {
        double current = node.estimate;

        for (MethodAnalyzer methodEnergyCalculator : node.children) {
            current += calculate(methodEnergyCalculator);
        }
        return current;
    }

    public Vector<Report> getReports() {
        return reports;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((method == null) ? 0 : method.hashCode());
        return result;
    }

    @Override
    public void visitElement(final PsiElement element) {
        if (element instanceof PsiComment || element instanceof PsiDocComment) {
            return;
        }
        super.visitElement(element);
        if (element instanceof PsiMethodCallExpression) {
            visitMethodCallExpression((PsiMethodCallExpression) element);
        }
        if (element instanceof PsiMethod) {
            visitMethod((PsiMethod) element);
        }

    }

    private void visitMethodCallExpression(PsiMethodCallExpression psiMethodCallExpression) {
        String className = IntelliJUtilFacade.getMethodCallExpressionClassName(psiMethodCallExpression);
        String methodName = IntelliJUtilFacade.getMethodCallExpressionMethodName(psiMethodCallExpression);
       // System.out.print(">> " + className + "." + methodName);
        Double est = EnergyKnowledgeManager.energyGreedyAPIs.get(className + "." + methodName);
        if (est != null) {
            MethodAnalyzer add = new MethodAnalyzer();
            add.parent = this;
            add.type = Type.API;
            add.estimate = est;
            add.className = className;
            add.methodName = methodName;
            children.add(add);
        } else {
            PsiMethod psiMethod = psiMethodCallExpression.resolveMethod();
            MethodAnalyzer add = new MethodAnalyzer();
            add.parent = this;
            add.type = Type.METHOD;
            add.method = psiMethod;
            add.className = className;
            add.methodName = methodName;
            visited.add(add);
            children.add(add);
            if (!visited.contains(add)) {
                method.accept(add);
                analyzeMethod(method);
            }
            add.estimate = 0;
        }

    }

    private void visitMethod(PsiMethod psiMethod) {
        String className = psiMethod.getClass().getCanonicalName();
        String methodName = psiMethod.getName();
        System.out.print(">> " + className + "." + methodName);
        Double est = EnergyKnowledgeManager.energyGreedyAPIs.get(className + "." + methodName);
        if (est != null) {
            MethodAnalyzer add = new MethodAnalyzer();
            add.parent = this;
            add.type = Type.API;
            add.estimate = est;
            add.className = className;
            add.methodName = methodName;
            children.add(add);
        } else {
            MethodAnalyzer add = new MethodAnalyzer();
            add.parent = this;
            add.type = Type.METHOD;
            add.method = psiMethod;
            add.className = className;
            add.methodName = methodName;
            visited.add(add);
            children.add(add);
            if (!visited.contains(add)) {
                method.accept(add);
                analyzeMethod(method);
            }
            add.estimate = 0;
        }

    }

    private void analyzeMethod(PsiMethod method) {
        for (Reportable reportable : reportables) {
            reports.addAll(reportable.getReports(method));
        }
    }

    enum Type {
        API, METHOD
    }


}
