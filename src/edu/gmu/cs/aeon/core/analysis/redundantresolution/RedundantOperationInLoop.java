package edu.gmu.cs.aeon.core.analysis.redundantresolution;

import com.intellij.lang.annotation.Annotation;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.*;
import edu.gmu.cs.aeon.core.AnnotationStyle;
import edu.gmu.cs.aeon.core.generator.VariableGenerator;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by DavidIgnacio on 6/1/2014.
 */
public class RedundantOperationInLoop implements Refactorer {

    public List<String> subjectClassesNames = new ArrayList<String>(Arrays.asList(
            "length",
            "size"
    ));

    ArrayList<String> alv = new ArrayList<String>();


    @Override
    public boolean isUseCase(PsiElement element) {


        if (!(element instanceof PsiReferenceExpression))
            return false;
        PsiReferenceExpression psiReferenceExpression = ((PsiReferenceExpression) element);
        final String canonicalText = psiReferenceExpression.getReferenceName();

        if (!subjectClassesNames.contains(canonicalText))
            return false;

        PsiBinaryExpression binaryExpression = IntelliJUtilFacade.lookUpForContainingBinaryOperation(psiReferenceExpression);
        if (binaryExpression == null) {
            return false;
        }

        PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(binaryExpression);
        if (loopStatement == null) {
            return false;
        }


        return true;
    }

    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {
        return false;
    }

    @Override
    public PsiElement annotateUseCase(PsiElement element, AnnotationHolder holder) {
        if (!isUseCase(element))
            return null;

        PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(element);
        if (loopStatement == null)
            return null;

        String value = loopStatement.getText();
        if (value == null)
            return null;
        if (holder != null) {
            Annotation annotation = holder.createInfoAnnotation(loopStatement.getTextRange(), null);
            annotation.setTextAttributes(AnnotationStyle.SUGGESTION.getTextAttributesKey());
        }
        return loopStatement;

    }

    @Override
    public PsiElement refactorElement(PsiElement element) {
//todo add the visitorExtensionPoint use case call
        if (!(element instanceof PsiReferenceExpression))
            return null;
        PsiReferenceExpression psiReferenceExpression = ((PsiReferenceExpression) element);
        final String canonicalText = psiReferenceExpression.getReferenceName();

        if (!subjectClassesNames.contains(canonicalText))
            return null;
//        System.out.println(element.getText() + " --> " + canonicalText);
        PsiBinaryExpression binaryExpression = IntelliJUtilFacade.lookUpForContainingBinaryOperation(psiReferenceExpression);
        if (binaryExpression == null) {
            return null;
        }

        PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(binaryExpression);
        if (loopStatement == null) {
            return null;
        }
        PsiExpression value = IntelliJUtilFacade.lookUpForContainingCallExpression(psiReferenceExpression) == null ? psiReferenceExpression : IntelliJUtilFacade.lookUpForContainingCallExpression(psiReferenceExpression);

        PsiType type = psiReferenceExpression.getType();
        PsiElement where = loopStatement;
        String name = element.getText().replaceAll("\\.", "_");
        new VariableGenerator(type, where, name, value)
                .execute();
        return where;
    }

    @Override
    public PsiElement previewRefactorElement(PsiElement element) {
        PsiElement copyOfElement = element.copy();
        return refactorElement(copyOfElement);
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return EnergyIcons.ENERGY_MARKER_WARNING_LOOP_CASE;
    }

    @Override
    public int getId() {
        return 001;
    }

    @Override
    public PsiElement getElement() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "Redundant operation in loop";
    }
}
