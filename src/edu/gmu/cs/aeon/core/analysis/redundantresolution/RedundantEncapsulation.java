package edu.gmu.cs.aeon.core.analysis.redundantresolution;

import com.intellij.lang.annotation.Annotation;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.*;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.util.Query;
import edu.gmu.cs.aeon.core.AnnotationStyle;
import edu.gmu.cs.aeon.core.generator.DecapsulationGenerator;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by DavidIgnacio on 6/1/2014.
 */
public class RedundantEncapsulation implements Refactorer {
    private final PsiClass cls;
    public List<String> subjectClassesNames = new ArrayList<String>(Arrays.asList(
            "length",
            "size"
    ));

    ArrayList<String> alv = new ArrayList<String>();

    public RedundantEncapsulation(PsiClass cls) {
        super();
        this.cls = cls;
    }


    @Override
    public boolean isUseCase(PsiElement element) {


        //finding getters and setters
        if (isGetterUseCase(element)) return true;
        if (isSetterUseCase(element)) return true;

        return false;
    }

    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {
        return false;
    }

    public boolean isSetterUseCase(PsiElement element) {

        if (!(element instanceof PsiMethod))
            return false;
        PsiMethod psiMethod = ((PsiMethod) element);
        final String canonicalText = psiMethod.getName();

        if (!canonicalText.startsWith("set"))
            return false;
        PsiParameterList parameterList = psiMethod.getParameterList();
        if (parameterList == null) {
            return false;
        }

        if (parameterList.getParameters().length > 1) {
            return false;
        }

        PsiParameter param = parameterList.getParameters()[0];
        PsiCodeBlock codeBlock = psiMethod.getBody();
        PsiExpressionStatement assignmentExpression = null;

        for (PsiStatement statement : codeBlock.getStatements()) {
            if (statement instanceof PsiExpressionStatement && ((PsiExpressionStatement) statement).getExpression() instanceof PsiAssignmentExpression)
                assignmentExpression = (PsiExpressionStatement) statement;
        }
        if (assignmentExpression == null) {
            return false;
        }
        System.out.println(" set method:" + psiMethod.getName() + " --> " + assignmentExpression.getText() + " >> " + param.getNameIdentifier());
        Query<PsiReference> qr = ReferencesSearch.search(psiMethod, psiMethod.getUseScope(), false);
        for (PsiReference ref : qr.findAll()) {
            System.out.println("ref:" + ref.getElement() + " --> " + ref.resolve().getText());
        }
        return true;
    }

    public boolean isGetterUseCase(PsiElement element) {
        if (!(element instanceof PsiMethod))
            return false;
        PsiMethod psiMethod = ((PsiMethod) element);
        final String canonicalText = psiMethod.getName();

        if (!canonicalText.startsWith("get"))
            return false;
        PsiParameterList parameterList = psiMethod.getParameterList();
        if (parameterList == null) {
            return false;
        }
        if (parameterList.getParametersCount() > 0) {
            return false;
        }

        PsiCodeBlock codeBlock = psiMethod.getBody();
        PsiReturnStatement returnStatement = null;

        for (PsiStatement statement : codeBlock.getStatements()) {
            if (statement instanceof PsiReturnStatement)
                returnStatement = (PsiReturnStatement) statement;
        }
        if (returnStatement == null) {
            return false;
        }

        return true;
    }

    @Override
    public PsiElement annotateUseCase(PsiElement element, AnnotationHolder holder) {
        if (!isUseCase(element))
            return null;
        Annotation annotation = holder.createInfoAnnotation(element.getTextRange(), null);
        annotation.setTextAttributes(AnnotationStyle.SUGGESTION.getTextAttributesKey());
        return element;
    }

    @Override
    public PsiElement refactorElement(PsiElement element) {
//todo add the visitorExtensionPoint use case call
        getterRefactorElement(element);
        setterRefactorElement(element);

        return null;
    }

    @Override
    public PsiElement previewRefactorElement(PsiElement element) {
        return null;
    }

    public boolean setterRefactorElement(PsiElement element) {
        if (!(element instanceof PsiMethod))
            return true;
        PsiMethod psiMethod = ((PsiMethod) element);
        final String canonicalText = psiMethod.getName();

        if (!canonicalText.startsWith("set"))
            return true;
        PsiParameterList parameterList = psiMethod.getParameterList();
        if (parameterList == null) {
            return true;
        }

        if (parameterList.getParameters().length > 1) {
            return true;
        }

        PsiParameter param = parameterList.getParameters()[0];
        PsiCodeBlock codeBlock = psiMethod.getBody();
        PsiExpressionStatement assignmentExpression = null;

        for (PsiStatement statement : codeBlock.getStatements()) {
            if (statement instanceof PsiExpressionStatement && ((PsiExpressionStatement) statement).getExpression() instanceof PsiAssignmentExpression)
                assignmentExpression = (PsiExpressionStatement) statement;
        }
        if (assignmentExpression == null) {
            return true;
        }
        System.out.println(" set method:" + psiMethod.getName() + " --> " + assignmentExpression.getText() + " >> " + param.getNameIdentifier());
        Query<PsiReference> qr = ReferencesSearch.search(psiMethod, psiMethod.getUseScope(), false);
        for (PsiReference ref : qr.findAll()) {
            System.out.println("ref:" + ref.getElement() + " --> " + ref.resolve().getText());
            new DecapsulationGenerator(cls, ref, assignmentExpression.getExpression())
                    .execute();
        }
        return false;
    }

    public boolean getterRefactorElement(PsiElement element) {
        if (!(element instanceof PsiMethod))
            return true;
        PsiMethod psiMethod = ((PsiMethod) element);
        final String canonicalText = psiMethod.getName();

        if (!canonicalText.startsWith("get"))
            return true;
        PsiParameterList parameterList = psiMethod.getParameterList();
        if (parameterList == null) {
            return true;
        }
        if (parameterList.getParametersCount() > 0) {
            return true;
        }

        PsiCodeBlock codeBlock = psiMethod.getBody();
        PsiReturnStatement returnStatement = null;

        for (PsiStatement statement : codeBlock.getStatements()) {
            if (statement instanceof PsiReturnStatement)
                returnStatement = (PsiReturnStatement) statement;
        }
        if (returnStatement == null) {
            return true;
        }
        System.out.println("get method:" + psiMethod.getName() + " --> " + returnStatement.getReturnValue().getText());
        Query<PsiReference> qr = ReferencesSearch.search(psiMethod, psiMethod.getUseScope(), false);
        for (PsiReference ref : qr.findAll()) {
            System.out.println("ref:" + ref.getElement() + " --> " + ref.resolve().getText());
            new DecapsulationGenerator(cls, ref, returnStatement.getReturnValue())
                    .execute();
        }
        return false;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public PsiElement getElement() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "Redundant method encapsulation";
    }
}
