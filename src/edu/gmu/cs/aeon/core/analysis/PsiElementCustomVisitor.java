package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.*;
import com.intellij.psi.javadoc.PsiDocComment;
import com.intellij.psi.util.PsiUtil;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 3/29/2015.
 */
public  class PsiElementCustomVisitor extends PsiRecursiveElementVisitor {

    protected String targetClassCanonical;//method to start tracing
    protected String targetMethod;
    public int count;
    protected Vector<PsiMethodCallExpression> detected;
    protected Vector<PsiNewExpression> detectedConstructors;
    protected Mode mode = Mode.COUNT;
    protected Fixer refactorer;
    public Set<PsiMethod> visited = new HashSet<PsiMethod>();
    public boolean ignoreLoops=false;
    public int loopsCount=0;
    public boolean ignoreConditions=false;
    public int conditionsCount=0;
    public boolean ignoreTrys=false;
    public int trysCount=0;
    //todo, handle branching
    public  int inConditionalCount=0;
    public static int inLoopCount=0;
    public  int inTryCount=0;
    public static int inAPICallCodeCount=0;
    public Stack<PsiMethod> callStack=new Stack<PsiMethod>();

    public PsiElementCustomVisitor(String targetClassCanonical, String targetMethod) {
        this.targetClassCanonical = targetClassCanonical;
        this.targetMethod = targetMethod;

    }

    public PsiElementCustomVisitor(Fixer refactorer) {
        this.refactorer = refactorer;
    }

    private PsiElementCustomVisitor() {

    }

    public static int visitAndGetCount(PsiElement psiMethod, String targetClassCanonical, String targetMethod) {
        PsiElementCustomVisitor visitor = new PsiElementCustomVisitor(targetClassCanonical, targetMethod);
        visitor.count = 0;
        visitor.setMode(Mode.COUNT);
        psiMethod.accept(visitor);
        // JavaHierarchyUtil.
        current=visitor;
        return visitor.count;

    }
    public static int visitAndGetCount(PsiElement psiMethod, PsiElementCustomVisitor visitor) {

        visitor.count = 0;
        visitor.setMode(Mode.COUNT);
        psiMethod.accept(visitor);
        // JavaHierarchyUtil.
        return visitor.count;

    }
    public static PsiElementCustomVisitor current;
    public static Vector<PsiMethodCallExpression> visitAndGetDetected(PsiElement psiMethod, String targetClassCanonical, String targetMethod) {
        PsiElementCustomVisitor visitor = new PsiElementCustomVisitor(targetClassCanonical, targetMethod);
        visitor.detected=new Vector<PsiMethodCallExpression>();
        visitor.detectedConstructors =new Vector<PsiNewExpression>();
        visitor.count = 0;
        visitor.setMode(Mode.DETECT);
        psiMethod.accept(visitor);
        current=visitor;
        return visitor.detected;
    }
    public static PsiElementCustomVisitor visitAndGetDetectedVisitor(PsiElement psiMethod, String targetClassCanonical, String targetMethod) {
        PsiElementCustomVisitor visitor = new PsiElementCustomVisitor(targetClassCanonical, targetMethod);
        visitor.detected=new Vector<PsiMethodCallExpression>();
        visitor.detectedConstructors =new Vector<PsiNewExpression>();
        visitor.setMode(Mode.DETECT);
        visitor.count = 0;
        psiMethod.accept(visitor);
        current=visitor;
        return visitor;
    }

    public static void visitAndRefactor(PsiElement element, Fixer refactorer) {
        PsiElementCustomVisitor visitor = new PsiElementCustomVisitor(refactorer);

        visitor.setMode(Mode.FIX);
        element.accept(visitor);
        current=visitor;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    @Override
    public void visitElement(final PsiElement element) {
        if (element instanceof PsiComment || element instanceof PsiDocComment||element instanceof PsiLiteralExpression) {
            return;
        }
        boolean isVisitedAlready=false;
        if (element instanceof PsiIfStatement||element instanceof PsiSwitchStatement) {
            if (ignoreConditions) {
                return;
            }
                conditionsCount++;
            inConditionalCount++;
            super.visitElement(element);
            inConditionalCount--;
            isVisitedAlready=true;

        }
        if (element instanceof PsiLoopStatement ){
            if (ignoreLoops) {
                return;
            }
                loopsCount++;
            inLoopCount++;
//            System.out.println("\nLOOOOOOOOOOOOOOOOOOOOOOOOOPPPPPPPPPPPPPP"+inLoopCount);
            synchronized (this) {
                super.visitElement(element);
            }
            inLoopCount--;
//            System.out.println("\nENDDDDDDDDDDDDDDDDLOOOOOOOOOOOOOOOOOOOOOOOOOPPPPPPPPPPPPPP"+inLoopCount);

            isVisitedAlready=true;

        }
        if (element instanceof PsiTryStatement ){
            if (ignoreTrys) {
                return;
            }
                trysCount++;
            inTryCount++;
            super.visitElement(element);
            inTryCount--;
            isVisitedAlready=true;

        }
        if(!isVisitedAlready){
            super.visitElement(element);
        }



        switch (this.mode) {
            case COUNT:

                    visitMethodCallCount(element);

                break;
            case DETECT:

                    visitMethodCallDetect( element);

                break;
            case FIX:
                visitMethodCallExpressionFix(element);
                break;
        }


    }


    private void visitMethodCallCount(PsiElement psiElement) {
        String methodName="";
        String qualifierClassQualifiedName = "";
        PsiMethod psiMethod=null;
        if (psiElement instanceof PsiMethodCallExpression) {
            final PsiMethodCallExpression psiMethodCallExpression=(PsiMethodCallExpression)psiElement;

            try {
                psiMethod = psiMethodCallExpression.resolveMethod();
                methodName=psiMethodCallExpression.getMethodExpression().getReferenceName();
                final PsiType psiType = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();

                if (psiType != null) {
                    qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                }

            } catch (Exception npe) {
                methodName="";
                qualifierClassQualifiedName = "";
            }
        }else{
            if(psiElement instanceof PsiNewExpression){
                final PsiNewExpression newExpression = (PsiNewExpression) psiElement;

                try {
                    psiMethod = newExpression.resolveConstructor();
                    methodName=psiMethod.getName();
                    final PsiType psiType = psiMethod.getReturnType();

                    if (psiType != null) {
                        qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                    }

                } catch (Exception npe) {
                    methodName="";
                    qualifierClassQualifiedName = "";
                }

            }
        }

        if (methodName.equals(targetMethod)&&qualifierClassQualifiedName.equals(targetClassCanonical)) {


            //  System.out.println(targetMethod+ " Visit -->"+qualifierClassQualifiedName);

                count++;

        } else {

            if (psiMethod != null) {
                if (!visited.contains(psiMethod)&& IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod)) {
                    visited.add(psiMethod);
                    callStack.push(psiMethod);
//                    psiMethod.accept(this);
                    visitElement(psiMethod);
                    callStack.pop();
                }
            }
        }

    }
    private void visitMethodCallDetect(PsiElement psiElement) {
        String methodName="";
        String qualifierClassQualifiedName = "";
        PsiMethod psiMethod=null;
        PsiMethodCallExpression psiMethodCallExpression=null;
        PsiNewExpression newExpression=null;
        if (psiElement instanceof PsiMethodCallExpression) {
            psiMethodCallExpression=(PsiMethodCallExpression)psiElement;

            try {
                psiMethod = psiMethodCallExpression.resolveMethod();
                methodName=psiMethodCallExpression.getMethodExpression().getReferenceName();
                final PsiType psiType = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();

                if (psiType != null) {
                    qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                }

            } catch (Exception npe) {
                methodName="";
                qualifierClassQualifiedName = "";
            }
        }else{
            if(psiElement instanceof PsiNewExpression){
                newExpression = (PsiNewExpression) psiElement;

                try {
                    psiMethod = newExpression.resolveConstructor();
                    methodName=psiMethod.getName();
                    final PsiType psiType = psiMethod.getReturnType();

                    if (psiType != null) {
                        qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                    }

                } catch (Exception npe) {
                    methodName="";
                    qualifierClassQualifiedName = "";
                }


            }
        }

        if (methodName.equals(targetMethod)&&qualifierClassQualifiedName.equals(targetClassCanonical)) {

            count++;
            //  System.out.println(targetMethod+ " Visit -->"+qualifierClassQualifiedName);
            if(newExpression!=null){
                detectedConstructors.add(newExpression);
            }else{
                if(psiMethodCallExpression!=null){
                    detected.add(psiMethodCallExpression);
                }
            }


        } else {

            if (psiMethod != null) {
                if (!visited.contains(psiMethod)&& IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod)) {
                    visited.add(psiMethod);
                    callStack.push(psiMethod);
                   // synchronized (this) {
//                        psiMethod.accept(this);
                    visitElement(psiMethod);
                    //}
                    callStack.pop();
                }
            }
        }

    }



    private void visitMethodCallExpressionFix(PsiElement element) {
        if(element==null){
            return;
        }
        refactorer.doFix(element);
        PsiMethod psiMethod =null;
        try {
            if (element instanceof PsiMethodCallExpression) {
                psiMethod = ((PsiMethodCallExpression) element).resolveMethod();
            } else {
                if (element instanceof PsiNewExpression) {
                    final PsiNewExpression newExpression = (PsiNewExpression) element;
                    psiMethod = newExpression.resolveConstructor();
                }
            }
        }catch (Exception e){

        }
            if (psiMethod != null) {
                if (!visited.contains(psiMethod)) {
                    visited.add(psiMethod);
                    callStack.push(psiMethod);
                    final boolean isInSourceRoots=IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod);
                    if(!isInSourceRoots){
                        inAPICallCodeCount++;
                    }
                    if(inAPICallCodeCount<MAX_API_CALLS_EXPLORATION_COUNT){
//                        psiMethod.accept(this);
                        synchronized (this) {
                            visitElement(psiMethod);
                        }
                    }

                    if(!isInSourceRoots){
                        inAPICallCodeCount--;
                    }

                    callStack.pop();
                }
            }

    }
    public static final int MAX_API_CALLS_EXPLORATION_COUNT=2;
    public enum Mode {COUNT, DETECT, FIX}

    public interface Fixer {
        void doFix(PsiElement psiElement);
    }

}
