package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.PsiAnonymousClass;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.util.ArrayUtil;
import edu.gmu.cs.aeon.core.analysis.report.Report;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 5/13/2015.
 */
public abstract class AnalyzerImpl implements Analyzer{
    public Mode mode = Mode.UI;
    protected Vector<Report> faultReports = new Vector<Report>();
    protected Vector<Report> okReports = new Vector<Report>();
    @Override
    public boolean analyze(PsiElement psiElementToAnalyze) {//class level by contract
        if (psiElementToAnalyze instanceof PsiAnonymousClass ||psiElementToAnalyze instanceof PsiClass) {
            return analyzeClass((PsiClass)psiElementToAnalyze);
        }
        return false;
    }

    protected  boolean analyzeClass(PsiClass psiClass) {
        boolean isEmpty=true;
        PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getMethods(), psiClass.getConstructors());
        for (PsiMethod psiMethod1 : psiMethods) {
            analyzeMethod(psiMethod1);
            isEmpty=false;
        }
        return isEmpty;
    }
    protected  boolean analyzeMethod(PsiMethod psiMethod1) {
        return false;
    }

    @Override
    public Vector<Report> getSuccessReports() {
        return okReports;
    }

    @Override
    public Vector<Report> getFailureReports() {
        return faultReports;
    }

    @Override
    public void analyzeAndPersist(PsiElement psiElementToAnalyze) {

    }

}
