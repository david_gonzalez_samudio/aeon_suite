package edu.gmu.cs.aeon.core.analysis.smells;

import com.intellij.codeInsight.daemon.impl.analysis.HighlightControlFlowUtil;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.psi.*;
import com.intellij.psi.controlFlow.ControlFlowUtil;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import gnu.trove.THashMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Collection;

/**
 * Created by DavidIgnacio on 11/24/2014.
 */
public class VariableFinalizerRefactor implements Refactorer {
    @Override
    public boolean isUseCase(@NotNull PsiElement element) {
        if (!(element instanceof PsiVariable)) {
            return false;
        }
        PsiVariable psiVariable = (PsiVariable) element;

        final boolean isReassigned = HighlightControlFlowUtil.isReassigned(psiVariable, new THashMap<PsiElement, Collection<ControlFlowUtil.VariableInfo>>());
        if (!isReassigned) {
            if (!psiVariable.hasModifierProperty(PsiModifier.FINAL)) {
                if (psiVariable instanceof PsiLocalVariable || psiVariable instanceof PsiParameter)
                    return true;
                if (psiVariable instanceof PsiField && psiVariable.hasInitializer())
                    return true;
            }
        }


//
//        if(!(psiVariable instanceof PsiLocalVariable || psiVariable instanceof PsiParameter)){
//            return false;
//        }
//        if(RefactoringUtil.canBeDeclaredFinal(psiVariable)){
//                if(!psiVariable.hasModifierProperty(PsiModifier.FINAL)) {
//                    return true;
//                }
//        }
        return false;
    }

    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {
        //TODO
        return false;
    }

    @Nullable
    @Override
    public PsiElement annotateUseCase(@NotNull PsiElement element, AnnotationHolder holder) {
        return null;
    }

    @Nullable
    @Override
    public PsiElement refactorElement(@NotNull PsiElement element) {
        if (!(element instanceof PsiVariable)) {
            return null;
        }
        final PsiVariable psiVariable = (PsiVariable) element;
        final boolean isReassigned = HighlightControlFlowUtil.isReassigned(psiVariable, new THashMap<PsiElement, Collection<ControlFlowUtil.VariableInfo>>());
        if (!isReassigned) {
            if (psiVariable.getModifierList() != null) {
//                if(!psiVariable.hasModifierProperty("final")) {
                ApplicationManager.getApplication().runWriteAction(new Runnable() {
                                                                       @Override
                                                                       public void run() {
                                                                           psiVariable.getModifierList().setModifierProperty(PsiModifier.FINAL, true);
                                                                       }
                                                                   }
                );

                return psiVariable;
//                }
            }


        }
        return null;


//        if(!(psiVariable instanceof PsiLocalVariable || psiVariable instanceof PsiParameter)){
//            return null;
//        }
//        if(RefactoringUtil.canBeDeclaredFinal(psiVariable)){
//            if(psiVariable.getModifierList()!=null) {
////                if(!psiVariable.hasModifierProperty("final")) {
//                ApplicationManager.getApplication().runWriteAction(new Runnable() {
//                                                                       @Override
//                                                                       public void run() {
//                                                                           psiVariable.getModifierList().setModifierProperty(PsiModifier.FINAL, true);
//                                                                       }
//                                                                   }
//                );
//
//                return psiVariable;
////                }
//            }
//
//        }
//        return null;

    }

    @Nullable
    @Override
    public PsiElement previewRefactorElement(@NotNull PsiElement element) {
        return null;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return EnergyIcons.ENERGY_MARKER_WARNING_JAVA_CASE;
    }

    @Override
    public int getId() {
        return 0;
    }

    @NotNull
    @Override
    public PsiElement getElement() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "Variable Finalizer";
    }
}
