package edu.gmu.cs.aeon.core.analysis.specialcase;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassObjectAccessExpression;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethodCallExpression;
import edu.gmu.cs.aeon.core.analysis.Analyzer;
import edu.gmu.cs.aeon.core.analysis.AnalyzerImpl;
import edu.gmu.cs.aeon.core.analysis.PsiElementSequentialVisitor;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 5/13/2015.
 */
public class NativeCallAnalyzer extends AnalyzerImpl{
    private static  final Set<String> nativeMethodCalls;
    static {
        nativeMethodCalls=new HashSet<String>();
        nativeMethodCalls.add("java.lang.System.loadLibrary");
        nativeMethodCalls.add("");
    }

    public class NativeCallVisitor  implements  PsiElementSequentialVisitor.VisitorExtensionPoint {
        @Override
        public Double onVisitingElement(PsiElement psiElement, Object inOutObject) {

            if (psiElement instanceof PsiClass) {
                final PsiClass psiClass = (PsiClass) psiElement;
                if (AndroidUtilFacade.extendsNativeActivity(psiClass)) {
                    return 1.0;
                }

            }else{
                if(psiElement instanceof PsiMethodCallExpression){
                    PsiMethodCallExpression psiMethodCallExpression=(PsiMethodCallExpression)psiElement;
                    final String className = IntelliJUtilFacade.getMethodCallExpressionClassName(psiMethodCallExpression);
                    final String methodName = IntelliJUtilFacade.getMethodCallExpressionMethodName(psiMethodCallExpression);
                    if(nativeMethodCalls.contains(className + "." + methodName)){
                        return 1.0;
                    }
                }
            }

            return 0.0;
        }
    }

}
