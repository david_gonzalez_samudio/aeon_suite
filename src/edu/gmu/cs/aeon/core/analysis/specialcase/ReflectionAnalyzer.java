package edu.gmu.cs.aeon.core.analysis.specialcase;

import com.intellij.psi.PsiClassObjectAccessExpression;
import com.intellij.psi.PsiElement;
import edu.gmu.cs.aeon.core.analysis.PsiElementSequentialVisitor;

/**
 * Created by DavidIgnacio on 5/13/2015.
 */
public class ReflectionAnalyzer implements PsiElementSequentialVisitor.VisitorExtensionPoint{
    @Override
    public Double onVisitingElement(PsiElement psiElement, Object inOutObject) {

        if(psiElement instanceof PsiClassObjectAccessExpression){
           // final PsiClassObjectAccessExpression psiClassObjectAccessExpression=(PsiClassObjectAccessExpression)psiElement;
          //  psiClassObjectAccessExpression.getType();
            return 1.0;
        }

        return 0.0;
    }
}
