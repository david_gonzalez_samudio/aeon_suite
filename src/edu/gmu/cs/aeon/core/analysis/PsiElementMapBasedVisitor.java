package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.*;
import com.intellij.psi.javadoc.PsiDocComment;
import com.intellij.psi.util.PsiUtil;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 3/29/2015.
 */
public class PsiElementMapBasedVisitor extends PsiRecursiveElementVisitor {
    public static PsiElementMapBasedVisitor current;
    public int count;
    protected Vector<PsiMethodCallExpression> detected;
    protected Vector<PsiNewExpression> detectedConstructors;
    protected Mode mode = Mode.COUNT;
    protected Fixer refactorer;
    public Set<PsiMethod> visited = new HashSet<PsiMethod>();
    public boolean ignoreLoops=false;
    public int loopsCount=0;
    public boolean ignoreConditions=false;
    public int conditionsCount=0;
    public boolean ignoreTrys=false;
    public int trysCount=0;
    public HashMap<String, HashMap<String, Integer>> resultingDetections;
    //todo, handle branching


    public PsiElementMapBasedVisitor(final HashMap<String, HashMap<String, Integer>> resultingDetections) {
            this.resultingDetections=resultingDetections;

    }

    private PsiElementMapBasedVisitor(Fixer refactorer) {
        this.refactorer = refactorer;
    }

    private PsiElementMapBasedVisitor() {

    }

    public static int visitAndGetCount(PsiElement toVisit, final HashMap<String, HashMap<String, Integer>> resultingDetections) {
        PsiElementMapBasedVisitor visitor = new PsiElementMapBasedVisitor(resultingDetections);
        visitor.count = 0;
        visitor.setMode(Mode.COUNT);
        toVisit.accept(visitor);
        current=visitor;
        return visitor.count;

    }


    public static Vector<PsiMethodCallExpression> visitAndGetDetected(PsiElement toVisit, final HashMap<String, HashMap<String, Integer>> resultingDetections) {
        PsiElementMapBasedVisitor visitor = new PsiElementMapBasedVisitor(resultingDetections);
        visitor.detected=new Vector<PsiMethodCallExpression>();
        visitor.detectedConstructors =new Vector<PsiNewExpression>();
        visitor.count = 0;
        visitor.setMode(Mode.DETECT);
        toVisit.accept(visitor);
        current=visitor;
        return visitor.detected;
    }
    public static PsiElementMapBasedVisitor visitAndGetDetectedVisitor(PsiElement toVisit, final HashMap<String, HashMap<String, Integer>> resultingDetections) {
        PsiElementMapBasedVisitor visitor = new PsiElementMapBasedVisitor(resultingDetections);
        visitor.detected=new Vector<PsiMethodCallExpression>();
        visitor.detectedConstructors =new Vector<PsiNewExpression>();
        visitor.setMode(Mode.DETECT);
        visitor.count = 0;
        toVisit.accept(visitor);
        current=visitor;
        return visitor;
    }

    public static void visitAndRefactor(PsiElement element, Fixer refactorer) {
        PsiElementMapBasedVisitor visitor = new PsiElementMapBasedVisitor(refactorer);
        visitor.setMode(Mode.FIX);
        element.accept(visitor);
        current=visitor;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    @Override
    public void visitElement(final PsiElement element) {
        if (element instanceof PsiComment || element instanceof PsiDocComment||element instanceof PsiLiteralExpression) {
            return;
        }

        if (element instanceof PsiIfStatement) {
            if (ignoreConditions) {
                return;
            }
                conditionsCount++;

        }
        if (element instanceof PsiLoopStatement ){
            if (ignoreLoops) {
                return;
            }
                loopsCount++;

        }
        if (element instanceof PsiTryStatement ){
            if (ignoreTrys) {
                return;
            }
                trysCount++;

        }
        super.visitElement(element);


        switch (this.mode) {
            case COUNT:

                    visitMethodCallCount(element);

                break;
            case DETECT:

                    visitMethodCallDetect( element);

                break;
            case FIX:
                visitMethodCallExpressionFix(element);
                break;
        }


    }


    private void visitMethodCallCount(PsiElement psiElement) {
        String methodName="";
        String qualifierClassQualifiedName = "";
        PsiMethod psiMethod=null;
        if (psiElement instanceof PsiMethodCallExpression) {
            final PsiMethodCallExpression psiMethodCallExpression=(PsiMethodCallExpression)psiElement;

            try {
                psiMethod = psiMethodCallExpression.resolveMethod();
                methodName=psiMethodCallExpression.getMethodExpression().getReferenceName();
                final PsiType psiType = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();

                if (psiType != null) {
                    qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                }

            } catch (Exception npe) {
                methodName="";
                qualifierClassQualifiedName = "";
            }
        }else{
            if(psiElement instanceof PsiNewExpression){
                final PsiNewExpression newExpression = (PsiNewExpression) psiElement;

                try {
                    psiMethod = newExpression.resolveConstructor();
                    methodName=psiMethod.getName();
                    final PsiType psiType = psiMethod.getReturnType();

                    if (psiType != null) {
                        qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                    }

                } catch (Exception npe) {
                    methodName="";
                    qualifierClassQualifiedName = "";
                }

            }
        }

        if (processMethodCall(qualifierClassQualifiedName, methodName)) {


            //  System.out.println(targetMethod+ " Visit -->"+qualifierClassQualifiedName);

                count++;

        } else {

            if (psiMethod != null) {
                if (!visited.contains(psiMethod)&& IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod)) {
                    visited.add(psiMethod);
                    psiMethod.accept(this);
                }
            }
        }

    }
    private boolean processMethodCall( String targetClassCanonical, String targetMethod){
        HashMap<String, Integer> methods=resultingDetections.get(targetClassCanonical);
        if(methods!=null){
            Integer methodCount=methods.get(targetMethod);
            if(methodCount!=null){
                methods.put(targetMethod, methodCount+1);
                return true;
            }
        }
        return false;
    }
    private void visitMethodCallDetect(PsiElement psiElement) {
        String methodName="";
        String qualifierClassQualifiedName = "";
        PsiMethod psiMethod=null;
        PsiMethodCallExpression psiMethodCallExpression=null;
        PsiNewExpression newExpression=null;
        if (psiElement instanceof PsiMethodCallExpression) {
            psiMethodCallExpression=(PsiMethodCallExpression)psiElement;

            try {
                psiMethod = psiMethodCallExpression.resolveMethod();
                methodName=psiMethodCallExpression.getMethodExpression().getReferenceName();
                final PsiType psiType = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();

                if (psiType != null) {
                    qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                }

            } catch (Exception npe) {
                methodName="";
                qualifierClassQualifiedName = "";
            }
        }else{
            if(psiElement instanceof PsiNewExpression){
                newExpression = (PsiNewExpression) psiElement;

                try {
                    psiMethod = newExpression.resolveConstructor();
                    methodName=psiMethod.getName();
                    final PsiType psiType = psiMethod.getReturnType();

                    if (psiType != null) {
                        qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
                    }

                } catch (Exception npe) {
                    methodName="";
                    qualifierClassQualifiedName = "";
                }


            }
        }

        if (processMethodCall(qualifierClassQualifiedName, methodName)) {

            count++;
            //  System.out.println(targetMethod+ " Visit -->"+qualifierClassQualifiedName);
            if(newExpression!=null){
                detectedConstructors.add(newExpression);
            }else{
                if(psiMethodCallExpression!=null){
                    detected.add(psiMethodCallExpression);
                }
            }


        } else {

            if (psiMethod != null) {
                if (!visited.contains(psiMethod)&& IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod)) {
                    visited.add(psiMethod);
                    psiMethod.accept(this);
                }
            }
        }

    }



    private void visitMethodCallExpressionFix(PsiElement element) {
        if(element==null){
            return;
        }
        refactorer.doFix(element);
        PsiMethod psiMethod =null;
        try {
            if (element instanceof PsiMethodCallExpression) {
                psiMethod = ((PsiMethodCallExpression) element).resolveMethod();
            } else {
                if (element instanceof PsiNewExpression) {
                    final PsiNewExpression newExpression = (PsiNewExpression) element;
                    psiMethod = newExpression.resolveConstructor();
                }
            }
        }catch (Exception e){

        }
            if (psiMethod != null) {
                if (!visited.contains(psiMethod)&& IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod)) {
                    visited.add(psiMethod);
                    psiMethod.accept(this);
                }
            }

    }

    public enum Mode {COUNT, DETECT, FIX}

    public interface Fixer {
        void doFix(PsiElement psiElement);
    }

}
