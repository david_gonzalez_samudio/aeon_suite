package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.PsiElement;
import edu.gmu.cs.aeon.core.analysis.report.Report;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/6/2015.
 */
public interface Analyzer {


     enum Mode {
        UI, BATCH
    }
    AnalysisScope analysisScope = AnalysisScope.CLASS;

    enum AnalysisScope {
        METHOD, CLASS, APPLICATION, SYSTEM
    }
    boolean analyze(PsiElement psiElementToAnalyze);
    Vector<Report> getSuccessReports();
    Vector<Report> getFailureReports();
    void analyzeAndPersist(PsiElement psiElementToAnalyze);//
}
