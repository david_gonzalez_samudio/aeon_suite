package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.*;
import com.intellij.psi.javadoc.PsiDocComment;
import com.intellij.psi.util.PsiUtil;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 3/29/2015.
 */
public  class PsiElementSequentialVisitor  {

    protected String targetClassCanonical;//method to start tracing
    protected String targetMethod;
    public int count;
    protected Vector<PsiMethodCallExpression> detected;
    protected Vector<PsiNewExpression> detectedConstructors;
    protected Mode mode = Mode.COUNT;
    protected VisitorExtensionPoint visitorExtensionPoint;
    public Set<PsiMethod> visited = new HashSet<PsiMethod>();
    public boolean ignoreLoops=false;
    public int loopsCount=0;
    public boolean ignoreConditions=false;
    public int conditionsCount=0;
    public boolean ignoreTrys=false;
    public int trysCount=0;
    //todo, handle branching
    public  int inConditionalCount=0;
    public static int inLoopCount=0;
    public  int inTryCount=0;
    public static int inAPICallCodeCount=0;
    public Stack<PsiExpression> callStack=new Stack<PsiExpression>();// PsiMethodCallExpression and PsiNewExpression
    public Stack<PsiElement> conditionStack=new Stack<PsiElement>();// PsiIfStatement and PsiSwitchStatement
    public Stack<PsiLoopStatement> loopStack=new Stack<PsiLoopStatement>();// All PsiLoops
    public Stack<PsiTryStatement> tryStack=new Stack<PsiTryStatement>();// PsiTryStatements
    public Stack<PsiElement> fullPsiElementStack=new Stack<PsiElement>();// all the previous ones
    public Object inOutObject;

//    public PsiElementSequentialVisitor(String targetClassCanonical, String targetMethod) {
//        this.targetClassCanonical = targetClassCanonical;
//        this.targetMethod = targetMethod;
//
//    }

    public PsiElementSequentialVisitor(VisitorExtensionPoint visitorExtensionPoint, Object inOutObject) {
        this.visitorExtensionPoint = visitorExtensionPoint;
        this.inOutObject=inOutObject;
    }

    private PsiElementSequentialVisitor() {

    }

//    public static int visitAndGetCount(PsiElement psiMethod, String targetClassCanonical, String targetMethod) {
//        PsiElementSequentialVisitor visitor = new PsiElementSequentialVisitor(targetClassCanonical, targetMethod);
//        visitor.count = 0;
//        visitor.setMode(Mode.COUNT);
//        Double result=visitor.visitElement(psiMethod);
//        // JavaHierarchyUtil.
//        current=visitor;
//        return visitor.count;
//
//    }
//    public static int visitAndGetCount(PsiElement psiMethod, PsiElementSequentialVisitor visitor) {
//
//        visitor.count = 0;
//        visitor.setMode(Mode.COUNT);
//        Double result=visitor.visitElement(psiMethod);
//        // JavaHierarchyUtil.
//        return visitor.count;
//
//    }
    public static PsiElementSequentialVisitor current;
//    public static Vector<PsiMethodCallExpression> visitAndGetDetected(PsiElement psiMethod, String targetClassCanonical, String targetMethod) {
//        PsiElementSequentialVisitor visitor = new PsiElementSequentialVisitor(targetClassCanonical, targetMethod);
//        visitor.detected=new Vector<PsiMethodCallExpression>();
//        visitor.detectedConstructors =new Vector<PsiNewExpression>();
//        visitor.count = 0;
//        visitor.setMode(Mode.DETECT);
//        Double result=visitor.visitElement(psiMethod);
//        current=visitor;
//        return visitor.detected;
//    }
//    public static PsiElementSequentialVisitor visitAndGetDetectedVisitor(PsiElement psiMethod, String targetClassCanonical, String targetMethod) {
//        PsiElementSequentialVisitor visitor = new PsiElementSequentialVisitor(targetClassCanonical, targetMethod);
//        visitor.detected=new Vector<PsiMethodCallExpression>();
//        visitor.detectedConstructors =new Vector<PsiNewExpression>();
//        visitor.setMode(Mode.DETECT);
//        visitor.count = 0;
//        Double result=visitor.visitElement(psiMethod);
//        current=visitor;
//        return visitor;
//    }
//
//    public static void visitAndRefactor(PsiElement element, VisitorExtensionPoint refactorer) {
//        PsiElementSequentialVisitor visitor = new PsiElementSequentialVisitor(refactorer);
//
//        visitor.setMode(Mode.EXTENSION_POINT);
//        Double result=visitor.visitElement(element);
//        current=visitor;
//    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }


    public Double visitElement(final PsiElement element) {
        if (element instanceof PsiComment || element instanceof PsiDocComment||element instanceof PsiLiteralExpression) {
            return 0.0;
        }

        Double collected=visitorExtensionPoint.onVisitingElement(element, inOutObject);;
        boolean isVisitedAlready=false;
        if (element instanceof PsiIfStatement||element instanceof PsiSwitchStatement) {
            if (ignoreConditions) {
                return 0.0;
            }
            conditionsCount++;
            inConditionalCount++;
            conditionStack.push(element);
            fullPsiElementStack.push(element);
            collected+=visitChildren(element);
            inConditionalCount--;
            conditionStack.pop();
            fullPsiElementStack.pop();
            isVisitedAlready=true;

        }
        if (element instanceof PsiLoopStatement ){
            if (ignoreLoops) {
                return 0.0;
            }
                loopsCount++;
            inLoopCount++;
       //     System.out.println("\nLOOOOOOOOOOOOOOOOOOOOOOOOOPPPPPPPPPPPPPP" + inLoopCount);
            loopStack.push((PsiLoopStatement)element);
            fullPsiElementStack.push(element);
            collected+= visitChildren(element);
            inLoopCount--;
            loopStack.pop();
            fullPsiElementStack.pop();
           // System.out.println("\nENDDDDDDDDDDDDDDDDLOOOOOOOOOOOOOOOOOOOOOOOOOPPPPPPPPPPPPPP"+inLoopCount);

            isVisitedAlready=true;

        }
        if (element instanceof PsiTryStatement ){
            if (ignoreTrys) {
                return 0.0;
            }
                trysCount++;
            inTryCount++;
            tryStack.push((PsiTryStatement)element);
            fullPsiElementStack.push(element);
            collected+= visitChildren(element);
            inTryCount--;
            tryStack.pop();
            fullPsiElementStack.pop();
            isVisitedAlready=true;

        }


        PsiMethod psiMethod =null;
        boolean doPop=false;
        try {
            if (element instanceof PsiMethodCallExpression) {
              //  isVisitedAlready=true;
                psiMethod = ((PsiMethodCallExpression) element).resolveMethod();
                callStack.push((PsiMethodCallExpression)element);
                fullPsiElementStack.push(element);
                doPop=true;

            } else {
                if (element instanceof PsiNewExpression) {
                   // isVisitedAlready=true;

                    final PsiNewExpression newExpression = (PsiNewExpression) element;
                    psiMethod = newExpression.resolveConstructor();

                    callStack.push((PsiNewExpression)element);
                    fullPsiElementStack.push(element);
                    doPop=true;
                }
            }
        }catch (Exception e){

        }
        try {
            if (psiMethod != null) {
                if (!visited.contains(psiMethod)) {
                    visited.add(psiMethod);
//                callStack.push((PsiExpression)element);
                    final boolean isInSourceRoots = IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod);
                    if (!isInSourceRoots) {
                        inAPICallCodeCount++;
                    }
                    if (inAPICallCodeCount < MAX_API_CALLS_EXPLORATION_COUNT) {
                        collected += visitChildren(psiMethod);
                    }

                    if (!isInSourceRoots) {
                        inAPICallCodeCount--;
                    }


                }

            }
        }finally {
            if(doPop){
                callStack.pop();
                fullPsiElementStack.pop();
            }
        }

        if(!isVisitedAlready){
            collected+= visitChildren(element);
        }

    return collected;
    }

    private Double visitChildren(PsiElement element) {
        Double result=0.0;
        for(PsiElement child:element.getChildren()){
            result+=visitElement(child);
        }
        return result;
    }


//    private void visitMethodCallCount(PsiElement psiElement) {
//        String methodName="";
//        String qualifierClassQualifiedName = "";
//        PsiMethod psiMethod=null;
//        if (psiElement instanceof PsiMethodCallExpression) {
//            final PsiMethodCallExpression psiMethodCallExpression=(PsiMethodCallExpression)psiElement;
//
//            try {
//                psiMethod = psiMethodCallExpression.resolveMethod();
//                methodName=psiMethodCallExpression.getMethodExpression().getReferenceName();
//                final PsiType psiType = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
//
//                if (psiType != null) {
//                    qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
//                }
//
//            } catch (Exception npe) {
//                methodName="";
//                qualifierClassQualifiedName = "";
//            }
//        }else{
//            if(psiElement instanceof PsiNewExpression){
//                final PsiNewExpression newExpression = (PsiNewExpression) psiElement;
//
//                try {
//                    psiMethod = newExpression.resolveConstructor();
//                    methodName=psiMethod.getName();
//                    final PsiType psiType = psiMethod.getReturnType();
//
//                    if (psiType != null) {
//                        qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
//                    }
//
//                } catch (Exception npe) {
//                    methodName="";
//                    qualifierClassQualifiedName = "";
//                }
//
//            }
//        }
//
//        if (methodName.equals(targetMethod)&&qualifierClassQualifiedName.equals(targetClassCanonical)) {
//
//
//            //  System.out.println(targetMethod+ " Visit -->"+qualifierClassQualifiedName);
//
//                count++;
//
//        } else {
//
//            if (psiMethod != null) {
//                if (!visited.contains(psiMethod)&& IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod)) {
//                    visited.add(psiMethod);
//                    callStack.push(psiMethod);
////                    psiMethod.accept(this);
//                    visitElement(psiMethod);
//                    callStack.pop();
//                }
//            }
//        }
//
//    }
//    private void visitMethodCallDetect(PsiElement psiElement) {
//        String methodName="";
//        String qualifierClassQualifiedName = "";
//        PsiMethod psiMethod=null;
//        PsiMethodCallExpression psiMethodCallExpression=null;
//        PsiNewExpression newExpression=null;
//        if (psiElement instanceof PsiMethodCallExpression) {
//            psiMethodCallExpression=(PsiMethodCallExpression)psiElement;
//
//            try {
//                psiMethod = psiMethodCallExpression.resolveMethod();
//                methodName=psiMethodCallExpression.getMethodExpression().getReferenceName();
//                final PsiType psiType = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
//
//                if (psiType != null) {
//                    qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
//                }
//
//            } catch (Exception npe) {
//                methodName="";
//                qualifierClassQualifiedName = "";
//            }
//        }else{
//            if(psiElement instanceof PsiNewExpression){
//                newExpression = (PsiNewExpression) psiElement;
//
//                try {
//                    psiMethod = newExpression.resolveConstructor();
//                    methodName=psiMethod.getName();
//                    final PsiType psiType = psiMethod.getReturnType();
//
//                    if (psiType != null) {
//                        qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
//                    }
//
//                } catch (Exception npe) {
//                    methodName="";
//                    qualifierClassQualifiedName = "";
//                }
//
//
//            }
//        }
//
//        if (methodName.equals(targetMethod)&&qualifierClassQualifiedName.equals(targetClassCanonical)) {
//
//            count++;
//            //  System.out.println(targetMethod+ " Visit -->"+qualifierClassQualifiedName);
//            if(newExpression!=null){
//                detectedConstructors.add(newExpression);
//            }else{
//                if(psiMethodCallExpression!=null){
//                    detected.add(psiMethodCallExpression);
//                }
//            }
//
//
//        } else {
//
//            if (psiMethod != null) {
//                if (!visited.contains(psiMethod)&& IntelliJUtilFacade.isPsiElementInSourceRoots(psiMethod)) {
//                    visited.add(psiMethod);
//                    callStack.push(psiMethod);
//                   // synchronized (this) {
////                        psiMethod.accept(this);
//                    visitElement(psiMethod);
//                    //}
//                    callStack.pop();
//                }
//            }
//        }
//
//    }


    public static final int MAX_API_CALLS_EXPLORATION_COUNT=2;
    public enum Mode {COUNT, DETECT, EXTENSION_POINT}

    public interface VisitorExtensionPoint {
         Double onVisitingElement(PsiElement psiElement, Object inOutObject);
    }

}
