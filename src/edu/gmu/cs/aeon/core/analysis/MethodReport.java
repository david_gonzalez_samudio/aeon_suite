package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.PsiMethod;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;

/**
 * Created by DavidIgnacio on 4/9/2015.
 */
public class MethodReport{
    public PsiMethod method;
    public HashSet<Report> faultReports=new HashSet<Report>();
    public HashSet<Report> okReports=new HashSet<Report>();
    MethodReport( PsiMethod method){
        this.method=method;
    }
    @Override
    public String toString(){
        return method.toString();
    }
    @Override
    public int hashCode(){
        return method.hashCode();
    }
}
