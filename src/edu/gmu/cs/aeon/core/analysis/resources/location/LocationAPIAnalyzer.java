package edu.gmu.cs.aeon.core.analysis.resources.location;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.psi.*;
import com.intellij.psi.impl.PsiExpressionEvaluator;
import com.intellij.psi.util.PsiUtil;
import com.intellij.util.ArrayUtil;
import edu.gmu.cs.aeon.core.analysis.Analyzer;
import edu.gmu.cs.aeon.core.analysis.AnalyzerImpl;
import edu.gmu.cs.aeon.core.analysis.PsiElementCustomVisitor;
import edu.gmu.cs.aeon.core.analysis.report.*;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 11/4/2014.
 */
public class LocationAPIAnalyzer extends AnalyzerImpl implements Refactorer, Reportable {
    public static final String CLASS_ANDROID_LOCATION_LOCATION_MANAGER = "android.location.LocationManager";
    public static final String METHOD_REMOVE_UPDATES = "removeUpdates";
    private static final String METHOD_REQUEST_LOCATION_UPDATES = "requestLocationUpdates";
    private static final String METHOD_ON_LOCATION_CHANGED = "onLocationChanged";
    private static final String METHOD_GET_LAST_KNOWN_LOCATION = "getLastKnownLocation";
    private static final String METHOD_ON_PAUSE = "onPause";
    private static final String METHOD_ON_RESUME = "onResume";
    private PsiElement targetUIElement;
    public int faultyCount = 0;
    public int totalCount = 0;
    Vector<Report> faultReports = new Vector<Report>();
//    static Set<PsiMethod> visitedMethodsForRequest=new HashSet<PsiMethod>();
//    static Set<PsiMethod> visitedMethodsForRemove=new HashSet<PsiMethod>();
//    static Set<PsiMethod> visitedMethodsForLastKnown=new HashSet<PsiMethod>();

    public Vector<Report> okReports = new Vector<Report>();
    public Mode mode;

    public LocationAPIAnalyzer(){
        faultyCount=0;
        totalCount=0;
        okReports = new Vector<Report>();
        faultReports = new Vector<Report>();

    }
    private static int isRemoveUpdatePresent(@NotNull PsiMethod psiMethod) {
       return PsiElementCustomVisitor.visitAndGetCount(psiMethod, CLASS_ANDROID_LOCATION_LOCATION_MANAGER, METHOD_REMOVE_UPDATES);
    }
    public  static String currentMethodText="";
    private static int isRemoveUpdatePresentWithMethodsText(@NotNull PsiMethod psiMethod) {
         PsiElementCustomVisitor visitor= PsiElementCustomVisitor.visitAndGetDetectedVisitor(psiMethod, CLASS_ANDROID_LOCATION_LOCATION_MANAGER, METHOD_REMOVE_UPDATES);
        StringBuffer methodsText=new StringBuffer();
        int visitedCount=0;
        for(PsiMethod method:visitor.visited){
            visitedCount++;
            methodsText.append(visitedCount+": "+method.getText()+" \n");
        }
        currentMethodText=methodsText.toString();
        return visitor.count;
    }
    private static int isRequestLocationUpdatesPresentWithMethodsText(@NotNull PsiMethod psiMethod) {
        PsiElementCustomVisitor visitor= PsiElementCustomVisitor.visitAndGetDetectedVisitor(psiMethod, CLASS_ANDROID_LOCATION_LOCATION_MANAGER, METHOD_REQUEST_LOCATION_UPDATES);
        StringBuffer methodsText=new StringBuffer();
        int visitedCount=0;
        for(PsiMethod method:visitor.visited){
            visitedCount++;
            methodsText.append(visitedCount+": "+method.getText()+" \n");
        }
        currentMethodText=methodsText.toString();
        return visitor.count;
    }
    private static int isGetLastKnownLocationWithMethodsText(@NotNull PsiMethod psiMethod) {
        PsiElementCustomVisitor visitor= PsiElementCustomVisitor.visitAndGetDetectedVisitor(psiMethod, CLASS_ANDROID_LOCATION_LOCATION_MANAGER, METHOD_GET_LAST_KNOWN_LOCATION);
        StringBuffer methodsText=new StringBuffer();
        int visitedCount=0;
        for(PsiMethod method:visitor.visited){
            visitedCount++;
            methodsText.append(visitedCount+": "+method.getText()+" \n");
        }
        currentMethodText=methodsText.toString();
        return visitor.count;
    }

    private static int isRequestLocationUpdatesPresent(@NotNull PsiMethod psiMethod) {
        return PsiElementCustomVisitor.visitAndGetCount(psiMethod, CLASS_ANDROID_LOCATION_LOCATION_MANAGER, METHOD_REQUEST_LOCATION_UPDATES);
    }
    private static boolean isGetLastKnownLocationInLoop(@NotNull PsiMethod psiMethod) {

       Vector<PsiMethodCallExpression> calls= PsiElementCustomVisitor.visitAndGetDetected(psiMethod,CLASS_ANDROID_LOCATION_LOCATION_MANAGER, METHOD_GET_LAST_KNOWN_LOCATION);

        for(PsiMethodCallExpression call:calls) {

           if(IntelliJUtilFacade.lookUpForContainingLoop(call)!=null){
               return true;
           }
        }
        return false;

    }
    private static int isGetLastKnownLocation(@NotNull PsiMethod psiMethod) {
        return PsiElementCustomVisitor.visitAndGetCount(psiMethod, CLASS_ANDROID_LOCATION_LOCATION_MANAGER, METHOD_GET_LAST_KNOWN_LOCATION);
    }

    private static void refactorRequestLocationUpdatesCalls(@NotNull PsiElement psiElement) {
//        System.out.println("UPDATE CHECK0: "+psiMethod.getName());


//                System.out.println("UPDATE CHECK1: \t"+psiElement.getText());
        if (psiElement instanceof PsiExpressionStatement) {
            PsiExpressionStatement psiMethodExpression = (PsiExpressionStatement) psiElement;
//                    System.out.println("UPDATE CHECK2: \t\t "+psiMethodExpression.getExpression().getText());
            if (psiMethodExpression.getExpression() instanceof PsiMethodCallExpression) {
                PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) psiMethodExpression.getExpression();
                if (psiMethodCallExpression.getMethodExpression().getReferenceName().equals(METHOD_REQUEST_LOCATION_UPDATES)) {
                    String qualifierClassQualifiedName = "";
                    PsiType psiElement1 = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
                    //PsiMethodReferenceUtil.
                    if (psiElement1 != null) {
                        qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiElement1)).getQualifiedName();
                    }
//                            System.out.println("UPDATE CHECK3: \t\t\t " +qualifierClassQualifiedName + ", " + psiMethodCallExpression.getMethodExpression().getReferenceName());
                    if (qualifierClassQualifiedName!=null&&qualifierClassQualifiedName.equals(CLASS_ANDROID_LOCATION_LOCATION_MANAGER)) {
                        PsiComment psiComment = JavaPsiFacade.getInstance(psiElement.getProject()).getElementFactory().createCommentFromText("//Bad Location Request Call " + psiElement.getText(), psiElement);
                        psiElement.replace(psiComment);
                    }
                }
            }

        }

//            locationManager.removeUpdates(this);


    }

    private static void refactorRemoveUpdateCalls(@NotNull PsiElement element) {


//                System.out.println("UPDATE CHECK1: \t"+psiStatement.getText());
        if (element instanceof PsiExpressionStatement) {
            PsiExpressionStatement psiMethodExpression = (PsiExpressionStatement) element;
//                    System.out.println("UPDATE CHECK2: \t\t "+psiMethodExpression.getExpression().getText());
            if (psiMethodExpression.getExpression() instanceof PsiMethodCallExpression) {
                PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) psiMethodExpression.getExpression();
                if (psiMethodCallExpression.getMethodExpression().getReferenceName().equals(METHOD_REMOVE_UPDATES)) {
                    String qualifierClassQualifiedName = "";
                    PsiType psiElement1 = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
                    //PsiMethodReferenceUtil.
                    if (psiElement1 != null) {
                        qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiElement1)).getQualifiedName();
                    }
//                            System.out.println("UPDATE CHECK3: \t\t\t " +qualifierClassQualifiedName + ", " + psiMethodCallExpression.getMethodExpression().getReferenceName());
                    if (qualifierClassQualifiedName!=null&&qualifierClassQualifiedName.equals(CLASS_ANDROID_LOCATION_LOCATION_MANAGER)) {
                        PsiComment psiComment = JavaPsiFacade.getInstance(element.getProject()).getElementFactory().createCommentFromText("//Bad Location Request Call " + element.getText(), element);
                        element.replace(psiComment);
                    }
                }
            }

        }

//            locationManager.removeUpdates(this);


    }

    private static void addRemoveUpdateCall(@NotNull PsiMethod psiMethod) {
        PsiCodeBlock psiMethodCodeBlock = psiMethod.getBody();
        boolean isEmpty = true;
        PsiStatement psiComment = JavaPsiFacade.getInstance(psiMethod.getProject()).getElementFactory().createStatementFromText("locationManager.removeUpdates(this);", null);
        for (PsiStatement psiStatement : psiMethodCodeBlock.getStatements()) {
            psiStatement.addBefore(psiComment, psiStatement.getFirstChild());
            return;
        }
        if (isEmpty) {
            psiMethodCodeBlock.add(psiComment);
        }

    }

    private static void addRequestUpdateCall(@NotNull PsiMethod psiMethod) {

        PsiCodeBlock psiMethodCodeBlock = psiMethod.getBody();
        boolean isEmpty = true;
        PsiStatement psiComment = JavaPsiFacade.getInstance(psiMethod.getProject()).getElementFactory().createStatementFromText("locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);", null);
        for (PsiStatement psiStatement : psiMethodCodeBlock.getStatements()) {
            psiStatement.addBefore(psiComment, psiStatement.getFirstChild());
            return;
        }
        if (isEmpty) {
            psiMethodCodeBlock.add(psiComment);
        }


    }

    @Override
    public boolean isUseCase(PsiElement element) {
        return isUseCase001(element) && !isRefactoredUseCase(element);
    }



    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {
        return isUseCase001Refactored(element);
    }

    public boolean isUseCase001(@NotNull PsiElement element) {
//        if (mode == Analyzer.Mode.BATCH) {
            if (element instanceof PsiAnonymousClass||element instanceof PsiClass) {
                PsiClass psiClass = (PsiClass) element;

                if (AndroidUtilFacade.implementsLocationListener(psiClass)) {
//                    System.out.println("LOCATION CHECK: loclist true ");
                    return true;
                }
                PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getMethods(), psiClass.getConstructors());
                int isRemoveUpdatePresent = 0;
                int isRequestLocationUpdatesPresent = 0;
                int isGetLastKnownLocationPresent = 0;
                for (PsiMethod psiMethod1 : psiMethods) {
                    isRemoveUpdatePresent += isRemoveUpdatePresent(psiMethod1);
                    isRequestLocationUpdatesPresent += isRequestLocationUpdatesPresent(psiMethod1);
                    isGetLastKnownLocationPresent += isGetLastKnownLocation(psiMethod1);
                }
//                System.out.println("LOCATION CHECK: rem = " + isRemoveUpdatePresent + ", req= " + isRequestLocationUpdatesPresent+ ", last= " +isGetLastKnownLocationPresent);
                if(isRemoveUpdatePresent + isRequestLocationUpdatesPresent+isGetLastKnownLocationPresent>0) {
                    return true;
                } else {
                    return false;
                }

            }
//        }else{
//            if (element instanceof PsiMethod) {
//                int isRemoveUpdatePresent = 0;
//                int isRequestLocationUpdatesPresent = 0;
//                int isGetLastKnownLocationPresent = 0;
//                PsiMethod psiMethod1=(PsiMethod)element;
//                    isRemoveUpdatePresent += isRemoveUpdatePresent(psiMethod1);
//                    isRequestLocationUpdatesPresent += isRequestLocationUpdatesPresent(psiMethod1);
//                    isGetLastKnownLocationPresent += isGetLastKnownLocation(psiMethod1);
//
//                System.out.println("LOCATION CHECK: rem = " + isRemoveUpdatePresent + ", req= " + isRequestLocationUpdatesPresent+ ", last= " +isGetLastKnownLocationPresent);
//                if(isRemoveUpdatePresent + isRequestLocationUpdatesPresent+isGetLastKnownLocationPresent>0) {
//                    return true;
//                } else {
//                    return false;
//                }
//
//            }
//
//        }
        return false;
    }
    public void isRefactoredMethod(PsiMethod psiMethod1) {

    }
    public boolean isUseCase001Refactored(@NotNull PsiElement element) {

//        if (mode == Mode.BATCH) {
            if (element instanceof PsiAnonymousClass||element instanceof PsiClass) {
                PsiClass psiClass = (PsiClass) element;
                PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getMethods(), psiClass.getConstructors());
                boolean faulty=false;
                int isRemoveUpdatePresentInClass=0;
                int isRequestUpdatePresentInClass=0;
                for (PsiMethod psiMethod1 : psiMethods) {
//                    System.out.println("LOC meth "+psiMethod1.getName());
                    if (AndroidUtilFacade.implementsLocationListener(psiClass) ) {
                        int isRemoveUpdatePresent = 0;

                    isRemoveUpdatePresent += isRemoveUpdatePresent(psiMethod1);
                    if (AndroidUtilFacade.isMethodCaseOfAndroidComponent(psiMethod1, AndroidUtilFacade.MethodCase.RESOURCE_RELEASE)){
                       if(isRemoveUpdatePresent>0) {
                           totalCount++;
                           reason = Reason.USES_LOCATION_LISTENER_AND_REMOVE_UPDATES_IN_ON_PAUSE;

                           okReports.add( ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.CORRECT_IMPLEMENTATION, null, psiMethod1, reason.name(), psiMethod1.getText(), true));

                       }else{
                           totalCount++;
                           faultyCount ++;
                           reason = Reason.USES_LOCATION_LISTENER_AND_REMOVE_UPDATES_NOT_IN_ON_PAUSE;
                           Report report = ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT, null, psiMethod1, reason.name(), psiMethod1.getText(), true);
                           faultReports.add(report);
                       }
                    isRemoveUpdatePresentInClass+=isRemoveUpdatePresent;
                    }
                        int isRequestLocationUpdatesPresent = 0;
                    isRequestLocationUpdatesPresent+=isRequestLocationUpdatesPresent(psiMethod1);
                    if (AndroidUtilFacade.isMethodCaseOfAndroidComponent(psiMethod1, AndroidUtilFacade.MethodCase.RESOURCE_ACQUIRE)) {
                            if (isRequestLocationUpdatesPresent >0) {
                                if(isRequestGreedy(psiMethod1)) {
                                    totalCount++;
                                    faultyCount++;
                                }else{
                                    totalCount++;
                                    reason = Reason.USES_LOCATION_LISTENER_AND_REQUEST_UPDATES_IN_ON_RESUME;
                                    okReports.add(ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.CORRECT_IMPLEMENTATION, null, psiMethod1, reason.name(), psiMethod1.getText(), true));

                                }
                            }else{
                                totalCount++;
                                faultyCount ++;
                                reason = Reason.USES_LOCATION_LISTENER_AND_REQUEST_UPDATES_NOT_IN_ON_RESUME;
                                Report report = ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT, null, psiMethod1, reason.name(), psiMethod1.getText(), true);
                                faultReports.add(report);
                            }
                        isRequestUpdatePresentInClass+=isRequestLocationUpdatesPresent;
                    }

//pending outside resume and others

                }else {
                        int isRemoveUpdatePresent = 0;

                        isRemoveUpdatePresent += isRemoveUpdatePresent(psiMethod1);

                        isRemoveUpdatePresentInClass+=isRemoveUpdatePresent;


                        int isRequestLocationUpdatesPresent = 0;
                        isRequestLocationUpdatesPresent += isRequestLocationUpdatesPresent(psiMethod1);

                        isRequestUpdatePresentInClass+=isRequestLocationUpdatesPresent;
                    }
                    int isGetLastKnownLocationPresent = isGetLastKnownLocation(psiMethod1);


                    if(isGetLastKnownLocationPresent>0) {
                        totalCount++;

                        if (isGetLastKnownLocationInLoop(psiMethod1)) {
                            reason = Reason.KNOWN_LOCATION_LOOP;
                            faultyCount++;
                            Report report = ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT, null, psiMethod1, reason.name(), psiMethod1.getText(), true);
                            faultReports.add(report);
                        }else{
                            Report report=ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.CORRECT_IMPLEMENTATION, null, psiMethod1, reason.name(), psiMethod1.getText(), true);
                            okReports.add(report);
                        }
                    }

//                    System.out.println("UPDATE CHECK: " + psiClass.getName() + ", loc " + AndroidUtilFacade.implementsLocationListener(psiClass) + ", cl " + AndroidUtilFacade.isClassSubclassOfComponent(psiClass));

                   }

                if(isRemoveUpdatePresentInClass>0&&isRequestUpdatePresentInClass>0){
                    reason=Reason.BOTH_REQUEST_AND_REMOVE_PRESENT;
                    totalCount++;
                    okReports.add(ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.CORRECT_IMPLEMENTATION, null, element, reason.name(), psiClass.getQualifiedName(), true));


                }else{
                    if(isRequestUpdatePresentInClass>0&&isRemoveUpdatePresentInClass==0){
                        reason=Reason.REQUEST_PRESENT_BUT_REMOVE_NOT_PRESENT;
                        totalCount++;
                        faultyCount++;
                        if(PsiUtil.isLocalOrAnonymousClass(psiClass)){
                            Report report = ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT, null, element, reason.name(), "LocalOrAnonymousClass: "+element.getText(), true);
                            faultReports.add(report);
                        }else {
                            Report report = ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT, null, element, reason.name(), element.getText(), true);
                            faultReports.add(report);
                        }

                    }else{
                        if(isRequestUpdatePresentInClass==0&&isRemoveUpdatePresentInClass>0){
                            reason=Reason.REMOVE_PRESENT_BUT_REQUEST_NOT_PRESENT;
                            totalCount++;
                            faultyCount++;
                            if(PsiUtil.isLocalOrAnonymousClass(psiClass)){
                                Report report = ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT, null, element, reason.name(), "LocalOrAnonymousClass: "+element.getText(), true);
                                faultReports.add(report);

                            }else {

                                Report report = ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT, null, element, reason.name(), element.getText(), true);
                                faultReports.add(report);
                            }

                        }

                    }

                }


            }
//        }else{
        targetUIElement = element;
        if(faultyCount>0){
            return true;
        }

//            targetUIElement = element;
//
//        }
        return false;
    }

    private boolean isRequestGreedy(PsiMethod psiMethod1) {
        Vector<PsiMethodCallExpression> calls=PsiElementCustomVisitor.visitAndGetDetected(psiMethod1, CLASS_ANDROID_LOCATION_LOCATION_MANAGER, METHOD_REQUEST_LOCATION_UPDATES);
        for(PsiMethodCallExpression psiMethodCallExpression:calls){
            PsiExpression[] expressions=psiMethodCallExpression.getArgumentList().getExpressions();
            if(expressions.length>3){
//                System.out.println("types: " + expressions[0].getType()+", "+expressions[1].getType()+", "+expressions[2].getType());
                if(!expressions[0].getType().equalsToText("int")){
                    try {
                        Object m = new PsiExpressionEvaluator().computeConstantExpression(expressions[1], false);
                        boolean isIntm=false;
                        if(expressions[1].getType().equalsToText("Integer")||expressions[1].getType().equalsToText("int")){
                            isIntm=true;
                        }

                        long minTime =  0;
                        if(isIntm) {
                            minTime   =(Integer) m;
                        }else{
                            minTime=(Long)m;
                        }
                        Object d = new PsiExpressionEvaluator().computeConstantExpression(expressions[2], false);
                        boolean isIntd=false;
                        if(expressions[2].getType().equalsToText("Integer")||expressions[2].getType().equalsToText("int")){
                            isIntd=true;
                        }

                        long minDistance = 0;
                        if(isIntd) {
                            minDistance   =(Integer) d;
                        }else{
                            minDistance=(Long)d;
                        }

                        if(minTime<GREEDY_TIME||minDistance<GREEDY_DISTANCE){
                            Report report=ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT,null,psiMethodCallExpression,Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), "[T="+minTime+", D="+minDistance+"]"+psiMethodCallExpression.getText(), true);
                            faultReports.add(report);
                            return true;
                        }else{
                            okReports.add(ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.CORRECT_IMPLEMENTATION, null, psiMethodCallExpression, Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), psiMethodCallExpression.getText(), true));

                        }
                    }catch(Exception e){

                        Report report=ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.REQUIRES_RUNTIME_VALUES_NOT_COMPUTABLE_STATICALLY,null,psiMethodCallExpression,Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), psiMethodCallExpression.getText(), true);
                        faultReports.add(report);
                        return true;
                    }
//                    SummaryReport summaryReport=ReportFactory.buildReport(AnalysisCategory.LOCATION,PredictionType.UNKNOWN_CASE_REQUIRES_HUMAN,null,psiMethodCallExpression,Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), psiMethodCallExpression.getText(), true);
//                    faultReports.add(summaryReport);
                }else{
                    if(expressions[0].getType().equalsToText("int")||expressions[0].getType().equalsToText("Integer")||expressions[0].getType().equalsToText("long")||expressions[0].getType().equalsToText("Long")){
                        try {
                            Object m = new PsiExpressionEvaluator().computeConstantExpression(expressions[0], false);
                            boolean isIntm=false;
                            if(expressions[0].getType().equalsToText("Integer")||expressions[0].getType().equalsToText("int")){
                                isIntm=true;
                            }

                            long minTime =  0;
                            if(isIntm) {
                                minTime   =(Integer) m;
                            }else{
                                minTime=(Long)m;
                            }
                            Object d = new PsiExpressionEvaluator().computeConstantExpression(expressions[1], false);
                            boolean isIntd=false;
                            if(expressions[1].getType().equalsToText("Integer")||expressions[1].getType().equalsToText("int")){
                                isIntd=true;
                            }

                            long minDistance = 0;
                            if(isIntd) {
                                minDistance   =(Integer) d;
                            }else{
                                minDistance=(Long)d;
                            }
                            if(minTime<GREEDY_TIME||minDistance<GREEDY_DISTANCE){
                                Report report=ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.ENERGY_FAULT,null,psiMethodCallExpression,Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), "[T="+minTime+", D="+minDistance+"]"+psiMethodCallExpression.getText(), true);
                                faultReports.add(report);
                                return true;
                            }else{
                                okReports.add(ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.CORRECT_IMPLEMENTATION, null, psiMethodCallExpression, Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), psiMethodCallExpression.getText(), true));

                            }
                        }catch(Exception e){

                            Report report=ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.REQUIRES_RUNTIME_VALUES_NOT_COMPUTABLE_STATICALLY,null,psiMethodCallExpression,Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), psiMethodCallExpression.getText(), true);
                            faultReports.add(report);
                            return true;
                        }
//                        SummaryReport summaryReport=ReportFactory.buildReport(AnalysisCategory.LOCATION,PredictionType.UNKNOWN_CASE_REQUIRES_HUMAN,null,psiMethodCallExpression,Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), psiMethodCallExpression.getText(), true);
//                        faultReports.add(summaryReport);
                    }else{
                        Report report=ReportFactory.buildReport(AnalysisCategory.LOCATION, PredictionType.UNKNOWN_CASE_REQUIRES_HUMAN,null,psiMethodCallExpression,Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), psiMethodCallExpression.getText(), true);
                        faultReports.add(report);
                    }
                }
//                System.out.println("p 1 "+expressions[0].getType()+"-- "+new PsiExpressionEvaluator().computeConstantExpression(expressions[0], false));
//                System.out.println("p 2 "+expressions[1].getType()+"-- "+new PsiExpressionEvaluator().computeConstantExpression(expressions[1], false));
//                System.out.println("p 3 " + expressions[2].getType()+"-- "+new PsiExpressionEvaluator().computeConstantExpression(expressions[2], false));

            }
        }


        return false;
    }
    private static final long GREEDY_TIME = 1000 * 60 *30;//ms
    private static final long GREEDY_DISTANCE = 100;//m

    @Override
    public Vector<Report> getReports(PsiElement psiElement) {
        return faultReports;
    }

    private enum Reason {
        REMOVE_UPDATES_NOT_IN_ON_PAUSE,
        REMOVE_UPDATES_IN_ON_PAUSE,
        REQUEST_UPDATES_NOT_IN_ON_RESUME,
        REQUEST_UPDATES_IN_ON_RESUME,
        OK,
        KNOWN_LOCATION_LOOP,
        BOTH_REQUEST_AND_REMOVE_PRESENT, REQUEST_PRESENT_BUT_REMOVE_NOT_PRESENT, REMOVE_PRESENT_BUT_REQUEST_NOT_PRESENT, USES_LOCATION_LISTENER_AND_REMOVE_UPDATES_IN_ON_PAUSE, USES_LOCATION_LISTENER_AND_REMOVE_UPDATES_NOT_IN_ON_PAUSE, USES_LOCATION_LISTENER_AND_REQUEST_UPDATES_IN_ON_RESUME, USES_LOCATION_LISTENER_AND_REQUEST_UPDATES_NOT_IN_ON_RESUME, REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY
    }
    public Reason reason=Reason.OK;
    @Override
    public PsiElement annotateUseCase(PsiElement element, AnnotationHolder holder) {
        return null;
    }

    @Override
    public PsiElement refactorElement(final PsiElement element) {
        ApplicationManager.getApplication().runWriteAction(new Runnable() {
            @Override
            public void run() {


                if (element instanceof PsiClass) {
                    PsiClass psiClass = (PsiClass) element;
                    PsiMethod[] psiMethods = psiClass.getMethods();
                    boolean hasLocChan = false;
                    for (PsiMethod psiMethod : psiMethods) {
                        if (psiMethod.getName().equals(METHOD_ON_LOCATION_CHANGED)) {
                            hasLocChan = true;
                        }
                    }


                    if (hasLocChan) {

                        //    System.out.println("REF CHECK: " + psiClass.getName() + ", loc " + AndroidUtilFacade.implementsLocationListener(psiClass) + ", cl " + AndroidUtilFacade.isClassSubclassOfActivity(psiClass));
                        if (psiClass != null && AndroidUtilFacade.implementsLocationListener(psiClass) && AndroidUtilFacade.isClassSubclassOfActivity(psiClass)) {

                            boolean isRemoveUpdatePresent = false;
                            boolean isRequestLocationUpdatesPresent = false;

                            for (PsiMethod psiMethod1 : psiMethods) {
                                boolean visitMethod = true;
                                if (psiMethod1.getName().equals(METHOD_ON_PAUSE)) {
                                    visitMethod = false;
                                    isRemoveUpdatePresent = isRemoveUpdatePresent ? true : isRemoveUpdatePresent(psiMethod1)>0;
                                    if (!isRemoveUpdatePresent) addRemoveUpdateCall(psiMethod1);
                                }
                                if (psiMethod1.getName().equals(METHOD_ON_RESUME)) {
                                    visitMethod = false;
                                    isRequestLocationUpdatesPresent = isRequestLocationUpdatesPresent ? true : isRequestLocationUpdatesPresent(psiMethod1)>0;
                                    if (!isRequestLocationUpdatesPresent) addRequestUpdateCall(psiMethod1);
                                }
                                if (visitMethod) {
                                    PsiElementCustomVisitor.visitAndRefactor(psiMethod1, new PsiElementCustomVisitor.Fixer() {
                                        @Override
                                        public void doFix(@NotNull PsiElement psiElement) {
                                            refactorRemoveUpdateCalls(psiElement);
                                            refactorRequestLocationUpdatesCalls(psiElement);
                                        }
                                    });
                                }

                            }
                            if (isRemoveUpdatePresent && isRequestLocationUpdatesPresent) {
                                // return element;
                            } else {
                                // return element;
                            }

                        }
                    }

                }
            }
        });
        return element;
    }

    @Override
    public PsiElement previewRefactorElement(PsiElement element) {
        //todo preview fix
        return targetUIElement;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return EnergyIcons.ENERGY_MARKER_WARNING_RESOURCE_CASE;
    }

    @Override
    public int getId() {
        return 01;
    }

    @Override
    public PsiElement getElement() {
        return targetUIElement;
    }

    @NotNull
    @Override
    public String getText() {
        return "Location API";
    }


}
