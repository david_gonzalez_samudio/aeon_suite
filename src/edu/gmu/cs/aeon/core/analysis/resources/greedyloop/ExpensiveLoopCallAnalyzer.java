package edu.gmu.cs.aeon.core.analysis.resources.greedyloop;

//import com.android.tools.lint.checks.WakelockDetector;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.*;
import com.intellij.psi.impl.PsiExpressionEvaluator;
import com.intellij.psi.util.PsiUtil;
import com.intellij.util.ArrayUtil;
import edu.gmu.cs.aeon.core.analysis.Analyzer;
import edu.gmu.cs.aeon.core.analysis.AnalyzerImpl;
import edu.gmu.cs.aeon.core.analysis.PsiElementCustomVisitor;
import edu.gmu.cs.aeon.core.analysis.report.*;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.Vector;

//import org.jetbrains.android.inspections.lint.AndroidLintUtil;

/**
 * Created by DavidIgnacio on 11/6/2014.
 */
public class ExpensiveLoopCallAnalyzer extends AnalyzerImpl implements Refactorer, Reportable {
    public static final String ANDROID_OS_POWER_MANAGER_WAKE_LOCK = "android.os.PowerManager.WakeLock";
    public static final String ANDROID_OS_POWER_MANAGER = "android.os.PowerManager";
    public static final String METHOD_ON_CREATE = "onCreate";
    private static final String METHOD_NEW_WAKE_LOCK = "newWakeLock";
    private static final String METHOD_ACQUIRE = "acquire";
    private static final String METHOD_RELEASE = "release";
    private static final String METHOD_IS_HELD = "isHeld";
    private static final String METHOD_ON_RESUME = "onResume";
    private static final String METHOD_ON_PAUSE = "onPause";
    private static final String METHOD_ON_DESTROY = "onDestroy";
    private static final String METHOD_SET_KEEP_SCREEN_ON = "setKeepScreenOn";
    public int faultyCount = 0;
    public int totalCount = 0;
    Vector<Report> faultReports = new Vector<Report>();

    public Vector<Report> okReports = new Vector<Report>();

    public int getReleaseCount(PsiMethod psiMethod) {
        return PsiElementCustomVisitor.visitAndGetCount(psiMethod, ANDROID_OS_POWER_MANAGER_WAKE_LOCK, METHOD_RELEASE);
    }

    public int getAcquireCount(PsiMethod psiMethod) {
        return PsiElementCustomVisitor.visitAndGetCount(psiMethod, ANDROID_OS_POWER_MANAGER_WAKE_LOCK, METHOD_ACQUIRE);
    }

    public int getNewWakeLockCount(PsiMethod psiMethod) {
        return PsiElementCustomVisitor.visitAndGetCount(psiMethod, ANDROID_OS_POWER_MANAGER, METHOD_NEW_WAKE_LOCK)+ PsiElementCustomVisitor.visitAndGetCount(psiMethod, ANDROID_OS_POWER_MANAGER, METHOD_SET_KEEP_SCREEN_ON);
    }

    @Override
    public boolean isUseCase(PsiElement element) {
        return isUseCase001(element) && !isRefactoredUseCase(element);
    }

    public void isRefactoredMethod(PsiMethod psiMethod1) {
         int acCount = getAcquireCount(psiMethod1);
        int releaseCount = getReleaseCount(psiMethod1);
        int newWakeLockCount = getNewWakeLockCount(psiMethod1);
        WakeLockVisitor wakeLockVisitor = new WakeLockVisitor();
        PsiElementCustomVisitor.visitAndRefactor(psiMethod1, wakeLockVisitor);
        boolean isHappen=false;

        if (psiMethod1.getName().equals(METHOD_ON_RESUME)) {
            if ((acCount > 0 && releaseCount == 0) && !wakeLockVisitor.isBad) {
                wakeLockVisitor.isBad = false;
                wakeLockVisitor.reason = Reason.CALLED_ACQUIRE_ON_RESUME;
            }

        } else {
            if (psiMethod1.getName().equals(METHOD_ON_PAUSE)) {
                if (releaseCount > 0 && !wakeLockVisitor.isBad) {
                    wakeLockVisitor.isBad = false;
                    wakeLockVisitor.reason = Reason.CALLED_RELEASE_ON_PAUSE;
                }

            } else {
                if (psiMethod1.getName().equals(METHOD_ON_DESTROY)) {
                    if (releaseCount > 0 && !wakeLockVisitor.isBad) {
                        wakeLockVisitor.isBad = true;
                        wakeLockVisitor.reason = Reason.CALLED_RELEASE_ON_DESTROY;
                    }

                } else {

                    if (acCount > 0 && releaseCount == 0) {
                        Vector<PsiMethodCallExpression> calls = PsiElementCustomVisitor.visitAndGetDetected(psiMethod1, ANDROID_OS_POWER_MANAGER_WAKE_LOCK, METHOD_ACQUIRE);
                          int ok=0;
                        for(PsiMethodCallExpression psiMethodCallExpression:calls){

                            if(psiMethodCallExpression.getArgumentList().getExpressions().length>0){
                                ok++;
                            }
                        }
                        if(ok>0){

                           isHappen= isRequestGreedyHappen(psiMethod1);
                            if(!isHappen) {
                                wakeLockVisitor.isBad = false;
                                wakeLockVisitor.reason = Reason.WAKELOCK_ACQUIRE_WITH_PARAMS;
                            }
                        }else {
                            wakeLockVisitor.isBad = true;
                            wakeLockVisitor.reason = Reason.WAKE_LOCK_NOT_RELEASED;
                        }
                    } else {
                        // System.out.println(psiMethod1.getName()+" "+wakeLockVisitor.reason);
                        if (wakeLockVisitor.reason != Reason.WAKELOCK_ACQUIRE_WITH_PARAMS_BUT_GREEDY&&wakeLockVisitor.reason != Reason.WAKELOCK_ACQUIRE_WITH_PARAMS&&wakeLockVisitor.reason != Reason.IN_FINALLY_BLOCK && wakeLockVisitor.reason != Reason.IS_HELD_CONDITION && wakeLockVisitor.callsCountInBetween > 0 && !wakeLockVisitor.isBad && (acCount > 0 && releaseCount > 0)) {
                            wakeLockVisitor.isBad = true;
                            wakeLockVisitor.reason = Reason.NEEDS_FINALLY_BLOCK;
                        }
                    }

                }


            }

        }

        if(!isHappen) {
            PredictionType predictionType = PredictionType.CORRECT_IMPLEMENTATION;
            Report report = ReportFactory.buildReport(AnalysisCategory.WAKELOCK, predictionType, null, psiMethod1, wakeLockVisitor.reason.name(), psiMethod1.getText(), true);
            if (wakeLockVisitor.isBad) {
                faultyCount++;
                report.predictionType = PredictionType.ENERGY_FAULT;
                faultReports.add(report);
            } else {
                if (acCount > 0 || releaseCount > 0 || newWakeLockCount > 0) {
                    totalCount++;
                    okReports.add(report);
                }
            }
            System.out.println("WAKELOCK CHECK :" + psiMethod1.getName() + "--> " + report.getReportData());
        }

    }
    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {

        if (mode == Mode.BATCH) {
        if (element instanceof PsiClass) {

            //    System.out.println("Class >> " + ((PsiClass) element).getQualifiedName());

            PsiClass psiClass = (PsiClass) element;


            PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getMethods(), psiClass.getConstructors());

            faultyCount = 0;
            totalCount = 0;
            for (PsiMethod psiMethod1 : psiMethods) {

                isRefactoredMethod(psiMethod1);

            }


            System.out.println("WAKELOCK CHECK: using = " + totalCount + ", fa= " + faultyCount);


            if (faultyCount == 0) {
                return false;
            } else {
                return true;
            }

        }
        } else {
            if (element instanceof PsiMethod) {
                isRefactoredMethod((PsiMethod) element);
                System.out.println("WAKELOCK CHECK: using = " + totalCount + ", fa= " + faultyCount);


                if (faultyCount == 0) {
                    return false;
                } else {
                    return true;
                }
            }
        }


        return false;
    }

    public boolean isUseCase001(@NotNull PsiElement element) {

        if (mode == Mode.BATCH) {
            if (element instanceof PsiClass) {
                System.out.println("Class : " + ((PsiClass) element).getQualifiedName());

                PsiClass psiClass = (PsiClass) element;
//           if (AndroidUtilFacade.isClassSubclassOfComponent(psiClass)) {
                PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getMethods(), psiClass.getConstructors());
                int acCount = 0;
                int releaseCount = 0;
                int newWakeLockCount = 0;
                for (PsiMethod psiMethod1 : psiMethods) {
                    releaseCount += getReleaseCount(psiMethod1);
                    acCount += getAcquireCount(psiMethod1);
                    newWakeLockCount += getNewWakeLockCount(psiMethod1);
                }
//                System.out.println("WAKELOCK CHECK: acc = " + acCount + ", rl= " + releaseCount );

                if (acCount + releaseCount + newWakeLockCount > 0) {
                    return true;
                } else {
                    return false;
                }
//                }
            }
        } else {
            if (element instanceof PsiMethod) {

                int acCount = 0;
                int releaseCount = 0;
                int newWakeLockCount = 0;
                PsiMethod psiMethod1 = (PsiMethod) element;
                System.out.println("Method >> " + psiMethod1.getName());
                releaseCount += getReleaseCount(psiMethod1);
                acCount += getAcquireCount(psiMethod1);
                newWakeLockCount += getNewWakeLockCount(psiMethod1);
                if (acCount + releaseCount + newWakeLockCount > 0) {
                    return true;
                } else {
                    return false;
                }
            }

        }

//        }


        return false;
    }



    @Override
    public PsiElement annotateUseCase(PsiElement element, AnnotationHolder holder) {
        return null;
    }

    @Override
    public PsiElement refactorElement(PsiElement element) {
        return null;
    }

    @Override
    public PsiElement previewRefactorElement(PsiElement element) {
        return null;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return EnergyIcons.ENERGY_MARKER_WARNING_RESOURCE_CASE;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public PsiElement getElement() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "WakeLock";
    }

    @Override
    public Vector<Report> getReports(PsiElement psiElement) {
        return faultReports;
    }

    private enum Reason {
        BAD_CONDITION,//default, any case not in the conditions group
        BAD_FLAG,
        NULL_CONDITION,
        IS_HELD_CONDITION,
        NULL_IS_HELD_CONDITION,
        UNKNOWN_CONDITION,
        NOT_IN_FINALLY_BLOCK,
        CALLED_RELEASE_ON_DESTROY,
        CALLED_RELEASE_ON_PAUSE,
        CALLED_ACQUIRE_ON_RESUME,
        WAKE_LOCK_NOT_RELEASED,
        NEEDS_FINALLY_BLOCK,
        IN_FINALLY_BLOCK,
        LOCKING_TOO_EARLY,
        WAKELOCK_ACQUIRE_WITH_PARAMS_BUT_GREEDY,
        WAKELOCK_ACQUIRE_WITH_PARAMS,
        DEPRECATED_SET_KEEP_SCREEN_ON

    }
   int  GREEDY_TIME=1000*5;
    private boolean isRequestGreedyHappen(PsiMethod psiMethod1) {
        Vector<PsiMethodCallExpression> calls=PsiElementCustomVisitor.visitAndGetDetected(psiMethod1, ANDROID_OS_POWER_MANAGER_WAKE_LOCK, METHOD_ACQUIRE);
        for(PsiMethodCallExpression psiMethodCallExpression:calls){
            PsiExpression[] expressions=psiMethodCallExpression.getArgumentList().getExpressions();
            if(expressions.length>0){

                    if(expressions[0].getType().equalsToText("int")||expressions[0].getType().equalsToText("long")){
                        boolean isInt=false;
                        if(expressions[0].getType().equalsToText("int")){
                            isInt=true;
                        }
                        try {
                            Object m = new PsiExpressionEvaluator().computeConstantExpression(expressions[0], false);
                            long minTime =0;
                            if(isInt) {
                             minTime   =(Integer) m;
                            }else{
                                minTime=(Long)m;
                            }
                            if(minTime>GREEDY_TIME){
                                Report report=ReportFactory.buildReport(AnalysisCategory.WAKELOCK, PredictionType.ENERGY_FAULT,null,psiMethodCallExpression, Reason.WAKELOCK_ACQUIRE_WITH_PARAMS_BUT_GREEDY.name(), "[T="+minTime+"]"+psiMethodCallExpression.getText(), true);
                                faultyCount++;
                                totalCount++;
                                faultReports.add(report);
                                return true;
                            }else{
                                Report report=ReportFactory.buildReport(AnalysisCategory.WAKELOCK, PredictionType.CORRECT_IMPLEMENTATION,null,psiMethodCallExpression, Reason.WAKELOCK_ACQUIRE_WITH_PARAMS.name(), "[T="+minTime+"]"+psiMethodCallExpression.getText(), true);
                                okReports.add(report);
                                totalCount++;
                                return true;
                            }
                        }catch(Exception e){

                            Report report=ReportFactory.buildReport(AnalysisCategory.WAKELOCK, PredictionType.REQUIRES_RUNTIME_VALUES_NOT_COMPUTABLE_STATICALLY,null,psiMethodCallExpression, Reason.WAKELOCK_ACQUIRE_WITH_PARAMS_BUT_GREEDY.name(), "[T=N/A]"+psiMethodCallExpression.getText(), true);
                            faultReports.add(report);
                            faultyCount++;
                            totalCount++;
                            return true;
                        }
//                        SummaryReport summaryReport=ReportFactory.buildReport(AnalysisCategory.LOCATION,PredictionType.UNKNOWN_CASE_REQUIRES_HUMAN,null,psiMethodCallExpression,Reason.REQUEST_UPDATES_IN_ON_RESUME_BUT_GREEDY.name(), psiMethodCallExpression.getText(), true);
//                        faultReports.add(summaryReport);
                    }else{
                        Report report=ReportFactory.buildReport(AnalysisCategory.WAKELOCK, PredictionType.UNKNOWN_CASE_REQUIRES_HUMAN,null,psiMethodCallExpression, Reason.WAKELOCK_ACQUIRE_WITH_PARAMS_BUT_GREEDY.name(),"[T=N/A]"+ psiMethodCallExpression.getText(), true);
                        faultReports.add(report);
                        faultyCount++;
                        totalCount++;
                        return true;
                    }

//                System.out.println("p 1 "+expressions[0].getType()+"-- "+new PsiExpressionEvaluator().computeConstantExpression(expressions[0], false));
//                System.out.println("p 2 "+expressions[1].getType()+"-- "+new PsiExpressionEvaluator().computeConstantExpression(expressions[1], false));
//                System.out.println("p 3 " + expressions[2].getType()+"-- "+new PsiExpressionEvaluator().computeConstantExpression(expressions[2], false));

            }
        }


        return false;
    }

    private class WakeLockVisitor implements PsiElementCustomVisitor.Fixer {
        boolean isBad = false;
        Reason reason = Reason.BAD_CONDITION;
        int callsCountInBetween = 0;
        int acquireCounting = 0;
        int releaseCounting = 0;
        PsiElementCustomVisitor visitor;
        int depth = 0;
        int MAX_DEPTH = 10;

        private boolean isConditionBad01(PsiBinaryExpression psiExpression) {
            // solving mWakeLock != null
            boolean isBad = true;
            // System.out.println("Psiexp text= " + psiExpression);

            int whereIsWakeLockID = 0;
            PsiElement operand = psiExpression.getLOperand();
            String wakelockQualifierClassQualifiedName = IntelliJUtilFacade.getResolvedClassQualifiedName(operand);

            if (wakelockQualifierClassQualifiedName.equals(ANDROID_OS_POWER_MANAGER_WAKE_LOCK)) {
                whereIsWakeLockID = -1;

            } else {
                operand = psiExpression.getROperand();
                wakelockQualifierClassQualifiedName = IntelliJUtilFacade.getResolvedClassQualifiedName(operand);
                if (wakelockQualifierClassQualifiedName.equals(ANDROID_OS_POWER_MANAGER_WAKE_LOCK)) {
                    whereIsWakeLockID = 1;
                }
            }

            if (whereIsWakeLockID > 0) {
                if (IntelliJUtilFacade.isNullKeyword(psiExpression.getLOperand())) {
                    isBad = false;
                }

            } else {
                if (whereIsWakeLockID < 0) {
                    if (IntelliJUtilFacade.isNullKeyword(psiExpression.getROperand())) {
                        isBad = false;
                    }
                }

            }

            return isBad;

        }

        private boolean isConditionBad02(PsiMethodCallExpression psiMethodCallExpression) {
            // solving  mWakeLock.isHeld()
            boolean isBad = true;
            if (PsiElementCustomVisitor.visitAndGetCount(psiMethodCallExpression, ANDROID_OS_POWER_MANAGER_WAKE_LOCK, METHOD_IS_HELD) > 0) {
                isBad = false;
            }
            return isBad;
        }

        private boolean isConditionBadMain(PsiExpression psiExpression) {
            // solving  mWakeLock.isHeld() ** mWakeLock != null,   or one of each
            boolean isBad = true;
            try {
                if (psiExpression instanceof PsiBinaryExpression) {
                    PsiBinaryExpression psiBinaryExpression = (PsiBinaryExpression) psiExpression;
                    if (psiBinaryExpression.getLOperand() instanceof PsiBinaryExpression) {
                        if (psiBinaryExpression.getROperand() instanceof PsiMethodCallExpression) {
                            isBad = isConditionBad01((PsiBinaryExpression) psiBinaryExpression.getLOperand()) || isConditionBad02((PsiMethodCallExpression) psiBinaryExpression.getROperand());
                            if (!isBad) {
                                reason = Reason.NULL_IS_HELD_CONDITION;
                            }
                        }

                    } else {
                        if (psiBinaryExpression.getROperand() instanceof PsiBinaryExpression) {
                            if (psiBinaryExpression.getLOperand() instanceof PsiMethodCallExpression) {
                                isBad = isConditionBad01((PsiBinaryExpression) psiBinaryExpression.getROperand()) || isConditionBad02((PsiMethodCallExpression) psiBinaryExpression.getLOperand());
                                if (!isBad) {
                                    reason = Reason.NULL_IS_HELD_CONDITION;
                                }
                            }

                        } else {
                            isBad = isConditionBad01((PsiBinaryExpression) psiExpression);
                            if (!isBad) {
                                reason = Reason.NULL_CONDITION;
                            }
                        }

                    }
                } else {
                    if (psiExpression instanceof PsiMethodCallExpression) {
                        isBad = isConditionBad02((PsiMethodCallExpression) psiExpression);
                        if (!isBad) {
                            reason = Reason.IS_HELD_CONDITION;
                        }
                    }
                }
            } catch (Exception e) {
                isBad = true;
                reason = Reason.UNKNOWN_CONDITION;
            }
            return isBad;
        }


        @Override
        public void doFix(@NotNull PsiElement psiElement) {


            if (psiElement instanceof PsiBinaryExpression) {// detects PowerManagerFlagTest
                if (psiElement.getText().equals("PowerManager.PARTIAL_WAKE_LOCK|ACQUIRE_CAUSES_WAKEUP")) {
                    //   System.out.println(" Yes PowerManager.PARTIAL_WAKE_LOCK|ACQUIRE_CAUSES_WAKEUP");
                    isBad = true;
                    reason = Reason.BAD_FLAG;
                }
            }
            if (psiElement instanceof PsiMethodCallExpression) {
                PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) psiElement;
                if (psiMethodCallExpression.getMethodExpression().getReferenceName().equals(METHOD_SET_KEEP_SCREEN_ON)) {
                    String qualifierClassQualifiedName = "";
                    try {
                        PsiType psiElement1 = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
                        //PsiMethodReferenceUtil.
                        if (psiElement1 != null) {
                            qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiElement1)).getQualifiedName();
                        }
                    } catch (NullPointerException npe) {
                        reason = Reason.UNKNOWN_CONDITION;
                    }
                    if (qualifierClassQualifiedName.equals(ANDROID_OS_POWER_MANAGER)) {
                        isBad=true;
                        reason= Reason.DEPRECATED_SET_KEEP_SCREEN_ON;
                    }
                }
            }

            if (psiElement instanceof PsiMethodCallExpression) {
                PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) psiElement;
                if (psiMethodCallExpression.getMethodExpression().getReferenceName().equals(METHOD_RELEASE)) {
                    String qualifierClassQualifiedName = "";
                    try {
                        PsiType psiElement1 = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
                        //PsiMethodReferenceUtil.
                        if (psiElement1 != null) {
                            qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiElement1)).getQualifiedName();
                        }
                    } catch (NullPointerException npe) {
                        reason = Reason.UNKNOWN_CONDITION;
                    }
                    if (qualifierClassQualifiedName.equals(ANDROID_OS_POWER_MANAGER_WAKE_LOCK)) {
                        releaseCounting++;
                        if (isBad) {
                            return;
                        }
                        PsiIfStatement psiIfStatement = IntelliJUtilFacade.lookUpForContainingIfStatement(psiElement);
                        if (psiIfStatement != null) {
                            // solving mWakeLock != null && mWakeLock.isHeld() and mWakeLock != null
                            isBad = isConditionBadMain(psiIfStatement.getCondition());
                        } else {
                            if (IntelliJUtilFacade.lookUpForContainingTryStatement(psiElement) != null) {
                                if (!IntelliJUtilFacade.isInFinallyBlock(psiElement)) {
                                    isBad = true;
                                    reason = Reason.NOT_IN_FINALLY_BLOCK;
                                } else {
                                    reason = Reason.IN_FINALLY_BLOCK;
                                }
                            }
                        }
                    }
                } else {
//                    if(psiMethodCallExpression.resolveMethod()!=null){
//
//                        psiMethodCallExpression.resolveMethod().accept(visitor);
//                    }


                }

            }

            if (releaseCounting == 0) {
                //System.out.println("Before Release "+psiElement.getText());
                if (acquireCounting == 0) {
                    acquireCounting = PsiElementCustomVisitor.visitAndGetCount(psiElement, ANDROID_OS_POWER_MANAGER_WAKE_LOCK, METHOD_ACQUIRE);
                } else {
                    if (acquireCounting > 0) {
                        if (psiElement instanceof PsiMethodCallExpression) {
                            callsCountInBetween++;
//                        reason=Reason.NEEDS_FINALLY_BLOCK;
//                        isBad=true;
                        }
                    }
                }
            } else {
                // System.out.println("After Release"+psiElement.getText());
            }


        }

    }


}