package edu.gmu.cs.aeon.core.analysis.resources.cast;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Created by DavidIgnacio on 11/4/2014.
 */
public class OnProvideAssistDataListenerAPIAnalyzer implements Refactorer {
    public static final String CLASS_APPLICATION_ON_PROVIDE_ASSIST_DATA_LISTENER = "android.app.Application.OnProvideAssistDataListener";
    public static final String METHOD_REMOVE_METHOD = " unregisterOnProvideAssistDataListener";
    private static final String METHOD_REGISTER_METHOD = "registerOnProvideAssistDataListener";
    private static final String METHOD_TO_LISTENER = "onProvideAssistData";
    private static final String METHOD_ON_PAUSE = "onPause";
    private static final String METHOD_ON_RESUME = "onResume";

    private static boolean isRemoveUpdatePresent(@NotNull PsiMethod psiMethod) {
//        System.out.println("UPDATE CHECK0: "+psiMethod.getName());


        PsiCodeBlock psiMethodCodeBlock = psiMethod.getBody();
        for (PsiStatement psiStatement : psiMethodCodeBlock.getStatements()) {

//                System.out.println("UPDATE CHECK1: \t"+psiStatement.getText());
            if (psiStatement instanceof PsiExpressionStatement) {
                PsiExpressionStatement psiMethodExpression = (PsiExpressionStatement) psiStatement;
                //  System.out.println("UPDATE CHECK2: \t\t "+psiMethodExpression.getExpression().getText()+" class"+psiMethodExpression.getExpression().getClass());
                if (psiMethodExpression.getExpression() instanceof PsiMethodCallExpression) {
                    PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) psiMethodExpression.getExpression();
                    if (psiMethodCallExpression.getMethodExpression().getReferenceName().equals(METHOD_REMOVE_METHOD)) {

                        String qualifierClassQualifiedName = "";
                        try {
                            qualifierClassQualifiedName = PsiUtil.resolveClassInType(PsiUtil.getTypeByPsiElement(psiMethodCallExpression.getMethodExpression().getQualifier().getReference().resolve())).getQualifiedName();
                        } catch (NullPointerException npe) {
                            //System.out.println("NPE");
                        }

                        //   System.out.println("UPDATE CHECK3: \t\t\t " +qualifierClassQualifiedName + ", " + psiMethodCallExpression.getMethodExpression().getReferenceName());
                        if (qualifierClassQualifiedName!=null&&qualifierClassQualifiedName.equals(CLASS_APPLICATION_ON_PROVIDE_ASSIST_DATA_LISTENER)) {
                            return true;
                        }
                    }
                }

            }

//            locationManager.removeUpdates(this);

        }
        return false;
    }

    private static boolean isRequestLocationUpdatesPresent(@NotNull PsiMethod psiMethod) {
//        System.out.println("UPDATE CHECK0: "+psiMethod.getName());


        PsiCodeBlock psiMethodCodeBlock = psiMethod.getBody();
        for (PsiStatement psiStatement : psiMethodCodeBlock.getStatements()) {

//                System.out.println("UPDATE CHECK1: \t"+psiStatement.getText());
            if (psiStatement instanceof PsiExpressionStatement) {
                PsiExpressionStatement psiMethodExpression = (PsiExpressionStatement) psiStatement;
//                    System.out.println("UPDATE CHECK2: \t\t "+psiMethodExpression.getExpression().getText());
                if (psiMethodExpression.getExpression() instanceof PsiMethodCallExpression) {
                    PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) psiMethodExpression.getExpression();
                    if (psiMethodCallExpression.getMethodExpression().getReferenceName().equals(METHOD_REGISTER_METHOD)) {
                        String qualifierClassQualifiedName = "";
                        PsiType psiElement1 = psiMethodCallExpression.getMethodExpression().getQualifierExpression().getType();
                        //PsiMethodReferenceUtil.
                        if (psiElement1 != null) {
                            qualifierClassQualifiedName = PsiUtil.resolveClassInType((psiElement1)).getQualifiedName();
                        }
//                            System.out.println("UPDATE CHECK3: \t\t\t " +qualifierClassQualifiedName + ", " + psiMethodCallExpression.getMethodExpression().getReferenceName());
                        if (qualifierClassQualifiedName!=null&&qualifierClassQualifiedName.equals(CLASS_APPLICATION_ON_PROVIDE_ASSIST_DATA_LISTENER)) {
                            return true;
                        }
                    }
                }

            }

//            locationManager.removeUpdates(this);

        }
        return false;
    }

    @Override
    public boolean isUseCase(PsiElement element) {
        return isUseCase001(element);
    }

    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {
        return false;
    }

    public boolean isUseCase001(@NotNull PsiElement element) {
        if (element instanceof PsiMethod) {
            PsiMethod psiMethod = (PsiMethod) element;


            if (psiMethod.getName().equals(METHOD_TO_LISTENER)) {

                PsiClass psiClass = IntelliJUtilFacade.lookUpForContainingClass(element);
//                System.out.println("UPDATE CHECK: "+psiClass.getName()+", loc "+AndroidUtilFacade.implementsOnProvideAssistDataListener(psiClass)+", cl "+AndroidUtilFacade.isClassSubclassOfActivity(psiClass));
                //todo if anonymous/inner class then look up for calling class
                if (psiClass != null && AndroidUtilFacade.implementsOnProvideAssistDataListener(psiClass) && AndroidUtilFacade.isClassSubclassOfActivity(psiClass)) {
                    PsiMethod[] psiMethods = psiClass.getMethods();
                    boolean isRemoveUpdatePresent = false;
                    boolean isRequestLocationUpdatesPresent = false;
                    for (PsiMethod psiMethod1 : psiMethods) {
                        if (psiMethod1.getName().equals(METHOD_ON_PAUSE)) {
                            isRemoveUpdatePresent = isRemoveUpdatePresent ? true : isRemoveUpdatePresent(psiMethod1);
                        }
                        if (psiMethod1.getName().equals(METHOD_ON_RESUME)) {
                            isRequestLocationUpdatesPresent = isRequestLocationUpdatesPresent ? true : isRequestLocationUpdatesPresent(psiMethod1);
                        }

                    }
                    if (isRemoveUpdatePresent && isRequestLocationUpdatesPresent) {
                        return false;
                    } else {
                        return true;
                    }

                }
            }

        }
        return false;
    }

    @Override
    public PsiElement annotateUseCase(PsiElement element, AnnotationHolder holder) {
        return null;
    }

    @Override
    public PsiElement refactorElement(PsiElement element) {
        return null;
    }

    @Override
    public PsiElement previewRefactorElement(PsiElement element) {
        return null;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return EnergyIcons.ENERGY_MARKER_WARNING_RESOURCE_CASE;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public PsiElement getElement() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "OnProvideAssistDataListener ";
    }
}
