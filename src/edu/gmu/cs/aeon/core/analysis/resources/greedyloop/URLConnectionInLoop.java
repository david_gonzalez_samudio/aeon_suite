package edu.gmu.cs.aeon.core.analysis.resources.greedyloop;

import com.intellij.lang.annotation.Annotation;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiTypeElement;
import edu.gmu.cs.aeon.core.AnnotationStyle;
import edu.gmu.cs.aeon.core.generator.AsyncTaskGenerator;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Created by DavidIgnacio on 6/1/2014.
 */
public class URLConnectionInLoop implements Refactorer {
    public String subjectClassCanonicalName = "java.net.URLConnection";
    public String refactoredClassCanonicalName = "android.os.AsyncTask";

    @Override
    public boolean isUseCase(PsiElement element) {
        if (!(element instanceof PsiTypeElement))
            return false;
        PsiTypeElement psiTypeElement = ((PsiTypeElement) element);
        final String canonicalText = psiTypeElement.getType().getCanonicalText();

        if (!subjectClassCanonicalName.equals(canonicalText))
            return false;

        PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(element);
        if (loopStatement == null) {
            return false;
        }
        PsiClass psiClass = IntelliJUtilFacade.lookUpForContainingClass(loopStatement);
        if (psiClass == null) {
            return true;
        }
        final PsiClass superClass = psiClass.getSuperClass();
        if (superClass == null) {
            return true;
        }

        if (!refactoredClassCanonicalName.equals(superClass.getQualifiedName()))
            return true;

        return false;
    }

    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {
        if (!(element instanceof PsiTypeElement))
            return false;
        PsiTypeElement psiTypeElement = ((PsiTypeElement) element);
        final String canonicalText = psiTypeElement.getType().getCanonicalText();

        if (!subjectClassCanonicalName.equals(canonicalText))
            return false;

        PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(element);
        if (loopStatement == null) {
            return false;
        }
        PsiClass psiClass = IntelliJUtilFacade.lookUpForContainingClass(loopStatement);
        if (psiClass == null) {
            return true;
        }
        final PsiClass superClass = psiClass.getSuperClass();
        if (superClass == null) {
            return true;
        }

        if (!refactoredClassCanonicalName.equals(superClass.getQualifiedName()))
            return true;
        return false;
    }

    @Override
    public PsiElement annotateUseCase(PsiElement element, AnnotationHolder holder) {
        if (!isUseCase(element))
            return null;

        PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(element);
        if (loopStatement == null)
            return null;

        String value = loopStatement.getText();
        if (value == null)
            return null;
        if (holder != null) {
            Annotation annotation = holder.createInfoAnnotation(loopStatement.getTextRange(), null);
            annotation.setTextAttributes(AnnotationStyle.CRITICAL.getTextAttributesKey());
        }
        return loopStatement;

    }

    @Override
    public PsiElement refactorElement(PsiElement element) {

        AsyncTaskGenerator asyncTaskGenerator = new AsyncTaskGenerator(annotateUseCase(element, null));
        asyncTaskGenerator.execute();
        return null;
    }

    @Override
    public PsiElement previewRefactorElement(PsiElement element) {
        return null;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return EnergyIcons.ENERGY_MARKER_WARNING_RESOURCE_CASE;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public PsiElement getElement() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "URL Connection in Loop";
    }
}
