package edu.gmu.cs.aeon.core.analysis.resources;

import com.intellij.codeInspection.*;
import com.intellij.codeInspection.dataFlow.DataFlowInspection;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;
import com.intellij.util.ArrayUtil;
import edu.gmu.cs.aeon.core.analysis.PsiElementSequentialVisitor;
import edu.gmu.cs.aeon.core.analysis.report.AnalysisCategory;
import edu.gmu.cs.aeon.core.analysis.report.PredictionType;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.core.analysis.report.ReportFactory;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.*;

/**
 * Created by DavidIgnacio on 6/1/2014.
 */
public class ResourceReleasingAnalyzer implements Refactorer, PsiElementSequentialVisitor.VisitorExtensionPoint {
    public int faultyCount = 0;
    public int totalCount = 0;


    public ResourceReleasingAnalyzer(){
        faultyCount=0;
        totalCount=0;
        okReports = new Vector<Report>();
        faultReports = new Vector<Report>();


    }

    public Vector<Report> faultReports = new Vector<Report>();

    public Vector<Report> okReports = new Vector<Report>();

    @Override
    public boolean isUseCase(PsiElement element) {

        return false;
    }

    PsiElementSequentialVisitor visitor;
    public boolean analyzeAllInMethod(PsiMethod psiMethod){
//        new DataFlowInspection().checkMethod(psiMethod, new InspectionManager() {
//                    @NotNull
//                    @Override
//                    public Project getProject() {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public CommonProblemDescriptor createProblemDescriptor(@NotNull String s, QuickFix... quickFixes) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull String s, LocalQuickFix localQuickFix, @NotNull ProblemHighlightType problemHighlightType, boolean b) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull String s, boolean b, LocalQuickFix[] localQuickFixes, @NotNull ProblemHighlightType problemHighlightType) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull String s, LocalQuickFix[] localQuickFixes, @NotNull ProblemHighlightType problemHighlightType, boolean b, boolean b1) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull PsiElement psiElement1, @NotNull String s, @NotNull ProblemHighlightType problemHighlightType, boolean b, LocalQuickFix... localQuickFixes) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @Nullable("null means the text range of the element") TextRange textRange, @NotNull String s, @NotNull ProblemHighlightType problemHighlightType, boolean b, LocalQuickFix... localQuickFixes) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull String s, boolean b, @NotNull ProblemHighlightType problemHighlightType, boolean b1, LocalQuickFix... localQuickFixes) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull String s, LocalQuickFix localQuickFix, @NotNull ProblemHighlightType problemHighlightType) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull String s, LocalQuickFix[] localQuickFixes, @NotNull ProblemHighlightType problemHighlightType) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull String s, LocalQuickFix[] localQuickFixes, @NotNull ProblemHighlightType problemHighlightType, boolean b) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull PsiElement psiElement1, @NotNull String s, @NotNull ProblemHighlightType problemHighlightType, LocalQuickFix... localQuickFixes) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, TextRange textRange, @NotNull String s, @NotNull ProblemHighlightType problemHighlightType, LocalQuickFix... localQuickFixes) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public ProblemDescriptor createProblemDescriptor(@NotNull PsiElement psiElement, @NotNull String s, boolean b, @NotNull ProblemHighlightType problemHighlightType, LocalQuickFix... localQuickFixes) {
//                        return null;
//                    }
//
//                    @NotNull
//                    @Override
//                    public GlobalInspectionContext createNewGlobalContext(boolean b) {
//                        return null;
//                    }
//                }, true);

                String callbackType = "None";
      //  boolean isReleasingComponentMethod=false;
        if(AndroidUtilFacade.methodLooksLikeImplementsUISomethingSomethingListener(psiMethod)){
            callbackType="UI";
        }else{
            if(AndroidUtilFacade.isAndroidCallbackMethod(psiMethod)){
                callbackType="System";
            }else{
                if(AndroidUtilFacade.isMethodCaseOfAndroidComponent(psiMethod, AndroidUtilFacade.MethodCase.ALL)){
                    callbackType="Component";
                   // isReleasingComponentMethod=AndroidUtilFacade.translateComponentMethodToActivityMethod(psiMethod).contains("onPause");//IDLE
                }else{
                    if(AndroidUtilFacade.isMethodCaseOfAndroidThreadClass(psiMethod)){
                        callbackType="Thread";
                    }else{

                    }
                }
            }
        }
        energyDataVectorReleasing =new Vector<EnergyData>();
        energyDataVectorAcquiring =new Vector<EnergyData>();
        visitor = new PsiElementSequentialVisitor(this,null);
        visitor.setMode(PsiElementSequentialVisitor.Mode.EXTENSION_POINT);
        Double result=visitor.visitElement(psiMethod);
      //  StringBuffer detailedEnergyData=new StringBuffer();
        double estimate=0;
//        detailedEnergyData.append("\nMethod: "+psiMethod.getName()+" (Callback Type: "+callbackType+")\n");
        for(EnergyData energyData: energyDataVectorReleasing){
            energyData.callBackType=callbackType;
            estimate+=energyData.energyConsumption;
//            detailedEnergyData.append("call: "+energyData.getText()+" \n");
        }
     //   detailedEnergyData.append("Total releasing calls: "+(int)estimate+"\n");
        if(estimate>0){//todo filter by calls and/or total consumption
            allEnergyDataVectorReleasing.put(psiMethod, energyDataVectorReleasing);
//            Report report = ReportFactory.buildReport(AnalysisCategory.GREEDY_INTERNET_CALL_IN_LOOP, PredictionType.ENERGY_FAULT, null, psiMethod, psiMethod.getContainingClass().getQualifiedName(), detailedEnergyData.toString(), true);
//            if(isReleasingComponentMethod){
//                report.isBad=false;
////                okReports.add(report);
//                report.reason="isReleasingComponentMethod";
//                faultReports.add(report);
//            }else {
//                faultReports.add(report);
//            }

        }
        double estimate2=0;
        for(EnergyData energyData: energyDataVectorAcquiring){
            energyData.callBackType=callbackType;
            estimate2+=energyData.energyConsumption;
        }
      //  detailedEnergyData.append("Total releasing calls: "+(int)estimate+"\n");
        if(estimate2>0){//todo filter by calls and/or total consumption
            allEnergyDataVectorAcquiring.put(psiMethod, energyDataVectorAcquiring);

        }
        return false;
    }
    Map<PsiMethod, Vector<EnergyData>> allEnergyDataVectorReleasing =new HashMap<PsiMethod, Vector<EnergyData>>();
    Map<PsiMethod, Vector<EnergyData>> allEnergyDataVectorAcquiring =new HashMap<PsiMethod, Vector<EnergyData>>();

    public  boolean doAnalysis(PsiElement element){
        if (element instanceof PsiAnonymousClass||element instanceof PsiClass) {

            PsiClass psiClass = (PsiClass) element;


            PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getMethods(), psiClass.getConstructors());

            for (PsiMethod psiMethod1 : psiMethods) {

                analyzeAllInMethod(psiMethod1);

            }
            fixAcquiringAndReleasingInClass();

return  true;
        }

return false;

    }
    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {
        return false;
    }

    @Override
    public PsiElement annotateUseCase(PsiElement element, AnnotationHolder holder) {

        return null;

    }

    @Override
    public PsiElement refactorElement(PsiElement element) {

        return null;
    }

    @Override
    public PsiElement previewRefactorElement(PsiElement element) {
        return null;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return EnergyIcons.ENERGY_MARKER_WARNING_RESOURCE_CASE;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public PsiElement getElement() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "";
    }
    private  Vector<EnergyData> energyDataVectorReleasing;
    private  Vector<EnergyData> energyDataVectorAcquiring;
    enum Type{ACQUIRE, RELEASE}
    private class EnergyData{
        String callBackType="";
       Type type=Type.ACQUIRE;
        String apiCallName="";
        Double energyConsumption=0.0;
        String contextClass="";
        String contextMethod="";
        String contextMethodText="";
        Stack<PsiExpression> callStack;
        public EnergyData(Type type, Double energyConsumption, String apiCallName, String contextClass, String contextMethod, String contextMethodText,   Stack<PsiExpression> callStack) {
            this.type=type;
            this.energyConsumption = energyConsumption;
            this.apiCallName = apiCallName;
            this.contextClass = contextClass;
            this.contextMethod = contextMethod;
            this.contextMethodText = contextMethodText;
            this.callStack = callStack;

        }
        @Override
        public int hashCode(){
            return 71+callBackType.hashCode()+type.hashCode()+apiCallName.hashCode()+contextClass.hashCode()+contextMethodText.hashCode();
        }


        String getText(){
            try {
                return "[" + apiCallName + " : " + energyConsumption + ", Called in " + contextClass + ", method: " + contextMethod + "][code:" + contextMethodText + "]";
            }catch (Exception e){
                return "[" + apiCallName + " : " + energyConsumption + ", error in resolving the rest]";
            }
        }

    }

    @Override
    public Double onVisitingElement(PsiElement psiElement, Object inOutObject) {
        String className="";
        String methodName="";
            if(psiElement instanceof PsiMethodCallExpression){
                PsiMethodCallExpression psiMethodCallExpression=(PsiMethodCallExpression)psiElement;
                    className = IntelliJUtilFacade.getMethodCallExpressionClassName(psiMethodCallExpression);
                    methodName = IntelliJUtilFacade.getMethodCallExpressionMethodName(psiMethodCallExpression);
//                System.out.print(">> " + className + "." + methodName);

                }else{
if(psiElement instanceof  PsiNewExpression){
    final PsiNewExpression newExpression = (PsiNewExpression)psiElement;
    final PsiMethod method = newExpression.resolveConstructor();
    if(method==null||method.getContainingClass()==null)
        return 0.0;
   className =method.getContainingClass().getQualifiedName();
    final PsiType psiType = method.getReturnType();

    if (psiType != null) {
        className = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
    }

     methodName = method.getName();


}}

        Double est =0.0;


        if (AndroidUtilFacade.isResourceReleasingCall(className + "." + methodName)) {
            est=1.0;
            if(!visitor.callStack.isEmpty()) {
                PsiExpression psiExpression = visitor.callStack.peek();
                String methodCName = psiExpression.getText();
                String methodCText = psiExpression.getContext()==null?"No Context Available":psiExpression.getContext().getText();
                String cName = psiExpression.getType()==null?"No Type Resolved":(psiExpression.getType().getCanonicalText()==null?"No Type Resolved":psiExpression.getType().getCanonicalText());
                EnergyData energyData = new EnergyData(Type.RELEASE,est, className + "." + methodName, cName, methodCName, methodCText, visitor.callStack);
                energyDataVectorReleasing.add(energyData);
            }else{
                PsiMethod methodC = IntelliJUtilFacade.lookUpForContainingMethod(psiElement);
                String methodCName = methodC != null ? methodC.getName() : "";
                String methodCText = methodC != null ? methodC.getText() : "";
                PsiClass cClass = methodC != null ? IntelliJUtilFacade.lookUpForContainingClass(methodC) : null;
                String cName = cClass != null ? cClass.getQualifiedName() : "";
                EnergyData energyData = new EnergyData(Type.RELEASE,est, className + "." + methodName, cName, methodCName, methodCText, visitor.callStack);
                energyDataVectorReleasing.add(energyData);
            }
            //System.out.print(visitor.inLoopCount+" data"+energyData.getText());
        }
        if (AndroidUtilFacade.isResourceAcquiringCall(className + "." + methodName)) {
            est=1.0;
            if(!visitor.callStack.isEmpty()) {
                PsiExpression psiExpression = visitor.callStack.peek();
                String methodCName = psiExpression.getText();
                String methodCText = psiExpression.getContext()==null?"No Context Available":psiExpression.getContext().getText();
                String cName = psiExpression.getType()==null?"No Type Resolved":(psiExpression.getType().getCanonicalText()==null?"No Type Resolved":psiExpression.getType().getCanonicalText());
                EnergyData energyData = new EnergyData(Type.ACQUIRE,est, className + "." + methodName, cName, methodCName, methodCText, visitor.callStack);
                energyDataVectorAcquiring.add(energyData);
            }else{
                PsiMethod methodC = IntelliJUtilFacade.lookUpForContainingMethod(psiElement);
                String methodCName = methodC != null ? methodC.getName() : "";
                String methodCText = methodC != null ? methodC.getText() : "";
                PsiClass cClass = methodC != null ? IntelliJUtilFacade.lookUpForContainingClass(methodC) : null;
                String cName = cClass != null ? cClass.getQualifiedName() : "";
                EnergyData energyData = new EnergyData(Type.ACQUIRE,est, className + "." + methodName, cName, methodCName, methodCText, visitor.callStack);
                energyDataVectorAcquiring.add(energyData);
            }
            //System.out.print(visitor.inLoopCount+" data"+energyData.getText());
        }
        return est;
    }

    public static boolean areCallsRelated(@Nullable String call1, @Nullable String call2){

        if(call1==null ||call2==null){
            return false;
        }
        //System.out.println("COMPARING "+call1+" AND  "+call2);
        final String[] split1=call1.split("\\.");
        final String[] split2=call2.split("\\.");
        final int length1=split1.length;
        final int length2=split2.length;
        if(length1>0 &&length1==length2){
           final int lengthMinusOne=length1-1;
            for(int  i=0; i<lengthMinusOne;i++){
               // System.out.println("COMPARING2 "+split1[i]+" AND  "+split2[i]);
                if(!split1[i].equals(split2[i])){
                    return false;
                }
            }
            return true;
        }

        return false;
    }
    public void fixAcquiringAndReleasingInClass(){//todo, resolve at system level
        Vector<EnergyData> evaluated=new Vector<EnergyData>();
        for(PsiMethod acquiringPsiMethod:allEnergyDataVectorAcquiring.keySet()) {
            Vector<EnergyData> acquiringCalls=allEnergyDataVectorAcquiring.get(acquiringPsiMethod);

            double estimate = 0;
            String releasingCallbackType="";
            String releasingDataApiCallName="";
            String releasingPsiMethodGetName="";
            String releasingDataGetText="";
            for (EnergyData acquiringData : acquiringCalls) {
                boolean wasReleased=false;
                boolean isReleasingComponentMethod=false;

                estimate += acquiringData.energyConsumption;

                for(PsiMethod releasingPsiMethod:allEnergyDataVectorReleasing.keySet()){
                    Vector<EnergyData> releasingCalls=allEnergyDataVectorReleasing.get(releasingPsiMethod);

                    for(EnergyData releasingData:releasingCalls){
                        if(areCallsRelated(acquiringData.apiCallName, releasingData.apiCallName)){
                            wasReleased=true;
                            releasingCallbackType=releasingData.callBackType;
                            releasingDataApiCallName=releasingData.apiCallName;
                            releasingPsiMethodGetName=releasingPsiMethod.getName();
                            releasingDataGetText=releasingData.getText();
                            isReleasingComponentMethod=AndroidUtilFacade.translateComponentMethodToActivityMethod(releasingPsiMethod).contains("android.app.Activity.onPause");//IDLE
                            evaluated.add(releasingData);
                        }
                    }
                }
                StringBuffer detailedEnergyData = new StringBuffer();
                detailedEnergyData.append("Resource Acquiring "+acquiringData.apiCallName+" found in Method: " + acquiringPsiMethod.getName()+" (Callback Type: " + acquiringData.callBackType + ") \n");
                detailedEnergyData.append("Details: " + acquiringData.getText() + " \n");

                if(wasReleased){
// todo, fix something
                    if(acquiringPsiMethod.getName().equals(releasingPsiMethodGetName)){
                        detailedEnergyData.append("Then Resource Releasing Correctly(Locally) " + releasingDataApiCallName + " happened in Method: " + releasingPsiMethodGetName + " (Callback Type: " + releasingCallbackType + ") \n");
                       // detailedEnergyData.append("Details: " + releasingDataGetText + " \n");
                        Report report = ReportFactory.buildReport(AnalysisCategory.RESOURCE_MANAGEMENT, PredictionType.CORRECT_IMPLEMENTATION, null, acquiringPsiMethod, acquiringPsiMethod.getContainingClass().getQualifiedName(), detailedEnergyData.toString(), true);
                        okReports.add(report);

                    }else {
                        if (isReleasingComponentMethod) {
                            detailedEnergyData.append("Then Resource Releasing Correctly(In Component Lifecycle) " + releasingDataApiCallName + " happened in Method: " + releasingPsiMethodGetName + " (Callback Type: " + releasingCallbackType + ") \n");
                            detailedEnergyData.append("Details: " + releasingDataGetText + " \n");
                            Report report = ReportFactory.buildReport(AnalysisCategory.RESOURCE_MANAGEMENT, PredictionType.CORRECT_IMPLEMENTATION, null, acquiringPsiMethod, acquiringPsiMethod.getContainingClass().getQualifiedName(), detailedEnergyData.toString(), true);
                            okReports.add(report);
                        } else {
                            detailedEnergyData.append("Then Resource Releasing Incorrectly(Out of the Component Lifecycle) " + releasingDataApiCallName + " happened in Method: " + releasingPsiMethodGetName + " (Callback Type: " + releasingCallbackType + ") \n");
                            detailedEnergyData.append("Details: " + releasingDataGetText + " \n");
                            Report report = ReportFactory.buildReport(AnalysisCategory.RESOURCE_MANAGEMENT, PredictionType.ENERGY_FAULT, null, acquiringPsiMethod, acquiringPsiMethod.getContainingClass().getQualifiedName(), detailedEnergyData.toString(), true);
                            faultReports.add(report);
                        }
                    }

                }else{
                    detailedEnergyData.append("Then the Resource release was not found or reachable in the class \n");
                    Report report = ReportFactory.buildReport(AnalysisCategory.RESOURCE_MANAGEMENT, PredictionType.ENERGY_FAULT, null, acquiringPsiMethod, acquiringPsiMethod.getContainingClass().getQualifiedName(), detailedEnergyData.toString(), true);
                    faultReports.add(report);
                }

            }

        }

        for(PsiMethod releasingPsiMethod:allEnergyDataVectorReleasing.keySet()){
            Vector<EnergyData> releasingCalls=allEnergyDataVectorReleasing.get(releasingPsiMethod);
            for(EnergyData releasingData:releasingCalls){
                boolean isEvaluated=false;
                for(EnergyData evaluatedData:evaluated){
                        if(evaluatedData.equals(releasingData)){
                            isEvaluated=true;
                        }
                }
                if(!isEvaluated){
                    StringBuffer detailedEnergyData = new StringBuffer();
                    detailedEnergyData.append("Only Resource Release "+releasingData.apiCallName+" found in Method: " + releasingPsiMethod.getName()+" (Callback Type: " + releasingData.callBackType + ") \n");
                    detailedEnergyData.append("Details: " + releasingData.getText() + " \n");

                    Report report = ReportFactory.buildReport(AnalysisCategory.RESOURCE_MANAGEMENT, PredictionType.CORRECT_IMPLEMENTATION, null, releasingPsiMethod, releasingPsiMethod.getContainingClass().getQualifiedName(), detailedEnergyData.toString(), true);
                    okReports.add(report);
                }
            }
        }
    }




}
