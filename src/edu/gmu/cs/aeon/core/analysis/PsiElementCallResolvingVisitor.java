package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiRecursiveElementVisitor;
import com.intellij.psi.util.PsiUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 3/29/2015.
 */
public class PsiElementCallResolvingVisitor extends PsiRecursiveElementVisitor {

    public static final int MAX_INDIRECTION_LEVEL = 10;
    protected String targetClassCanonical;//method to start tracing
    protected String targetMethod;
    protected int count;
    protected Vector<PsiMethodCallExpression> detected;
    protected Mode mode = Mode.COUNT;
    protected Fixer refactorer;
    int indirectionLevel;
    boolean visitConditional = true;
    boolean visitLoop = true;
    boolean visitTry = true;
    PsiElementCallResolvingVisitor parent;

    private PsiElementCallResolvingVisitor(String targetClassCanonical, String targetMethod, int indirectionLevel) {
        this.targetClassCanonical = targetClassCanonical;
        this.targetMethod = targetMethod;
        this.indirectionLevel = indirectionLevel;

    }

    private PsiElementCallResolvingVisitor(Fixer refactorer, int indirectionLevel) {
        this.refactorer = refactorer;
        this.indirectionLevel = indirectionLevel;

    }

    private PsiElementCallResolvingVisitor() {

    }

    public static int visitAndGetCount(PsiElement psiMethod, String targetClassCanonical, String targetMethod) {
        PsiElementCallResolvingVisitor visitor = new PsiElementCallResolvingVisitor(targetClassCanonical, targetMethod, 0);
        visitor.count = 0;
        visitor.setMode(Mode.COUNT);
        psiMethod.accept(visitor);
        return visitor.count;

    }

    public static Vector<PsiMethodCallExpression> visitAndGetDetected(PsiElement psiMethod, String targetClassCanonical, String targetMethod) {
        PsiElementCallResolvingVisitor visitor = new PsiElementCallResolvingVisitor(targetClassCanonical, targetMethod, 0);
        visitor.setMode(Mode.DETECT);
        psiMethod.accept(visitor);
        return visitor.detected;
    }

    public static void visitAndRefactor(PsiElement element, Fixer refactorer) {
        PsiElementCallResolvingVisitor visitor = new PsiElementCallResolvingVisitor(refactorer, 0);
        visitor.setMode(Mode.FIX);
        element.accept(visitor);
    }

    public int getIndirectionLevel() {
        return indirectionLevel;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    @Override
    public void visitElement(final PsiElement element) {
        super.visitElement(element);


        switch (this.mode) {
            case COUNT:
                if (element instanceof PsiMethodCallExpression) {
                    visitMethodCallExpressionCount((PsiMethodCallExpression) element);
                }
                break;
            case DETECT:
                if (element instanceof PsiMethodCallExpression) {
                    detected = new Vector<PsiMethodCallExpression>();
                    visitMethodCallExpressionDetect((PsiMethodCallExpression) element);
                }
                break;
            case FIX:
                visitMethodCallExpressionFix(element);
                break;
        }


    }

    private void visitMethodCallExpressionCount(PsiMethodCallExpression psiMethodCallExpression) {
        if (psiMethodCallExpression.getMethodExpression().getReferenceName().equals(targetMethod)) {
            String qualifierClassQualifiedName = "";
            try {
                PsiElement psiElement = psiMethodCallExpression.getMethodExpression().getQualifier().getReference().resolve();
                if (psiElement != null) {
                    qualifierClassQualifiedName = PsiUtil.resolveClassInType(PsiUtil.getTypeByPsiElement(psiElement)).getQualifiedName();
                }
            } catch (NullPointerException npe) {


            }

            if (qualifierClassQualifiedName!=null&&qualifierClassQualifiedName.equals(targetClassCanonical)) {
//                System.out.println(targetMethod+ " Visit -->"+qualifierClassQualifiedName);
                count++;
            }
        }

    }

    private void visitMethodCallExpressionDetect(PsiMethodCallExpression psiMethodCallExpression) {
        if (psiMethodCallExpression.getMethodExpression().getReferenceName().equals(targetMethod)) {
            String qualifierClassQualifiedName = "";
            try {
                PsiElement psiElement = psiMethodCallExpression.getMethodExpression().getQualifier().getReference().resolve();
                if (psiElement != null) {
                    qualifierClassQualifiedName = PsiUtil.resolveClassInType(PsiUtil.getTypeByPsiElement(psiElement)).getQualifiedName();
                }
            } catch (NullPointerException npe) {
            }
            if (qualifierClassQualifiedName!=null&&qualifierClassQualifiedName.equals(targetClassCanonical)) {
                detected.add(psiMethodCallExpression);
            }
        }

    }

    // public int getCount(){return count;}

    private void visitMethodCallExpressionFix(PsiElement element) {
        refactorer.doFix(element);
    }

    public enum Mode {COUNT, DETECT, FIX}

    public interface Fixer {
        void doFix(@NotNull PsiElement psiElement);
    }

    //  Linked`
    public class Caller {

    }

}
