package edu.gmu.cs.aeon.core.analysis.greedyloop;

import com.intellij.codeInspection.dataFlow.DataFlowInspection;
import com.intellij.lang.annotation.Annotation;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;
import com.intellij.util.ArrayUtil;
import edu.gmu.cs.aeon.core.AnnotationStyle;
import edu.gmu.cs.aeon.core.analysis.PsiElementSequentialVisitor;
import edu.gmu.cs.aeon.core.analysis.report.AnalysisCategory;
import edu.gmu.cs.aeon.core.analysis.report.PredictionType;
import edu.gmu.cs.aeon.core.analysis.report.Report;
import edu.gmu.cs.aeon.core.analysis.report.ReportFactory;
import edu.gmu.cs.aeon.core.generator.AsyncTaskGenerator;
import edu.gmu.cs.aeon.core.refactor.Refactorer;
import edu.gmu.cs.aeon.core.utils.AndroidUtilFacade;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;
import edu.gmu.cs.aeon.knowledge.energy.EnergyKnowledgeManager;
import edu.gmu.cs.aeon.plugin.view.configuration.EnergyIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.HashMap;
import java.util.Stack;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 6/1/2014.
 */
public class GreedyCallsInLoopAnalyzer implements Refactorer, PsiElementSequentialVisitor.VisitorExtensionPoint {
    public String subjectClassCanonicalName = "java.net.URLConnection";
    public String refactoredClassCanonicalName = "android.os.AsyncTask";
    public int faultyCount = 0;
    public int totalCount = 0;
    public static String CLASS_NAME="java.net.HttpURLConnection";
    public static String METHOD_NAME="openConnection";
    public static HashMap<String, Vector<String>> internetAccessGreedyCalls;
    static {
        internetAccessGreedyCalls =new HashMap<String, Vector<String>>();
        Vector<String> methods=new Vector<String>();
        methods.add("openConnection");
        internetAccessGreedyCalls.put("java.net.HttpURLConnection", methods);
        Vector<String> methods1=new Vector<String>();
        methods1.add("openConnection");
        internetAccessGreedyCalls.put("java.net.URLConnection", methods1);

        Vector<String> methods2=new Vector<String>();
        methods2.add("execute");
        internetAccessGreedyCalls.put("org.apache.http.client.HttpClient", methods2);

        Vector<String> methods3=new Vector<String>();
        methods3.add("openConnection");
        methods3.add("openStream");
        internetAccessGreedyCalls.put("java.net.URL", methods3);



    }
    public GreedyCallsInLoopAnalyzer(){
        faultyCount=0;
        totalCount=0;
        okReports = new Vector<Report>();
        faultReports = new Vector<Report>();

    }

    public Vector<Report> faultReports = new Vector<Report>();

    public Vector<Report> okReports = new Vector<Report>();

    @Override
    public boolean isUseCase(PsiElement element) {
        if (!(element instanceof PsiTypeElement))
            return false;
        PsiTypeElement psiTypeElement = ((PsiTypeElement) element);
        final String canonicalText = psiTypeElement.getType().getCanonicalText();

        if (!subjectClassCanonicalName.equals(canonicalText))
            return false;

        PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(element);
        if (loopStatement == null) {
            return false;
        }
        PsiClass psiClass = IntelliJUtilFacade.lookUpForContainingClass(loopStatement);
        if (psiClass == null) {
            return true;
        }
        final PsiClass superClass = psiClass.getSuperClass();
        if (superClass == null) {
            return true;
        }

        if (!refactoredClassCanonicalName.equals(superClass.getQualifiedName()))
            return true;

        return false;
    }

//    public boolean analyzeMethod(PsiMethod psiMethod){
//        for(String className:internetAccessGreedyCalls.keySet()) {
//            Vector<String> methodNames = internetAccessGreedyCalls.get(className);
//            for (String methodName : methodNames) {
//                Vector<PsiMethodCallExpression> calls = PsiElementCustomVisitor.visitAndGetDetected(psiMethod, className, methodName);
//                for (PsiMethodCallExpression psiMethodCallExpression : calls) {
//                    PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(psiMethodCallExpression);
//                    if (loopStatement != null) {
//
//
//                        Report report = ReportFactory.buildReport(AnalysisCategory.GREEDY_INTERNET_CALL_IN_LOOP, PredictionType.ENERGY_FAULT, null, psiMethod, className + "", loopStatement.getText(), true);
//                        faultReports.add(report);
//                    }else{
////                        SummaryReport summaryReport = ReportFactory.buildReport(AnalysisCategory.GREEDY_INTERNET_CALL_IN_LOOP, PredictionType.CORRECT_IMPLEMENTATION, null, psiMethod, className + "", psiMethod.getText(), true);
////                        okReports.add(summaryReport);
//                    }
//                }
//            }
//        }
//        return false;
//    }
    PsiElementSequentialVisitor visitor;
    public boolean analyzeAllInMethod(PsiMethod psiMethod){
        String callbackType="None";
        if(AndroidUtilFacade.methodLooksLikeImplementsUISomethingSomethingListener(psiMethod)){
            callbackType="UI Event";
        }else{
            if(AndroidUtilFacade.isAndroidCallbackMethod(psiMethod)){
                callbackType="System Related";
            }else{
                if(AndroidUtilFacade.isMethodCaseOfAndroidComponent(psiMethod, AndroidUtilFacade.MethodCase.ALL)){
                    callbackType="Component Lifecycle";
                }else{
                    if(AndroidUtilFacade.isMethodCaseOfAndroidThreadClass(psiMethod)){
                        callbackType="Thread Pattern";
                    }else{
                        if(AndroidUtilFacade.methodLooksLikeImplementsSomethingSomethingListener(psiMethod)){
                            callbackType="Listener Pattern";
                        }else{

                        }

                    }
                }
            }
        }
        energyDataVector=new Vector<EnergyData>();
        energyDataVectorNotInLoop=new Vector<EnergyData>();
        visitor = new PsiElementSequentialVisitor(this,null);
        visitor.setMode(PsiElementSequentialVisitor.Mode.EXTENSION_POINT);
        Double result=visitor.visitElement(psiMethod);
        StringBuffer detailedEnergyData=new StringBuffer();
        double estimate=0;
        detailedEnergyData.append("\nMethod: "+psiMethod.getName()+" (Callback Type: "+callbackType+")\n");
        for(EnergyData energyData:energyDataVector){
            estimate+=energyData.energyConsumption;
            detailedEnergyData.append("Loop call: "+energyData.getText()+" \n");
        }
        detailedEnergyData.append("Total energy: "+estimate+" of "+energyDataVector.size()+" calls (EP: "+result+")\n");
        if(estimate>0){//todo filter by calls and/or total consumption

            if(callbackType.equals("Thread Pattern")){
                Report report = ReportFactory.buildReport(AnalysisCategory.GREEDY_INTERNET_CALL_IN_LOOP, PredictionType.CORRECT_IMPLEMENTATION, null, psiMethod, psiMethod.getContainingClass().getQualifiedName(), detailedEnergyData.toString(), true);
                okReports.add(report);

            }else{
                Report report = ReportFactory.buildReport(AnalysisCategory.GREEDY_INTERNET_CALL_IN_LOOP, PredictionType.ENERGY_FAULT, null, psiMethod, psiMethod.getContainingClass().getQualifiedName(), detailedEnergyData.toString(), true);
                faultReports.add(report);

            }

        }

        StringBuffer detailedEnergyDataNotInLoop=new StringBuffer();
        double estimateNotInLoop=0;
        detailedEnergyDataNotInLoop.append("\nMethod: "+psiMethod.getName()+" (Callback Type: "+callbackType+")\n");
        for(EnergyData energyData:energyDataVectorNotInLoop){
            estimateNotInLoop+=energyData.energyConsumption;
            detailedEnergyDataNotInLoop.append("Simple call: "+energyData.getText()+" \n");
        }
        detailedEnergyDataNotInLoop.append("Total energy: "+estimateNotInLoop+" of "+energyDataVector.size()+" calls (EP: "+result+")\n");
        if(estimateNotInLoop>0){//todo filter by calls and/or total consumption


            Report report = ReportFactory.buildReport(AnalysisCategory.GREEDY_INTERNET_CALL_IN_LOOP, PredictionType.CORRECT_IMPLEMENTATION, null, psiMethod, psiMethod.getContainingClass().getQualifiedName(), detailedEnergyDataNotInLoop.toString(), true);
            okReports.add(report);

        }

        return false;
    }

    public  boolean doAnalysis(PsiElement element){
        if (element instanceof PsiAnonymousClass||element instanceof PsiClass) {

            //    System.out.println("Class >> " + ((PsiClass) element).getQualifiedName());

            PsiClass psiClass = (PsiClass) element;


            PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getMethods(), psiClass.getConstructors());

//            faultyCount = 0;
//            totalCount = 0;
            for (PsiMethod psiMethod1 : psiMethods) {

                analyzeAllInMethod(psiMethod1);

            }


          //  System.out.println("WAKELOCK CHECK: using = " + totalCount + ", fa= " + faultyCount);

//
//            if (faultyCount == 0) {
//                return false;
//            } else {
//                return true;
//            }
return  true;
        }

return false;

    }
    @Override
    public boolean isRefactoredUseCase(@NotNull PsiElement element) {
        return false;
    }

    @Override
    public PsiElement annotateUseCase(PsiElement element, AnnotationHolder holder) {
        if (!isUseCase(element))
            return null;

        PsiElement loopStatement = IntelliJUtilFacade.lookUpForContainingLoop(element);
        if (loopStatement == null)
            return null;

        String value = (String) loopStatement.getText();
        if (value == null)
            return null;
        Annotation annotation = holder.createInfoAnnotation(loopStatement.getTextRange(), null);
        annotation.setTextAttributes(AnnotationStyle.CRITICAL.getTextAttributesKey());
        return loopStatement;

    }

    @Override
    public PsiElement refactorElement(PsiElement element) {

        new AsyncTaskGenerator((PsiClass) element)
                .execute();
        return null;
    }

    @Override
    public PsiElement previewRefactorElement(PsiElement element) {
        return null;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return EnergyIcons.ENERGY_MARKER_WARNING_RESOURCE_CASE;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public PsiElement getElement() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "";
    }
    private  Vector<EnergyData> energyDataVector;
    private  Vector<EnergyData> energyDataVectorNotInLoop;

    private class EnergyData{
        String apiCallName="";
        Double energyConsumption=0.0;
        String contextClass="";
        String contextMethod="";
        String contextMethodText="";
        Stack<PsiExpression> callStack;
        public EnergyData(Double energyConsumption, String apiCallName, String contextClass, String contextMethod, String contextMethodText,   Stack<PsiExpression> callStack) {
            this.energyConsumption = energyConsumption;
            this.apiCallName = apiCallName;
            this.contextClass = contextClass;
            this.contextMethod = contextMethod;
            this.contextMethodText = contextMethodText;
            this.callStack = callStack;

        }


        String getText(){
            try {
                return "[" + apiCallName + " : " + energyConsumption + ", Called in " + contextClass + ", method: " + contextMethod + "][code:" + contextMethodText + "]";
            }catch (Exception e){
                return "[" + apiCallName + " : " + energyConsumption + ", error in resolving the rest]";
            }
        }

    }
private static final Double internetEstimate=0.6E10-6;
    @Override
    public Double onVisitingElement(PsiElement psiElement, Object inOutObject) {
        String className="";
        String methodName="";
            if(psiElement instanceof PsiMethodCallExpression){
                PsiMethodCallExpression psiMethodCallExpression=(PsiMethodCallExpression)psiElement;
                    className = IntelliJUtilFacade.getMethodCallExpressionClassName(psiMethodCallExpression);
                    methodName = IntelliJUtilFacade.getMethodCallExpressionMethodName(psiMethodCallExpression);
//                System.out.print(">> " + className + "." + methodName);

                }else{
if(psiElement instanceof  PsiNewExpression){
    final PsiNewExpression newExpression = (PsiNewExpression)psiElement;
    final PsiMethod method = newExpression.resolveConstructor();
    if(method==null||method.getContainingClass()==null)
        return 0.0;
   className =method.getContainingClass().getQualifiedName();
    final PsiType psiType = method.getReturnType();

    if (psiType != null) {
        className = PsiUtil.resolveClassInType((psiType)).getQualifiedName();
    }


     methodName = method.getName();


}}

        Double est = EnergyKnowledgeManager.energyGreedyAPIs.get(className + "." + methodName);
        if (est == null) {
            boolean isMatch=isInternetGreedyCall(className, methodName);
            if(isMatch){
                est=internetEstimate;
            }else{
                est=0.0;
            }
        }
        if (est>0) {
            if(!visitor.callStack.isEmpty()) {
                PsiExpression psiExpression = visitor.callStack.peek();
                String methodCName = psiExpression.getText();
                String methodCText = psiExpression.getContext()==null?"No Context Available":psiExpression.getContext().getText();
                String cName = psiExpression.getType()==null?"No Type Resolved":(psiExpression.getType().getCanonicalText()==null?"No Type Resolved":psiExpression.getType().getCanonicalText());

                EnergyData energyData = new EnergyData(est, className + "." + methodName, cName, methodCName, methodCText, visitor.callStack);

                if(visitor.inLoopCount > 0) {
                    energyDataVector.add(energyData);
                }else{
                    energyDataVectorNotInLoop.add(energyData);
                }
            }else{
                PsiMethod methodC = IntelliJUtilFacade.lookUpForContainingMethod(psiElement);
                String methodCName = methodC != null ? methodC.getName() : "";
                String methodCText = methodC != null ? methodC.getText() : "";
                PsiClass cClass = methodC != null ? IntelliJUtilFacade.lookUpForContainingClass(methodC) : null;
                String cName = cClass != null ? cClass.getQualifiedName() : "";
                EnergyData energyData = new EnergyData(est, className + "." + methodName, cName, methodCName, methodCText, visitor.callStack);
                if(visitor.inLoopCount > 0) {
                    energyDataVector.add(energyData);
                }else{
                    energyDataVectorNotInLoop.add(energyData);
                }
            }
            //System.out.print(visitor.inLoopCount+" data"+energyData.getText());
        }

        return est;
    }

    public static boolean isInternetGreedyCall(final String className, final String methodName) {
        if(className==null||methodName==null){
            return false;
        }
        for(String greedyClassName: internetAccessGreedyCalls.keySet()) {
            Vector<String> methodNames = internetAccessGreedyCalls.get(greedyClassName);
            if(methodNames==null){
                return false;
            }
            for (String greedyMethodName : methodNames) {
                if(greedyMethodName==null){
                    return false;
                }
                if(className.equals(greedyClassName)&& methodName.equals(greedyMethodName)){
                    return true;
                }
            }
        }
        return false;
    }
}
