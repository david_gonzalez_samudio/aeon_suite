package edu.gmu.cs.aeon.core.analysis.report;

/**
 * Created by DavidIgnacio on 4/25/2015.
 */
public class Fault{
    public enum Level{
        CRITICAL,
        WARNING,
        SUGGESTION,
        NONE
    }
    public enum Solution {
        NONE,
        INFORM_DEVELOPER_REQUIRES_PROFILING,
        FIX_AVAILABLE,
        REFACTORING_AVAILABLE,
        SUGGESTION_AVAILABLE
    }
}
