package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiMethod;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public class ClassReporter extends Reporter {
    Reporter caller;
    Vector<Reporter> innerClassesReporters;// method calls


    int currentIndirectionLevel;
    boolean isCertain;// is there a conditional in the path

    public ClassReporter(Reporter caller, PsiClass psiClass, int currentIndirectionLevel) {
        this.caller = caller;
        this.this_element = psiClass;
        innerClassesReporters = new Vector<Reporter>();

        this.currentIndirectionLevel = currentIndirectionLevel;
        isCertain = caller.isCertain;
    }

    public ClassReporter(PsiClass psiClass, int currentIndirectionLevel) {
        this.caller = null;
        this.this_element = psiClass;
        innerClassesReporters = new Vector<Reporter>();

        this.currentIndirectionLevel = currentIndirectionLevel;
        isCertain = true;
    }

    public void makeReport(PsiMethod psiMethod) {

    }


    @Override
    public String getReportData() {
        StringBuffer indent = new StringBuffer();
        for (int i = 0; i < currentIndirectionLevel; i++) {
            indent.append("\t");

        }
        String indentation = indent.toString();
        StringBuffer out = new StringBuffer();
        for (Report faultReport : faultReports) {
            out.append("\t" + faultReport.getReportData());
        }
        if (this_element instanceof PsiClass) {
            out.append(indentation + ">>>  Class SummaryReport " + ((PsiClass) this_element).getQualifiedName() + "\n");
        } else {
            out.append(indentation + ">>> Class SummaryReport " + this_element.getText() + "\n");
        }

        for (Reporter methodReporter : innerClassesReporters) {
            out.append(methodReporter.getReportData());
        }
        out.append("\n");
        return out.toString();

    }

    @Override
    public void addReporter(Reporter reporter) {
        innerClassesReporters.add(reporter);
    }
}