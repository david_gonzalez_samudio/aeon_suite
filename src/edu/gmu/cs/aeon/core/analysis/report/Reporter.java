package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.psi.PsiElement;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public abstract class Reporter {

    public static boolean isExploreTry = true;
    static boolean isExploreConditional = true; // explore conditional
    static boolean isExploreLoop = true; // explore conditional
    static boolean useIndirectionLevel = true; // limit depth
    static int maxIndirectionLevel = 10;
    static boolean isToExport = false;
    Reporter parent;// null if root
    PsiElement this_element;
    int currentIndirectionLevel = 0;
    Vector<Report> faultReports = new Vector<Report>();// reports in method

    boolean isCertain;// is there a conditional in the path

    public abstract String getReportData();

    public abstract void addReporter(Reporter reporter);

    public Vector<Report> getFaultReports() {
        return faultReports;
    }

    public Reporter getRootReporter() {
        Reporter current = this;
        while (current.parent != null) {
            current = current.parent;
        }
        return current;
    }
}
