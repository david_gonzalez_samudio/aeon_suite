package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.openapi.util.io.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public class Reports {
    int csv_length = 11;
    long bufferSize = 100; //in bytes change to Integer.size
    HashMap<String, SummaryReport> reports;

    Reports() {
        reports = new HashMap<String, SummaryReport>();
    }

    public SummaryReport getReport(String appName) {
        SummaryReport summaryReport = null;
        if (reports.containsKey(appName)) {
            summaryReport = reports.get(appName);
        } else {
            summaryReport = new SummaryReport(appName);
            reports.put(appName, summaryReport);
        }
        return summaryReport;
    }

    public String getFinalReport() {
        StringBuffer data = new StringBuffer();
        for (SummaryReport summaryReport : reports.values()) {
            data.append("App Name: " + summaryReport.appName + "\n");
            data.append("\t total files: " + summaryReport.totalFiles + "\n");
            // data.append("\t refactoring cases"+summaryReport.refactoringTotal+"\n");
        }

        return data.toString();

    }

    public String getDetailedFinalReport() {
        StringBuffer data = new StringBuffer();
        SummaryReport finalSummaryReport = new SummaryReport("Final SummaryReport");
        for (SummaryReport summaryReport : reports.values()) {

            data.append("-------------------------------\n");
            data.append("App Name: " + summaryReport.appName + "\n");

            data.append("\t total files: " + summaryReport.totalFiles + "\n");
            finalSummaryReport.totalFiles += summaryReport.totalFiles;

            for (String refCase : summaryReport.detailedData.keySet()) {
                int refTot = summaryReport.detailedData.get(refCase).intValue();
                summaryReport.refactoringTotal += refTot;
                data.append("\t\t refactoring case " + refCase + " : " + refTot + "\n");
                finalSummaryReport.addDetailedData(finalSummaryReport, refCase, refTot);

            }

            data.append("\t total refactoring cases " + summaryReport.refactoringTotal + "\n");
            finalSummaryReport.refactoringTotal += summaryReport.refactoringTotal;
        }

        data.append("-------------------------------\n");
        data.append("-------------------------------\n");
        data.append(finalSummaryReport.appName + "\n");

        data.append("\t total files: " + finalSummaryReport.totalFiles + "\n");


        for (String refCase : finalSummaryReport.detailedData.keySet()) {
            int refTot = finalSummaryReport.detailedData.get(refCase).intValue();

            data.append("\t\t refactoring case " + refCase + " : " + refTot + "\n");


        }

        data.append("\t total refactoring cases " + finalSummaryReport.refactoringTotal + "\n");

        return data.toString();

    }

//    public String getDetailedFinalReportCSVFormat(String path) {
//        String fileName = "Report_" + System.currentTimeMillis() + ".csv";
//        String cellSeparator = ",";
//        StringBuffer data = new StringBuffer();
//        boolean append = false;
//
//        SummaryReport finalReport = new SummaryReport("Final SummaryReport");
//        String[] header = new String[csv_length];
//        header[0] = "App Name";
//        header[1] = "Total Java Files";
//        header[2] = "Total Refactorings found";
//        int index = 3;
//
//
//        HashMap<String, Integer> nameToCol = new HashMap<String, Integer>();
//        for (String appName : reports.keySet()) {
//            SummaryReport summaryReport=reports.get(appName);
//            String[] row = new String[csv_length];
//
//            row[0] = summaryReport.appName;
//            row[1] = "" + summaryReport.totalFiles;
//
//            finalReport.totalFiles += summaryReport.totalFiles;
//
//            for (String refCase : summaryReport.detailedData.keySet()) {
//                int refTot = summaryReport.detailedData.get(refCase).intValue();
//                summaryReport.refactoringTotal += refTot;
//                finalReport.addDetailedData(finalReport, refCase, refTot);
//                int where = 0;
//                if (!nameToCol.containsKey(refCase)) {
//                    where = index;
//                    nameToCol.put(refCase, index);
//                    header[index] = refCase;
//                    index++;
//                } else {
//                    where = nameToCol.get(refCase);
//                }
//
//                row[where] = "" + refTot;
//            }
//
//            row[2] = "" + summaryReport.refactoringTotal;
//
//            //  finalReport.refactoringTotal+=summaryReport.refactoringTotal;
//
//            for (int in = 0; in < csv_length - 1; in++) {
//                if (row[in] == null) {
//                    data.append(0 + cellSeparator);
//                } else {
//                    data.append(row[in] + cellSeparator);
//                }
//            }
//            if (row[csv_length - 1] == null) {
//                data.append(0 + "\n");
//            } else {
//                data.append(row[csv_length - 1] + "\n");
//            }
//            if (data.length() > bufferSize) {
//                ReportManager.writeToFile(path, fileName, data.toString(), append);
//                data = new StringBuffer();
//                append = true;
//            }
//        }
//        String[] row = new String[csv_length];
//        row[0] = finalReport.appName;
//
//        row[1] = "" + finalReport.totalFiles;
//
//
//        for (String refCase : finalReport.detailedData.keySet()) {
//            int refTot = finalReport.detailedData.get(refCase).intValue();
//            finalReport.refactoringTotal += refTot;
//            // finalReport.addDetailedData(finalReport, refCase, refTot);
//            int where = 0;
//            if (!nameToCol.containsKey(refCase)) {
//                where = index;
//                nameToCol.put(refCase, index);
//                header[index] = refCase;
//                index++;
//            } else {
//                where = nameToCol.get(refCase);
//            }
//
//            row[where] = "" + refTot;
//        }
//
//        row[2] = "" + finalReport.refactoringTotal;
//
//        for (int in = 0; in < csv_length - 1; in++) {
//            if (row[in] == null) {
//                data.append(0 + cellSeparator);
//            } else {
//                data.append(row[in] + cellSeparator);
//            }
//        }
//        if (row[csv_length - 1] == null) {
//            data.append(0 + "\n");
//        } else {
//            data.append(row[csv_length - 1] + "\n");
//        }
//
//        StringBuffer headerBuff = new StringBuffer();
//        for (int in = 0; in < csv_length - 1; in++) {
//            if (header[in] == null) {
//                headerBuff.append(cellSeparator);
//            } else {
//                headerBuff.append(header[in] + cellSeparator);
//            }
//        }
//        if (header[csv_length - 1] == null) {
//            headerBuff.append(0 + "\n");
//        } else {
//            headerBuff.append(header[csv_length - 1] + "\n");
//        }
//
//
//        ReportManager.writeToFile(path, fileName, data.toString(), true);
//        ReportManager.writeToFile(path, fileName, headerBuff.toString(), true);
//       // ReportManager.writeToFile(path, fileName, re, true);
//        // addOnTopOfFile(path,fileName, headerBuff.toString());
//
//        return path + File.separator + fileName;
//
//    }

    public String getDetailedFinalReportCSVFormatUtil(String path, int javaCount, int currentFileCount, int totalProjectPsiFiles, int totalLOC, HashMap<String, Integer> appJavaFilesCounts) {
        String finalDataDetails="Java Files: "+javaCount +", Files Scanned: "+currentFileCount+", Total Files in Project: "+totalProjectPsiFiles+", total LOC "+totalLOC+"\n";
        String fileName = "Report_" + System.currentTimeMillis()+".csv";
        String cellSeparator = ",";
        StringBuffer data = new StringBuffer();

        File output = new File(path, fileName);
        FileUtil.createIfDoesntExist(output);

        SummaryReport finalSummaryReport = new SummaryReport("Final SummaryReport");
        String[] header = new String[15];
        header[0] = "App Name";
      //  header[1] = "Total Files";
        header[1] = "Java Files";
     //   header[3] = "Total LOC";
        header[2] = "Java Lines Of Code";
        int indexInit=3;
        header[indexInit] = ReportManager.HeaderName.TOTAL_METHODS_WAKE_LOCK_IS_PRESENT.name();
        header[indexInit+1] = ReportManager.HeaderName.METHODS_WAKE_LOCK_IS_USED_CORRECTLY.name();
        header[indexInit+2] = ReportManager.HeaderName.METHODS_WAKE_LOCK_IS_FAULTY.name();
        header[indexInit+3] = ReportManager.HeaderName.TOTAL_METHODS_LOCATION_IS_PRESENT.name();
        header[indexInit+4] = ReportManager.HeaderName.METHODS_LOCATION_IS_USED_CORRECTLY.name();
        header[indexInit+5] = ReportManager.HeaderName.METHODS_LOCATION_IS_FAULTY.name();
        header[indexInit+6] = ReportManager.HeaderName.TOTAL_ENERGY_GREEDY_METHODS.name();
        header[indexInit+7] = ReportManager.HeaderName.TOTAL_GREEDY_CALLS_IN_LOOPS.name();
        header[indexInit+8] = ReportManager.HeaderName.RELEASING_RESOURCES_OK.name();
        header[indexInit+9] = ReportManager.HeaderName.RELEASING_RESOURCES_WRONG.name();
        int indexEnd=indexInit+10;

        header[indexEnd] = "Using Methods  to inspect";
        header[indexEnd+1] = "Faulty Methods to inspect";
        csv_length=header.length;


        StringBuffer headerBuff = new StringBuffer();
        for (int in = 0; in < csv_length - 1; in++) {
            if (header[in] == null) {
                headerBuff.append(cellSeparator);
            } else {
                headerBuff.append(header[in] + cellSeparator);
            }
        }
        if (header[csv_length - 1] == null) {
            headerBuff.append(0 + "\n");
        } else {
            headerBuff.append(header[csv_length - 1] + "\n");
        }
        try {
            FileUtil.appendToFile(output, headerBuff.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

//        HashMap<String, Integer> nameToCol = new HashMap<String, Integer>();
        int k = 0;
        for (String appName : reports.keySet()) {
            SummaryReport summaryReport =reports.get(appName);
            String[] row = new String[csv_length];

            row[0] = summaryReport.appName;
//            row[1] = ""+summaryReport.totalFiles ;
//            row[1] = "" +appJavaFilesCounts.get(appName);
            row[1] = "" +appJavaFilesCounts.get(appName);
//            row[3] = "" +summaryReport.appLOC;
            row[2] = "" + summaryReport.javaLOC;

            finalSummaryReport.totalFiles += summaryReport.totalFiles;

            for (String refCase : summaryReport.detailedData.keySet()) {
                int refTot = summaryReport.detailedData.get(refCase).intValue();
                summaryReport.refactoringTotal += refTot;
                for(int index=indexInit;index<indexEnd;index++)
                    if(refCase.equals(header[index])){
                        row[index]=""+refTot;
                    }
            }

            //row[2] = "" + summaryReport.refactoringTotal;
            row[indexEnd] = "" + summaryReport.usingpaths;
            row[indexEnd+1] = "" + summaryReport.faultyPaths;

            //  finalSummaryReport.refactoringTotal+=summaryReport.refactoringTotal;

            for (int in = 0; in < csv_length - 1; in++) {
                if (row[in] == null) {
                    data.append(0 + cellSeparator);
                } else {
                    data.append(row[in] + cellSeparator);
                }
            }
            if (row[csv_length - 1] == null) {
                data.append(0 + "\n");
            } else {
                data.append(row[csv_length - 1] + "\n");
            }
//            if (data.length() > FileUtil.LARGE_FOR_CONTENT_LOADING - bufferSize) {
            if (k++ > 10) {
                try {
                    FileUtil.appendToFile(output, data.toString());
                    data = new StringBuffer();
                    k = 0;
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

//            }
        }
//        String[] row = new String[csv_length];
//        row[0] = finalSummaryReport.appName;
//
//        row[1] = "" + finalSummaryReport.totalFiles;
//
//
//        for (String refCase : finalSummaryReport.detailedData.keySet()) {
//            int refTot = finalSummaryReport.detailedData.get(refCase).intValue();
//            finalSummaryReport.refactoringTotal += refTot;
//            // finalSummaryReport.addDetailedData(finalSummaryReport, refCase, refTot);
//            int where = 0;
//            if (!nameToCol.containsKey(refCase)) {
//                where = index;
//                nameToCol.put(refCase, index);
//                header[index] = refCase;
//                index++;
//            } else {
//                where = nameToCol.get(refCase);
//            }
//
//            row[where] = "" + refTot;
//        }
//
//        row[2] = "" + finalSummaryReport.refactoringTotal;
//        row[3] = "" + finalSummaryReport.data;
//
//
//        for (int in = 0; in < csv_length - 1; in++) {
//            if (row[in] == null) {
//                data.append(0 + cellSeparator);
//            } else {
//                data.append(row[in] + cellSeparator);
//            }
//        }
//        if (row[csv_length - 1] == null) {
//            data.append(0 + "\n");
//        } else {
//            data.append(row[csv_length - 1] + "\n");
//        }



        try {
            FileUtil.appendToFile(output, data.toString());
         //   FileUtil.appendToFile(output, finalDataDetails);

        } catch (IOException e) {
            e.printStackTrace();
        }
//       output = new File(path, fileName);
//        File outputFinal = new File(path, fileName + ".csv");
//        FileUtil.createIfDoesntExist(outputFinal);
//        try {
//            FileUtil.appendToFile(outputFinal, headerBuff.toString());
//            InputStream is = new FileInputStream(output);
//            byte[] buffer = new byte[1024];
//            while (is.read(buffer) > 0) {
//                FileUtil.writeToFile(outputFinal, buffer, true);
//            }
//            is.close();
//            //FileUtil.copyContent(output, outputFinal);
//            FileUtil.delete(output);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        // addOnTopOfFile(path,fileName, headerBuff.toString());

        return path + File.separator + fileName;

    }

    protected class SummaryReport {
        String appName;
        int totalFiles;
        int javaFiles;
        int refactoringTotal;
        int appLOC;
        int javaLOC;
        HashMap<String, Integer> detailedData;
        String faultyPaths = "";
        String usingpaths = "";

        SummaryReport(String appName) {
            this.appName = appName;
            totalFiles = 0;
            refactoringTotal = 0;
            appLOC=0;
            detailedData = new HashMap<String, Integer>();

        }

        public  void addDetailedData(SummaryReport summaryReport, String refactoringCase, int value) {
            HashMap<String, Integer> detailedData = summaryReport.detailedData;
            if (detailedData.containsKey(refactoringCase)) {
                detailedData.put(refactoringCase, detailedData.get(refactoringCase) + value);
            } else {
                detailedData.put(refactoringCase, value);
            }
        }
    }


//public class RootReporter extends Reporter{
//
//    @Override
//    public String getReportData() {
//        return null;
//    }
//
//    @Override
//    public void addReporter(Reporter reporter) {
//
//    }
//}


}
