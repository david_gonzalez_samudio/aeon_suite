package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.psi.PsiMethod;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public class MethodReporter extends Reporter {
    Vector<MethodReporter> methodCallReporters;// method calls


    public MethodReporter(Reporter caller, PsiMethod method, int currentIndirectionLevel) {
        this.parent = caller;
        this.this_element = method;
        methodCallReporters = new Vector<MethodReporter>();

        this.currentIndirectionLevel = currentIndirectionLevel;
        isCertain = caller.isCertain;
    }

    public MethodReporter(PsiMethod method, int currentIndirectionLevel) {
        this.parent = null;
        this.this_element = method;
        methodCallReporters = new Vector<MethodReporter>();

        this.currentIndirectionLevel = currentIndirectionLevel;
        isCertain = true;
    }

    public void makeReport(PsiMethod psiMethod) {

    }

    public boolean isCyclicCall() {

        while (parent != null) {
            if (parent instanceof MethodReporter) {
                if (((MethodReporter) parent).this_element.equals(this_element)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String getReportData() {
        StringBuffer indent = new StringBuffer();
        for (int i = 0; i < currentIndirectionLevel; i++) {
            indent.append("\t");

        }
        String indentation = indent.toString();
        StringBuffer out = new StringBuffer();
        for (Report faultReport : faultReports) {
            out.append("\t" + faultReport.getReportData());
        }

        out.append(indentation + ">>> Method SummaryReport " + ((PsiMethod) this_element).getName() + "\n");
        for (MethodReporter methodReporter : methodCallReporters) {
            out.append(methodReporter.getReportData());
        }
        out.append("\n");
        return out.toString();
    }

    @Override
    public void addReporter(Reporter reporter) {

    }
}