package edu.gmu.cs.aeon.core.analysis.report;

/**
 * Created by DavidIgnacio on 4/25/2015.
 */
public class MethodCallbackContext {
    public CallbackType callbackType=CallbackType.NONE;
    public String callbackOwnerClassQualifiedName="NONE";
    public String callbackOwnerClassUsedMethod="NONE";
    public enum CallbackType{
        NONE,
        APPLICATION,
        COMPONENT_LIFECYCLE,
        UI_LISTENER,
        ANDROID_SYSTEM
    }
    public static final MethodCallbackContext EMPTY_CALLBACK_CONTEXT=new MethodCallbackContext();
}
