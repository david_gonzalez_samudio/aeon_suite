package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.psi.*;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public class ReporterVisitor extends PsiRecursiveElementVisitor {
    Reporter reporter;

    public ReporterVisitor(Reporter reporter) {
        this.reporter = reporter;
    }

    public ReporterVisitor() {
        this.reporter = null;
    }

    private boolean isRootReporterSet() {
        if (this.reporter == null) {
            return true;
        }
        return false;
    }

    private void setRootReporter(Reporter reporter) {

        this.reporter = reporter;

    }

    @Override
    public void visitElement(final PsiElement element) {
        super.visitElement(element);
        //System.out.println(">>"+element.getText());
        boolean keepVisiting = true;
        if (element instanceof PsiClass) {
            if (reporter.this_element != element) {
                PsiClass psiClass = (PsiClass) element;
                Reporter classReporter = new ClassReporter(reporter, psiClass, reporter.currentIndirectionLevel + 1);

                reporter.addReporter(classReporter);
                // reporter=classReporter;
                psiClass.accept(new ReporterVisitor(classReporter));
                keepVisiting = false;

            }

        } else {

            if (element instanceof PsiMethod) {

//                PsiMethod psiMethod = (PsiMethod)element;
//
//                MethodReporter methodReporter = new MethodReporter(reporter, psiMethod, reporter.currentIndirectionLevel + 1);
//                    if(!methodReporter.isCyclicCall() && methodReporter.currentIndirectionLevel<Reporter.maxIndirectionLevel) {
//                        reporter.addReporter(methodReporter);
//                        psiMethod.accept(new ReporterVisitor(methodReporter));
//                        reporter=methodReporter;
//                        keepVisiting=false;
//                    }


            } else {
                if (element instanceof PsiMethodCallExpression) {
                    PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) element;
                    PsiMethod psiMethod = psiMethodCallExpression.resolveMethod();
                    if (psiMethod != null) {
                        Reporter methodReporter = new MethodReporter(reporter, psiMethod, reporter.currentIndirectionLevel + 1);

                        reporter.addReporter(methodReporter);
                        // reporter=methodReporter;
                        psiMethod.accept(new ReporterVisitor(methodReporter));
                        keepVisiting = false;
                    }


                } else {
                    if (element instanceof PsiIfStatement) {
                        if (!Reporter.isExploreConditional) {
                            keepVisiting = false;
                        }
                        reporter.isCertain = false;

                    } else {
                        if (element instanceof PsiLoopStatement) {
                            if (!Reporter.isExploreLoop) {
                                keepVisiting = false;
                            }
                            reporter.isCertain = false;

                        } else {
                            if (element instanceof PsiTryStatement) {
                                if (!Reporter.isExploreTry) {
                                    keepVisiting = false;
                                }
                                reporter.isCertain = false;
                            }


                        }
                    }

                }
            }
        }
//        super.visitElement(element);
//        if(keepVisiting) {
//            super.visitElement(element);
//        }
    }

}