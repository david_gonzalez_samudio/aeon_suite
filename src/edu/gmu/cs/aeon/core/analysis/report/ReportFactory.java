package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.psi.PsiElement;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/5/2015.
 */
public class ReportFactory {
    public static final Vector<Report> faultReports = new Vector<Report>();
    ;

    public static Report buildReport(AnalysisCategory analysisCategory, PredictionType predictionType, Reports.SummaryReport summaryReport, PsiElement cause, String reason, String extra, boolean isCertain) {
//        if(faultReports==null){
//            faultReports=new Vector<FaultReport>();
//        }
        //todo add specific element that cause the problem and fix factory
        Report newFaultReporter = new Report(analysisCategory, predictionType, summaryReport, cause, reason, extra, isCertain);
        //System.out.println(newFaultReporter.getReportData());
        for (Report faultReporter : faultReports) {
            if (faultReporter.toString().equals(newFaultReporter.toString())) {
                return faultReporter;
            }
        }
        faultReports.add(newFaultReporter);
        return newFaultReporter;
    }
}
