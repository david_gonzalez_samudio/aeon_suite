package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.psi.*;
import com.intellij.util.ArrayUtil;
import edu.gmu.cs.aeon.core.analysis.Analyzer;
import edu.gmu.cs.aeon.core.analysis.greedyloop.GreedyCallsInLoopAnalyzer;
import edu.gmu.cs.aeon.core.analysis.resources.ResourceReleasingAnalyzer;
import edu.gmu.cs.aeon.core.analysis.resources.location.LocationAPIAnalyzer;
import edu.gmu.cs.aeon.core.analysis.resources.powermanager.WakeLockAnalyzer;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;
import edu.gmu.cs.aeon.core.utils.PluginPsiUtil;

import java.io.*;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public class ReportManager {
    public static boolean writeToFile(String path, String fileName, String data, boolean isAppend) {
        File output = new File(path, fileName);
        System.out.println("Appending data to " + output.getAbsolutePath());

//        if(!isOverwrite) {
//            if (!output.exists()) {
//                try {
//                    output.createNewFile();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    return false;
//                }
//            }
//
//        }else{
//            if (output.exists()) {
//                output.delete();
//            }
//            try {
//                output.createNewFile();
//            } catch (IOException e) {
//                e.printStackTrace();
//                return false;
//            }
//
//        }
        FileWriter out = null;
        try {
            out = new FileWriter(output, isAppend);
            out.write(data);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

//    public static String applicationsBatchAnalysis(HashMap<String, HashMap<String, String>> appMap) {
//
//        for (Project project : ProjectManager.getInstance().getOpenProjects()) {
//            System.out.println("PRJ NAME " + project.getName());
//            return processPsiFiles(PluginPsiUtil.getProjectPsiFiles(project), appMap);
//        }
//        return "";
//    }

    public static HashMap<String, HashMap<String, String>> applicationsBatchLoad(String rootFolder, com.intellij.openapi.progress.ProgressIndicator progressIndicator, Project project) {
        HashMap<String, HashMap<String, String>> appMap = new HashMap<String, HashMap<String, String>>();

        File rootFolderFile = new File(rootFolder);
        File[] appFolders = null;
        if (rootFolderFile.isDirectory()) {
            appFolders = rootFolderFile.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    if (pathname.isDirectory()) {
                        return true;
                    }
                    return false;
                }
            });
        }


        if (appFolders == null) {
            System.out.println("Cannot find Directory " + rootFolder);
            return appMap;
        }
        double currentFraction = 0;
        int totalAppFolders = appFolders.length;
        int currentAppCount = 0;

        for (File appFolder : appFolders) {
            String projectName = appFolder.getName().trim();
            String projectFilePath = appFolder.getAbsolutePath();

            currentAppCount++;
            currentFraction = (double) currentAppCount / totalAppFolders;


            progressIndicator.setFraction(currentFraction);
            progressIndicator.setText("Copying App: " + projectName + " [path: " + projectFilePath + "]");
            progressIndicator.setText2(String.format("%3.2f", currentFraction * 100.0) + "% (" + currentAppCount + " of " + totalAppFolders + ")");
            //System.out.println("APP NAME " + projectName + " PATH " + projectFilePath);
            File[] outputFolderList = appFolder.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    if (pathname.isDirectory() && pathname.getName().equals("output")) {
                        return true;
                    }
                    return false;
                }
            });
            for (File outputFolder : outputFolderList) {
                File[] srcFolderList = outputFolder.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        if (pathname.isDirectory() && pathname.getName().equals("src")) {
                            return true;
                        }
                        return false;
                    }
                });
                //todo validate type of projec so it loads in src
                //ModuleUtil.getModulesOfType(, ModuleType.)



                String srcFilename = IntelliJUtilFacade.getSourceRootPathForCopying(project);


//                Collection<JavaModuleSourceRoot> roots=JavaSourceRootDetectionUtil.suggestRoots(new File(project.getBasePath()));
//
//                for (JavaModuleSourceRoot javaModuleSourceRoot:roots){
//                    srcFilename=javaModuleSourceRoot.getDirectory().getAbsolutePath();
//                }
                if(srcFilename==null) {
                    if (new File(project.getBasePath() + File.separator + "src").exists()) {
                        srcFilename = project.getBasePath() + File.separator + "src";// In plugin project
                    } else {
                        srcFilename = project.getBasePath() + File.separator + "app" + File.separator + "src";// in Android project
                    }
                }
                File srcFileProject = new File(srcFilename);
                // if(srcFileProject.exists() && srcFileProject.canWrite()){
                for (File srcFolder : srcFolderList) {

                    try {

//                                System.out.println("copying from "+ srcFolder.toPath()+" to "+ srcFilename);
                        HashMap<String, String> map = new HashMap<String, String>();

                        copyFolder(srcFolder, srcFileProject, map, projectName);
                        appMap.put(projectName, map);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
//                        }else{
//                            System.out.println("fail opening " + srcFilename);
//                        }
            }

        }
        progressIndicator.setFraction(1.0);
        progressIndicator.setText("finished");
        // FileContentUtil.reparseFiles();
        return appMap;

    }

    public static void copyFolder(File src, File dest, HashMap<String, String> map, String appKey)
            throws IOException {

        if (src.isDirectory()) {

            //if directory not exists, create it
            if (!dest.exists()) {
                dest.mkdir();
//                System.out.println("Directory copied from "
//                        + src + "  to " + dest);
            }

            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyFolder(srcFile, destFile, map, appKey);
            }

        } else {
            //if file, then copy it
            //Use bytes stream to support all file types
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);


            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
            map.put(dest.getCanonicalPath().replace('\\', '/'), appKey);
//            System.out.println("File copied from " + src.getAbsolutePath() + " to " + dest.getCanonicalPath());
        }
    }


    private static String getAppFromMap(final String filePath, final HashMap<String, HashMap<String, String>> appMap) {
        //  System.out.println("looking for file app: " +filePath.replace('\\','/'));
//        if (appMap != null) {
//            for (String appKey : appMap.keySet()) {
//                HashMap<String, String> appFiles = appMap.get(appKey);
//                if (appFiles.containsKey(filePath.replace('\\', '/'))) {
//                    return appKey;
//                }
//            }
//        } else {
//            int srcIndex = filePath.indexOf("src");
//            if (srcIndex > 0) {
//                String rest = filePath.substring(srcIndex + 4);
//                return rest.replace('\\', '/').substring(0, rest.indexOf('/'));
//            }
//        }
        if (appMap != null) {
            for (String appKey : appMap.keySet()) {
                HashMap<String, String> appFiles = appMap.get(appKey);
                String val=filePath.replace('\\', '/');
                if (appFiles.containsKey(val)) {
                    String leAppKey=appFiles.get(val);
                    if(leAppKey!=null){
                        return leAppKey;
                    }

                }
            }
        }
        return null;
    }

    public static String applicationsBatchAnalysis(HashMap<String, HashMap<String, String>> appMap, Project project, com.intellij.openapi.progress.ProgressIndicator progressIndicator) {
        return processPsiFiles(PluginPsiUtil.getProjectPsiFiles(project), project, appMap, progressIndicator);
    }
//
//    public static String processPsiFiles(Vector<PsiFile> projectPsiFiles, HashMap<String, HashMap<String, String>> appMap) {
//        final Reports reports = new Reports();
//        for (PsiFile psiFile : projectPsiFiles) {
//            String appName = getAppFromMap(psiFile.getVirtualFile().getCanonicalPath(), appMap);
//            if (appName != null) {
//
//                final Reports.SummaryReport summaryReport = reports.getReport(appName);
//                summaryReport.totalFiles++;
//                //   System.out.println("analyzing file: " + psiFile.getName()+" from app "+appName);
//                psiFile.accept(new PsiRecursiveElementWalkingVisitor() {
//                                   @Override
//                                   public void visitElement(PsiElement element) {
//
//                                       Refactorer redundantOperationInLoop = new RedundantOperationInLoop();
//                                       if (redundantOperationInLoop.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, redundantOperationInLoop.getText(), 1);
//                                       }
//                                       Refactorer locationProviderAnalyzer = new LocationAPIAnalyzer();
//                                       if (locationProviderAnalyzer.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, locationProviderAnalyzer.getText(), 1);
//                                       }
//
//                                       Refactorer wakeLockAnalyzer = new WakeLockAnalyzer();
//                                       if (wakeLockAnalyzer.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, wakeLockAnalyzer.getText(), 1);
//                                       }
//
//                                       Refactorer urlConnectionInLoop = new URLConnectionInLoop();
//                                       if (urlConnectionInLoop.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, urlConnectionInLoop.getText(), 1);
//                                       }
//                                       Refactorer httpClientInLoop = new HttpClientInLoop();
//                                       if (httpClientInLoop.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, httpClientInLoop.getText(), 1);
//                                       }
//                                       Refactorer variableFinalizerRefactor = new VariableFinalizerRefactor();
//                                       if (variableFinalizerRefactor.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, variableFinalizerRefactor.getText(), 1);
//                                       }
//
//                                       Refactorer onProvideAssistDataListenerAPIAnalyzer = new OnProvideAssistDataListenerAPIAnalyzer();
//                                       if (onProvideAssistDataListenerAPIAnalyzer.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, onProvideAssistDataListenerAPIAnalyzer.getText(), 1);
//                                       }
//                                       super.visitElement(element);
//                                   }
//                               }
//
//                );
//
//            }
//        }
//
//        return reports.getDetailedFinalReport();
//    }

    static int currentFileCount = 0;
    static int totalLOC ;
    static int javaCount=0;
    static int totalProjectPsiFiles;
    public enum HeaderName{
        METHODS_WAKE_LOCK_IS_USED_CORRECTLY,
        METHODS_WAKE_LOCK_IS_FAULTY,
        METHODS_LOCATION_IS_USED_CORRECTLY,
        TOTAL_METHODS_WAKE_LOCK_IS_PRESENT, TOTAL_METHODS_LOCATION_IS_PRESENT, METHODS_LOCATION_IS_FAULTY,
        TOTAL_GREEDY_CALLS_IN_LOOPS, RELEASING_RESOURCES_OK, RELEASING_RESOURCES_WRONG, TOTAL_ENERGY_GREEDY_METHODS

    }

    public static String processPsiFiles(Vector<PsiFile> projectPsiFiles, final Project project, HashMap<String, HashMap<String, String>> appMap, com.intellij.openapi.progress.ProgressIndicator progressIndicator) {
        final Reports reports = new Reports();
        double currentFraction = 0;
        totalProjectPsiFiles = projectPsiFiles.size();
        currentFileCount = 0;
        double pulse = 0.05;
        double lastFraction = 0;
        javaCount=0;
        totalLOC = 0;
        HashMap<String, Integer> appJavaFilesCounts=new HashMap<String, Integer>();

        String srcFilename=null;
        if(new File(project.getBasePath() + File.separator + "src").exists()) {
            srcFilename = project.getBasePath() + File.separator + "src";// In plugin project
        }else{
            srcFilename = project.getBasePath() + File.separator + "app" + File.separator + "src";// in Android project
        }
//try {
    for (PsiFile psiFile : projectPsiFiles) {
        currentFileCount++;
        int lineCount = FileDocumentManager.getInstance().getDocument(psiFile.getVirtualFile()).getLineCount();
        totalLOC += lineCount;

        currentFraction = (double) currentFileCount / totalProjectPsiFiles;
        String appName = null;
        if (appMap != null) {

            appName = getAppFromMap(psiFile.getVirtualFile().getCanonicalPath(), appMap);
        } else {
            if (psiFile.getVirtualFile().getCanonicalPath().replace('\\', '/').startsWith(srcFilename.replace('\\', '/'))) {
                appName = project.getName();
            }
        }

        progressIndicator.setFraction(currentFraction);
        progressIndicator.setText("Analyzing file: " + psiFile.getName() + " [path: " + psiFile.getContainingDirectory().getVirtualFile().getCanonicalPath() + "]");
        progressIndicator.setText2(String.format("%3.2f", currentFraction * 100.0) + "% (" + currentFileCount + " of " + totalProjectPsiFiles + ")");
        if ((currentFraction - lastFraction) >= pulse) {
            System.out.println(String.format("%3.2f", currentFraction * 100.0) + "% (" + currentFileCount + " of " + totalProjectPsiFiles + ")");
            lastFraction = currentFraction;
        }
        if (appName != null) {


            final Reports.SummaryReport summaryReport = reports.getReport(appName);
            summaryReport.totalFiles++;
            summaryReport.appLOC += lineCount;

            System.out.println("analyzing file: " + psiFile.getName() + " from app " + appName);
            final String AfileName = psiFile.getName() + "_" + System.nanoTime();
//&& IntelliJUtilFacade.isPsiElementInSourceRoots(psiFile)
            if (psiFile instanceof PsiJavaFile) {

                javaCount++;
                Integer count = 0;
                summaryReport.javaLOC += lineCount;
                if (appJavaFilesCounts.containsKey(appName)) {
                    count = appJavaFilesCounts.get(appName);

                }
                appJavaFilesCounts.put(appName, count + 1);

                psiFile.accept(new PsiRecursiveElementWalkingVisitor() {
                                   @Override
                                   public void visitElement(PsiElement element) {

//                                       Refactorer redundantOperationInLoop = new RedundantOperationInLoop();
//                                       if (redundantOperationInLoop.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, redundantOperationInLoop.getText(), 1);
//                                       }
//                                       Refactorer locationProviderAnalyzer = new LocationAPIAnalyzer();
//                                       if (locationProviderAnalyzer.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, locationProviderAnalyzer.getText(), 1);
//                                       }

                                       WakeLockAnalyzer wakeLockAnalyzer = new WakeLockAnalyzer();
                                       wakeLockAnalyzer.mode = Analyzer.Mode.BATCH;
                                       if (wakeLockAnalyzer.isUseCase(element)) {
                                           // summaryReport.addDetailedData(summaryReport, wakeLockAnalyzer.getText(), 1);
                                       }
                                       summaryReport.addDetailedData(summaryReport, HeaderName.TOTAL_METHODS_WAKE_LOCK_IS_PRESENT.name(), wakeLockAnalyzer.totalCount);
                                       summaryReport.addDetailedData(summaryReport, HeaderName.METHODS_WAKE_LOCK_IS_USED_CORRECTLY.name(), wakeLockAnalyzer.totalCount - wakeLockAnalyzer.faultyCount);
                                       summaryReport.addDetailedData(summaryReport, HeaderName.METHODS_WAKE_LOCK_IS_FAULTY.name(), wakeLockAnalyzer.faultyCount);
                                       String wakeData = "";
                                       String pathwakefaulty = project.getBasePath() + File.separator + "WakeLockData" + File.separator + summaryReport.appName + File.separator + "faulty";
                                       String pathwakeok = project.getBasePath() + File.separator + "WakeLockData" + File.separator + summaryReport.appName + File.separator + "using";
                                       //  FileUtil.de;

                                       String wafefile = AfileName + "_Faulty.txt";
                                       int found = 0;
                                       for (Report faultReport : wakeLockAnalyzer.getReports(null)) {
                                           found++;
                                           wakeData += "Fault " + found + ":" + faultReport.getReportData() + "\n";

                                       }
                                       if (found > 0) {
                                           FileUtil.createDirectory(new File(pathwakefaulty));
                                           writeToFile(pathwakefaulty, wafefile, wakeData, false);
                                           summaryReport.faultyPaths += pathwakefaulty + File.separator + wafefile + ";";
                                       }
                                       String wakeDataok = "";
                                       String wafefileok = AfileName + "_Using.txt";
                                       int foundok = 0;
                                       for (Report faultReport : wakeLockAnalyzer.okReports) {
                                           foundok++;
                                           wakeDataok += "Using " + foundok + ":" + faultReport.getReportData() + "\n";

                                       }
                                       if (foundok > 0) {
                                           FileUtil.createDirectory(new File(pathwakeok));
                                           writeToFile(pathwakeok, wafefileok, wakeDataok, false);
                                           summaryReport.usingpaths += pathwakeok + File.separator + wafefileok + ";";
                                       }


                                       LocationAPIAnalyzer locationAPIAnalyzer = new LocationAPIAnalyzer();
                                       locationAPIAnalyzer.mode = Analyzer.Mode.BATCH;
                                       if (locationAPIAnalyzer.isUseCase(element)) {
                                           //  summaryReport.addDetailedData(summaryReport, locationAPIAnalyzer.getText(), 1);
                                       }

                                       summaryReport.addDetailedData(summaryReport, HeaderName.METHODS_LOCATION_IS_FAULTY.name(), locationAPIAnalyzer.faultyCount);
                                       summaryReport.addDetailedData(summaryReport, HeaderName.METHODS_LOCATION_IS_USED_CORRECTLY.name(), locationAPIAnalyzer.totalCount - locationAPIAnalyzer.faultyCount);
                                       summaryReport.addDetailedData(summaryReport, HeaderName.TOTAL_METHODS_LOCATION_IS_PRESENT.name(), locationAPIAnalyzer.totalCount);
                                       String locData = "";
                                       String pathlocfaulty = project.getBasePath() + File.separator + "LocationData" + File.separator + summaryReport.appName + File.separator + "faulty";
                                       String pathlocok = project.getBasePath() + File.separator + "LocationData" + File.separator + summaryReport.appName + File.separator + "using";
                                       //  FileUtil.de;

                                       String locfile = AfileName + "_Faulty.txt";
                                       int foundloc = 0;
                                       for (Report faultReport : locationAPIAnalyzer.getReports(null)) {
                                           foundloc++;
                                           locData += "Fault " + foundloc + ":" + faultReport.getReportData() + "\n";

                                       }
                                       if (foundloc > 0) {
                                           FileUtil.createDirectory(new File(pathlocfaulty));
                                           writeToFile(pathlocfaulty, locfile, locData, false);
                                           summaryReport.faultyPaths += pathlocfaulty + File.separator + locfile + ";";
                                       }
                                       String locDataok = "";
                                       //  FileUtil.de;

                                       String locfileok = AfileName + "_Using.txt";
                                       int foundlocok = 0;
                                       for (Report faultReport : locationAPIAnalyzer.okReports) {
                                           foundlocok++;
                                           locDataok += "Using " + foundlocok + ":" + faultReport.getReportData() + "\n";

                                       }
                                       if (foundlocok > 0) {
                                           FileUtil.createDirectory(new File(pathlocok));
                                           writeToFile(pathlocok, locfileok, locDataok, false);
                                           summaryReport.usingpaths += pathlocok + File.separator + locfileok + ";";
                                       }


//



                                           int greedycount = 0;
                                           String pathgreedyfaulty = project.getBasePath() + File.separator + "GreedyLoopData" + File.separator + summaryReport.appName + File.separator + "faulty";

                                           String filegreedy = AfileName + "_Faulty.txt";
                                           StringBuffer textgreedy = new StringBuffer();
                                           GreedyCallsInLoopAnalyzer greedyAnalyzer = new GreedyCallsInLoopAnalyzer();
                                           greedyAnalyzer.doAnalysis(element);
                                           Vector<Report> greedyreports = greedyAnalyzer.faultReports;
                                           summaryReport.addDetailedData(summaryReport, HeaderName.TOTAL_GREEDY_CALLS_IN_LOOPS.name(), greedyreports.size());

                                           for (Report report1 : greedyreports) {
                                               greedycount++;
                                               textgreedy.append( "Greedy ICall " + greedycount + ": " + report1.getReportData() + "\n");
                                           }
                                           if (greedycount > 0) {
                                               FileUtil.createDirectory(new File(pathgreedyfaulty));
                                               writeToFile(pathgreedyfaulty, filegreedy, textgreedy.toString(), false);
                                               summaryReport.faultyPaths += pathgreedyfaulty + File.separator + filegreedy + ";";

                                           }




                                       int foundenergyfaulty = 0;
                                       String pathenergyfaulty = project.getBasePath() + File.separator + "GreedyData" + File.separator + summaryReport.appName + File.separator + "faulty";

                                       String energyfilefaulty = AfileName + "_Faulty.txt";
                                       String energydatatext = "";
                                       summaryReport.addDetailedData(summaryReport, HeaderName.TOTAL_ENERGY_GREEDY_METHODS.name(), greedyAnalyzer.okReports.size());
                                               for (Report report1 : greedyAnalyzer.okReports) {
                                                   foundenergyfaulty++;
                                                   energydatatext += "Energy " + foundenergyfaulty + ": " + report1.getReportData() + "\n";
                                               }

                                       if (foundenergyfaulty > 0) {
                                           FileUtil.createDirectory(new File(pathenergyfaulty));
                                           writeToFile(pathenergyfaulty, energyfilefaulty, energydatatext, false);
                                           summaryReport.faultyPaths += pathenergyfaulty + File.separator + energyfilefaulty + ";";

                                       }







                                       ResourceReleasingAnalyzer releasingAnalyzer = new ResourceReleasingAnalyzer();
                                       releasingAnalyzer.doAnalysis(element);
                                       StringBuffer textreleasing = new StringBuffer();
                                       String filereleasing= AfileName + "_Faulty.txt";
                                       int releasingcount = 0;
                                       String pathreleasingfaulty = project.getBasePath() + File.separator + "ReleasingData" + File.separator + summaryReport.appName + File.separator + "faulty";

                                       Vector<Report> releasingreports = releasingAnalyzer.faultReports;
                                       summaryReport.addDetailedData(summaryReport, HeaderName.RELEASING_RESOURCES_WRONG.name(), releasingreports.size());

                                       for (Report report1 : releasingreports) {
                                           releasingcount++;
                                           textreleasing.append( "Releasing Call " + releasingcount + ": " + report1.getReportData() + "\n");
                                       }
                                       if (releasingcount > 0) {
                                           FileUtil.createDirectory(new File(pathreleasingfaulty));
                                           writeToFile(pathreleasingfaulty, filereleasing, textreleasing.toString(), false);
                                           summaryReport.faultyPaths += pathreleasingfaulty + File.separator + filereleasing + ";";

                                       }
                                       StringBuffer textreleasingOk = new StringBuffer();
                                       String filereleasingOk= AfileName + "_Using.txt";
                                       int releasingcountOk = 0;
                                       String pathreleasingOk = project.getBasePath() + File.separator + "ReleasingData" + File.separator + summaryReport.appName + File.separator + "using";

                                       Vector<Report> releasingreportsOk = releasingAnalyzer.okReports;
                                       summaryReport.addDetailedData(summaryReport, HeaderName.RELEASING_RESOURCES_OK.name(), releasingreportsOk.size());

                                       for (Report report1 : releasingreportsOk) {
                                           releasingcountOk++;
                                           textreleasingOk.append( "Releasing Call " + releasingcountOk + ": " + report1.getReportData() + "\n");
                                       }
                                       if (releasingcountOk > 0) {
                                           FileUtil.createDirectory(new File(pathreleasingOk));
                                           writeToFile(pathreleasingOk, filereleasingOk, textreleasingOk.toString(), false);
                                           summaryReport.faultyPaths += pathreleasingOk + File.separator + filereleasingOk + ";";

                                       }




//                                       Refactorer urlConnectionInLoop = new URLConnectionInLoop();
//                                       if (urlConnectionInLoop.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, urlConnectionInLoop.getText(), 1);
//                                       }
//                                       Refactorer httpClientInLoop = new HttpClientInLoop();
//                                       if (httpClientInLoop.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, httpClientInLoop.getText(), 1);
//                                       }
////                                       Refactorer variableFinalizerRefactor = new VariableFinalizerRefactor();
////                                       if (variableFinalizerRefactor.isUseCase(element)) {
////                                           summaryReport.addDetailedData(summaryReport, variableFinalizerRefactor.getText(), 1);
////                                       }
//
//                                       Refactorer onProvideAssistDataListenerAPIAnalyzer = new OnProvideAssistDataListenerAPIAnalyzer();
//                                       if (onProvideAssistDataListenerAPIAnalyzer.isUseCase(element)) {
//                                           summaryReport.addDetailedData(summaryReport, onProvideAssistDataListenerAPIAnalyzer.getText(), 1);
//                                       }
                                       super.visitElement(element);
                                   }
                               }

                );


            }
        }
    }
//}catch(Exception e){
//    e.printStackTrace();
//}
        progressIndicator.setFraction(0.99);
        progressIndicator.setIndeterminate(true);
        progressIndicator.setText("Analysis Finished. Generating SummaryReport");
        return reports.getDetailedFinalReportCSVFormatUtil(project.getBasePath(), javaCount, currentFileCount, totalProjectPsiFiles, totalLOC, appJavaFilesCounts);
    }

    public static Reporter getReport(PsiClass psiClass) {// prefers the Main Class of a Psifile
        Reporter root = new ClassReporter(psiClass, 0);
        ReporterVisitor reporterVisitor = new ReporterVisitor(root);
        // psiClass.accept(reporterVisitor);
        PsiMethod[] psiMethods = ArrayUtil.mergeArrays(psiClass.getAllMethods(), psiClass.getConstructors());

        for (PsiMethod method : psiMethods) {
            Reporter reporter = new MethodReporter(method, 0);
            method.accept(new ReporterVisitor(root));
            root.addReporter(reporter);
        }
        return reporterVisitor.reporter;
    }




}
