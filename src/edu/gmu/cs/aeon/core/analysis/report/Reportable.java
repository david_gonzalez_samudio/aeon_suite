package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.psi.PsiElement;

import java.util.Vector;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public interface Reportable {
    public Vector<Report> getReports(PsiElement psiElement);
}
