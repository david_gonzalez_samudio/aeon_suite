package edu.gmu.cs.aeon.core.analysis.report;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public enum PredictionType {
    NONE,
    NO_USAGE_FOUND,
    CORRECT_IMPLEMENTATION,
    ENERGY_FAULT,
    REQUIRES_RUNTIME_VALUES_NOT_COMPUTABLE_STATICALLY,
    UNKNOWN_CASE_REQUIRES_HUMAN,
    DECOMPILER_ERROR_FOUND
}
