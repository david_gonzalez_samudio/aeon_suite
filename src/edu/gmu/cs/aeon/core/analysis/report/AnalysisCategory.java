package edu.gmu.cs.aeon.core.analysis.report;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public enum AnalysisCategory {
    LOCATION,
    WAKELOCK,
    GREEDY_API,
    GREEDY_CALL_IN_LOOP,
    GREEDY_INTERNET_CALL_IN_LOOP,
    RESOURCE_MANAGEMENT, THREAD_FAULT


}
