package edu.gmu.cs.aeon.core.analysis.report;

import com.intellij.psi.PsiElement;

import java.util.ArrayList;

/**
 * Created by DavidIgnacio on 4/2/2015.
 */
public class Report {

    public PsiElement cause;// the causing element
    public PsiElement elementToHighLight;
    public ArrayList<PsiElement> involvedElements;
    public AnalysisCategory analysisCategory;
    public PredictionType predictionType;
    public MethodCallbackContext methodCallbackContext=MethodCallbackContext.EMPTY_CALLBACK_CONTEXT;
    public boolean isBad;
    public Fault.Level faultLevel;
    public Fault.Solution faultSolution;
    public String reason;// coming from the Fault Analyzer Reason enum
    public String extra; // unformatted data
    boolean isCertain;// is there a conditional/loop/try in the path
    public Reports.SummaryReport summaryReport;


    public Report(AnalysisCategory analysisCategory, PredictionType predictionType, Reports.SummaryReport summaryReport, PsiElement cause, String reason, String extra, boolean isCertain) {
        this.analysisCategory = analysisCategory;
        this.predictionType = predictionType;
        this.summaryReport = summaryReport;
        this.cause = cause;
        this.reason = reason;
        this.extra = extra;
        this.isCertain = isCertain;
    }


    public String getReportData() {
        //format to csv
        return this.toString();
    }
    public String getReportDataForUI() {
        if(isBad) {
            return "Fault found: " + predictionType.name() + ",  " + reason;
        }else{
            return "Good Fix found: " + predictionType.name() + ",  " + reason;
        }

    }

    @Override
    public String toString() {
        return "[" + cause.toString() + "]" + ", predictionType: " + predictionType.name() + ", Reason: " + reason + ", extra : " +extra;
    }


}