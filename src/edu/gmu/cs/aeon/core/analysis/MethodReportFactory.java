package edu.gmu.cs.aeon.core.analysis;

import com.intellij.psi.PsiMethod;

import java.util.*;

/**
 * Created by DavidIgnacio on 4/5/2015.
 */
class MethodReportFactory {
    public static final Map<PsiMethod, MethodReport> methods = new HashMap<PsiMethod, MethodReport>();


    public static MethodReport buildMethodReport( PsiMethod psiMethod) {

       if( methods.containsKey(psiMethod)){
           return methods.get(psiMethod);
       }else{
           MethodReport methodReport=new MethodReport(psiMethod);
           methods.put(psiMethod, new MethodReport(psiMethod));
           return methodReport;
       }

    }
    public static boolean isMethodReportAlready( PsiMethod psiMethod) {

        if( methods.containsKey(psiMethod)){
            return true;
        }else{
            return false;
        }

    }

}
