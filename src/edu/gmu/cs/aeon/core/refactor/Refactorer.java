package edu.gmu.cs.aeon.core.refactor;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.PsiElement;
import edu.gmu.cs.aeon.plugin.view.Showable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by DavidIgnacio on 6/1/2014.
 */
public interface Refactorer extends Showable {

    public boolean isUseCase(@NotNull PsiElement element);

    public boolean isRefactoredUseCase(@NotNull PsiElement element);

    @Nullable
    public PsiElement annotateUseCase(@NotNull PsiElement element, AnnotationHolder holder);

    @Nullable
    public PsiElement refactorElement(@NotNull PsiElement element);

    @Nullable
    public PsiElement previewRefactorElement(@NotNull PsiElement element);
}
