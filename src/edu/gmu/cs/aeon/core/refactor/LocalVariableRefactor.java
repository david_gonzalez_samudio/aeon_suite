package edu.gmu.cs.aeon.core.refactor;

/**
 * Created by DavidIgnacio on 5/11/2014.
 */

import com.intellij.codeInsight.actions.OptimizeImportsProcessor;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElementFactory;
import com.intellij.psi.PsiType;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.search.EverythingGlobalScope;
import edu.gmu.cs.aeon.core.BaseCodeGenerator;
import edu.gmu.cs.aeon.core.utils.IntelliJUtilFacade;

import java.util.List;

/**
 * User: anatoly
 * Date: 04.07.13
 * Time: 15:54
 */
public class LocalVariableRefactor extends BaseCodeGenerator {
    private final List<PsiType> mIds;
    private final PsiElementFactory elementFactory;

    public LocalVariableRefactor(PsiClass cls, List<PsiType> ids) {
        super(cls, "Inject views");
        mIds = ids;
        elementFactory = JavaPsiFacade.getElementFactory(prj);
    }


    @Override
    public void generate() {
        PsiClass cls = IntelliJUtilFacade.lookUpForContainingClass(_myPsiElement);
        PsiClass butterKnifeInjectAnnotation = JavaPsiFacade.getInstance(prj)
                .findClass("butterknife.InjectView", new EverythingGlobalScope(prj));
        if (butterKnifeInjectAnnotation == null) return;
        for (PsiType v : mIds) {
//            String sb = "@butterknife.InjectView(" + v.getId() + ")" + " " + v.getName() + " " + v.getFieldName() + ";";
//            cls.add(elementFactory.createFieldFromText(sb, cls));
        }

        JavaCodeStyleManager.getInstance(prj).shortenClassReferences(cls);
        new OptimizeImportsProcessor(prj, cls.getContainingFile())
                .runWithoutProgress();
    }

    @Override
    public void unGenerate() {

    }
}