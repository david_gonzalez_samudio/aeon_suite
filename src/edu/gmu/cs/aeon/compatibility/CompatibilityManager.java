package edu.gmu.cs.aeon.compatibility;

import com.intellij.openapi.application.ApplicationInfo;
import org.jetbrains.annotations.Nullable;

/**
 * Created by DavidIgnacio on 5/19/2015.
 */
public class CompatibilityManager {
    public static final String COMPANY_NAME_JETBRAINS="jetbrains";
    public static final String COMPANY_NAME_ANDROID_STUDIO="google";
    public static final boolean isIDECompatibleWithVersion(@Nullable String companyName,@Nullable String versionNumbers){// e.g. Google and  1.1.2
        if(companyName==null||versionNumbers==null){
            return false;
        }
        if(!ApplicationInfo.getInstance().getCompanyName().toLowerCase().startsWith(companyName.toLowerCase())){
            return false;
        }
        String[] splitVersion=versionNumbers.split("\\.");
        if(splitVersion.length==0){
            return false;
        }
        final int majorVersion=Integer.parseInt(ApplicationInfo.getInstance().getMajorVersion());
        if(majorVersion<Integer.parseInt(splitVersion[0])){
            return false;
        }
        final String[] minorVersion=ApplicationInfo.getInstance().getMinorVersion().split("\\.");
        final int minIndex=Math.min(splitVersion.length-1, minorVersion.length);
        for(int i=0;i<minIndex;i++){
            if(Integer.parseInt(minorVersion[i])>Integer.parseInt(splitVersion[i+1])){
                return true;
            }else{
                if(Integer.parseInt(minorVersion[i])<Integer.parseInt(splitVersion[i+1])){
                    return false;
                }
            }
        }

        return true;
    }
}
