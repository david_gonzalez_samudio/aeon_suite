package edu.gmu.cs.aeon.compatibility;

import com.intellij.openapi.application.ApplicationInfo;
import com.intellij.ui.components.JBPanel;
import com.intellij.ui.components.panels.VerticalLayout;
import org.jetbrains.annotations.Nullable;

/**
 * Created by DavidIgnacio on 5/19/2015.
 */
public class UICompatibilityManager extends CompatibilityManager{

    public static boolean addVerticalLayout(JBPanel jbPanel){
        boolean isCompatible=isIDECompatibleWithVersion(COMPANY_NAME_JETBRAINS, "14.1.1")||isIDECompatibleWithVersion(COMPANY_NAME_ANDROID_STUDIO, "1.1.1");
        if(isCompatible) {
            jbPanel.setLayout(new VerticalLayout(1));
        }
        return isCompatible;
    }


}
