package edu.gmu.cs.aeon.trepn.common;

import com.intellij.ide.util.PropertiesComponent;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */
public class PreferenceStore {

    public PreferenceStore(String oldPrefPath) {
//todo specify project to store preferences
    }

    public PreferenceStore() {

    }

    public void setValue(String lastSdkPath, String osSdkPath) {
        PropertiesComponent.getInstance().setValue(lastSdkPath, osSdkPath);
    }

    public void save() throws IOException {
//   handled by     com.intellij.openapi.components.PersistentStateComponent
    }

    public void setValue(String adtUsed, boolean used) {

        PropertiesComponent.getInstance().setValue(adtUsed, String.valueOf(used));
    }

    public String getString(String lastSdkPath) {
        return PropertiesComponent.getInstance().getValue(lastSdkPath);
    }

    public boolean getBoolean(String adtUsed) {

        return PropertiesComponent.getInstance().getBoolean(adtUsed, false);
    }

    public boolean contains(String adtUsed) {

        return PropertiesComponent.getInstance().isValueSet(adtUsed);
    }

    public void load() throws IOException {
// handled by com.intellij.openapi.components.PersistentStateComponent
    }

    public void save(FileOutputStream fileOutputStream, String s) {
//todo specify project to store preferences

// handled by com.intellij.openapi.components.PersistentStateComponent
    }

    public void setValue(String timePref, long timeStamp) {
        PropertiesComponent.getInstance().setValue(timePref, String.valueOf(timeStamp));
    }

    public long getLong(String pingId) {
        return PropertiesComponent.getInstance().getOrInitLong(pingId, 0);
    }
}


