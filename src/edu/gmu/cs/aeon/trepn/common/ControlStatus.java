package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */


public class ControlStatus {
    private static ControlStatus controlStatus = null;
    private final OnStatusUpdateListener listener;
    private ControlState currentState;

    public ControlStatus(OnStatusUpdateListener listener) {
        this.listener = listener;
        controlStatus = this;
        this.currentState = ControlState.NO_DEVICES_FOUND;
    }

    public static ControlStatus getCurrent() {
        return controlStatus;
    }

    public void updateStatus(ControlState state) {
        updateStatusMsg(state, "");
    }

    public ControlState getState() {
        return this.currentState;
    }

    public void updateStatusMsg(ControlState state, String status) {
        boolean statusChange = false;
        switch (this.currentState) {
            case READY:
                if ((state == ControlState.PROCESSING_CHARTS) || (state == ControlState.GENERATING_CHARTS)) {
                    this.currentState = state;
                    statusChange = true;
                }
                break;
            case CONFIGURING_TREPN:
                this.currentState = state;
                statusChange = true;
                break;
            case STARTING_TREPN:
                if (state == ControlState.GENERATING_CHARTS_COMPLETE) {
                    this.currentState = state;
                    statusChange = true;
                }
                break;
            case GENERATING_CHARTS:
            case GENERATING_CHARTS_COMPLETE:
            case NOT_AUTHORIZED:
            case NO_DEVICES_FOUND:
                this.currentState = state;
                statusChange = true;
                break;
            case PROFILING_IN_PROGRESS:
                if ((state != ControlState.READY) && (state != ControlState.PROFILING_IN_PROGRESS)) {
                    this.currentState = state;
                    statusChange = true;
                }
                break;
            case PULLING_DATA_FROM_DEVICE:
                if (state != ControlState.READY) {
                    this.currentState = state;
                    statusChange = true;
                }
                break;
            case ERROR:
                this.currentState = state;
                statusChange = true;
                break;
            case PROCESSING_CHARTS:
            default:
                if (this.currentState != state) {
                    this.currentState = state;
                    statusChange = true;
                }
                break;
        }
        if (statusChange) {
            this.listener.onStatusUpdate(state, status);
        }
    }

    public static enum ControlState {
        NO_DEVICES_FOUND, NOT_AUTHORIZED, ERROR, READY, CHECKING_TREPN_VERSION, UPDATING_TREPN, STARTING_TREPN, CONFIGURING_TREPN, PROFILING_IN_PROGRESS, STOPPING, STOPPING_SERVICE, PULLING_DATA_FROM_DEVICE, PROCESSING_CHARTS, GENERATING_CHARTS, GENERATING_CHARTS_COMPLETE, ACTION_DONE;
    }
}
