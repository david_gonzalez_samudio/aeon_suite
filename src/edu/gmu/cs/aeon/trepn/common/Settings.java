package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Settings {
    public static final int MINIMUM_PROFILE_INTERVAL = 100;
    public static final int MAXIMUM_PROFILE_INTERVAL = 5000000;
    public static final String SETTINGS_NODE_GENERAL = "generalSettingsNode";
    private static final String SETTINGS_VERSION = "1.1.0";
    private static final String SETTINGS_NODE_CONTROL = "controlSettingsNode";
    private static final String SETTINGS_NODE_DATAPOINTS = "datapointsNode";
    private static final String SETTINGS_NODE_PREMIER_DATAPOINTS = "premierDataPoints";
    private static Settings current = new Settings();
    private static String sensorIDs;
    private static HashMap<String, String> sensorIDLookup;
    private static HashMap<String, String> premierStatLookup;
    //   private static IEclipsePreferences prefs;
    private final ArrayList<ISettingsListener> listeners;

    public Settings() {
        this.listeners = new ArrayList();
//        setUpDefaults();
    }

    public static Settings getCurrent() {
        if (current == null) {
            current = new Settings();
        }
        return current;
    }

    public static ArrayList<StatType> getEnabledDatapoints(boolean includePremierStatTypes) {
        ArrayList<StatType> enabledList = new ArrayList();
//
//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
//        if (prefs != null)
//        {
//            Preferences subNode = prefs.node("datapointsNode");
        for (StatType statType : StatType.getAllOrderedStatTypes()) {
//                $SWITCH_TABLE$com$qualcomm$trepn$ide$eclipse$common$StatType()[statType.ordinal()];
//                if (subNode.getBoolean(statType.toString(), false)) {
            enabledList.add(statType);
//                }
        }
//            if (includePremierStatTypes) {
//                for (StatType statType : StatType.getAllPremierOrderedStatTypes()) {
//                    if ((isSensorIDPresent(((Integer)statType.getSensorIDs().get(0)).toString())) &&
//                            (subNode.getBoolean(statType.toString(), false))) {
//                        enabledList.add(statType);
//                    }
//                }
//            }
//        }
        return enabledList;
    }

    public static void setProfilingDefaults() {
        setSettingInt("generalSettingsNode", "PROFILE_INTERVAL", 100, false);
        setDefaultDatapoints();
    }

    public static void setDefaultDatapoints() {
        clearNode("datapointsNode");

        setSettingBool("datapointsNode", StatType.BATTERY_POWER.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.BLUETOOTH_STATE.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.CPU.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.CPU_NORM_LOAD.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.GPU3D_LOAD.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.DISPLAY_BRIGHTNESS.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.WAKELOCKS.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.WIFILOCKS.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.MOBILE_NETWORK_STATE.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.WIFI_STATE.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.GPS_STATE.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.MOBILE_DATA_USAGE_PER_APP.toString(), true, false, true);
        setSettingBool("datapointsNode", StatType.WIFI_DATA_USAGE_PER_APP.toString(), true, true, true);
    }

    public static int getEnabledDatapointsCount() {
//        int enabledCount = 0;
//        boolean premierSensorsPresent = arePremierSensorIDsPresent();
//        if (premierStatLookup == null)
//        {
//            premierStatLookup = new HashMap();
//            for (StatType statType : StatType.getAllPremierOrderedStatTypes()) {
//                premierStatLookup.put(statType.toString(), statType.toString());
//            }
//        }
//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
//        if (prefs != null)
//        {
//            Preferences subNode = prefs.node("datapointsNode");
//            try
//            {
//                for (String key : subNode.keys()) {
//                    if ((subNode.getBoolean(key, false)) && (
//                            (premierSensorsPresent) || (!premierStatLookup.containsKey(key)))) {
//                        enabledCount++;
//                    }
//                }
//            }
//            catch (BackingStoreException e)
//            {
//                e.printStackTrace();
//            }
//        }
//        return enabledCount;
        return getEnabledDatapoints(true).size();
    }

    static void clearNode(String nodeID) {
//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
//        if (prefs != null)
//        {
//            Preferences subNode = prefs.node(nodeID);
//            try
//            {
//                subNode.clear();
//
//
//                subNode.flush();
//            }
//            catch (BackingStoreException localBackingStoreException) {}
//        }
    }

    public static boolean getDatapointValue(StatType statType) {
        return getSettingBool("datapointsNode", statType.toString(), false);
    }

    //
    public static void setDatapoint(StatType statType, boolean checked) {
        setSettingBool("datapointsNode", statType.toString(), checked, true, true);
    }

    public static void setDatapoint(StatType statType, boolean checked, boolean notify) {
        setSettingBool("datapointsNode", statType.toString(), checked, notify, true);
    }

    private static boolean areGeneralSettingsPresent() {
        boolean result = false;
//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
//        if (prefs != null)
//        {
//            Preferences subNode = prefs.node("generalSettingsNode");
//            try
//            {
//                if (subNode.keys().length > 0) {
//                    result = true;
//                }
//            }
//            catch (BackingStoreException localBackingStoreException) {}
//        }
        return result;
    }

    public static void setDefaultGeneralSettings() {
        clearNode("generalSettingsNode");
        setSettingBool("generalSettingsNode", "HIDE_DISCONNECT_PROMPT", false, false, true);
        setSettingInt("generalSettingsNode", "PROFILE_INTERVAL", 100, true);
        setSettingBool("generalSettingsNode", "HIDE_SETTINGS_CHARTS_TOOL_TIPS", false, false, true);
    }

    public static void setGeneralSettingBool(String setting, boolean value) {
        setSettingBool("generalSettingsNode", setting, value, true, true);
    }

    public static boolean getGeneralSettingBool(String setting, boolean defaultValue) {
        return getSettingBool("generalSettingsNode", setting, defaultValue);
    }

    public static void setGeneralSettingInt(String setting, int value) {
        setGeneralSettingInt(setting, value, true);
    }

    public static void setGeneralSettingInt(String setting, int value, boolean notify) {
        setSettingInt("generalSettingsNode", setting, value, notify);
    }

    public static int getGeneralSettingInt(String setting, int defaultValue) {
        return getSettingInt("generalSettingsNode", setting, defaultValue);
    }

    //
    public static int getGeneralSettingIntWithRangeCheck(String setting, int defaultValue, int minValue, int maxValue) {
        return getSettingIntWithRangeCheck("generalSettingsNode", setting, defaultValue, minValue, maxValue);
    }

    public static void setControlSettingBool(String setting, boolean value) {
        setSettingBool("controlSettingsNode", setting, value, false, true);
    }

    public static boolean getControlSettingBool(String setting, boolean defaultValue) {
        return getSettingBool("controlSettingsNode", setting, defaultValue);
    }

    //
    static void setSettingBool(String nodeID, String setting, boolean defaultValue, boolean notify, boolean flush) {
        PreferenceStore preferenceStore = new PreferenceStore();
        preferenceStore.setValue(setting, defaultValue);
    }

    public static boolean getSettingBool(String nodeID, String setting, boolean defaultValue) {
        boolean value = false;

//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
//        Preferences subNode = prefs.node(nodeID);
//        if ((subNode != null) &&
//                (setting != null) && (!setting.isEmpty())) {
//            try
//            {
//                value = subNode.getBoolean(setting, defaultValue);
//            }
//            catch (Exception localException) {}
//        }
        PreferenceStore preferenceStore = new PreferenceStore();
        value = preferenceStore.getBoolean(setting);
        return value;
    }

    public static int getSettingIntWithRangeCheck(String nodeID, String setting, int defaultValue, int minValue, int maxValue) {
        int value = defaultValue;

//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
        PreferenceStore preferenceStore = new PreferenceStore();
//        Preferences subNode = prefs.node(nodeID);
        String subNode = preferenceStore.getString(nodeID);
//        if ((subNode != null) &&
//                (setting != null) && (!setting.isEmpty())) {
//            try
//            {
//        value = subNode.getInt(setting, defaultValue);
        try {

            value = Integer.valueOf(setting);
        } catch (Exception e) {
            value = defaultValue;
        }
        if (value < minValue) {
            value = minValue;
        } else if (value > maxValue) {
            value = maxValue;
        }
//            }
//            catch (Exception localException) {}
//        }
//        return value;
        return value;
    }

    static int getSettingInt(String nodeID, String setting, int defaultValue) {
        int value = defaultValue;

//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
//        Preferences subNode = prefs.node(nodeID);
//        if ((subNode != null) &&
//                (setting != null) && (!setting.isEmpty())) {
//            try
//            {
//                value = subNode.getInt(setting, defaultValue);
//            }
//            catch (Exception localException) {}
//        }
        return value;
    }

    static void setSettingInt(String nodeID, String setting, int value, boolean notify) {
        PreferenceStore preferenceStore = new PreferenceStore();
        preferenceStore.setValue(setting, value);
//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
//        Preferences subNode = prefs.node(nodeID);
//        if ((subNode != null) &&
//                (setting != null) && (!setting.isEmpty())) {
//            try
//            {
//                subNode.putInt(setting, value);
//                subNode.flush();
//                if (notify) {
//                    getCurrent().notifyListeners();
//                }
//            }
//            catch (BackingStoreException localBackingStoreException) {}
//        }
    }

    //    public static int getVisibleChartsCount()
//    {
//        int enabledCount = 0;
//
//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
//        if (prefs != null)
//        {
//            Preferences subNode = prefs.node("controlSettingsNode");
//            try
//            {
//                for (String key : subNode.keys()) {
//                    if (subNode.getBoolean(key, false)) {
//                        enabledCount++;
//                    }
//                }
//            }
//            catch (BackingStoreException e)
//            {
//                e.printStackTrace();
//            }
//        }
//        return enabledCount;
//    }
//
    public static String getSensorIDs() {
        return sensorIDs;
    }

    public static void setSensorIDs(String sensors) {
        if (((sensorIDs == null) && (sensors == null)) || (
                (sensorIDs != null) && (sensors != null) && (sensorIDs.equalsIgnoreCase(sensors)))) {
            return;
        }
        sensorIDs = sensors;
        if ((sensorIDs != null) && (!sensorIDs.isEmpty())) {
            String[] sensorIDList = sensorIDs.split("[,]");
            sensorIDLookup = new HashMap();
            for (String sensorID : sensorIDList) {
                if ((sensorID != null) && (!sensorID.isEmpty())) {
                    sensorID.trim();
                    try {
                        new Integer(sensorID).intValue();
                        sensorIDLookup.put(sensorID, sensorID);
                    } catch (Exception localException) {
                    }
                }
            }
        } else {
            sensorIDLookup = null;
        }
        getCurrent().notifyListeners();
    }

    public static boolean isSensorIDPresent(String sensorID) {
        boolean result = false;
        if ((sensorIDLookup != null) && (!sensorIDLookup.isEmpty())) {
            String lookupResult = (String) sensorIDLookup.get(sensorID);
            if ((lookupResult != null) && (!lookupResult.isEmpty())) {
                result = true;
            }
        }
        return result;
    }

    public static boolean arePremierSensorIDsPresent() {
        boolean result = false;

        List<StatType> statTypeList = StatType.getAllPremierOrderedStatTypes();
        for (StatType statType : statTypeList) {
            if (isSensorIDPresent(((Integer) statType.getSensorIDs().get(0)).toString())) {
                return true;
            }
        }
        return result;
    }

    public void registerListener(ISettingsListener listener) {
        if (listener != null) {
            this.listeners.add(listener);
        }
    }

    public void removeListener(ISettingsListener listener) {
        if (listener != null) {
            this.listeners.remove(listener);
        }
    }

    private void notifyListeners() {
        for (ISettingsListener listener : this.listeners) {
            if (listener != null) {
                listener.settingsChanged();
            }
        }
    }

    private void setUpDefaults() {
        checkVersion();
        if (getEnabledDatapointsCount() == 0) {
            setDefaultDatapoints();
        }
        if (!areGeneralSettingsPresent()) {
            setDefaultGeneralSettings();
        }
    }

    private void checkVersion() {
//        prefs = InstanceScope.INSTANCE.getNode("com.qualcomm.trepn.ide.eclipse.common");
        PreferenceStore preferenceStore = new PreferenceStore();
        String prefs = preferenceStore.getString("trepn.settings.version");
        if (prefs != null) {
            String version = prefs;
//            String version = prefs.get("trepn.settings.version", "");
            if (!version.contains("1.1.0")) {
//                try
//                {
//                    for (String key : prefs.keys()) {
//                        prefs.remove(key);
//                    }
//                    prefs.flush();
//                }
//                catch (BackingStoreException e)
//                {
//                    e.printStackTrace();
//                }

                preferenceStore.setValue("trepn.settings.version", "");
            }
            preferenceStore.setValue("trepn.settings.version", "1.1.0");
//            prefs.put("trepn.settings.version", "1.1.0");
        }
    }
}
