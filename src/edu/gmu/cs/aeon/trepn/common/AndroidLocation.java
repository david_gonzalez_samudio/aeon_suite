package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */

import org.jetbrains.annotations.NotNull;

import java.io.File;


public final class AndroidLocation {
    public static final String FOLDER_DOT_ANDROID = ".android";
    public static final String FOLDER_AVD = "avd";
    private static String sPrefsLocation = null;

    @NotNull
    public static final String getFolder()
            throws AndroidLocation.AndroidLocationException {
        if (sPrefsLocation == null) {
            String home = findValidPath(new String[]{"ANDROID_SDK_HOME", "user.home", "HOME"});
            if (home == null) {
                throw new AndroidLocationException(
                        "Unable to get the Android SDK home directory.\nMake sure the environment variable ANDROID_SDK_HOME is set up.");
            }
            sPrefsLocation = home;
            if (!sPrefsLocation.endsWith(File.separator)) {
                sPrefsLocation += File.separator;
            }
            sPrefsLocation = sPrefsLocation + ".android" + File.separator;
        }
        File f = new File(sPrefsLocation);
        if (!f.exists()) {
            try {
                f.mkdir();
            } catch (SecurityException e) {
                AndroidLocationException e2 = new AndroidLocationException(String.format(
                        "Unable to create folder '%1$s'. This is the path of preference folder expected by the Android tools.", new Object[]{

                                sPrefsLocation}));
                e2.initCause(e);
                throw e2;
            }
        } else if (f.isFile()) {
            throw new AndroidLocationException(sPrefsLocation +
                    " is not a directory! " +
                    "This is the path of preference folder expected by the Android tools.");
        }
        return sPrefsLocation;
    }

    public static final void resetFolder() {
        sPrefsLocation = null;
    }

    private static String findValidPath(String... names) {
        String[] arrayOfString = names;
        int j = names.length;
        for (int i = 0; i < j; i++) {
            String name = arrayOfString[i];
            String path;
            if (name.indexOf('.') != -1) {
                path = System.getProperty(name);
            } else {
                path = System.getenv(name);
            }
            if (path != null) {
                File f = new File(path);
                if (f.isDirectory()) {
                    return path;
                }
            }
        }
        return null;
    }

    public static final class AndroidLocationException
            extends Exception {
        private static final long serialVersionUID = 1L;

        public AndroidLocationException(String string) {
            super();
        }
    }
}
