package edu.gmu.cs.aeon.trepn.common;

import com.intellij.ide.plugins.PluginManager;
import com.intellij.openapi.extensions.PluginId;

import java.io.File;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */


public class TrepnGeneralValues {
    //    public static final String DEVICE_ROOT_PATH = "/mnt/sdcard/trepn/";
//    public static final String DEVICE_PREFERENCES_PATH = "/mnt/sdcard/trepn/saved_preferences/";
//    public static final long POLLING_INTERVAL = 100L;//in ms
//    public static final long APP_STATS_POLLING_INTERVAL = 1000L; //in ms
//    public static final int NETWORK_USAGE_MAX_TOP_APPS = 100;
    public static final String AEON_TREPN_PATH;
    public static final String SYSTEM_FILE_SEPARATOR;
    public static final String LOCAL_TREPN_DATAFILE_DIR;
    public static final String LOCAL_TREPN_LIB_DIR;
    public static final String LOCAL_TREPN_APK_DIR;
    public static final String LOCAL_TREPN_PREFS_DIR;

    static {
        String path = PluginManager.getPlugin(PluginId.getId("edu.gmu.cs.plugin.aeon")).getPath().getAbsolutePath();
        SYSTEM_FILE_SEPARATOR = File.separator;
        AEON_TREPN_PATH = path + SYSTEM_FILE_SEPARATOR + "_trepn" + SYSTEM_FILE_SEPARATOR;
        LOCAL_TREPN_DATAFILE_DIR = AEON_TREPN_PATH + "logs" + SYSTEM_FILE_SEPARATOR;
        LOCAL_TREPN_LIB_DIR = AEON_TREPN_PATH + "lib" + SYSTEM_FILE_SEPARATOR;
        LOCAL_TREPN_APK_DIR = AEON_TREPN_PATH + "apk" + SYSTEM_FILE_SEPARATOR;
        LOCAL_TREPN_PREFS_DIR = AEON_TREPN_PATH + "prefs" + SYSTEM_FILE_SEPARATOR;
    }


}

