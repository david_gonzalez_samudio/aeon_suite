package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */


public abstract interface OnStatusUpdateListener {
    public abstract void onStatusUpdate(ControlStatus.ControlState paramControlState, String paramString);
}

