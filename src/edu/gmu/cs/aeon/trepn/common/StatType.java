package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

public enum StatType {
    BATTERY_POWER("Battery Power", new Integer[]{Integer.valueOf(332)}),
    BLUETOOTH_STATE("Bluetooth State", new Integer[]{Integer.valueOf(207)}),
    CPU("CPU", new Integer[]{Integer.valueOf(1000), Integer.valueOf(1001), Integer.valueOf(1002), Integer.valueOf(1003)}),
    DISPLAY_BRIGHTNESS("Display Brightness", new Integer[]{Integer.valueOf(331)}),
    GPS_STATE("GPS State", new Integer[]{Integer.valueOf(208)}),
    GPU3D("GPU", new Integer[]{Integer.valueOf(400)}),
    MOBILE_DATA_USAGE_PER_APP("Mobile Data Usage", new Integer[0]),
    MOBILE_NETWORK_STATE("Mobile Network State", new Integer[]{Integer.valueOf(200)}),
    WAKELOCKS("Wakelocks", new Integer[]{Integer.valueOf(330)}),
    WIFILOCKS("Wifilocks", new Integer[]{Integer.valueOf(333)}),
    WIFI_DATA_USAGE_PER_APP("Wifi Data Usage", new Integer[0]),
    WIFI_STATE("Wifi State", new Integer[]{Integer.valueOf(201)}),
    CPU_NORM_LOAD("CPU Normalized Load", new Integer[]{Integer.valueOf(601)}),
    WIFI_RSSI_LEVEL("Wifi RSSI Level", new Integer[]{Integer.valueOf(202)}),
    BATTERY_REMAINING("Percent Battery Remaining", new Integer[]{Integer.valueOf(205)}),
    BATTERY_STATUS("Battery Status", new Integer[]{Integer.valueOf(206)}),
    MEMORY_USAGE("Memory Usage", new Integer[]{Integer.valueOf(328)}),
    CPU_LOAD("CPU Load", new Integer[]{Integer.valueOf(600)}),
    GPU3D_LOAD("GPU Load", new Integer[]{Integer.valueOf(401)}),
    APPLICATION_STATE("Application State", new Integer[]{Integer.valueOf(203)}),
    DISPLAY_STATE("Display State", new Integer[]{Integer.valueOf(204)}),
    CPU_EPM("CPU EPM", new Integer[]{Integer.valueOf(1)}),
    CODEC("Codec/GPS/Wireless", new Integer[]{Integer.valueOf(2)}),
    AUDIO("Audio", new Integer[]{Integer.valueOf(3)}),
    CAMERA("Camera", new Integer[]{Integer.valueOf(4)}),
    WLAN_BT_FM("WLAN/BT/FM", new Integer[]{Integer.valueOf(5)}),
    INTERNAL_MEM("Internal Memory", new Integer[]{Integer.valueOf(6)}),
    SD_CARD("SD Card", new Integer[]{Integer.valueOf(7)}),
    USB("USB", new Integer[]{Integer.valueOf(8)}),
    LCD_BACKLIGHT("LCD Backlight", new Integer[]{Integer.valueOf(9)}),
    DIGITAL_CORE_SDCARD_USB("Digital Core/SD Card/USB", new Integer[]{Integer.valueOf(10)}),
    GRAPHICS("Graphics", new Integer[]{Integer.valueOf(11)}),
    BACK_CAMERA("Back Camera", new Integer[]{Integer.valueOf(12)}),
    FRONT_CAMERA("Front Camera", new Integer[]{Integer.valueOf(13)}),
    DIGITAL_CORE("Digital Core", new Integer[]{Integer.valueOf(14)}),
    WLAN_BT("WLAN/BT", new Integer[]{Integer.valueOf(15)});
    public static final int MIN_PE_SENSORID = 1;
    public static final int MAX_PE_SENSORID = 11;
    private static List<StatType> orderedStatTypes;
    private static List<StatType> orderedPEStatTypes;

    static {
        setupOrderedStatTypes();
    }

    private String uiName;
    private List<Integer> sensorIDs;

    private StatType(String uiName, Integer... ids) {
        this.uiName = uiName;
        this.sensorIDs = Arrays.asList(ids);
    }

    static void setupOrderedStatTypes() {
        orderedStatTypes = new ArrayList();
        orderedStatTypes.add(BATTERY_POWER);
        orderedStatTypes.add(CPU);
        orderedStatTypes.add(CPU_NORM_LOAD);
        orderedStatTypes.add(CPU_LOAD);
        orderedStatTypes.add(GPU3D_LOAD);
        orderedStatTypes.add(BATTERY_REMAINING);
        orderedStatTypes.add(BATTERY_STATUS);
        orderedStatTypes.add(GPU3D);
        orderedStatTypes.add(DISPLAY_BRIGHTNESS);
        orderedStatTypes.add(DISPLAY_STATE);
        orderedStatTypes.add(WAKELOCKS);
        orderedStatTypes.add(BLUETOOTH_STATE);
        orderedStatTypes.add(GPS_STATE);
        orderedStatTypes.add(MOBILE_NETWORK_STATE);
        orderedStatTypes.add(MOBILE_DATA_USAGE_PER_APP);
        orderedStatTypes.add(WIFI_STATE);
        orderedStatTypes.add(WIFI_DATA_USAGE_PER_APP);
        orderedStatTypes.add(WIFILOCKS);
        orderedStatTypes.add(WIFI_RSSI_LEVEL);
        orderedStatTypes.add(MEMORY_USAGE);
        orderedStatTypes.add(APPLICATION_STATE);


        orderedPEStatTypes = new ArrayList();
        orderedPEStatTypes.add(CPU_EPM);
        orderedPEStatTypes.add(CODEC);
        orderedPEStatTypes.add(AUDIO);
        orderedPEStatTypes.add(CAMERA);
        orderedPEStatTypes.add(WLAN_BT_FM);
        orderedPEStatTypes.add(INTERNAL_MEM);
        orderedPEStatTypes.add(SD_CARD);
        orderedPEStatTypes.add(USB);
        orderedPEStatTypes.add(LCD_BACKLIGHT);
        orderedPEStatTypes.add(DIGITAL_CORE_SDCARD_USB);
        orderedPEStatTypes.add(GRAPHICS);
        orderedPEStatTypes.add(BACK_CAMERA);
        orderedPEStatTypes.add(FRONT_CAMERA);
        orderedPEStatTypes.add(DIGITAL_CORE);
        orderedPEStatTypes.add(WLAN_BT);
    }

    public static List<StatType> getAllOrderedStatTypes() {
        String sensorIDs = Settings.getSensorIDs();
        setupOrderedStatTypes();
        if ((sensorIDs != null) && (!sensorIDs.isEmpty())) {
            if (!Settings.isSensorIDPresent(((Integer) WAKELOCKS.getSensorIDs().get(0)).toString())) {
                orderedStatTypes.remove(WAKELOCKS);
                Settings.setDatapoint(WAKELOCKS, false, false);
            }
            if (!Settings.isSensorIDPresent(((Integer) WIFILOCKS.getSensorIDs().get(0)).toString())) {
                orderedStatTypes.remove(WIFILOCKS);
                Settings.setDatapoint(WIFILOCKS, false, false);
            }
        }
        return orderedStatTypes;
    }

    public static List<StatType> getAllPremierOrderedStatTypes() {
        return orderedPEStatTypes;
    }

    public static List<StatType> orderStatTypes(EnumSet<StatType> statTypes) {
        List<StatType> retval = new ArrayList();
        for (StatType statType : orderedStatTypes) {
            if (statTypes.contains(statType)) {
                retval.add(statType);
            }
        }
        for (StatType statType : orderedPEStatTypes) {
            if (statTypes.contains(statType)) {
                retval.add(statType);
            }
        }
        return retval;
    }

    public static StatType getStatTypeFromUIName(String uiName) {
        for (StatType statType : orderedStatTypes) {
            if (statType.getUIString().equals(uiName)) {
                return statType;
            }
        }
        throw new IllegalArgumentException("Name does not match StatType.");
    }

    public static String getStatTypeNameFromUIName(String uiName) {
        for (StatType statType : orderedStatTypes) {
            if (statType.getUIString().equals(uiName)) {
                return statType.toString();
            }
        }
        return "";
    }

    public static StatType getStatTypeFromNonUIName(String nonUIName) {
        for (StatType statType : orderedStatTypes) {
            if (statType.toString().equals(nonUIName)) {
                return statType;
            }
        }
        throw new IllegalArgumentException("Name does not match StatType.");
    }

    public String getUIString() {
        return this.uiName;
    }

    public List<Integer> getSensorIDs() {
        return this.sensorIDs;
    }

    public List<Integer> getIDList() {
        return this.sensorIDs;
    }
}
