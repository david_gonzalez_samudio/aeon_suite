package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */

import java.io.File;

public class LocalTrepnDirectoryCreator {
    public static void createLocalTrepnDirectory() {
        File localTrepnDir = new File(TrepnGeneralValues.AEON_TREPN_PATH);
        if (!localTrepnDir.exists()) {
            localTrepnDir.mkdirs();
        }
    }

    public static void createLocalTrepnLibDirectory() {
        File localTrepnLibDir = new File(TrepnGeneralValues.LOCAL_TREPN_LIB_DIR);
        if (!localTrepnLibDir.exists()) {
            localTrepnLibDir.mkdirs();
        }
    }

    public static void createLocalTrepnDatafileDirectory() {
        File localTrepnDatafileDir = new File(TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR);
        if (!localTrepnDatafileDir.exists()) {
            localTrepnDatafileDir.mkdirs();
        }
    }

    public static void createLocalTrepnAPKDirectory() {
        File localTrepnAPKDir = new File(TrepnGeneralValues.LOCAL_TREPN_APK_DIR);
        if (!localTrepnAPKDir.exists()) {
            localTrepnAPKDir.mkdirs();
        }
    }

    public static void createLocalTrepnPrefsDirectory() {
        File localTrepnPrefsDir = new File(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR);
        if (!localTrepnPrefsDir.exists()) {
            localTrepnPrefsDir.mkdirs();
        }
    }
}
