package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class DdmsPreferenceStore {
    public static final String PING_OPT_IN = "pingOptIn";
    private static final String PING_TIME = "pingTime";
    private static final String PING_ID = "pingId";
    private static final String ADT_USED = "adtUsed";
    private static final String LAST_SDK_PATH = "lastSdkPath";
    private static volatile PreferenceStore sPrefStore;

    public PreferenceStore getPreferenceStore() {
        synchronized (DdmsPreferenceStore.class) {
            if (sPrefStore == null) {
                String homeDir = null;
                try {
                    homeDir = AndroidLocation.getFolder();
                } catch (AndroidLocation.AndroidLocationException localAndroidLocationException) {
                }
                if (homeDir == null) {
                    sPrefStore = new PreferenceStore();
                    return sPrefStore;
                }
                assert (homeDir != null);

                String rcFileName = homeDir + "ddms.cfg";


                String oldPrefPath = System.getProperty("user.home") +
                        File.separator + ".ddmsrc";
                File oldPrefFile = new File(oldPrefPath);
                if (oldPrefFile.isFile()) {
                    FileOutputStream fileOutputStream = null;
                    try {
                        PreferenceStore oldStore = new PreferenceStore(oldPrefPath);
                        oldStore.load();

                        fileOutputStream = new FileOutputStream(rcFileName);
                        oldStore.save(fileOutputStream, "");
                        oldPrefFile.delete();

                        PreferenceStore newStore = new PreferenceStore(rcFileName);
                        newStore.load();
                        sPrefStore = newStore;
                    } catch (IOException localIOException1) {
                        sPrefStore = new PreferenceStore(rcFileName);
                        if (fileOutputStream == null) {
                            return null;
                        }
                        try {
                            fileOutputStream.close();
                        } catch (IOException localIOException2) {
                        }
                    } finally {
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException localIOException3) {
                            }
                        }
                    }
                    try {
                        fileOutputStream.close();
                    } catch (IOException localIOException4) {
                    }
                } else {
                    sPrefStore = new PreferenceStore(rcFileName);
                    try {
                        sPrefStore.load();
                    } catch (IOException localIOException5) {
                        System.err.println("Error Loading DDMS Preferences");
                    }
                }
            }
            label290:
            assert (sPrefStore != null);
            return sPrefStore;
        }
    }

    public void save() {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            try {
                prefs.save();
            } catch (IOException localIOException) {
            }
        }
    }

    public boolean hasPingId() {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            return (prefs != null) && (prefs.contains("pingId"));
        }
    }

    public long getPingId() {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            return prefs == null ? 0L : prefs.getLong("pingId");
        }
    }

    public long generateNewPingId() {
        PreferenceStore prefs = getPreferenceStore();

        Random rnd = new Random();
        long id = rnd.nextLong();
        synchronized (DdmsPreferenceStore.class) {
            prefs.setValue("pingId", id);
            try {
                prefs.save();
            } catch (IOException localIOException) {
            }
        }
        return id;
    }

    public boolean isPingOptIn() {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            return (prefs != null) && (prefs.contains("pingOptIn"));
        }
    }

    public void setPingOptIn(boolean optIn) {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            prefs.setValue("pingOptIn", optIn);
            try {
                prefs.save();
            } catch (IOException localIOException) {
            }
        }
    }

    public long getPingTime(String app) {
        PreferenceStore prefs = getPreferenceStore();
        String timePref = "pingTime." + app;
        synchronized (DdmsPreferenceStore.class) {
            return prefs == null ? 0L : prefs.getLong(timePref);
        }
    }

    public void setPingTime(String app, long timeStamp) {
        PreferenceStore prefs = getPreferenceStore();
        String timePref = "pingTime." + app;
        synchronized (DdmsPreferenceStore.class) {
            prefs.setValue(timePref, timeStamp);
            try {
                prefs.save();
            } catch (IOException localIOException) {
            }
        }
    }

    public boolean isAdtUsed() {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            if ((prefs == null) || (!prefs.contains("adtUsed"))) {
                return false;
            }
            return prefs.getBoolean("adtUsed");
        }
    }

    public void setAdtUsed(boolean used) {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            prefs.setValue("adtUsed", used);
            try {
                prefs.save();
            } catch (IOException localIOException) {
            }
        }
    }

    public String getLastSdkPath() {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            return prefs == null ? null : prefs.getString("lastSdkPath");
        }
    }

    public void setLastSdkPath(String osSdkPath) {
        PreferenceStore prefs = getPreferenceStore();
        synchronized (DdmsPreferenceStore.class) {
            prefs.setValue("lastSdkPath", osSdkPath);
            try {
                prefs.save();
            } catch (IOException localIOException) {
            }
        }
    }
}

