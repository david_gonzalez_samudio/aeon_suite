package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */


public class AdbPathGenerator {
    private static String adbPath;

    public static String getAdbPath() {
        if (adbPath == null) {
            createAdbPath();
        }
        return adbPath;
    }

    private static synchronized void createAdbPath() {
        if (adbPath == null) {
            switch (OperatingSystemPlatform.getCurrentPlatform()) {
                case MAC_OS:
                case UNKNOWN:
                    DdmsPreferenceStore mStore = new DdmsPreferenceStore();
                    adbPath = mStore.getLastSdkPath() + "/platform-tools/adb";
                    break;
                case LINUX:
                default:
                    adbPath = "adb";
            }
        }
    }
}
