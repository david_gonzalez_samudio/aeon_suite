package edu.gmu.cs.aeon.trepn.common;

/**
 * Created by DavidIgnacio on 7/12/2014.
 */

public enum OperatingSystemPlatform {
    WINDOWS, MAC_OS, LINUX, UNKNOWN;

    private static OperatingSystemPlatform currentPlatform;

    public static OperatingSystemPlatform getCurrentPlatform() {
        if (currentPlatform == null) {
            String osName = System.getProperty("os.name");
            if (osName.startsWith("Windows")) {
                currentPlatform = WINDOWS;
            } else if (osName.startsWith("Mac OS")) {
                currentPlatform = MAC_OS;
            } else if (osName.startsWith("Linux")) {
                currentPlatform = LINUX;
            } else {
                currentPlatform = UNKNOWN;
            }
        }
        return currentPlatform;
    }
}

