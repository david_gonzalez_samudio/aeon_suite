package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.jfreechart.seriesdatapointpairer;

import org.jfree.data.xy.XYSeries;

import java.util.List;

public abstract interface ISeriesDatapointPairer {
    public abstract void pairXYDatapoints(XYSeries paramXYSeries1, XYSeries paramXYSeries2);

    public abstract void pairXYDatapoints(List<XYSeries> paramList);
}