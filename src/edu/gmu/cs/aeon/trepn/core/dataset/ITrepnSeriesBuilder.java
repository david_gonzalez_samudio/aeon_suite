package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.jfreechart.trepn;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.sql.Connection;

public abstract interface ITrepnSeriesBuilder {
    public abstract XYSeries createTrepnXYSeries(Connection paramConnection, TrepnStat paramTrepnStat, ITrepnStatValueMapper paramITrepnStatValueMapper);

    public abstract XYSeriesCollection createTrepnXYSeriesCollection(Connection paramConnection, ITrepnStatValueMapper paramITrepnStatValueMapper);

    public abstract XYSeriesCollection createAppFilteredTrepnXYSeriesCollection(Connection paramConnection, ITrepnStatValueMapper paramITrepnStatValueMapper, int offset);

    public abstract XYSeries createNumPointsAverageTrepnXYSeries(XYSeries paramXYSeries, int paramInt);

    public abstract int getOffset();
}
