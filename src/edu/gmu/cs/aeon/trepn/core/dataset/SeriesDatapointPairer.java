package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.jfreechart.seriesdatapointpairer;

import org.jfree.data.general.SeriesException;
import org.jfree.data.xy.XYSeries;

import java.util.List;

public abstract class SeriesDatapointPairer
        implements ISeriesDatapointPairer {
    public static final float INVALID_MIDPOINT = -1.0F;
    protected double xValuePairingRange;

    SeriesDatapointPairer(Number pollInterval) {
        if (pollInterval == null) {
            this.xValuePairingRange = 0.0D;
        }
        this.xValuePairingRange = (Math.abs(pollInterval.doubleValue()) / 2.0D);
    }

    protected boolean isWithinPollIntervalRange(double valueOne, double valueTwo) {
        return Math.abs(valueOne - valueTwo) <= this.xValuePairingRange;
    }

    protected void addXMidpointValuesAndRemoveOldValues(XYSeries seriesOne, int seriesOneIndex, XYSeries seriesTwo, int seriesTwoIndex) {
        Number seriesOneX = seriesOne.getX(seriesOneIndex);
        Number seriesOneY = seriesOne.getY(seriesOneIndex);
        Number seriesTwoX = seriesTwo.getX(seriesTwoIndex);
        Number seriesTwoY = seriesTwo.getY(seriesTwoIndex);

        seriesOne.remove(seriesOneIndex);
        seriesTwo.remove(seriesTwoIndex);

        double xMidpoint = (seriesOneX.doubleValue() + seriesTwoX.doubleValue()) / 2.0D;
        try {
            seriesOne.add(xMidpoint, seriesOneY);
            seriesTwo.add(xMidpoint, seriesTwoY);
        } catch (SeriesException localSeriesException) {
            seriesOne.remove(Double.valueOf(xMidpoint));
        }
    }

    protected boolean isWithinPollIntervalRange(int[] values) {
        int minFound = 2147483647;
        int maxFound = -2147483648;
        for (int value : values) {
            if (value < minFound) {
                minFound = value;
            }
            if (value > maxFound) {
                maxFound = value;
            }
        }
        return Math.abs(maxFound - minFound) <= this.xValuePairingRange;
    }

    protected void addXMidpointValuesAndRemoveOldValues(List<XYSeries> seriesList, int[] seriesIndexValues, int[] tempXValuesArray, int[] tempYValuesArray) {
        int numberOfSeries = seriesList.size();
        if ((numberOfSeries != tempXValuesArray.length) || (numberOfSeries != tempYValuesArray.length)) {
            throw new IllegalArgumentException("array lengths were not of the expected length");
        }
        for (int i = 0; i < numberOfSeries; i++) {
            tempXValuesArray[i] = 0;
            tempYValuesArray[i] = 0;
        }
        int numXValuesAdded = 0;
        for (int i = 0; i < numberOfSeries; i++) {
            XYSeries series = (XYSeries) seriesList.get(i);
            int index = seriesIndexValues[i];
            if (index < series.getItemCount()) {
                tempXValuesArray[i] = series.getX(index).intValue();
                tempYValuesArray[i] = series.getY(index).intValue();
                series.remove(index);
                numXValuesAdded++;
            }
        }
        float xMidpoint = calculateMidpoint(tempXValuesArray, numXValuesAdded);
        if (-1.0F != xMidpoint) {
            try {
                for (int i = 0; i < numberOfSeries; i++) {
                    if (tempXValuesArray[i] != 0) {
                        XYSeries series = (XYSeries) seriesList.get(i);
                        series.add(xMidpoint, tempYValuesArray[i]);
                    }
                }
            } catch (SeriesException localSeriesException) {
            }
        }
    }

    private float calculateMidpoint(int[] xValues, int numXValuesAdded) {
        int total = 0;
        for (int i = 0; i < xValues.length; i++) {
            total += xValues[i];
        }
        if (numXValuesAdded > 0) {
            return total / numXValuesAdded;
        }
        return -1.0F;
    }

    abstract void applyPolicyToEndOfSeries(XYSeries paramXYSeries, int paramInt);
}
