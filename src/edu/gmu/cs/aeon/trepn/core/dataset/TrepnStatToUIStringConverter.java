package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.trepn.stats;


public class TrepnStatToUIStringConverter {
    public static String getUIString(TrepnStat trepnStat) {
        String uiString = TrepnStatToStatTypeConverter.getStatType(trepnStat).getUIString();
        switch (trepnStat) {
            case BACK_CAMERA:
                uiString = uiString + "-0";
                break;
            case BATTERY_POWER_EPM:
                uiString = uiString + "-1";
                break;
            case BATTERY_POWER_PMIC:
                uiString = uiString + "-2";
                break;
            case BATTERY_REMAINING:
                uiString = uiString + "-3";
                break;
        }
        return uiString;
    }
}
