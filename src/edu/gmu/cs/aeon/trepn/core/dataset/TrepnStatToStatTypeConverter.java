package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.trepn.stats;

import edu.gmu.cs.aeon.trepn.common.StatType;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class TrepnStatToStatTypeConverter {
    public static EnumSet<StatType> getStatTypes(EnumSet<TrepnStat> trepnStats) {
        EnumSet<StatType> retval = EnumSet.noneOf(StatType.class);
        List<StatType> statTypes = new ArrayList();
        for (TrepnStat trepnStat : trepnStats) {
            StatType statType = getStatType(trepnStat);
            if (statType != null) {
                statTypes.add(statType);
            }
        }
        if (!statTypes.isEmpty()) {
            retval = EnumSet.copyOf(statTypes);
        }
        return retval;
    }

    public static StatType getStatType(TrepnStat trepnStat) {
        switch (trepnStat) {
            case APPLICATION_STATE:
//                return StatType.BATTERY_POWER;
                return StatType.APPLICATION_STATE;
            case AUDIO:
                return StatType.BATTERY_POWER;
            case CAMERA:
                return StatType.BLUETOOTH_STATE;
            case BACK_CAMERA:
                return StatType.CPU;
            case BATTERY_POWER_EPM:
//                return StatType.CPU;
                return StatType.BATTERY_POWER;
            case BATTERY_POWER_PMIC:
//                return StatType.CPU;
                return StatType.BATTERY_POWER;
            case BATTERY_REMAINING:
                return StatType.CPU;
            case CPU_0_FREQ:
                return StatType.MOBILE_NETWORK_STATE;
            case BATTERY_STATUS:
//                return StatType.GPU3D;
                return StatType.DISPLAY_STATE;
            case CODEC:
                return StatType.GPS_STATE;
            case BLUETOOTH_STATE:
                return StatType.DISPLAY_BRIGHTNESS;
            case CPU_2_FREQ:
                return StatType.WAKELOCKS;
            case CPU_3_FREQ:
                return StatType.WIFILOCKS;
            case CPU_1_FREQ:
                return StatType.WIFI_STATE;
            case CPU_EPM:
                return StatType.CPU_NORM_LOAD;
            case CPU_LOAD:
                return StatType.WIFI_RSSI_LEVEL;
            case CPU_NORM_LOAD:
                return StatType.BATTERY_REMAINING;
            case DATA_STATE:
                return StatType.BATTERY_STATUS;
            case DIGITAL_CORE:
                return StatType.MEMORY_USAGE;
            case DIGITAL_CORE_SDCARD_USB:
                return StatType.CPU_LOAD;
            case DISPLAY_STATE:
//                return StatType.GPU3D_LOAD;
                return StatType.DISPLAY_STATE;
            case FRONT_CAMERA:
                return StatType.APPLICATION_STATE;
            case GPS_STATE:
                return StatType.DISPLAY_STATE;
            case GPU3D_FREQ:
                return StatType.CPU_EPM;
            case GPU3D_LOAD:
                return StatType.CODEC;
            case GRAPHICS:
                return StatType.AUDIO;
            case INTERNAL_MEM:
                return StatType.CAMERA;
            case LCD_BACKLIGHT:
                return StatType.WLAN_BT_FM;
            case MEMORY_USAGE:
                return StatType.INTERNAL_MEM;
            case SCREEN_BRIGHTNESS:
                return StatType.SD_CARD;
            case SD_CARD:
                return StatType.USB;
            case USB:
                return StatType.LCD_BACKLIGHT;
            case WAKELOCKS:
                return StatType.DIGITAL_CORE_SDCARD_USB;
            case WIFILOCKS:
                return StatType.GRAPHICS;
            case WIFI_RSSI_LEVEL:
                return StatType.BACK_CAMERA;
            case WIFI_STATE:
                return StatType.FRONT_CAMERA;
            case WLAN_BT:
                return StatType.DIGITAL_CORE;
            case WLAN_BT_FM:
                return StatType.WLAN_BT;
        }
        throw new UnsupportedOperationException("TrepnStat " + trepnStat + " is not supported");
    }
}
