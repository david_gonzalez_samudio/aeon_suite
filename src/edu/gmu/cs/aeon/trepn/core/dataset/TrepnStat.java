package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.trepn.stats;


import edu.gmu.cs.aeon.trepn.core.charting.color.ColorSet;
import edu.gmu.cs.aeon.trepn.core.charting.color.ColorSetVariation;
import edu.gmu.cs.aeon.trepn.core.charting.color.StatTypeToColorSetMapper;

import java.awt.*;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public enum TrepnStat {
    BATTERY_POWER_EPM(7, Units.MICROWATTS, ColorSet.DARK_RED),
    BATTERY_POWER_PMIC(332, Units.MICROWATTS, ColorSet.DARK_RED, "delta"),
    CPU_0_FREQ(1000, Units.KILOHERTZ, ColorSet.RED),
    CPU_1_FREQ(1001, Units.KILOHERTZ, ColorSet.RED),
    CPU_2_FREQ(1002, Units.KILOHERTZ, ColorSet.RED),
    CPU_3_FREQ(1003, Units.KILOHERTZ, ColorSet.RED),
    GPU3D_FREQ(400, Units.KILOHERTZ, ColorSet.ORANGE),
    SCREEN_BRIGHTNESS(331, Units.PERCENT, ColorSet.YELLOW),
    BLUETOOTH_STATE(207, Units.NONE, ColorSet.TURQUOISE),
    GPS_STATE(208, Units.NONE, ColorSet.BLUE),
    DATA_STATE(200, Units.NONE, ColorSet.DARK_BLUE),
    WIFI_STATE(201, Units.NONE, ColorSet.PURPLE),
    WAKELOCKS(330, Units.NONE, ColorSet.GREEN, "wakelocks"),
    WIFILOCKS(333, Units.NONE, ColorSet.BROWN, "wifilocks"),
    CPU_NORM_LOAD(601, Units.PERCENT, ColorSet.PINK),
    WIFI_RSSI_LEVEL(202, Units.DECIBELMILLIWATTS, ColorSet.PINK),
    BATTERY_REMAINING(205, Units.PERCENT, ColorSet.PINK),
    BATTERY_STATUS(206, Units.NONE, ColorSet.PINK),
    MEMORY_USAGE(328, Units.KILOBYTES, ColorSet.PINK),
    CPU_LOAD(600, Units.PERCENT, ColorSet.PINK),
    GPU3D_LOAD(401, Units.PERCENT, ColorSet.PINK),
    APPLICATION_STATE(203, Units.NONE, ColorSet.BLUE),
    DISPLAY_STATE(204, Units.NONE, ColorSet.BABY_BLUE),
    CPU_EPM(1, Units.NONE, ColorSet.BLUE),
    CODEC(2, Units.NONE, ColorSet.BLUE),
    AUDIO(3, Units.NONE, ColorSet.BLUE),
    CAMERA(4, Units.NONE, ColorSet.BLUE),
    WLAN_BT_FM(5, Units.NONE, ColorSet.BLUE),
    INTERNAL_MEM(6, Units.NONE, ColorSet.BLUE),
    SD_CARD(7, Units.NONE, ColorSet.BLUE),
    USB(8, Units.NONE, ColorSet.BLUE),
    LCD_BACKLIGHT(9, Units.NONE, ColorSet.BLUE),
    DIGITAL_CORE_SDCARD_USB(10, Units.NONE, ColorSet.BLUE),
    GRAPHICS(11, Units.NONE, ColorSet.BLUE),
    BACK_CAMERA(12, Units.NONE, ColorSet.BLUE),
    FRONT_CAMERA(13, Units.NONE, ColorSet.BLUE),
    DIGITAL_CORE(14, Units.NONE, ColorSet.BLUE),
    WLAN_BT(15, Units.NONE, ColorSet.BLUE);

    private static final ColorSetVariation DEFAULT_VARIATION = ColorSetVariation.SECOND_DARKEST;
    private int tableId;
    private Units units;
    private ColorSet colorSet;
    private String tableValueColumnName;

    private TrepnStat(int tableId, Units units, ColorSet colorSet) {
        this.tableId = tableId;
        this.units = units;
        this.colorSet = colorSet;
        this.tableValueColumnName = "value";
    }

    private TrepnStat(int tableId, Units units, ColorSet colorSet, String tableValueColumnName) {
        this.tableId = tableId;
        this.units = units;
        this.colorSet = colorSet;
        this.tableValueColumnName = tableValueColumnName;
    }

    public static EnumSet<TrepnStat> getAllMatchingTrepnStats(Set<Integer> tableIds) {
        EnumSet<TrepnStat> retval = EnumSet.noneOf(TrepnStat.class);
        List<TrepnStat> matchedTrepnStats = new ArrayList();
        if (tableIds != null) {
            for (TrepnStat trepnStat : values()) {
                if (tableIds.contains(Integer.valueOf(trepnStat.tableId))) {
                    matchedTrepnStats.add(trepnStat);
                }
            }
        }
        if (!matchedTrepnStats.isEmpty()) {
            retval = EnumSet.copyOf(matchedTrepnStats);
        }
        return retval;
    }

    public String getTableName() {
        String prefix;
        switch (this) {
            case CPU_2_FREQ:
                prefix = "wakelock_";
                break;
            case CPU_3_FREQ:
                prefix = "wifilock_";
                break;
            default:
                prefix = "";
        }
        return prefix + "sensor_" + this.tableId;
    }

    public String getName() {
        String name = "";
        switch (this) {
            case BACK_CAMERA:
                name = "Core 0";
                break;
            case BATTERY_POWER_EPM:
                name = "Core 1";
                break;
            case BATTERY_POWER_PMIC:
                name = "Core 2";
                break;
            case BATTERY_REMAINING:
                name = "Core 3";
                break;
        }
        return name;
    }

    public Units getUnits() {
        return this.units;
    }

    public Color getColor() {
        switch (this) {
            case BACK_CAMERA:
                return this.colorSet.getDarkest();
            case BATTERY_POWER_EPM:
                return this.colorSet.getSecondDarkest();
            case BATTERY_POWER_PMIC:
                return this.colorSet.getSecondLightest();
            case BATTERY_REMAINING:
                return this.colorSet.getLightest();
        }
        switch (DEFAULT_VARIATION) {
            case DARKEST:
                return StatTypeToColorSetMapper.getColorSet(TrepnStatToStatTypeConverter.getStatType(this)).getDarkest();
            case LIGHTEST:
                return StatTypeToColorSetMapper.getColorSet(TrepnStatToStatTypeConverter.getStatType(this)).getSecondDarkest();
            case SECOND_DARKEST:
                return StatTypeToColorSetMapper.getColorSet(TrepnStatToStatTypeConverter.getStatType(this)).getSecondLightest();
            case SECOND_LIGHTEST:
                return StatTypeToColorSetMapper.getColorSet(TrepnStatToStatTypeConverter.getStatType(this)).getLightest();
        }
        throw new IllegalStateException();
    }

    public String getTableValueColumnName() {
        return this.tableValueColumnName;
    }
}
