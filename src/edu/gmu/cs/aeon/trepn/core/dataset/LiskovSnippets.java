package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 8/9/2014.
 */
public class LiskovSnippets {
    private boolean isRepVisibleDuringImmutableMethods = false;

    public static void main(String args[]) {

        LiskovSnippets liskovSnippets = new LiskovSnippets();
        boolean ex = liskovSnippets.isRepVisibleDuringImmutableMethods;
    }

    public void chapter1() {
        //todo: remove Liskov
        Object o = "abc";
        boolean b = o.equals("a,b,c");
//        char c= o.chartAt(1);
        Object o2 = b;
//        String s=o;
        String t = (String) o;
        char c = t.charAt(1);
        c = t.charAt(3);

        //end Liskov
    }

    public boolean isRepVisibleDuringImmutableMethods() {
        LiskovSnippets liskovSnippets = new LiskovSnippets();
        return liskovSnippets.isRepVisibleDuringImmutableMethods;
    }
}
