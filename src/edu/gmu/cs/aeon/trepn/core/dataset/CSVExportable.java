package edu.gmu.cs.aeon.trepn.core.dataset;

import java.sql.Connection;

/**
 * Created by DavidIgnacio on 7/28/2014.
 */
public interface CSVExportable {

    public abstract boolean toCSVFile(Connection paramConnection, TrepnStat paramTrepnStat, ITrepnStatValueMapper paramITrepnStatValueMapper, String outputFilename);
}
