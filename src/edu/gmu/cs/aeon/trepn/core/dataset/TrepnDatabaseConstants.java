package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
public class TrepnDatabaseConstants {
    public static final int QUERY_TIMEOUT_IN_SECONDS = 10;
    public static final String METADATA_TABLE = "metadata";
    public static final String DATALINES_TABLE = "datalines";
    public static final String APPLICATION_STATISTICS_TABLE = "application_statistics";
    public static final String APPSTATS_TABLE_PREFIX = "application_sensor_";
    public static final String WAKELOCK_SENSOR_TABLE = "wakelock_sensor_330";
    public static final String WIFILOCK_SENSOR_TABLE = "wifilock_sensor_333";
    public static final String APPLICATION_STATE_SENSOR_TABLE = "sensor_203";
    public static final String WAKELOCKS_TABLE = "wakelocks";
    public static final String WIFILOCKS_TABLE = "wifilocks";
    public static final String APPLICATION_STATE_TABLE = "application_state";
    public static final String ID = "_id";
    public static final String TIMESTAMP = "timestamp";
    public static final String VALUE = "value";
    public static final String DELTA = "delta";
    public static final String START_TIME_IN_MS = "start_time";
    public static final String DURATION_IN_MS = "duration";
    public static final String PER_APPLICATION_NETWORK_STATISTICS_SUPPORTED = "per_application_network_statistics_supported";
    public static final String WAKELOCKS = "wakelocks";
    public static final String WIFILOCKS = "wifilocks";
    public static final String APPLICATION_STATE = "application_state";
    public static final String APPLICATION_STATE_DESCRIPTION = "description";
    public static final String NAME = "name";
    public static final String MOBILE_BYTES_SENT_IN_KB = "mobile_bytes_sent";
    public static final String MOBILE_BYTES_RECEIVED_IN_KB = "mobile_bytes_received";
    public static final String OTHER_BYTES_SENT_IN_KB = "other_bytes_sent";
    public static final String OTHER_BYTES_RECEIVED_IN_KB = "other_bytes_received";
    public static final String TYPE = "type";
    public static final String PACKAGES = "packages";
    public static final String APPLICATION_NAME = "application_name";
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.core_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.core.trepn.database.TrepnDatabaseConstants
 * JD-Core Version:    0.7.0.1
 */