package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.trepn.stats;

public enum Units {
    NONE(1.0D, "", 0), PERCENT(1.0D, "%", 1), BYTES(1.0D, "B", 3), KILOBYTES(1000.0D, "B", 3), KILOHERTZ(1000.0D, "Hz", 3), MICROWATTS(1000000.0D, "W", 3), MILLISECONDS(1000.0D, "s", 3), DECIBELMILLIWATTS(1.0D, "dBm", 3);

    private static final StringBuffer EMPTY_STRING_BUFFER = new StringBuffer("");
    private double baseUnitsValue;
    private String suffix;
    private int suggestedNumPrecisionDigits;

    private Units(double baseUnitsValue, String suffix, int suggestedNumPrecisionDigits) {
        this.baseUnitsValue = baseUnitsValue;
        this.suffix = suffix;
        this.suggestedNumPrecisionDigits = suggestedNumPrecisionDigits;
    }

    public StringBuffer getCondensedValueAndUnits(double value, int numPrecisionDigits) {
        switch (this) {
            case BYTES:
            case DECIBELMILLIWATTS:
            case KILOBYTES:
            case PERCENT:
                return getCondensedLargeValueAndUnits(value, numPrecisionDigits);
            case KILOHERTZ:
                return getCondensedLargeValueAndUnits(value, numPrecisionDigits);
            case MICROWATTS:
                return getCondensedLargeValueAndUnits(value, numPrecisionDigits);
            case MILLISECONDS:
            case NONE:
                return getCondensedSmallValueAndUnits(value, numPrecisionDigits);
        }
        throw new UnsupportedOperationException("Unsupported operation for Units of type " + toString());
    }

    private StringBuffer getCondensedLargeValueAndUnits(double value, int numPrecisionDigits) {
        String suffixUnits = "";
        value *= this.baseUnitsValue;
        if (0.0D == value) {
            return EMPTY_STRING_BUFFER;
        }
        if (value >= 1000000000.0D) {
            value /= 1000000000.0D;
            suffixUnits = "G";
        } else if (value >= 1000000.0D) {
            value /= 1000000.0D;
            suffixUnits = "M";
        } else if (value >= 1000.0D) {
            value /= 1000.0D;
            suffixUnits = "K";
        }
        if ((int) value == value) {
            return new StringBuffer(String.valueOf((int) value) + suffixUnits + this.suffix);
        }
        return new StringBuffer(String.format(new StringBuilder("%.").append(numPrecisionDigits).append("f").toString(), new Object[]{Double.valueOf(value)}) + suffixUnits + this.suffix);
    }

    private StringBuffer getCondensedSmallValueAndUnits(double value, int numPrecisionDigits) {
        String suffixUnits = "";

        double conversionToDefaultFactor = this.baseUnitsValue / 1.0D;
        double conversionToMillisFactor = this.baseUnitsValue / 1000.0D;
        if (0.0D == value) {
            return EMPTY_STRING_BUFFER;
        }
        if (value >= conversionToDefaultFactor) {
            value /= conversionToDefaultFactor;
            suffixUnits = "";
        } else if (value >= conversionToMillisFactor) {
            value /= conversionToMillisFactor;
            suffixUnits = "m";
        }
        if ((int) value == value) {
            return new StringBuffer(String.valueOf((int) value) + suffixUnits + this.suffix);
        }
        return new StringBuffer(String.format(new StringBuilder("%.").append(numPrecisionDigits).append("f").toString(), new Object[]{Double.valueOf(value)}) + suffixUnits + this.suffix);
    }

    public String getSuffix() {
        return this.suffix;
    }

    public int getSuggestedNumPrecisionDigits() {
        return this.suggestedNumPrecisionDigits;
    }
}
