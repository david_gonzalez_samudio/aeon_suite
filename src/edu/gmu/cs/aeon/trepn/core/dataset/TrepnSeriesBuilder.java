package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.jfreechart.trepn;


import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class TrepnSeriesBuilder
        implements ITrepnSeriesBuilder, CSVExportable {
    int offset = 0;
    private boolean shouldInsertZeroValuesForMissingExpectedDatapoints;
    private long expectedPollIntervalInMs;
    private int allowedPollIntervalMultipleRange;

    public TrepnSeriesBuilder() {
        this.shouldInsertZeroValuesForMissingExpectedDatapoints = false;
    }

    public TrepnSeriesBuilder(long expectedPollIntervalInMs, int allowedPollIntervalMultipleRange) {
        this.shouldInsertZeroValuesForMissingExpectedDatapoints = false;
        this.expectedPollIntervalInMs = expectedPollIntervalInMs;
        this.allowedPollIntervalMultipleRange = allowedPollIntervalMultipleRange;
    }

    public XYSeries createTrepnXYSeries(Connection connection, TrepnStat trepnStat, ITrepnStatValueMapper optMapper) {
        //XYSeries series = new XYSeries(TrepnStatToUIStringConverter.getUIString(trepnStat), true, false);
        XYSeries series = new XYSeries(trepnStat.toString(), true, false);
        String tableName = trepnStat.getTableName();
        try {
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);
            ResultSet resultSet = statement.executeQuery(
                    "select timestamp, " + trepnStat.getTableValueColumnName() +
                            " from " + tableName + " order by " + "timestamp");

            int previousTimestamp = -1;
            while (resultSet.next()) {
                int timestamp = resultSet.getInt("timestamp");
                double value;
                switch (trepnStat) {
                    case CPU_2_FREQ:
                    case CPU_3_FREQ:
                        if (optMapper != null) {
                            value = optMapper.mapString(resultSet.getString(trepnStat.getTableValueColumnName()));
                        } else {
                            throw new IllegalArgumentException("Expected a ITrepnStatValueMapper to map String value to double");
                        }
                        break;
                    default:
                        if (optMapper != null) {
                            value = optMapper.mapValue(resultSet.getInt(trepnStat.getTableValueColumnName()));
                        } else {
                            value = resultSet.getDouble(trepnStat.getTableValueColumnName());
                        }
                        break;
                }
                if (previousTimestamp != timestamp) {
                    addToSeries(series, previousTimestamp, timestamp, value);
                    previousTimestamp = timestamp;
                }
            }
        } catch (SQLException e) {
            if (TrepnStat.BATTERY_POWER_EPM == trepnStat) {
                series = createTrepnXYSeries(connection, TrepnStat.BATTERY_POWER_PMIC, optMapper);
            } else if ((trepnStat == TrepnStat.CPU_1_FREQ) || (trepnStat == TrepnStat.CPU_2_FREQ) || (trepnStat == TrepnStat.CPU_3_FREQ)) {
                series = null;
            } else {
                System.out.println("WARNING: caught SQLException for TrepnStat " + trepnStat + ": " + e.getMessage());
                series = null;
            }
        }
        return series;
    }

    public XYSeriesCollection createTrepnXYSeriesCollection(Connection connection, ITrepnStatValueMapper optMapper) {
        XYSeriesCollection seriesCollection = new XYSeriesCollection();
        for (TrepnStat trepnStat : TrepnStat.values()) {
            System.out.println("*** createTrepnXYSeriesCollection " + trepnStat);
            //XYSeries series = new XYSeries(TrepnStatToUIStringConverter.getUIString(trepnStat), true, false);
            XYSeries series = new XYSeries(trepnStat.toString(), true, false);

            String tableName = trepnStat.getTableName();
            try {
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);
                ResultSet resultSet = statement.executeQuery(
                        "select timestamp, " + trepnStat.getTableValueColumnName() +
                                " from " + tableName + " order by " + "timestamp");

                int previousTimestamp = -1;
                while (resultSet.next()) {
                    int timestamp = resultSet.getInt("timestamp");
                    double value;
                    switch (trepnStat) {
                        case CPU_2_FREQ:
                        case CPU_3_FREQ:
                            if (optMapper != null) {
                                value = optMapper.mapString(resultSet.getString(trepnStat.getTableValueColumnName()));
                            } else {
                                throw new IllegalArgumentException("Expected a ITrepnStatValueMapper to map String value to double");
                            }
                            break;
                        default:
                            if (optMapper != null) {
                                value = optMapper.mapValue(resultSet.getInt(trepnStat.getTableValueColumnName()));
                            } else {
                                value = resultSet.getDouble(trepnStat.getTableValueColumnName());
                            }
                            break;
                    }
                    if (previousTimestamp != timestamp) {
                        addToSeries(series, previousTimestamp, timestamp, value);
                        previousTimestamp = timestamp;
                    }
                }

                //store the series color to use it in EnergyDataPlotPanel
                String seriesRGBColor = String.valueOf(trepnStat.getColor().getRGB());
                series.setDescription(seriesRGBColor);

                seriesCollection.addSeries(series);
                //Mahmoud: create another dataSeries for the application stat and add it to this dataset
                // select * from application_state, we will use this dataset to add a tooltip


            } catch (SQLException e) {
                if (TrepnStat.BATTERY_POWER_EPM == trepnStat) {
                    series = createTrepnXYSeries(connection, TrepnStat.BATTERY_POWER_PMIC, optMapper);
                } else if ((trepnStat == TrepnStat.CPU_1_FREQ) || (trepnStat == TrepnStat.CPU_2_FREQ) || (trepnStat == TrepnStat.CPU_3_FREQ)) {
                    series = null;
                } else {
                    System.out.println("WARNING: caught SQLException for TrepnStat " + trepnStat + ": " + e.getMessage());
                    series = null;
                }
            }
        }
        return seriesCollection;
    }

    public int getOffset() {
        return offset;
    }

    public XYSeriesCollection createAppFilteredTrepnXYSeriesCollection(Connection connection, ITrepnStatValueMapper optMapper, int oldTimestamp) {
        XYSeriesCollection seriesCollection = new XYSeriesCollection();

        int appPreviousTimestamp = -1;
        int appTimestamp = -1;

        TrepnStat trepnStat = TrepnStat.APPLICATION_STATE;
        //XYSeries series = new XYSeries(TrepnStatToUIStringConverter.getUIString(trepnStat), true, false);
        XYSeries series = new XYSeries(trepnStat.toString(), true, false);

        String tableName = trepnStat.getTableName();
        try {
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);
            ResultSet resultSet = statement.executeQuery(
                    "select timestamp, " + trepnStat.getTableValueColumnName() +
                            " from " + tableName + " order by " + "timestamp");

            int previousTimestamp = -1;
            while (resultSet.next()) {
                int timestamp = resultSet.getInt("timestamp");
                double value;
                switch (trepnStat) {
                    case CPU_2_FREQ:
                    case CPU_3_FREQ:
                        if (optMapper != null) {
                            value = optMapper.mapString(resultSet.getString(trepnStat.getTableValueColumnName()));
                        } else {
                            throw new IllegalArgumentException("Expected a ITrepnStatValueMapper to map String value to double");
                        }
                        break;
                    default:
                        if (optMapper != null) {
                            value = optMapper.mapValue(resultSet.getInt(trepnStat.getTableValueColumnName()));
                        } else {
                            value = resultSet.getDouble(trepnStat.getTableValueColumnName());
                        }
                        break;
                }
                if (previousTimestamp != timestamp) {

                    addToSeries(series, previousTimestamp, timestamp, value);


                    previousTimestamp = timestamp;
                }
                if (timestamp > appTimestamp) {
                    appTimestamp = timestamp;
                }
                if (previousTimestamp == -1) {
                    previousTimestamp = timestamp;
                } else {
                    if (timestamp < previousTimestamp) {
                        previousTimestamp = timestamp;
                    }

                }
            }
            //store the series color to use it in EnergyDataPlotPanel
            String seriesRGBColor = String.valueOf(trepnStat.getColor().getRGB());
            series.setDescription(seriesRGBColor);


            seriesCollection.addSeries(series);

        } catch (SQLException e) {
            if (TrepnStat.BATTERY_POWER_EPM == trepnStat) {
                series = createTrepnXYSeries(connection, TrepnStat.BATTERY_POWER_PMIC, optMapper);
            } else if ((trepnStat == TrepnStat.CPU_1_FREQ) || (trepnStat == TrepnStat.CPU_2_FREQ) || (trepnStat == TrepnStat.CPU_3_FREQ)) {
                series = null;
            } else {
                System.out.println("WARNING: caught SQLException for TrepnStat " + trepnStat + ": " + e.getMessage());
                series = null;
            }
        }

        offset = appPreviousTimestamp;
        trepnStat = TrepnStat.BATTERY_POWER_PMIC;
        //series = new XYSeries(TrepnStatToUIStringConverter.getUIString(trepnStat), true, false);
        series = new XYSeries(trepnStat.toString(), true, false);
        tableName = trepnStat.getTableName();
        try {
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);
            ResultSet resultSet = statement.executeQuery(
                    "select timestamp, " + trepnStat.getTableValueColumnName() +
                            " from " + tableName + " order by " + "timestamp");

            int previousTimestamp = -1;
            while (resultSet.next()) {
                int timestamp = resultSet.getInt("timestamp");
                double value;
                switch (trepnStat) {
                    case CPU_2_FREQ:
                    case CPU_3_FREQ:
                        if (optMapper != null) {
                            value = optMapper.mapString(resultSet.getString(trepnStat.getTableValueColumnName()));
                        } else {
                            throw new IllegalArgumentException("Expected a ITrepnStatValueMapper to map String value to double");
                        }
                        break;
                    default:
                        if (optMapper != null) {
                            value = optMapper.mapValue(resultSet.getInt(trepnStat.getTableValueColumnName()));
                        } else {
                            value = resultSet.getDouble(trepnStat.getTableValueColumnName());
                        }
                        break;
                }
                if (previousTimestamp != timestamp) {
                    if (appPreviousTimestamp >= timestamp && timestamp <= appTimestamp) {
                        if (oldTimestamp > 0) {
                            addToSeries(series, oldTimestamp + (previousTimestamp - oldTimestamp), oldTimestamp + (timestamp - oldTimestamp), value);
                        } else {
                            addToSeries(series, previousTimestamp, timestamp, value);
                        }
                    }
                    previousTimestamp = timestamp;
                }
            }
            seriesCollection.addSeries(series);

        } catch (SQLException e) {
            if (TrepnStat.BATTERY_POWER_EPM == trepnStat) {
                series = createTrepnXYSeries(connection, TrepnStat.BATTERY_POWER_PMIC, optMapper);
            } else if ((trepnStat == TrepnStat.CPU_1_FREQ) || (trepnStat == TrepnStat.CPU_2_FREQ) || (trepnStat == TrepnStat.CPU_3_FREQ)) {
                series = null;
            } else {
                System.out.println("WARNING: caught SQLException for TrepnStat " + trepnStat + ": " + e.getMessage());
                series = null;
            }
        }

        return seriesCollection;
    }


    private void addToSeries(XYSeries series, int previousTimestamp, int timestamp, double value) {
        if (checkIfNeedToInsertZeroValues(previousTimestamp, timestamp)) {
            while (previousTimestamp + this.expectedPollIntervalInMs < timestamp) {
                series.add(previousTimestamp + this.expectedPollIntervalInMs, 0.0D);
                previousTimestamp = (int) (previousTimestamp + this.expectedPollIntervalInMs);
            }
        }
        series.add(timestamp, value);
    }

    boolean checkIfNeedToInsertZeroValues(int previousTimestamp, int timestamp) {
        return this.shouldInsertZeroValuesForMissingExpectedDatapoints && timestamp - previousTimestamp > this.expectedPollIntervalInMs * this.allowedPollIntervalMultipleRange;
    }

    public XYSeries createNumPointsAverageTrepnXYSeries(XYSeries series, int numberOfPointsToAverage) {
        if (series == null) {
            throw new NullPointerException("Invalid null value received for series argument");
        }
        if (numberOfPointsToAverage < 1) {
            throw new IllegalArgumentException("numberOfPointsToAverage argument should be >= 1, received: " + numberOfPointsToAverage);
        }
        XYSeries numPointsAverageSeries = new XYSeries(series.getKey(), true, false);
        LinkedList<Number> lastNumPointsToAverage = new LinkedList<Number>();
        for (int i = 0; i < series.getItemCount(); i++) {
            lastNumPointsToAverage.add(series.getY(i));
            if (lastNumPointsToAverage.size() > numberOfPointsToAverage) {
                lastNumPointsToAverage.removeFirst();
            }
            double total = 0.0D;
            for (Number num : lastNumPointsToAverage) {
                if (num != null) {
                    total += num.doubleValue();
                }
            }
            numPointsAverageSeries.add(series.getX(i), total / lastNumPointsToAverage.size());
        }
        return numPointsAverageSeries;
    }

    @Override
    public boolean toCSVFile(Connection connection, TrepnStat trepnStat, ITrepnStatValueMapper optMapper, String outputFilename) {

        boolean series = true;
        // boolean seriesConversion=true;
        String tableName = trepnStat.getTableName();
        try {
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);
//            boolean result0 = statement.execute(".mode list ");
//            boolean result1 = statement.execute(".separator ,");
//            boolean result2 = statement.execute(".output "+outputFilename);
//            boolean result3 = statement.execute(
//                    "select timestamp, " + trepnStat.getTableValueColumnName() +
//                            " from " + tableName + " order by " + "timestamp");
//
//            boolean result4 = statement.execute(".exit");
            statement.addBatch("mode list ");
            statement.addBatch(".separator ,");
            statement.addBatch(".output " + outputFilename);
            statement.addBatch(
                    "select timestamp, " + trepnStat.getTableValueColumnName() +
                            " from " + tableName + " order by " + "timestamp");

            statement.addBatch(".exit");
//            seriesConversion= result0 &&result1 &&result2 &&result3;
//            seriesConversion=statement.executeBatch();
            for (int res : statement.executeBatch()) {
                System.out.println("Batch res: " + res);
            }

//            int previousTimestamp = -1;
//            while (resultSet.next())
//            {
//                int timestamp = resultSet.getInt("timestamp");
//                double value;
//                switch (trepnStat)
//                {
//                    case CPU_2_FREQ:
//                    case CPU_3_FREQ:
//                        if (optMapper != null) {
//                            value = optMapper.mapString(resultSet.getString(trepnStat.getTableValueColumnName()));
//                        } else {
//                            throw new IllegalArgumentException("Expected a ITrepnStatValueMapper to map String value to double");
//                        }
//                        break;
//                    default:
//                        if (optMapper != null) {
//                            value = optMapper.mapValue(resultSet.getInt(trepnStat.getTableValueColumnName()));
//                        } else {
//                            value = resultSet.getDouble(trepnStat.getTableValueColumnName());
//                        }
//                        break;
//                }
//                if (previousTimestamp != timestamp)
//                {
//                    addToSeries(series, previousTimestamp, timestamp, value);
//                    previousTimestamp = timestamp;
//                }
//            }
        } catch (SQLException e) {
            if (TrepnStat.BATTERY_POWER_EPM == trepnStat) {
                series = toCSVFile(connection, TrepnStat.BATTERY_POWER_PMIC, optMapper, outputFilename);
            } else if ((trepnStat == TrepnStat.CPU_1_FREQ) || (trepnStat == TrepnStat.CPU_2_FREQ) || (trepnStat == TrepnStat.CPU_3_FREQ)) {
                series = false;
            } else {
                System.out.println("WARNING: caught SQLException for TrepnStat " + trepnStat + ": " + e.getMessage());
                series = false;
            }
        }
        return series;// && seriesConversion;
    }
}
