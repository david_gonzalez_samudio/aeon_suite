package edu.gmu.cs.aeon.trepn.core.dataset;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.trepn.data;

public abstract interface ITrepnStatValueMapper {
    public abstract double mapValue(int paramInt);

    public abstract double mapString(String paramString);
}
