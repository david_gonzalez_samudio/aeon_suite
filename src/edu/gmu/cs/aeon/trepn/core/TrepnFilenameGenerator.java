package edu.gmu.cs.aeon.trepn.core;

/**
 * Created by DavidIgnacio on 7/14/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.trepn.datafile;

import java.util.Calendar;

public class TrepnFilenameGenerator {
    public static final String ON_DEVICE_FILEPATH_PREFIX = "/mnt/sdcard/trepn/";
    public static final String FILENAME_PREFIX = "Trepn_";
    public static final String TREPN_DB_DATAFILE_SUFFIX = ".db";
    public static final String LOGCAT_FILENAME_SUFFIX = ".txt";
    private String lastGeneratedFilename = null;

    public static String getTrepnDatafileRegexp() {
        return "Trepn_*.db";
    }

    public static String getTrepnDatafileTemporaryFilesRegexp() {
        return "Trepn_*.db-*";
    }

    public static String getTrepnLogCatFileRegexp() {
        return "Trepn_*.txt";
    }

    public static String generateExpectedLogCatFilePath(String trepnDbDatafilePath) {
        String retval = null;
        if ((trepnDbDatafilePath != null) && (!trepnDbDatafilePath.isEmpty())) {
            int index = trepnDbDatafilePath.lastIndexOf(".db");
            if (-1 != index) {
                retval = trepnDbDatafilePath.substring(0, index) + ".txt";
            } else {
                retval = trepnDbDatafilePath + ".txt";
            }
        }
        return retval;
    }

    public static String generateExpectedOnDeviceLogCatFilePath(String trepnDbDatafileName) {
        String retval = null;
        if (trepnDbDatafileName != null) {
            if (trepnDbDatafileName.contains("/")) {
                int index = trepnDbDatafileName.lastIndexOf("/");
                if (trepnDbDatafileName.length() >= index + 1) {
                    trepnDbDatafileName = trepnDbDatafileName.substring(index + 1);
                } else {
                    throw new IllegalArgumentException("Invalid arg: " + trepnDbDatafileName);
                }
            }
            int index = trepnDbDatafileName.lastIndexOf(".db");
            if (-1 != index) {
                retval = "/mnt/sdcard/trepn/" + trepnDbDatafileName.substring(0, index) + ".txt";
            } else {
                retval = "/mnt/sdcard/trepn/" + trepnDbDatafileName + ".txt";
            }
        }
        return retval;
    }

    public String generateFilename() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(1);
        int month = 1 + calendar.get(2);
        int date = calendar.get(5);
        int hourOfTheDay = calendar.get(11);
        int minute = calendar.get(12);
        int second = calendar.get(13);

        this.lastGeneratedFilename = String.format("%s%04d.%02d.%02d_%02d%02d%02d%s", new Object[]{"Trepn_", Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(date), Integer.valueOf(hourOfTheDay), Integer.valueOf(minute),
                Integer.valueOf(second), ".db"});

        return this.lastGeneratedFilename;
    }

    public String getLastGeneratedFilename() {
        return this.lastGeneratedFilename;
    }
}
