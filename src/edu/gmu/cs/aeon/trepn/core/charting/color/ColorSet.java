package edu.gmu.cs.aeon.trepn.core.charting.color;
/*  2:   */ 
/*  3:   */

import java.awt.*;

/*  4:   */
/*  5:   */ public enum ColorSet
/*  6:   */ {
    /*  7:10 */   DARK_RED(
/*  8:11 */     8919580, 9517628, 10382187, 11246747), RED(13828096, 13716299, 14183537, 14983838), LIGHTPINK(12487075, 16036050, 16040154, 16373223), PINK(15538758, 15229065, 16750505, 16367050), LIGHTHOTPINK(11871339, 15937680, 15946145, 15955123), HOTPINK(8200786, 15992443, 15937680, 15818663), CORAL(16213313, 15031100, 12798508, 13130263), ORANGE(16730112, 16732686, 16739379, 16744237), MED_ORANGE(11882752, 16740608, 16748342, 16756076), MED_ORANGE2(14519308, 16753166, 16758340, 16765838), LIGHT_ORANGE(14388249, 16629312, 16700029, 16771002), YELLOW(16765952, 16771626, 16383744, 16773532), LIGHTYELLOW(16774001, 16774806, 16775344, 16775344), GREEN(9289789, 10603354, 12573574, 14609329), KHAKI(8288070, 8615982, 13221474, 11381102), DARKGREEN(4673792, 2857216, 8820992, 12902400), BLUEGREEN(7011218, 6613382, 6224750, 7338364), BLUEGREEN2(39283, 51608, 3786404, 7980723), TURQUOISE(1130552, 2721655, 966046, 457166), BABY_BLUE(5879221, 8257535, 10682367, 13041663), BABY_BLUE2(1411205, 2217939, 6608852, 8706005), BLUEGREEN3(3757397, 5866885, 7119522, 9822432), BLUE(31688, 2527953, 7255776, 12705264), DARK_BLUE(934515, 2579593, 6264248, 10406113), DARK_BLUE2(601165, 934515, 1201555, 1535937), DARK_BLUE3(210, 255, 3553023, 6316287), BLUEGRAY(10006471, 7568545, 7568545, 4285054), PURPLE(287848, 8421631, 11382271, 13421823), PURPLE2(8795286, 10048935, 12293320, 14338788), PURPLE3(5570730, 8388863, 10700799, 12880127), MAGENTA1(7081617, 10950368, 11747808, 13143008), MAGENTA2(6291552, 8388736, 10944679, 11154344), MAGENTA3(12595655, 14825708, 16004095, 16711935), BROWN(10119199, 10779198, 11703916, 12628378), BROWN1(10119199, 10779198, 11703916, 12628378), BROWN2(10119199, 10779198, 11703916, 12628378), BROWN3(10119199, 10779198, 11703916, 12628378), BROWN4(10119199, 10779198, 11703916, 12628378);
    /*  9:   */
/* 10:   */   private Color darkest;
    /* 11:   */   private Color secondDarkest;
    /* 12:   */   private Color secondLightest;
    /* 13:   */   private Color lightest;

    /* 14:   */
/* 15:   */
    private ColorSet(int darkest, int secondDarkest, int secondLightest, int lightest)
/* 16:   */ {
/* 17:56 */
        this.darkest = new Color(darkest);
/* 18:57 */
        this.secondDarkest = new Color(secondDarkest);
/* 19:58 */
        this.secondLightest = new Color(secondLightest);
/* 20:59 */
        this.lightest = new Color(lightest);
/* 21:   */
    }

    /* 22:   */
/* 23:   */
    public Color getDarkest()
/* 24:   */ {
/* 25:63 */
        return this.darkest;
/* 26:   */
    }

    /* 27:   */
/* 28:   */
    public Color getSecondDarkest()
/* 29:   */ {
/* 30:67 */
        return this.secondDarkest;
/* 31:   */
    }

    /* 32:   */
/* 33:   */
    public Color getSecondLightest()
/* 34:   */ {
/* 35:71 */
        return this.secondLightest;
/* 36:   */
    }

    /* 37:   */
/* 38:   */
    public Color getLightest()
/* 39:   */ {
/* 40:75 */
        return this.lightest;
/* 41:   */
    }

    /* 42:   */
/* 43:   */
    public Color getColor(ColorSetVariation colorSetVariation)
/* 44:   */ {
/* 45:79 */
        switch (colorSetVariation)
/* 46:   */ {
/* 47:   */
            case DARKEST:
/* 48:81 */
                return this.darkest;
/* 49:   */
            case LIGHTEST:
/* 50:83 */
                return this.secondDarkest;
/* 51:   */
            case SECOND_DARKEST:
/* 52:85 */
                return this.secondLightest;
/* 53:   */
            case SECOND_LIGHTEST:
/* 54:87 */
                return this.lightest;
/* 55:   */
        }
/* 56:89 */
        throw new IllegalStateException();
/* 57:   */
    }
/* 58:   */
}



/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.core_1.1.0.201406060127.jar

 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.core.charting.color.ColorSet

 * JD-Core Version:    0.7.0.1

 */