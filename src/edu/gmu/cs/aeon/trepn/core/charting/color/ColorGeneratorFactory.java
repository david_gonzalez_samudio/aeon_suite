package edu.gmu.cs.aeon.trepn.core.charting.color;

/*  2:   */
/*  3:   */ public class ColorGeneratorFactory
/*  4:   */ {
    /*  5:   */   private static final int FIRST_NUM_COLORS_TO_NOT_REPEAT = 2;
    /*  6:10 */   private static IColorGenerator perAppUsageColorGenerator = null;
    /*  7:11 */   private static IColorGenerator customChartColorGenerator = null;

    /*  8:   */
/*  9:   */
    public static IColorGenerator getPerAppUsageColorGenerator()
/* 10:   */ {
/* 11:14 */
        if (perAppUsageColorGenerator == null) {
/* 12:15 */
            createPerAppColorGenerator();
/* 13:   */
        }
/* 14:17 */
        return perAppUsageColorGenerator;
/* 15:   */
    }

    /* 16:   */
/* 17:   */
    private static synchronized void createPerAppColorGenerator()
/* 18:   */ {
/* 19:21 */
        if (perAppUsageColorGenerator == null) {
/* 20:22 */
            perAppUsageColorGenerator = new ColorGenerator();
/* 21:   */
        }
/* 22:   */
    }

    /* 23:   */
/* 24:   */
    public static IColorGenerator getCustomChartColorGenerator()
/* 25:   */ {
/* 26:27 */
        if (customChartColorGenerator == null) {
/* 27:28 */
            createCustomChartColorGenerator();
/* 28:   */
        }
/* 29:30 */
        return customChartColorGenerator;
/* 30:   */
    }

    /* 31:   */
/* 32:   */
    private static synchronized void createCustomChartColorGenerator()
/* 33:   */ {
/* 34:34 */
        if (customChartColorGenerator == null) {
/* 35:35 */
            customChartColorGenerator = new ColorGenerator();
/* 36:   */
        }
/* 37:   */
    }
/* 38:   */
}



/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.core_1.1.0.201406060127.jar

 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.core.charting.color.ColorGeneratorFactory

 * JD-Core Version:    0.7.0.1

 */