package edu.gmu.cs.aeon.trepn.core.charting.color;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.charting.color;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class ColorGenerator
        implements IColorGenerator {
    private static final ColorSetVariation DEFAULT_COLOR_VARIATION = ColorSetVariation.SECOND_DARKEST;
    protected ColorSet[] standardColorSetOrder = ColorSet.values();
    protected ColorSet[] applicationColorSetOrder = {
            ColorSet.RED,
            ColorSet.DARK_BLUE,
            ColorSet.GREEN,
            ColorSet.ORANGE,
            ColorSet.PINK,
            ColorSet.BLUE,
            ColorSet.PURPLE,
            ColorSet.BROWN};
    protected Map<String, ColorSet> applicationColorSetMap;

    ColorGenerator() {
        this.applicationColorSetMap = new HashMap();
    }

    public Color getColor(int index) {
        return getColorSet(index).getColor(DEFAULT_COLOR_VARIATION);
    }

    public ColorSet getColorSet(int index) {
        int colorIndex = index % this.standardColorSetOrder.length;
        return this.standardColorSetOrder[colorIndex];
    }

    public ColorSet getApplicationColorSet(String applicationName) {
        ColorSet retval = (ColorSet) this.applicationColorSetMap.get(applicationName);
        if (retval == null) {
            retval = addColorSetToApplicationColorSetMap(applicationName);
        }
        return retval;
    }

    private synchronized ColorSet addColorSetToApplicationColorSetMap(String applicationName) {
        ColorSet retval = (ColorSet) this.applicationColorSetMap.get(applicationName);
        if (retval == null) {
            retval = this.applicationColorSetOrder[(this.applicationColorSetMap.size() % this.applicationColorSetOrder.length)];
            this.applicationColorSetMap.put(applicationName, retval);
        }
        return retval;
    }

    public void clearApplicationColors() {
        this.applicationColorSetMap.clear();
    }
}
