package edu.gmu.cs.aeon.trepn.core.charting.color;

import java.awt.*;

public abstract interface IColorGenerator {
    public abstract Color getColor(int paramInt);

    public abstract ColorSet getColorSet(int paramInt);

    public abstract ColorSet getApplicationColorSet(String paramString);

    public abstract void clearApplicationColors();
}



/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.core_1.1.0.201406060127.jar

 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.core.charting.color.IColorGenerator

 * JD-Core Version:    0.7.0.1

 */