package edu.gmu.cs.aeon.trepn.core.charting.color;
/*  2:   */ 
/*  3:   */

import java.awt.*;

/*  4:   */

/*  5:   */
/*  6:   */ public class ColorGeneratorDoNotRepeatFirstXColors
/*  7:   */ extends ColorGenerator
/*  8:   */ {
    /*  9:   */   private int firstNumColorsToNotRepeat;

    /* 10:   */
/* 11:   */
    public ColorGeneratorDoNotRepeatFirstXColors(int firstNumColorsToNotRepeat)
/* 12:   */ {
/* 13:13 */
        this.firstNumColorsToNotRepeat = firstNumColorsToNotRepeat;
/* 14:14 */
        if (firstNumColorsToNotRepeat >= this.standardColorSetOrder.length) {
/* 15:15 */
            throw new IllegalArgumentException("Value too large - not enough colors defined in ColorGenerator");
/* 16:   */
        }
/* 17:   */
    }

    /* 18:   */
/* 19:   */
    public Color getColor(int index)
/* 20:   */ {
/* 21:21 */
        return super.getColor(calculateIndex(index, this.standardColorSetOrder));
/* 22:   */
    }

    /* 23:   */
/* 24:   */
    public ColorSet getColorSet(int index)
/* 25:   */ {
/* 26:26 */
        return super.getColorSet(calculateIndex(index, this.standardColorSetOrder));
/* 27:   */
    }

    /* 28:   */
/* 29:   */
    public ColorSet getApplicationColorSet(String applicationName)
/* 30:   */ {
/* 31:31 */
        ColorSet retval = (ColorSet) this.applicationColorSetMap.get(applicationName);
/* 32:32 */
        if (retval == null) {
/* 33:33 */
            retval = addColorSetToApplicationColorSetMap(applicationName);
/* 34:   */
        }
/* 35:35 */
        return retval;
/* 36:   */
    }

    /* 37:   */
/* 38:   */
    private synchronized ColorSet addColorSetToApplicationColorSetMap(String applicationName)
/* 39:   */ {
/* 40:39 */
        ColorSet retval = (ColorSet) this.applicationColorSetMap.get(applicationName);
/* 41:40 */
        if (retval == null)
/* 42:   */ {
/* 43:41 */
            retval = this.applicationColorSetOrder[calculateIndex(this.applicationColorSetMap.size(), this.applicationColorSetOrder)];
/* 44:42 */
            this.applicationColorSetMap.put(applicationName, retval);
/* 45:   */
        }
/* 46:44 */
        return retval;
/* 47:   */
    }

    /* 48:   */
/* 49:   */   int calculateIndex(int index, Object[] array)
/* 50:   */ {
/* 51:48 */
        int retval = index;
/* 52:49 */
        if (index >= array.length) {
/* 53:50 */
            retval = (index - this.firstNumColorsToNotRepeat) % (array.length - this.firstNumColorsToNotRepeat) +
/* 54:51 */         this.firstNumColorsToNotRepeat;
/* 55:   */
        }
/* 56:53 */
        return retval;
/* 57:   */
    }
/* 58:   */
}



/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.core_1.1.0.201406060127.jar

 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.core.charting.color.ColorGeneratorDoNotRepeatFirstXColors

 * JD-Core Version:    0.7.0.1

 */