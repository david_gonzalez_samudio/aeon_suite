package edu.gmu.cs.aeon.trepn.core.charting.color;

public enum ColorSetVariation {
    DARKEST, SECOND_DARKEST, SECOND_LIGHTEST, LIGHTEST;
}



/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.core_1.1.0.201406060127.jar

 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.core.charting.color.ColorSetVariation

 * JD-Core Version:    0.7.0.1

 */