package edu.gmu.cs.aeon.trepn.core.charting.color;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.core.charting;

import edu.gmu.cs.aeon.trepn.common.StatType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class StatTypeToColorSetMapper {
    private static Map<StatType, ColorSet> statToColor = new HashMap();

    public static void setStatList(List<StatType> statTypeList) {
        int index = 0;
        ColorSet[] colors = ColorSet.values();

        int maxIndex = colors.length - 1;

        statToColor.clear();
        for (StatType type : statTypeList) {
            if ((!type.equals(StatType.MOBILE_DATA_USAGE_PER_APP)) && (!type.equals(StatType.WIFI_DATA_USAGE_PER_APP))) {
                if (index <= maxIndex) {
                    statToColor.put(type, colors[(index++)]);
                }
            }
        }
    }

    public static final ColorSet getColorSet(StatType statType) {
        if (statToColor.isEmpty()) {
            List<StatType> statTypeList = StatType.getAllOrderedStatTypes();
            statTypeList.addAll(StatType.getAllPremierOrderedStatTypes());
            setStatList(statTypeList);
        }
        if (statToColor.containsKey(statType)) {
            return (ColorSet) statToColor.get(statType);
        }
        throw new UnsupportedOperationException("StatType " + statType + " is not supported");
    }
}
