/*     */
package edu.gmu.cs.aeon.trepn.control.actions;
/*     */
/*     */

import edu.gmu.cs.aeon.trepn.common.ControlStatus;
import edu.gmu.cs.aeon.trepn.control.DeviceManager;
import edu.gmu.cs.aeon.trepn.control.OnControlActionRunListener;
import edu.gmu.cs.aeon.trepn.control.SelectedDeviceSupplier;
import edu.gmu.cs.aeon.trepn.control.TrepnCommands;
import edu.gmu.cs.aeon.trepn.control.util.PrefFilesUpdater;
import edu.gmu.cs.aeon.trepn.core.TrepnFilenameGenerator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*     */
/*     */
/*     */
/*     */
/*     */

/*     */ 
/*     */ public class startService
/*     */ extends ControlAction
/*     */ {
    /*     */   private static final boolean START_LOGCAT_ON_DEVICE_IN_BACKGROUND_PROCESS = true;
    private final boolean startOnPowerSourceRemove;
    /*     */   private DeviceManager deviceManager;
    /*     */   private OnControlActionRunListener listener;
    /*     */   private TrepnFilenameGenerator trepnFilenameGenerator;
    /*     */   private boolean useWakelock;
    /*     */   private SafeToDisconnectDialog dialog;

    /*     */
/*     */
    public startService(OnControlActionRunListener listener, TrepnFilenameGenerator trepnFilenameGenerator, boolean useWakelock, boolean startOnPowerSourceRemove)
/*     */ {

        this.listener = listener;
/*  30 */
        this.trepnFilenameGenerator = trepnFilenameGenerator;
/*  31 */
        this.useWakelock = useWakelock;
        this.startOnPowerSourceRemove = startOnPowerSourceRemove;
/*     */
    }

    /*     */
/*     */
    public void run()
/*     */ {
/*  38 */
        final startService parent_this = this;
/*     */     
/*  40 */
        ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.STARTING_TREPN);
/*  41 */
        new Thread()
/*     */ {
            /*     */
            public void run()
/*     */ {
/*     */
                try
/*     */ {
/*  45 */
                    Thread.sleep(200L);
/*     */
                }
/*     */ catch (Exception localException) {
                }
/*  48 */
                startAction(parent_this);
/*     */
            }
/*     */
        }.start();
/*     */
    }

    /*     */
/*     */
    private void startAction(final startService controlAction)
/*     */ {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.execute(new Runnable() {
            /*     */
            public void run()
/*     */ {
/*  58 */
                String selectedDevice = controlAction.deviceManager.getSelectedDeviceString();
/*     */         
/*     */ 
/*  61 */
                ExecCommandRetvalInfo retvalInfo = null;
/*     */         
/*     */ 
/*     */ 
/*  65 */
                // LogCatOnDeviceFactory.getLogCatOnDeviceController().stopLogCatOnDeviceInBackground(selectedDevice);
/*     */         
/*     */ 
/*  68 */
                // new ClearLogCatAction(selectedDevice, false).run();
/*     */         
/*     */ 
/*     */ 
/*  72 */
                ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.CONFIGURING_TREPN);
/*     */         
/*     */ 
/*     */ 
/*  76 */
                retvalInfo = PrefFilesUpdater.update(selectedDevice, controlAction.useWakelock, controlAction);
/*  77 */
                if (retvalInfo.hasError())
/*     */ {
                    System.out.println("" + retvalInfo.getLastNonEmptyOutputLine());
                }
/*  79 */
                String trepnDatafileName = controlAction.trepnFilenameGenerator.generateFilename();
/*  80 */
                retvalInfo = TrepnCommands.startCommand(TrepnCommands.Action.START_SERVICE,
/*  81 */             trepnDatafileName,
/*  82 */             selectedDevice, controlAction, false);
                if (!retvalInfo.hasError()) {
/*  85 */
//                    LogCatOnDeviceFactory.getLogCatOnDeviceController().startLogCatOnDeviceInBackground(
/*  86 */
                    System.out.println("generateExpectedOnDeviceLogCatFilePath >> " + TrepnFilenameGenerator.generateExpectedOnDeviceLogCatFilePath(trepnDatafileName));
///*  87 */               selectedDevice);
                    System.out.println("" + retvalInfo.getLastNonEmptyOutputLine());
/*     */
                }

                if (!startOnPowerSourceRemove) {
                    retvalInfo = TrepnCommands.startCommand(TrepnCommands.Action.START_PROFILING,
/*  81 */             trepnDatafileName,
/*  82 */             selectedDevice, controlAction, false);
                } else {
                    retvalInfo = TrepnCommands.startCommand(TrepnCommands.Action.UPDATE_GENERATED_FILENAME,
/*  81 */             trepnDatafileName,
/*  82 */             selectedDevice, controlAction, false);

                }
/*  84 */
                if (!retvalInfo.hasError()) {
/*  85 */
//                    LogCatOnDeviceFactory.getLogCatOnDeviceController().startLogCatOnDeviceInBackground(
/*  86 */
                    System.out.println("generateExpectedOnDeviceLogCatFilePath >> " + TrepnFilenameGenerator.generateExpectedOnDeviceLogCatFilePath(trepnDatafileName));
///*  87 */               selectedDevice);
                    System.out.println("" + retvalInfo.getLastNonEmptyOutputLine());
/*     */
                }
/*     */

/*  93 */
                if (retvalInfo.hasError())
/*     */ {
/*  94 */
                    ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.ACTION_DONE);
/*  95 */
                    controlAction.displayErrorMessage(retvalInfo, startService.this.listener);
/*     */
                }
/*     */
                else
/*     */ {
/*  99 */
                    controlAction.listener.onControlActionRun(controlAction);
/*     */           
/* 101 */
                    ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.ACTION_DONE);
/*     */           
/*     */ 
/* 104 */
                    controlAction.showSafeToDisconnectDlg();
/*     */
                }
/*     */
            }
/*     */
        });
        executorService.shutdown();
/*     */
    }

    /*     */
/*     */
    private void showSafeToDisconnectDlg()
/*     */ {
///* 113 */     boolean hideDialog = Settings.getGeneralSettingBool("HIDE_DISCONNECT_PROMPT", false);
/* 113 */
        boolean hideDialog = false;
/* 115 */
        if (!hideDialog)
/*     */ {
/* 116 */
            if (this.dialog != null) {
/* 117 */
                this.dialog.isChecked();
/*     */
            }
/* 120 */
            this.dialog = new SafeToDisconnectDialog("Safe to disconnect device",
/* 121 */         "Device configuration is complete and profiling has started.  You may disconnect at this time.", null, null);
/* 122 */
            this.dialog.isChecked();
/*     */
        }
/*     */
    }
/*     */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.actions.startService
 * JD-Core Version:    0.7.0.1
 */