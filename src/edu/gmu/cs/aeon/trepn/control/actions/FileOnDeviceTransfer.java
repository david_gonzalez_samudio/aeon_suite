/*     */
package edu.gmu.cs.aeon.trepn.control.actions;
/*     */ 
/*     */

import edu.gmu.cs.aeon.trepn.common.ControlStatus;
import edu.gmu.cs.aeon.trepn.common.LocalTrepnDirectoryCreator;
import edu.gmu.cs.aeon.trepn.common.TrepnGeneralValues;
import edu.gmu.cs.aeon.trepn.control.DeviceManager;
import edu.gmu.cs.aeon.trepn.control.OnControlActionRunListener;
import edu.gmu.cs.aeon.trepn.control.OnDatafileChangedListener;
import edu.gmu.cs.aeon.trepn.core.TrepnFilenameGenerator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */

/*     */ 
/*     */ public class FileOnDeviceTransfer
/*     */ extends ControlAction
/*     */ {
    /*  20 */   private static final String TREPN_DATA_DIR_DATAFILES_REGEXP = "/mnt/sdcard/trepn/" +
/*  21 */     TrepnFilenameGenerator.getTrepnDatafileRegexp();
    /*     */ boolean shouldDisplayErrorMessages;
    /*  19 */   private boolean DEBUG_PRINT_MSGS = true;
    /*     */   private DeviceManager supplier;
    /*     */   private OnDatafileChangedListener datafileChangedListener;
    /*     */   private OnControlActionRunListener controlActionRunListener;
    /*     */   private TrepnFilenameGenerator trepnFilenameGenerator;

    /*     */
/*     */
    public FileOnDeviceTransfer(DeviceManager supplier, OnDatafileChangedListener datafileChangedListener, OnControlActionRunListener controlActionRunListener, TrepnFilenameGenerator trepnFilenameGenerator)
/*     */ {
/*  31 */
        this(supplier, datafileChangedListener, controlActionRunListener, trepnFilenameGenerator, true);
/*     */
    }

    /*     */
/*     */
    public FileOnDeviceTransfer(DeviceManager supplier, OnDatafileChangedListener datafileChangedListener, OnControlActionRunListener controlActionRunListener, TrepnFilenameGenerator trepnFilenameGenerator, boolean shouldDisplayErrorMessages)
/*     */ {
/*  37 */
        this.supplier = supplier;
/*  38 */
        this.datafileChangedListener = datafileChangedListener;
/*  39 */
        this.controlActionRunListener = controlActionRunListener;
/*  40 */
        this.trepnFilenameGenerator = trepnFilenameGenerator;
/*  41 */
        this.shouldDisplayErrorMessages = shouldDisplayErrorMessages;
/*     */
    }

    /*     */
/*     */
    public void run()
/*     */ {
/*  46 */
        final FileOnDeviceTransfer parent_this = this;
/*     */     
/*  48 */
        new Thread()
/*     */ {
            /*     */
            public void run()
/*     */ {
/*     */
                try
/*     */ {
/*  52 */
                    Thread.sleep(200L);
/*     */
                }
/*     */ catch (Exception localException) {
                }
/*  55 */
                FileOnDeviceTransfer.this.startRetrieve(parent_this);
/*     */
            }
/*     */
        }.start();
/*     */
    }

    /*     */
/*     */
    private void startRetrieve(final FileOnDeviceTransfer parent_this)
/*     */ {
///*  62 */     Display.getDefault().asyncExec(new Runnable()
///*     */     {
///*     */       public void run()
///*     */       {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.execute(new Runnable() {


            @Override
            public void run() {
            /*  65 */
                String selectedDevice = FileOnDeviceTransfer.this.supplier.getSelectedDeviceString();
/*  66 */
                String onDeviceTrepnDatabaseFilepath = FileOnDeviceTransfer.this.getBestEffortMostRecentOnDeviceTrepnDatafilePath(selectedDevice);
/*  68 */
                if (onDeviceTrepnDatabaseFilepath != null)
/*     */ {
/*  69 */
                    LocalTrepnDirectoryCreator.createLocalTrepnDatafileDirectory();
/*  71 */
                    if (FileOnDeviceTransfer.this.DEBUG_PRINT_MSGS) {
/*  72 */
                        System.out.println("Retrieve Trepn DB file");
/*     */
                    }
/*  74 */
                    ExecCommandRetvalInfo retvalInfo = FileOnDeviceTransfer.this.runAdbCommandLineCommand(
/*  75 */             new String[]{"pull", onDeviceTrepnDatabaseFilepath,
/*  76 */             FileOnDeviceTransfer.removeTrailingSlash(TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR)},
/*  77 */             selectedDevice);
/*  79 */
                    if (retvalInfo.exitValue == 0)
/*     */ {
                        ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.ACTION_DONE);
/*  80 */
                        if (FileOnDeviceTransfer.this.DEBUG_PRINT_MSGS) {
/*  81 */
                            System.out.println("Retrieve Trepn LogCat file");
/*     */
                        }
/*  84 */
                        FileOnDeviceTransfer.this.runAdbCommandLineCommand(
/*  85 */               new String[]{"pull",
/*  86 */               TrepnFilenameGenerator.generateExpectedOnDeviceLogCatFilePath(onDeviceTrepnDatabaseFilepath),
/*  87 */               FileOnDeviceTransfer.removeTrailingSlash(TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR)},
/*  88 */               selectedDevice);
/*     */
/*  90 */
                        String localFilepath = FileOnDeviceTransfer.createLocalTrepnDatafileDirectoryFilepath(FileOnDeviceTransfer.this.getFilename(onDeviceTrepnDatabaseFilepath));
/*  91 */
                        if (localFilepath != null) {
/*  92 */
                            FileOnDeviceTransfer.this.datafileChangedListener.onDatafileChanged(localFilepath);
/*     */
                        }
/*  94 */
                        FileOnDeviceTransfer.this.controlActionRunListener.onControlActionRun(parent_this);
/*     */
                    }
/*  97 */
                    else if (FileOnDeviceTransfer.this.shouldDisplayErrorMessages)
/*     */ {
/*  98 */
                        FileOnDeviceTransfer.this.displayErrorMessage(retvalInfo, FileOnDeviceTransfer.this.controlActionRunListener);
/*     */
                    }
/*     */
                }
/* 103 */
                ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.ACTION_DONE);

            }
        });

///*     */       }
///*     */     });
        executorService.shutdown();
/*     */
    }

    /*     */
/*     */
    private String getBestEffortMostRecentOnDeviceTrepnDatafilePath(String selectedDevice)
/*     */ {
/* 109 */
        String retval = null;
/* 110 */
        String lastGeneratedFilename = this.trepnFilenameGenerator.getLastGeneratedFilename();
/* 111 */
        if (lastGeneratedFilename != null)
/*     */ {
/* 112 */
            retval = "/mnt/sdcard/trepn/" + lastGeneratedFilename;
/*     */
        }
/*     */
        else
/*     */ {
/* 115 */
            ExecCommandRetvalInfo retvalInfo = runAdbCommandLineCommand(
/* 116 */         new String[]{"shell", "ls", TREPN_DATA_DIR_DATAFILES_REGEXP},
/* 117 */         selectedDevice);
/* 118 */
            if (!retvalInfo.hasError())
/*     */ {
///* 119 */         String onDeviceTrepnDatabaseFilepath = retvalInfo.getLastNonEmptyOutputLine();
/*     */
/* 119 */
                String onDeviceTrepnDatabaseFilepath = retvalInfo.getLastNonEmptyOutputLine();
/*     */ 
/*     */ 
/* 123 */
                retval = onDeviceTrepnDatabaseFilepath;
/*     */
            }
/* 126 */
            else if (this.shouldDisplayErrorMessages)
/*     */ {
/* 127 */
                displayErrorMessage(retvalInfo, this.controlActionRunListener);
/*     */
            }
/*     */
        }
/* 131 */
        return retval;
/*     */
    }
/*     */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.actions.getfilefromDevice
 * JD-Core Version:    0.7.0.1
 */