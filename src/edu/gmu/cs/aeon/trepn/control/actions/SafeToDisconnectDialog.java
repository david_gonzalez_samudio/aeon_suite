package edu.gmu.cs.aeon.trepn.control.actions;
/*     */ 
/*     */

import javax.swing.*;
import java.awt.*;

//todo not working
/*     */ 
/*     */ public class SafeToDisconnectDialog

/*     */ {
    /*     */   private static final String OKAY_BUTTON = "Ok";
    /*     */   private static final int GRIDLAYOUT_NUM_COLUMNS = 0;
    /*     */   private static final int GRIDDATA_HORZ_SPAN = 2;
    /*     */   private Button okayButton;
    /*     */   private Button checkbox;
    /*     */   private boolean checkboxState;
    /*     */   private String dialogMessage;
    /*     */   private String dialogTitle;
/*     */   
/*     */

    public SafeToDisconnectDialog(String dialogTitle, String dialogMessage, String initialValue, String validator)
/*     */ {
/*  38 */     //super(parentShell);
/*  39 */
        this.dialogMessage = dialogMessage;
/*  40 */
        this.dialogTitle = dialogTitle;
/*     */     
/*  42 */     //setShellStyle(84021);
/*     */
    }

    /*     */
/*     */
    protected void createDialogArea(Frame frame)
/*     */ {
/*  48 */
        Dialog control = new Dialog(frame);
/*  49 */
        Composite composite = (Composite) control;
/*     */     
/*  51 */
        GridLayout layout = new GridLayout();
/*  52 */
        layout.setRows(15);
/*  53 */
        layout.setColumns(30);
/*  54 */    // composite.(layout);
/*  55 */    // Label message = new Label(composite, 0);
/*  56 */    // message.setLayoutData(new GridData(4, 128, true, false));
/*  57 */
        control.setTitle(this.dialogMessage);
        JOptionPane.showConfirmDialog(frame, "do not show again?");
/*     */     
/*  59 */    // Label horizontalSeparator = new Label(composite, 256);
/*  60 */     //orizontalSeparator.setLayoutData(new GridData(768));
/*  61 */     //horizontalSeparator.setText(" ");
/*     */     
/*  63 */   //  this.checkbox = new Button(composite, 32);
/*  64 */    // this.checkbox.setText("Do not show again?");
/*  65 */     //this.checkbox.setSelection(false);
/*  66 */     //this.checkbox.setLayoutData(new GridData(4, 128, true, false));
/*     */     
/*  68 */    // return control;
/*     */
    }

    /*     */
///*     */   protected void configureShell(Shell newerShell)
///*     */   {
///*  73 */     super.configureShell(newerShell);
///*  74 */     newerShell.setText(this.dialogTitle);
///*     */   }
/*     */   
/*     */
    protected void okPressed()
/*     */ {
/*  79 */   //  this.checkboxState = this.checkbox.getSelection();
/*  80 */ //    super.okPressed();
/*  81 */     //Settings.setGeneralSettingBool("HIDE_DISCONNECT_PROMPT", this.checkboxState);
/*     */
    }

    /*     */
/*     */
    public boolean isChecked()
/*     */ {
/*  85 */
        return this.checkboxState;
/*     */
    }

    /*     */
/*     */
    protected void createButtonsForButtonBar(Composite parent)
/*     */ {
/*  94 */ //    this.okayButton = createButton(parent, 0, "Ok", true);
/*  95 */
        this.okayButton.setEnabled(true);
/*     */
    }
/*     */
///*     */   protected Control createButtonBar(Composite parent)
///*     */   {
///* 106 */     Composite composite = new Composite(parent, 0);
///*     */
///*     */
///* 109 */     GridLayout gridLayout = new GridLayout();
///* 110 */     gridLayout.numColumns = 0;
///* 111 */     gridLayout.makeColumnsEqualWidth = true;
///* 112 */     gridLayout.marginWidth = convertHorizontalDLUsToPixels(7);
///* 113 */     gridLayout.marginHeight = convertVerticalDLUsToPixels(7);
///* 114 */     gridLayout.horizontalSpacing = convertHorizontalDLUsToPixels(4);
///* 115 */     gridLayout.verticalSpacing = convertVerticalDLUsToPixels(4);
///*     */
///*     */
///* 118 */     GridData gridData = new GridData(132);
///* 119 */     gridData.horizontalSpan = 2;
///* 120 */     gridData.grabExcessHorizontalSpace = true;
///*     */
///* 122 */     composite.setLayout(gridLayout);
///* 123 */     composite.setLayoutData(gridData);
///*     */
///*     */
///* 126 */     composite.setFont(parent.getFont());
///*     */
///*     */
///* 129 */     createButtonsForButtonBar(composite);
///*     */
///* 131 */     return composite;
///*     */   }
/*     */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.actions.SafeToDisconnectDialog
 * JD-Core Version:    0.7.0.1
 */