package edu.gmu.cs.aeon.trepn.control.actions;

import edu.gmu.cs.aeon.trepn.common.ControlStatus;
import edu.gmu.cs.aeon.trepn.control.*;
import edu.gmu.cs.aeon.trepn.core.TrepnFilenameGenerator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*    */
/*    */ public class stopService
/*    */ extends ControlAction
/*    */ {
    /* 20 */   private boolean DEBUG_PRINT_MSGS = true;
    /*    */   private DeviceManager deviceManager;
    /*    */   private OnDatafileChangedListener datafileChangedListener;
    /*    */   private OnControlActionRunListener controlActionRunListener;
    /*    */   private TrepnFilenameGenerator trepnFilenameGenerator;

    /*    */   // Remove Shell parameter
/*    */
    public stopService(DeviceManager deviceManager, OnDatafileChangedListener datafileChangedListener, OnControlActionRunListener controlActionRunListener, TrepnFilenameGenerator trepnFilenameGenerator)
/*    */ {
/* 32 */
        this.deviceManager = deviceManager;
/* 33 */
        this.datafileChangedListener = datafileChangedListener;
/* 34 */
        this.controlActionRunListener = controlActionRunListener;
/* 35 */
        this.trepnFilenameGenerator = trepnFilenameGenerator;
/* 36 */     //setShell(shell);
/*    */
    }

    /*    */
/*    */
    public void run()
/*    */ {
/* 41 */
        final stopService parent_this = this;
/*    */     
/* 43 */
        ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.STOPPING_SERVICE);
/* 44 */
        new Thread()
/*    */ {
            /*    */
            public void run()
/*    */ {
/*    */
                try
/*    */ {
/* 48 */
                    Thread.sleep(200L);
/*    */
                }
/*    */ catch (Exception localException) {
                }
/* 51 */
                stopService.this.stopAction(parent_this);
/*    */
            }
/*    */
        }.start();
/*    */
    }

    /*    */
/*    */
    private void stopAction(final stopService parent_this)
/*    */ {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.execute(new Runnable() {
            /*    */
            public void run()
/*    */ {
/* 61 */
                String selectedDevice = stopService.this.deviceManager.getSelectedDeviceString();
/*    */
                ExecCommandRetvalInfo retvalInfo = TrepnCommands.startCommand(TrepnCommands.Action.STOP_SERVICE,
/*  81 */             null,
/*  82 */             selectedDevice, parent_this, false);
                if (!retvalInfo.hasError()) {
/*  85 */
//                    LogCatOnDeviceFactory.getLogCatOnDeviceController().startLogCatOnDeviceInBackground(
/*  86 */
                    System.out.println("generateExpectedOnDeviceLogCatFilePath >> " + TrepnFilenameGenerator.generateExpectedOnDeviceLogCatFilePath("STOPPED TREPN SERVICE"));
///*  87 */               selectedDevice);
                    System.out.println("" + retvalInfo.getLastNonEmptyOutputLine());
/*     */
                }
            }
/*    */
        });
        executorService.shutdown();
/*    */
    }
/*    */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.actions.StopTrepnAction
 * JD-Core Version:    0.7.0.1
 */