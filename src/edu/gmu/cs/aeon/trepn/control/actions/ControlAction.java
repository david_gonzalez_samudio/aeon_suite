package edu.gmu.cs.aeon.trepn.control.actions;
/*     */ 

/*     */

import edu.gmu.cs.aeon.trepn.common.AdbPathGenerator;
import edu.gmu.cs.aeon.trepn.common.TrepnGeneralValues;
import edu.gmu.cs.aeon.trepn.control.OnControlActionRunListener;
import edu.gmu.cs.aeon.trepn.control.SelectedDeviceSupplier;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */

/*     */ 
/*     */ public abstract class ControlAction
/*     */ {
    /*     */   public static final String OUTPUT_FROM_ADB_DAEMON_START_SUCCESSFULLY = "* daemon started successfully *";
    /*     */   protected static final String ON_DEVICE_TREPN_STATE_FILEPATH = "/mnt/sdcard/trepn/trepn_state";
    /*     */   static final String ERROR_MESSAGE_NEED_TO_ADD_ADB_TO_PATH = "Please add 'adb' (Android Debug Bridge) to your path";
    /*     */   private static final String TREPN_APP_PACKAGE = "com.quicinc.trepn";
    /*  31 */   private static final String[] COMMAND_ARRAY_DEVICES = {"devices"};
    /*  32 */   private static final String[] COMMAND_ARRAY_DEVICE_MODEL = {"shell", "getprop", "ro.product.model"};
    /*  33 */   private static final String[] COMMAND_ARRAY_CAT_TREPN_STATE = {"shell", "cat", "/mnt/sdcard/trepn/trepn_state"};
    /*  34 */   private static final String[] COMMAND_ARRAY_PS_TREPN = {"shell", "ps", getLastFifteenChars("com.quicinc.trepn")};
    /*     */   private static final String OUTPUT_FROM_NO_ADB_DEVICES = "List of devices attached";

    /*     */
/*     */
    private static String getLastFifteenChars(String s)
/*     */ {
/* 105 */
        String retval = s;
/* 106 */
        int length = retval.length();
/* 107 */
        if (length > 15) {
/* 108 */
            retval = retval.substring(length - 15);
/*     */
        }
/* 110 */
        return retval;
/*     */
    }

    /*     */
/*     */
    static String[] createAdbCommandStringArray(String optSelectedDevice, String[] commandArray)
/*     */ {
/* 265 */
        String adbPath = AdbPathGenerator.getAdbPath();
/* 266 */
        String[] retval = {adbPath};
/* 267 */
        if (commandArray != null)
/*     */ {
/* 268 */
            int index = 0;
/* 269 */
            if ((optSelectedDevice != null) && (!optSelectedDevice.isEmpty()))
/*     */ {
/* 270 */
                index = 3;
/* 271 */
                retval = new String[index + commandArray.length];
/* 272 */
                retval[0] = adbPath;
/* 273 */
                retval[1] = "-s";
/* 274 */
                retval[2] = optSelectedDevice;
/*     */
            }
/*     */
            else
/*     */ {
/* 278 */
                index = 1;
/* 279 */
                retval = new String[index + commandArray.length];
/* 280 */
                retval[0] = adbPath;
/*     */
            }
/* 282 */
            for (String entry : commandArray)
/*     */ {
/* 283 */
                retval[index] = entry;
/* 284 */
                index++;
/*     */
            }
/*     */
        }
/* 287 */
        return retval;
/*     */
    }

    /*     */
/*     */
    static String toString(String[] array)
/*     */ {
/* 291 */
        String retval = "";
/* 292 */
        if (array != null)
/*     */ {
/* 293 */
            String[] arrayOfString = array;
            int j = array.length;
/* 293 */
            for (int i = 0; i < j; i++)
/*     */ {
/* 293 */
                String entry = arrayOfString[i];
/* 294 */
                retval = retval + entry + " ";
/*     */
            }
/*     */
        }
/* 297 */
        if (retval.endsWith(" ")) {
/* 298 */
            retval = retval.substring(0, retval.length() - 1);
/*     */
        }
/* 300 */
        return retval;
/*     */
    }

    /*     */
///*     */   public void setShell(Shell shell)
///*     */   {
///* 304 */     this.shell = shell;
///*     */   }
///*     */
///*     */   public Shell getShell()
///*     */   {
///* 308 */     return this.shell;
///*     */   }
/*     */
/*     */
    static String createLocalTrepnDatafileDirectoryFilepath(String filename)
/*     */ {
/* 312 */
        return TrepnGeneralValues.LOCAL_TREPN_DATAFILE_DIR + filename;
/*     */
    }

    /*     */
/*     */
    public static String removeTrailingSlash(String directory)
/*     */ {
/* 370 */
        String retval = directory;
/* 371 */
        if (retval != null) {
/* 372 */
            while ((retval.length() > 0) && (retval.charAt(retval.length() - 1) == TrepnGeneralValues.SYSTEM_FILE_SEPARATOR.charAt(0))) {
///* 372 */       while ((retval.length() > 0) && (retval.charAt(retval.length() - 1) == '/')) {
/* 373 */
                retval = retval.substring(0, retval.length() - 1);
/*     */
            }
/*     */
        }
/* 376 */
        return retval;
/*     */
    }

    ///*     */   protected Shell shell;
/*     */
/*     */
    public Map<String, String> getAdbDeviceList()
/*     */ {
/*  44 */
        Map<String, String> retval = new HashMap();
/*  45 */
        ExecCommandRetvalInfo retvalInfo = runAdbCommandLineCommand(
/*  46 */       COMMAND_ARRAY_DEVICES, null, "Please add 'adb' (Android Debug Bridge) to your path", true,
/*  47 */       new String[]{"List of devices attached"});
/*  48 */
        if (retvalInfo.hasError()) {
/*  49 */
            return null;
/*     */
        }
/*  51 */
        if ((retvalInfo.getLastNonEmptyOutputLine() == null) ||
/*  52 */       ("List of devices attached".equalsIgnoreCase(retvalInfo.getLastNonEmptyOutputLine()))) {
/*  53 */
            return retval;
/*     */
        }
/*  55 */
        List<String> tempDeviceList = retvalInfo.getOutputLines();
/*  56 */
        for (String device : tempDeviceList)
/*     */ {
/*  57 */
            String temp = device.trim().replace("\t", "   ");
/*  58 */
            String deviceID = temp.split(" ")[0];
/*     */
/*     */
/*     */
/*  62 */
            retvalInfo = runAdbCommandLineCommand(
/*  63 */         COMMAND_ARRAY_DEVICE_MODEL, deviceID, null, true,
/*  64 */         null);
/*  65 */
            if ((!retvalInfo.hasError()) || (
/*  66 */         (retvalInfo.getLastNonEmptyOutputLine() != null) && (!retvalInfo.getLastNonEmptyOutputLine().isEmpty())))
/*     */ {
/*  68 */
                String deviceModel = retvalInfo.getLastNonEmptyOutputLine();
/*  69 */
                if ((deviceModel.contains("msm")) || (deviceModel.contains("apq"))) {
/*  70 */
                    deviceModel = deviceModel.toUpperCase();
/*     */
                }
/*  73 */
                retval.put(deviceID, deviceModel + " (" + deviceID + ")");
/*     */
            }
/*  75 */
            else if (retvalInfo.hasErrorDeviceUnauthorized())
/*     */ {
/*  76 */
                retval.put(deviceID, deviceID + " (device unauthorized)");
/*     */
            }
/*  78 */
            else if (retvalInfo.hasErrorDeviceOffline())
/*     */ {
/*  79 */
                retval.put(deviceID, deviceID + " (device offline)");
/*     */
            }
/*     */
            else
/*     */ {
/*  83 */
                retval.put(deviceID, deviceID);
/*     */
            }
/*     */
        }
/*  87 */
        return retval;
/*     */
    }

    /*     */
/*     */
    public boolean isTrepnCurrentlyRunningOnDevice(SelectedDeviceSupplier supplier)
/*     */ {
/*  92 */
        ExecCommandRetvalInfo retvalInfo = runAdbCommandLineCommand(
/*  93 */       COMMAND_ARRAY_PS_TREPN, supplier.getSelectedDeviceString(), "Please add 'adb' (Android Debug Bridge) to your path", true,
/*  94 */       null);
/*  96 */
        if (retvalInfo.hasError()) {
/*  97 */
            return false;
/*     */
        }
/*  99 */
        List<String> outputLines = retvalInfo.getOutputLines();
/* 100 */
        return outputLines.size() >= 2;
/*     */
    }

    /*     */
/*     */
    protected void displayErrorMessage(ExecCommandRetvalInfo retvalInfo, OnControlActionRunListener optListener)
/*     */ {
/* 114 */
        String errorMessage = "";
/* 115 */
        if (retvalInfo.hasNoSuchFileOrDirectory())
/*     */ {
/* 116 */
            if (!errorMessage.isEmpty()) {
/* 117 */
                errorMessage = errorMessage + "\n";
/*     */
            }
/* 119 */
            errorMessage = errorMessage + "No such file found";
/*     */
        }
/* 121 */
        if (retvalInfo.hasErrorDeviceNotFound())
/*     */ {
/* 122 */
            if (!errorMessage.isEmpty()) {
/* 123 */
                errorMessage = errorMessage + "\n";
/*     */
            }
/* 125 */
            errorMessage = errorMessage + "Unable to connect to device - please check connection";
/*     */
        }
/* 127 */
        if (retvalInfo.hasMoreThanOneDeviceAndEmulator())
/*     */ {
/* 128 */
            if (!errorMessage.isEmpty()) {
/* 129 */
                errorMessage = errorMessage + "\n";
/*     */
            }
/* 131 */
            errorMessage = errorMessage + "More than one device/emulator detected - please connect only one";
/*     */
        }
/* 134 */
        if ((retvalInfo.hasErrorDeviceUnauthorized()) || (retvalInfo.hasErrorDeviceOffline()))
/*     */ {
/* 135 */
            if (!errorMessage.isEmpty()) {
/* 136 */
                errorMessage = errorMessage + "\n";
/*     */
            }
/* 138 */
            errorMessage = errorMessage + "Make sure debugging is allowed on the device and please check connection";
/*     */
        }
/* 141 */
        if (retvalInfo.hasErrorNoSuchFileOrDirectory())
/*     */ {
/* 142 */
            if (!errorMessage.isEmpty()) {
/* 143 */
                errorMessage = errorMessage + "\n";
/*     */
            }
/* 145 */
            errorMessage = errorMessage + "Unable to write to the SD card";
/*     */
        }
/* 149 */
        if ((errorMessage.isEmpty()) && (retvalInfo.hasError()))
/*     */ {
/* 150 */
            String errorLine = retvalInfo.getLastNonEmptyErrorLine();
/* 151 */
            errorMessage = (errorLine != null) && (!errorLine.isEmpty()) ? errorLine : retvalInfo.getLastNonEmptyOutputLine();
/* 153 */
            if ((errorMessage == null) || (errorMessage.isEmpty())) {
/* 154 */
                errorMessage = errorMessage + "Make sure debugging is allowed on the device and please check connection";
/*     */
            }
/*     */
        }
/* 158 */
        if (optListener != null) {
/* 159 */
            optListener.onControlActionRunError(errorMessage, this);
/*     */
        }
/* 161 */
        System.out.println("ERROR: " + errorMessage);
/* 162 */
        showMessage("ERROR", errorMessage);
/*     */
    }

    /*     */
/*     */   ExecCommandRetvalInfo runAdbCommandLineCommand(String[] commandArray)
/*     */ {
        //todo change to false the output all lines
/* 166 */
        return runAdbCommandLineCommand(commandArray, null, "Please add 'adb' (Android Debug Bridge) to your path", true, null);
/*     */
    }

    /*     */
/*     */
    public ExecCommandRetvalInfo runAdbCommandLineCommand(String[] commandArray, String selectedDevice)
/*     */ {
/* 170 */
        return runAdbCommandLineCommand(commandArray, selectedDevice, "Please add 'adb' (Android Debug Bridge) to your path", false, null);
/*     */
    }

    /*     */
/*     */   ExecCommandRetvalInfo runAdbCommandLineCommand(String[] commandArray, String selectedDevice, String optErrorMessage, boolean saveAllOutputLines, String[] outputLinesToSkip)
/*     */ {
/* 175 */
        return helperRunAdbCommand(commandArray, selectedDevice, optErrorMessage, saveAllOutputLines, outputLinesToSkip);
/*     */
    }

    /*     */
/*     */
    private ExecCommandRetvalInfo helperRunAdbCommand(String[] commandArray, String selectedDevice, String optErrorMessage, boolean saveAllOutputLines, String[] outputLinesToSkip)
/*     */ {
/* 183 */
        ExecCommandRetvalInfo retval = new ExecCommandRetvalInfo(-1);
/* 184 */
        BufferedReader stdinReader = null;
/* 185 */
        BufferedReader stderrReader = null;
/*     */
        try
/*     */ {
/* 187 */
            String[] adbCommandStringArray = createAdbCommandStringArray(selectedDevice, commandArray);
            //  System.out.println("CMD:: " + Arrays.asList(adbCommandStringArray).toString());
/* 188 */
            Process cmdProc = Runtime.getRuntime().exec(adbCommandStringArray);
/* 189 */
            stdinReader = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
/* 192 */
            if ((this instanceof PushTrepnPrefFilesAction))
/*     */ {
/* 193 */
                Thread.sleep(500L);
/* 194 */
                cmdProc.destroy();
/* 195 */
                retval.exitValue = 0;
/*     */
            }
/*     */
            else
/*     */ {
/*     */
                String line;
/* 198 */
                while ((line = stdinReader.readLine()) != null)
/*     */ {
/* 199 */
                    line = line.trim();
/* 200 */
                    if ((line != null) && (!line.isEmpty()))
/*     */ {
                        System.out.println("OUT: " + line);
/* 201 */
                        if (line.contains("* daemon started successfully *"))
/*     */ {
/* 202 */
                            System.out.println("Adb had to start daemon - rerun command");
/* 203 */
                            ExecCommandRetvalInfo localExecCommandRetvalInfo1 = helperRunAdbCommand(commandArray, selectedDevice, optErrorMessage, saveAllOutputLines, outputLinesToSkip);
                            return localExecCommandRetvalInfo1;
/*     */
                        }
/* 205 */
                        retval.setLastNonEmptyOutputLine(line);
/* 206 */
                        boolean skipLine = false;
/* 207 */
                        if (saveAllOutputLines) {
/* 208 */
                            if (outputLinesToSkip != null)
/*     */ {
/* 209 */
                                for (String lineToSkip : outputLinesToSkip) {
/* 210 */
                                    if (lineToSkip.equalsIgnoreCase(line))
/*     */ {
/* 211 */
                                        skipLine = true;
/* 212 */
                                        break;
/*     */
                                    }
/*     */
                                }
/* 215 */
                                if (!skipLine) {
/* 216 */
                                    retval.addOutputLine(line);
/*     */
                                }
/*     */
                            }
/*     */
                            else
/*     */ {
/* 220 */
                                retval.addOutputLine(line);
/*     */
                            }
/*     */
                        }
/*     */
                    }
/*     */
                }
/* 225 */
                stderrReader = new BufferedReader(new InputStreamReader(cmdProc.getErrorStream()));
/* 226 */
                while ((line = stderrReader.readLine()) != null)
/*     */ {
/* 227 */
                    System.out.println("STDERR: " + line);
/* 228 */
                    line = line.trim();
/* 229 */
                    if ((line != null) && (!line.isEmpty())) {
/* 230 */
                        retval.setLastNonEmptyErrorLine(line);
/*     */
                    }
/*     */
                }
/* 234 */
                cmdProc.waitFor();
/* 235 */
                retval.exitValue = cmdProc.exitValue();
/*     */
            }
/*     */
        }
/*     */ catch (IOException ioe)
/*     */ {
/* 238 */
            ioe.printStackTrace();
/* 239 */
            if (optErrorMessage != null) {
/* 240 */
                showMessage("ERROR", optErrorMessage);
/*     */
            }
///* 242 */       Activator.getDefault().getLog().log(new Status(4, "com.qualcomm.trepn.ide.eclipse.control", 0, optErrorMessage, ioe));
/* 246 */
            if (stdinReader != null) {
/*     */
                try
/*     */ {
/* 248 */
                    stdinReader.close();
/*     */
                }
/*     */ catch (IOException e)
/*     */ {
/* 250 */
                    e.printStackTrace();
/*     */
                }
/*     */
            }
/* 253 */
            if (stderrReader != null) {
/*     */
                try
/*     */ {
/* 255 */
                    stderrReader.close();
/*     */
                }
/*     */ catch (IOException e)
/*     */ {
/* 257 */
                    e.printStackTrace();
/*     */
                }
/*     */
            }
/*     */
        }
/*     */ catch (InterruptedException ie)
/*     */ {
/* 244 */
            ie.printStackTrace();
/* 246 */
            if (stdinReader != null) {
/*     */
                try
/*     */ {
/* 248 */
                    stdinReader.close();
/*     */
                }
/*     */ catch (IOException e)
/*     */ {
/* 250 */
                    e.printStackTrace();
/*     */
                }
/*     */
            }
/* 253 */
            if (stderrReader != null) {
/*     */
                try
/*     */ {
/* 255 */
                    stderrReader.close();
/*     */
                }
/*     */ catch (IOException e)
/*     */ {
/* 257 */
                    e.printStackTrace();
/*     */
                }
/*     */
            }
/*     */
        }
/*     */ finally
/*     */ {
/* 246 */
            if (stdinReader != null) {
/*     */
                try
/*     */ {
/* 248 */
                    stdinReader.close();
/*     */
                }
/*     */ catch (IOException e)
/*     */ {
/* 250 */
                    e.printStackTrace();
/*     */
                }
/*     */
            }
/* 253 */
            if (stderrReader != null) {
/*     */
                try
/*     */ {
/* 255 */
                    stderrReader.close();
/*     */
                }
/*     */ catch (IOException e)
/*     */ {
/* 257 */
                    e.printStackTrace();
/*     */
                }
/*     */
            }
/*     */
        }
/* 261 */
        return retval;
/*     */
    }

    /*     */
/*     */   void showMessage(final String optTitle, final String message)
/*     */ {
///* 316 */     if ((this.shell != null) && (!this.shell.isDisposed())) {
///* 317 */       Display.getDefault().asyncExec(new Runnable()
///*     */       {
///*     */         public void run()
///*     */         {
///* 320 */           if ((ControlAction.this.shell != null) && (!ControlAction.this.shell.isDisposed())) {
///* 321 */             MessageDialog.openInformation(ControlAction.this.shell, optTitle, message);
///*     */           }
///*     */         }
///*     */       });
///* 326 */     } else
        if (optTitle != null) {
/* 327 */
            System.out.println(optTitle + ": " + message);
/*     */
        } else {
/* 329 */
            System.out.println(message);
/*     */
        }
/*     */
    }

    /*     */
/*     */   String getFilename(String filepath)
/*     */ {
/* 335 */
        if (filepath == null) {
/* 336 */
            return null;
/*     */
        }
///* 338 */     int idx = filepath.lastIndexOf("/");
/* 338 */
        int idx = filepath.lastIndexOf(TrepnGeneralValues.SYSTEM_FILE_SEPARATOR);
/* 339 */
        if (-1 == idx) {
/* 340 */
            return filepath;
/*     */
        }
/* 341 */
        if (filepath.length() == idx + 1) {
/* 342 */
            return "";
/*     */
        }
/* 344 */
        return filepath.substring(idx + 1);
/*     */
    }

    /*     */
/*     */   String normalizePath(String path)
/*     */ {
/* 349 */
        if (path == null) {
/* 350 */
            return null;
/*     */
        }
/* 352 */
        String normalizedPath = path.replace("/cygdrive/c/", "c:/");
/* 353 */
        return normalizedPath;
/*     */
    }

    /*     */
/*     */   void deleteLocalFile(String filepath)
/*     */ {
/* 357 */
        File file = new File(filepath);
/* 358 */
        if (file.exists()) {
/*     */
            try
/*     */ {
/* 360 */
                file.delete();
/*     */
            }
/*     */ catch (SecurityException e)
/*     */ {
/* 362 */
                e.printStackTrace();
///* 363 */         Activator.getDefault().getLog()
///* 364 */           .log(new Status(4, "ControlAction", 0, "Unable to delete file due to permissions issue", e));
/*     */
            }
/*     */
        }
/*     */
    }
/*     */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.actions.ControlAction
 * JD-Core Version:    0.7.0.1
 */