package edu.gmu.cs.aeon.trepn.control.actions;

import edu.gmu.cs.aeon.trepn.common.ControlStatus;
import edu.gmu.cs.aeon.trepn.control.DeviceManager;
import edu.gmu.cs.aeon.trepn.control.OnControlActionRunListener;
import edu.gmu.cs.aeon.trepn.control.OnDatafileChangedListener;
import edu.gmu.cs.aeon.trepn.control.SelectedDeviceSupplier;
import edu.gmu.cs.aeon.trepn.control.util.TrepnInfoHelper;
import edu.gmu.cs.aeon.trepn.core.TrepnFilenameGenerator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*    */
/*    */ public class StopTrepnAction
/*    */ extends ControlAction
/*    */ {
    /* 22 */   private static final String[] COMMAND_ARRAY_STOP_TREPN_PROFILING = {"shell", "am", "broadcast", "-a", "com.quicinc.trepn.stop_profiling"};
    /* 20 */   private boolean DEBUG_PRINT_MSGS = true;
    /*    */   private DeviceManager deviceManager;
    /*    */   private OnDatafileChangedListener datafileChangedListener;
    /*    */   private OnControlActionRunListener controlActionRunListener;
    /*    */   private TrepnFilenameGenerator trepnFilenameGenerator;

    /*    */   // Remove Shell parameter
/*    */
    public StopTrepnAction(DeviceManager deviceManager, OnDatafileChangedListener datafileChangedListener, OnControlActionRunListener controlActionRunListener, TrepnFilenameGenerator trepnFilenameGenerator)
/*    */ {
/* 32 */
        this.deviceManager = deviceManager;
/* 33 */
        this.datafileChangedListener = datafileChangedListener;
/* 34 */
        this.controlActionRunListener = controlActionRunListener;
/* 35 */
        this.trepnFilenameGenerator = trepnFilenameGenerator;
/* 36 */     //setShell(shell);
/*    */
    }

    /*    */
/*    */
    public void run()
/*    */ {
/* 41 */
        final StopTrepnAction parent_this = this;
/*    */     
/* 43 */
        ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.STOPPING);
/* 44 */
        new Thread()
/*    */ {
            /*    */
            public void run()
/*    */ {
/*    */
                try
/*    */ {
/* 48 */
                    Thread.sleep(200L);
/*    */
                }
/*    */ catch (Exception localException) {
                }
/* 51 */
                StopTrepnAction.this.stopAction(parent_this);
/*    */
            }
/*    */
        }.start();
/*    */
    }

    /*    */
/*    */
    private void stopAction(final StopTrepnAction parent_this)
/*    */ {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.execute(new Runnable() {
            /*    */
            public void run()
/*    */ {
/* 61 */
                String selectedDevice = StopTrepnAction.this.deviceManager.getSelectedDeviceString();
/*    */         
/* 63 */
                StopTrepnAction.this.controlActionRunListener.onControlActionRun(parent_this);
/* 64 */
                if (StopTrepnAction.this.DEBUG_PRINT_MSGS) {
/* 65 */
                    System.out.println("Stop profiling");
/*    */
                }
/* 67 */
                ExecCommandRetvalInfo retvalInfo = StopTrepnAction.this.runAdbCommandLineCommand(StopTrepnAction.COMMAND_ARRAY_STOP_TREPN_PROFILING, selectedDevice);
/* 68 */
                if (!retvalInfo.hasError())
/*    */ {
/* 69 */
                    if (StopTrepnAction.this.DEBUG_PRINT_MSGS) {
/* 70 */
                        System.out.println("Profiling has stopped.");
/*    */
                    }
/* 73 */
                    if (!TrepnInfoHelper.isStoppedState(StopTrepnAction.this.deviceManager)) {
/* 74 */
                        TrepnInfoHelper.removeTrepnState(StopTrepnAction.this.deviceManager.getSelectedDeviceString());
/*    */
                    }
/* 76 */
                    //  LogCatOnDeviceFactory.getLogCatOnDeviceController().stopLogCatOnDeviceInBackground(selectedDevice);
/*    */           
/* 78 */
                    ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.PULLING_DATA_FROM_DEVICE);
/* 79 */
                    FileOnDeviceTransfer action = new FileOnDeviceTransfer(StopTrepnAction.this.deviceManager, StopTrepnAction.this.datafileChangedListener,
/* 80 */             StopTrepnAction.this.controlActionRunListener, StopTrepnAction.this.trepnFilenameGenerator, true);
/* 81 */
                    //     action.setShell(parent_this.getShell());
/* 82 */
                    action.run();
/*    */
                }
/*    */
                else
/*    */ {
/* 85 */
                    if (StopTrepnAction.this.DEBUG_PRINT_MSGS) {
/* 86 */
                        System.out.println("Profiling has not stopped");
/*    */
                    }
/* 89 */
                    StopTrepnAction.this.displayErrorMessage(retvalInfo, StopTrepnAction.this.controlActionRunListener);
/* 90 */
                    ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.ACTION_DONE);
/*    */
                }
/*    */
            }
/*    */
        });
        executorService.shutdown();
/*    */
    }
/*    */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.actions.StopTrepnAction
 * JD-Core Version:    0.7.0.1
 */