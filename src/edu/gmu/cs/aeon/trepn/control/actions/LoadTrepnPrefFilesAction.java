package edu.gmu.cs.aeon.trepn.control.actions;

/**
 * Created by DavidIgnacio on 7/21/2014.
 */

public class LoadTrepnPrefFilesAction
        extends ControlAction {
    private String selectedDevice;
    private boolean shouldDisplayErrorMessages;

    public LoadTrepnPrefFilesAction(String selectedDevice, boolean shouldDisplayErrorMessages) {
        this.selectedDevice = selectedDevice;
        this.shouldDisplayErrorMessages = shouldDisplayErrorMessages;
    }

    public void run() {
        ExecCommandRetvalInfo retvalInfo = runAdbCommandLineCommand(
                new String[]{
                        "shell", "am", "broadcast", "-a", "com.quicinc.trepn.load_preferences", "-e",
                        "com.quicinc.trepn.load_preferences_file", "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref"},

                this.selectedDevice);
        if (retvalInfo.exitValue != 0) {
            if (this.shouldDisplayErrorMessages) {
                displayErrorMessage(retvalInfo, null);
            }
        }
    }
}
