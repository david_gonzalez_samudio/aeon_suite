/*    */
package edu.gmu.cs.aeon.trepn.control.actions;
/*    */ 
/*    */

/*    */
/*    */


import edu.gmu.cs.aeon.trepn.common.TrepnGeneralValues;

/*    */
/*    */ public class PullTrepnPrefFilesAction
/*    */ extends ControlAction
/*    */ {
    /*    */   private static final String URI_FILE_PREFIX;

    /*    */
/*    */   static
/*    */ {
/* 24 */
        switch (edu.gmu.cs.aeon.trepn.common.OperatingSystemPlatform.getCurrentPlatform())
/*    */ {
/*    */
            case LINUX:
/* 26 */
                URI_FILE_PREFIX = "file:/";
/* 27 */
                break;
/*    */
            case MAC_OS:
/*    */
            case UNKNOWN:
/*    */
            case WINDOWS:
/*    */
            default:
/* 35 */
                URI_FILE_PREFIX = "file:";
/*    */
        }
/*    */
    }

    /*    */   private String selectedDevice;
    /*    */   private boolean shouldDisplayErrorMessages;
    /*    */   private boolean useWakelock;
    /*    */   private ExecCommandRetvalInfo retvalInfo;

    /*    */
/*    */
    public PullTrepnPrefFilesAction(String selectedDevice, boolean shouldDisplayErrorMessages, boolean useWakelock)
/*    */ {
/* 42 */
        this.selectedDevice = selectedDevice;
/* 43 */
        this.shouldDisplayErrorMessages = shouldDisplayErrorMessages;
/* 44 */
        this.useWakelock = useWakelock;
/*    */
    }

    /*    */
/*    */
    public void run()
/*    */ {
///* 51 */     Bundle bundle = Platform.getBundle("com.qualcomm.trepn.ide.eclipse.control");
/*    */
        String prefDir;
/* 52 */
        if (this.useWakelock) {
/* 53 */
            prefDir = "trepn_eclipse_plugin.pref";
/*    */
        } else {
/* 55 */
            prefDir = "trepn_eclipse_plugin_no_wakelock.pref";
/*    */
        }
///* 58 */     URL fileURL = bundle.getEntry(prefDir);
///*    */     try
///*    */     {
///* 61 */       String fileUriString = FileLocator.toFileURL(fileURL).toString();
        //todo root folder for plugin
//        String fileUriString = "";


///* 63 */       if (fileUriString != null)
///*    */       {
///* 64 */         if (fileUriString.startsWith(URI_FILE_PREFIX)) {
///* 65 */           fileUriString = fileUriString.substring(URI_FILE_PREFIX.length());
///*    */         }
/////* 67 */         while ((fileUriString.endsWith("/")) && (!fileUriString.isEmpty())) {
///* 67 */         while ((fileUriString.endsWith(TrepnGeneralValues.SYSTEM_FILE_SEPARATOR)) && (!fileUriString.isEmpty())) {
///* 68 */           fileUriString = fileUriString.substring(0, fileUriString.length() - 1);
///*    */         }
/* 71 */
        this.retvalInfo = runAdbCommandLineCommand(
/* 72 */           new String[]{"pull", "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref", ControlAction.removeTrailingSlash(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR)},
/* 73 */           this.selectedDevice);
/* 75 */
        if (this.retvalInfo.exitValue != 0) {
/* 79 */
            if (this.shouldDisplayErrorMessages) {
/* 80 */
                displayErrorMessage(this.retvalInfo, null);
/*    */
            }
/*    */
        }
/*    */
    }

    ///*    */     }
///*    */     catch (Exception e)
///*    */     {
///* 85 */       e.printStackTrace();
///*    */     }
///*    */   }
///*    */
/*    */
    public ExecCommandRetvalInfo getRetValInfo()
/*    */ {
/* 92 */
        return this.retvalInfo;
/*    */
    }
/*    */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.actions.PushTrepnPrefFilesAction
 * JD-Core Version:    0.7.0.1
 */