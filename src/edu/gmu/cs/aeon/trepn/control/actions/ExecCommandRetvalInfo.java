package edu.gmu.cs.aeon.trepn.control.actions;

/**
 * Created by DavidIgnacio on 7/14/2014.
 */


import java.util.ArrayList;
import java.util.List;

public class ExecCommandRetvalInfo {
    private static final String OUTPUT_NO_SUCH_FILE_OR_DIRECTORY = "no such file or directory";
    private static final String ERROR_DEVICE_NOT_FOUND = "device not found";
    private static final String ERROR_MORE_THAN_ONE_DEVICE_AND_EMULATOR = "more than one device and emulator";
    private static final String ERROR_DEVICE_UNAUTHORIZED = "device unauthorized";
    private static final String ERROR_DEVICE_OFFLINE = "device offline";
    private static final String ERROR_INSTALL_INSUFFICIENT_STORAGE = "install_failed_insufficient_storage";
    private static final String[][] errorLineLookupTable = {
            {"no such file or directory", "Error: unable to write to the SD card"},
            {"device not found", "Unable to connect to device - please check connection"},
            {"more than one device and emulator", "More than one device/emulator detected - please connect only one"},
            {"device unauthorized", "Make sure debugging is allowed on the device and please check connection"},
            {"device offline", "Make sure debugging is allowed on the device and please check connection"}};
    private static final String[][] outputLineLookupTable = {
            {"no such file or directory", "No such file found"},
            {"install_failed_insufficient_storage", "Error: insufficient device storage to install APK"},
            {"Failure [INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES]", "Install failed due to inconsistent certificates"}};
    private final List<String> outputLines;
    public int exitValue;
    private String lastNonEmptyOutputLine = null;
    private String lastNonEmptyErrorLine = null;

    public ExecCommandRetvalInfo(int initialExitValue) {
        this.exitValue = initialExitValue;
        this.outputLines = new ArrayList();
    }

    public String getLastNonEmptyOutputLine() {
        return this.lastNonEmptyOutputLine;
    }

    public void setLastNonEmptyOutputLine(String lastNonEmptyOutputLine) {
        this.lastNonEmptyOutputLine = lastNonEmptyOutputLine;
    }

    public String getLastNonEmptyErrorLine() {
        return this.lastNonEmptyErrorLine;
    }

    public void setLastNonEmptyErrorLine(String lastNonEmptyErrorLine) {
        this.lastNonEmptyErrorLine = lastNonEmptyErrorLine;
    }

    public void addOutputLine(String outputLine) {
        this.outputLines.add(outputLine);
    }

    public List<String> getOutputLines() {
        return this.outputLines;
    }

    public boolean hasError() {
        return (this.exitValue != 0) ||
                (hasErrorResponse());
    }

    public boolean hasNoSuchFileOrDirectory() {
        if (getLastNonEmptyOutputLine() == null) {
            return false;
        }
        return getLastNonEmptyOutputLine().toLowerCase().contains("no such file or directory");
    }

    public boolean hasErrorNoSuchFileOrDirectory() {
        if (getLastNonEmptyErrorLine() == null) {
            return false;
        }
        return getLastNonEmptyErrorLine().toLowerCase().contains("no such file or directory");
    }

    public boolean hasErrorDeviceNotFound() {
        if (getLastNonEmptyErrorLine() == null) {
            return false;
        }
        return getLastNonEmptyErrorLine().toLowerCase().contains("device not found");
    }

    public boolean hasMoreThanOneDeviceAndEmulator() {
        if (getLastNonEmptyErrorLine() == null) {
            return false;
        }
        return getLastNonEmptyErrorLine().toLowerCase().contains("more than one device and emulator");
    }

    public boolean hasErrorDeviceUnauthorized() {
        if (getLastNonEmptyErrorLine() == null) {
            return false;
        }
        return getLastNonEmptyErrorLine().toLowerCase().contains("device unauthorized");
    }

    public boolean hasErrorDeviceOffline() {
        if (getLastNonEmptyErrorLine() == null) {
            return false;
        }
        return getLastNonEmptyErrorLine().toLowerCase().contains("device offline");
    }

    public boolean hasErrorInsufficientStorageForInstall() {
        if (getLastNonEmptyOutputLine() == null) {
            return false;
        }
        return getLastNonEmptyOutputLine().toLowerCase().contains("install_failed_insufficient_storage");
    }

    public boolean hasErrorResponse() {
        boolean result = false;

        String errorLine = getLastNonEmptyErrorLine();
        if ((errorLine != null) && (!errorLine.isEmpty())) {
            errorLine = errorLine.toLowerCase();
            for (String[] error : errorLineLookupTable) {
                if (errorLine.contains(error[0])) {
                    return true;
                }
            }
            if ((errorLine.contains("error")) || (errorLine.contains("failure"))) {
                return true;
            }
        }
        String outputLine = getLastNonEmptyOutputLine();
        if ((outputLine != null) && (!outputLine.isEmpty())) {
            outputLine = outputLine.toLowerCase();
            for (String[] error : outputLineLookupTable) {
                if (outputLine.contains(error[0])) {
                    return true;
                }
            }
            if ((outputLine.contains("error")) || (outputLine.contains("failure"))) {
                return true;
            }
        }
        return result;
    }

    public String getErrorResponseStatusMessage() {
        String errorMessage = "";

        String errorLine = getLastNonEmptyErrorLine();
        if ((errorLine != null) && (!errorLine.isEmpty())) {
            errorLine = errorLine.toLowerCase();
            for (String[] error : errorLineLookupTable) {
                if (errorLine.contains(error[0])) {
                    if (!errorMessage.isEmpty()) {
                        errorMessage = errorMessage + "\n";
                    }
                    errorMessage = errorMessage + error[1];
                }
            }
            if ((errorMessage.isEmpty()) && ((errorLine.contains("error")) || (errorLine.contains("failure")))) {
                errorMessage = getLastNonEmptyErrorLine();
            }
        }
        String outputLine = getLastNonEmptyOutputLine();
        if ((outputLine != null) && (!outputLine.isEmpty())) {
            outputLine = outputLine.toLowerCase();
            for (String[] error : outputLineLookupTable) {
                if (outputLine.contains(error[0])) {
                    if (!errorMessage.isEmpty()) {
                        errorMessage = errorMessage + "\n";
                    }
                    errorMessage = errorMessage + error[1];
                }
            }
            if ((errorMessage.isEmpty()) && ((outputLine.contains("error")) || (outputLine.contains("failure")))) {
                errorMessage = getLastNonEmptyOutputLine();
            }
        }
        if ((errorMessage.isEmpty()) && (hasError())) {
            errorMessage = (errorLine != null) && (!errorLine.isEmpty()) ? errorLine : getLastNonEmptyOutputLine();
            if ((errorMessage == null) || (errorMessage.isEmpty())) {
                errorMessage = errorMessage + "Make sure debugging is allowed on the device and please check connection";
            }
        }
        return errorMessage;
    }
}
