package edu.gmu.cs.aeon.trepn.control;

/**
 * Created by DavidIgnacio on 7/14/2014.
 */
public  interface SelectedDeviceSupplier {
    String getSelectedDeviceString();
}
