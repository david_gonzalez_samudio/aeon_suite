package edu.gmu.cs.aeon.trepn.control.stubs;

import edu.gmu.cs.aeon.trepn.common.ControlStatus;
import edu.gmu.cs.aeon.trepn.common.OnStatusUpdateListener;
import edu.gmu.cs.aeon.trepn.control.OnControlActionRunListener;
import edu.gmu.cs.aeon.trepn.control.OnDatafileChangedListener;
import edu.gmu.cs.aeon.trepn.control.actions.ControlAction;

/**
 * Created by DavidIgnacio on 7/20/2014.
 */
public class ControlListener implements OnControlActionRunListener, OnStatusUpdateListener, OnDatafileChangedListener {
    @Override
    public void onControlActionRun(ControlAction paramControlAction) {
        System.out.println("onControlActionRun " + paramControlAction.getAdbDeviceList());
    }

    @Override
    public void onControlActionRunError(String paramString, ControlAction paramControlAction) {

        System.out.println("onControlActionRun Error " + paramString);
    }

    @Override
    public void onStatusUpdate(ControlStatus.ControlState paramControlState, String paramString) {
        System.out.println("onStatusUpdate " + paramString);
    }

    @Override
    public void onDatafileChanged(String paramString) {
        System.out.println("onDatafileChanged " + paramString);
    }
}
