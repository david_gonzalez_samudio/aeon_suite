package edu.gmu.cs.aeon.trepn.control;

import edu.gmu.cs.aeon.trepn.control.actions.GetDeviceSupplierAction;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by DavidIgnacio on 7/20/2014.
 */
public class DeviceManager  {


    public static String getSelectedDeviceString() {
        String deviceString = "NONE";
        GetDeviceSupplierAction controlAction = new GetDeviceSupplierAction();
        if (controlAction == null)
            return null;
        Map<String, String> AdbDeviceList = controlAction.getAdbDeviceList();
        if (AdbDeviceList == null) return null;
        Iterator<String> list = AdbDeviceList.keySet().iterator();
        if (list.hasNext())
            deviceString = list.next();

        System.out.println("DEVICE ID >>>>>>>>>>>>>>>>>" + deviceString);
        return deviceString;
//        return "05853466";
    }
}
