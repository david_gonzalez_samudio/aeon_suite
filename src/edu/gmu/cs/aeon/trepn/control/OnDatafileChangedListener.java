package edu.gmu.cs.aeon.trepn.control;

/**
 * Created by DavidIgnacio on 7/14/2014.
 */
public abstract interface OnDatafileChangedListener {
    public abstract void onDatafileChanged(String paramString);
}
