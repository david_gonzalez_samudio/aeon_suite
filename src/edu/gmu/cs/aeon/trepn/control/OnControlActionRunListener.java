package edu.gmu.cs.aeon.trepn.control;

import edu.gmu.cs.aeon.trepn.control.actions.ControlAction;

public  interface OnControlActionRunListener {
    void onControlActionRun(ControlAction paramControlAction);

    void onControlActionRunError(String paramString, ControlAction paramControlAction);
}

