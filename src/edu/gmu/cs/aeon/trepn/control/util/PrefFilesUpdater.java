package edu.gmu.cs.aeon.trepn.control.util;

/**
 * Created by DavidIgnacio on 7/20/2014.
 */
//package com.qualcomm.trepn.ide.eclipse.control.actions;

import edu.gmu.cs.aeon.trepn.common.LocalTrepnDirectoryCreator;
import edu.gmu.cs.aeon.trepn.common.Settings;
import edu.gmu.cs.aeon.trepn.common.TrepnGeneralValues;
import edu.gmu.cs.aeon.trepn.control.TrepnCommands;
import edu.gmu.cs.aeon.trepn.control.actions.ControlAction;
import edu.gmu.cs.aeon.trepn.control.actions.ExecCommandRetvalInfo;

import java.io.*;
import java.util.*;

//        import edu.gmu.cs.edu.gmu.cs.aeon.trepn.common.StatType;

public class PrefFilesUpdater {
    private static final String ERROR_DATAPOINTS_IO_ERROR = "A problem occurred while creating the local datapoints file: " +
            TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.preferences.saved_data_points.xml";
    private static final String ERROR_PREFS_IO_ERROR = "A problem occurred while creating the local preferences file: " +
            TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.trepn_preferences.xml";
    private static final String ERROR_TREPN_FAILED_TO_LOAD_PREFS = "An error occurred when configuring Trepn";
    private static final String ERROR_TREPN_FAILED_TO_WRITE_TO_SDCARD = "An error occurred when attempting to write to the SD card.  Make sure an SD card is present and not full.";
    private static final String PREFS_FILE = "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref/com.quicinc.trepn_preferences.xml";
    private static final String DATAPOINTS_FILE = "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref/com.quicinc.preferences.saved_data_points.xml";
    private static final String PREFS_FILES = "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref/*preference*.xml";
    private static final String URI_FILE_PREFIX;
    private static final boolean DEBUG_ENABLED = true;

    static {
        switch (edu.gmu.cs.aeon.trepn.common.OperatingSystemPlatform.getCurrentPlatform()) {
            case LINUX:
                URI_FILE_PREFIX = "file:/";
                break;
            case MAC_OS:
            case UNKNOWN:
            case WINDOWS:
            default:
                URI_FILE_PREFIX = "file:";
        }
    }

    static void removePrefFilesOnLocalFS() {
        File file = new File(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.trepn_preferences.xml");
        if ((file != null) && (file.exists())) {
            file.delete();
        }
        file = new File(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.preferences.saved_data_points.xml");
        if ((file != null) && (file.exists())) {
            file.delete();
        }
    }

    public static ExecCommandRetvalInfo update(String device, boolean useWakelock, ControlAction controlAction) {
        int dataPointsUpdate = 0;
        int prefUpdate = 0;

        removePrefFilesOnLocalFS();
        LocalTrepnDirectoryCreator.createLocalTrepnPrefsDirectory();


        ExecCommandRetvalInfo retvalInfo = controlAction.runAdbCommandLineCommand(
                new String[]{"pull", "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref/com.quicinc.trepn_preferences.xml",
                        ControlAction.removeTrailingSlash(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR)},
                device);
//        System.out.println(ControlAction.removeTrailingSlash(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR)+ "   OUT:::: "+ Arrays.toString(retvalInfo.getOutputLines().toArray()));
        if (false && ((retvalInfo.exitValue == 0) || ((retvalInfo.exitValue != 0) && (retvalInfo.getLastNonEmptyErrorLine().contains("does not exist"))))) {
            dataPointsUpdate = checkDataPointsUpdateNeeded();
            if (dataPointsUpdate == -1) {
                retvalInfo.setLastNonEmptyErrorLine(ERROR_DATAPOINTS_IO_ERROR);
                return retvalInfo;
            }
            prefUpdate = checkPrefFileUpdateNeeded(useWakelock);
            if (prefUpdate == -1) {
                retvalInfo.setLastNonEmptyErrorLine(ERROR_PREFS_IO_ERROR);
                return retvalInfo;
            }
            if ((dataPointsUpdate > 0) || (prefUpdate > 0)) {
                retvalInfo = controlAction.runAdbCommandLineCommand(
                        new String[]{"push", ControlAction.removeTrailingSlash(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR),
                                "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref"},
                        device);
                if (retvalInfo.exitValue == 0) {
                    retvalInfo = TrepnCommands.startCommand(TrepnCommands.Action.LOAD_PREFS, null, device, controlAction, false);
                    if (retvalInfo.hasError()) {
                        retvalInfo.setLastNonEmptyErrorLine("An error occurred when configuring Trepn");
                    }
                } else if (retvalInfo.hasError()) {
                    retvalInfo.setLastNonEmptyErrorLine("An error occurred when attempting to write to the SD card.  Make sure an SD card is present and not full.");
                }
            }
        }
        return retvalInfo;
    }

    static int checkDataPointsUpdateNeeded() {
        int result = 0;
        boolean createDatapointsFile = true;
        List<String> existingFile = new ArrayList();
        File dpFile = new File(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.preferences.saved_data_points.xml");
        if ((dpFile != null) && (dpFile.isFile()) && (dpFile.canRead())) {
//            ArrayList<StatType> selectedDatapoints = Settings.getEnabledDatapoints(true);
            Map<String, String> dataPoints = new HashMap();
            Iterator localIterator2;
//            for (Iterator localIterator1 = selectedDatapoints.iterator(); localIterator1.hasNext(); localIterator2.hasNext())
//            {
//                StatType datapoint = (StatType)localIterator1.next();
//                List<Integer> sensorIDs = datapoint.getSensorIDs();
//                localIterator2 = sensorIDs.iterator(); continue;Integer sensorID = (Integer)localIterator2.next();
//                dataPoints.put(sensorID.toString(), sensorID.toString());
//            }
            Object br;
            try {
                FileInputStream fis = new FileInputStream(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR +
                        "com.quicinc.preferences.saved_data_points.xml");
                br = new BufferedReader(new InputStreamReader(fis));
                String readLine;
                while ((readLine = ((BufferedReader) br).readLine()) != null) {

                    existingFile.add(readLine);
                }
                ((BufferedReader) br).close();
                fis.close();
            } catch (IOException ioe) {
                System.out.println("Data points file check: IOException " + ioe.toString());


                existingFile.clear();
            }
            if ((existingFile != null) && (!existingFile.isEmpty())) {
                for (br = existingFile.iterator(); ((Iterator) br).hasNext(); ) {
                    String line = (String) ((Iterator) br).next();
                    if (line.contains("name=")) {
                        int startIndex = line.indexOf("\"");
                        int endIndex = line.indexOf("\"", startIndex + 1);
                        String key = line.substring(startIndex + 1, endIndex);
                        if (dataPoints.containsKey(key)) {
                            dataPoints.remove(key);
                        }
                    }
                }
                if (dataPoints.size() == 0) {
                    createDatapointsFile = false;
                }
            }
        }
        if (createDatapointsFile) {
            result = createDataPointsFile() ? 1 : -1;
        }
        return result;
    }

    private static boolean createDataPointsFile() {
        boolean result = false;
        try {
            File file = new File(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.preferences.saved_data_points.xml");
            FileOutputStream fos = new FileOutputStream(file);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));


//            ArrayList<StatType> selectedDatapoints = Settings.getEnabledDatapoints(true);

            addLine(bw, "<?xml version='1.0' encoding='utf-8' standalone='yes' ?>");
            addLine(bw,
                    "<!-- Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.  Qualcomm Technologies Proprietary and Confidential.  -->");
            addLine(bw, "<map>");
            Iterator localIterator2;
//            for (Iterator localIterator1 = selectedDatapoints.iterator(); localIterator1.hasNext(); localIterator2.hasNext())
//            {
//                StatType datapoint = (StatType)localIterator1.next();
//                List<Integer> sensorIDs = datapoint.getSensorIDs();
//                localIterator2 = sensorIDs.iterator(); continue;Integer sensorID = (Integer)localIterator2.next();
//                addLine(bw, "<int name=\"" + sensorID.toString() + "\" value=\"" + sensorID.toString() + "\" />");
//            }
            addLine(bw, "</map>");
            bw.flush();
            bw.close();
            fos.close();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();

            File file = new File(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.preferences.saved_data_points.xml");
            if ((file != null) && (file.exists())) {
                file.delete();
            }
        }
        return result;
    }

    private static int checkPrefFileUpdateNeeded(boolean useWakeLock) {
        int result = 0;
        List<String> file = new ArrayList();
        List<String> output = new ArrayList();
        File dpFile = new File(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.trepn_preferences.xml");
        BufferedReader br;
        if ((dpFile != null) && (dpFile.isFile()) && (dpFile.canRead())) {
            try {
                FileInputStream fis = new FileInputStream(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.trepn_preferences.xml");
                br = new BufferedReader(new InputStreamReader(fis));
                String readLine;
                while ((readLine = br.readLine()) != null) {

                    file.add(readLine);
                }
                br.close();
                fis.close();
            } catch (IOException e) {
                System.err.println("checkPrefFileUpdateNeeded: I/O exception, " + e.toString());
                e.printStackTrace();


                file.clear();
            }
        }
        if ((file != null) && (!file.isEmpty())) {
            for (String line : file) {
                if ((!line.contains("<?xml ")) && (!line.contains("<map>")) && (!line.contains("</map>")) &&
                        (!line.contains("<!--")) && (!line.contains("show_deltas")) && (!line.contains("show_app_stats")) &&
                        (!line.contains("disabled_overlay_mode_prompt")) && (!line.contains("acquire_profiling_wakelock")) &&
                        (!line.contains("profiling_interval")) && (!line.contains("eula_accepted_v1"))) {
                    output.add(line);
                }
            }
        }
        try {
            File updatedFile = new File(TrepnGeneralValues.LOCAL_TREPN_PREFS_DIR + "com.quicinc.trepn_preferences.xml");
            FileOutputStream fos = new FileOutputStream(updatedFile);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            addLine(bw, "<?xml version='1.0' encoding='utf-8' standalone='yes' ?>");
            addLine(bw,
                    "<!-- Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved. Qualcomm Technologies Proprietary and Confidential. -->");
            addLine(bw, "<map>");
            addLine(bw, "<boolean name=\"com.quicinc.preferences.general.show_deltas\" value=\"false\" />");
            addLine(bw, "<boolean name=\"com.quicinc.preferences.disabled_overlay_mode_prompt\" value=\"true\" />");
            addLine(bw, "<boolean name=\"com.quicinc.preferences.eula_accepted_v1\" value=\"true\" />");
            addLine(bw,
                    "<boolean name=\"com.quicinc.preferences.general.acquire_profiling_wakelock\" value=\"" +
                            Boolean.toString(useWakeLock) +
                            "\" />");
            addLine(bw, "<int name=\"com.quicinc.preferences.general.profiling_interval\" value=\"" +
                    Settings.getGeneralSettingInt("PROFILE_INTERVAL", 100) + "\" />");

            addLine(bw, "<boolean name=\"com.quicinc.preferences.general.show_app_stats\" value=\"true\" />");
//            addLine(bw, "<boolean name=\"com.quicinc.preferences.general.show_app_stats\" value=\"" + (
//                    Settings.getDatapointValue(StatType.MOBILE_DATA_USAGE_PER_APP) ? "true" : "false") + "\" />");
            if ((output == null) || (output.isEmpty())) {
                addLine(bw, "<int name=\"com.quicinc.preferences.general.averaging_interval\" value=\"1\" />");
                addLine(bw, "<boolean name=\"com.quicinc.preferences.general.show_frequency\" value=\"true\" />");
                addLine(bw, "<string name=\"com.quicinc.preferences.general.storage\">Main Memory</string>");
                addLine(bw, "<boolean name=\"com.quicinc.preferences.general.help_dialogs\" value=\"false\" />");
                addLine(bw, "<int name=\"com.quicinc.preferences.general.baselining_interval\" value=\"1\" />");
                addLine(bw, "<boolean name=\"com.quicinc.preferences.saved_first_run\" value=\"true\" />");
            } else {
                for (String line : output) {
                    addLine(bw, line);
                }
            }
            addLine(bw, "</map>");
            bw.flush();
            bw.close();
            fos.close();
            result = 1;
        } catch (IOException e) {
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    private static int addLine(BufferedWriter bw, String line) {
        int result = 1;
        try {
            bw.write(line);
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
            result = -1;
        }
        return result;
    }
}
