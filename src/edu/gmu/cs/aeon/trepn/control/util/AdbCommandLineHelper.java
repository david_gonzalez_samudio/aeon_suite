package edu.gmu.cs.aeon.trepn.control.util;

/**
 * Created by DavidIgnacio on 7/14/2014.
 */

import edu.gmu.cs.aeon.trepn.common.AdbPathGenerator;
import edu.gmu.cs.aeon.trepn.control.actions.ExecCommandRetvalInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class AdbCommandLineHelper {
    public static final String OUTPUT_FROM_ADB_DAEMON_START_SUCCESSFULLY = "* daemon started successfully *";
    public static final String ERROR_MESSAGE_NEED_TO_ADD_ADB_TO_PATH = "Please add 'adb' (Android Debug Bridge) to your path";

    public static ExecCommandRetvalInfo runAdbCommandLineCommand(String[] commandArray) {
        return runAdbCommandLineCommand(commandArray, null, "Please add 'adb' (Android Debug Bridge) to your path", false, null);
    }

    public static ExecCommandRetvalInfo runAdbCommandLineCommand(String[] commandArray, String selectedDevice) {
        return runAdbCommandLineCommand(commandArray, selectedDevice, "Please add 'adb' (Android Debug Bridge) to your path", false, null);
    }

    public static ExecCommandRetvalInfo runAdbCommandLineCommand(String[] commandArray, String selectedDevice, String optErrorMessage, boolean saveAllOutputLines, String[] outputLinesToSkip) {
        return helperRunAdbCommand(commandArray, selectedDevice, optErrorMessage, saveAllOutputLines, outputLinesToSkip);
    }

    private static ExecCommandRetvalInfo helperRunAdbCommand(String[] commandArray, String selectedDevice, String optErrorMessage, boolean saveAllOutputLines, String[] outputLinesToSkip) {
        ExecCommandRetvalInfo retval = new ExecCommandRetvalInfo(-1);
        BufferedReader stdinReader = null;
        BufferedReader stderrReader = null;
        try {
            String[] adbCommandStringArray = createAdbCommandStringArray(selectedDevice, commandArray);
            Process cmdProc = Runtime.getRuntime().exec(adbCommandStringArray);
            stdinReader = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
            String line = "";
            while ((line = stdinReader.readLine()) != null) {
                line = line.trim();
                if ((line != null) && (!line.isEmpty())) {
                    if (line.contains("* daemon started successfully *")) {
                        System.out.println("Adb had to start daemon - rerun command");
                        ExecCommandRetvalInfo localExecCommandRetvalInfo1 = helperRunAdbCommand(commandArray, selectedDevice, optErrorMessage, saveAllOutputLines, outputLinesToSkip);
                        return localExecCommandRetvalInfo1;
                    }
                    retval.setLastNonEmptyOutputLine(line);
                    boolean skipLine = false;
                    if (saveAllOutputLines) {
                        if (outputLinesToSkip != null) {
                            for (String lineToSkip : outputLinesToSkip) {
                                if (lineToSkip.equalsIgnoreCase(line)) {
                                    skipLine = true;
                                    break;
                                }
                            }
                            if (!skipLine) {
                                retval.addOutputLine(line);
                            }
                        } else {
                            retval.addOutputLine(line);
                        }
                    }
                }
            }
            stderrReader = new BufferedReader(new InputStreamReader(cmdProc.getErrorStream()));
            while ((line = stderrReader.readLine()) != null) {
                System.out.println("STDERR: " + line);
                line = line.trim();
                if ((line != null) && (!line.isEmpty())) {
                    retval.setLastNonEmptyErrorLine(line);
                }
            }
            cmdProc.waitFor();
            retval.exitValue = cmdProc.exitValue();
        } catch (IOException ioe) {
            ioe.printStackTrace();


//            Activator.getDefault().getLog().log(new Status(4, "com.qualcomm.trepn.ide.eclipse.control", 0, optErrorMessage, ioe));
            if (stdinReader != null) {
                try {
                    stdinReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (stderrReader != null) {
                try {
                    stderrReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException ie) {
            ie.printStackTrace();
            if (stdinReader != null) {
                try {
                    stdinReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (stderrReader != null) {
                try {
                    stderrReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            if (stdinReader != null) {
                try {
                    stdinReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (stderrReader != null) {
                try {
                    stderrReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    static String[] createAdbCommandStringArray(String optSelectedDevice, String[] commandArray) {
        String adbPath = AdbPathGenerator.getAdbPath();
        String[] retval = {adbPath};
        if (commandArray != null) {
            int index = 0;
            if ((optSelectedDevice != null) && (!optSelectedDevice.isEmpty())) {
                index = 3;
                retval = new String[index + commandArray.length];
                retval[0] = adbPath;
                retval[1] = "-s";
                retval[2] = optSelectedDevice;
            } else {
                index = 1;
                retval = new String[index + commandArray.length];
                retval[0] = adbPath;
            }
            for (String entry : commandArray) {
                retval[index] = entry;
                index++;
            }
        }
        return retval;
    }
}
