package edu.gmu.cs.aeon.trepn.control.util;

import edu.gmu.cs.aeon.trepn.control.DeviceManager;
import edu.gmu.cs.aeon.trepn.control.SelectedDeviceSupplier;
import edu.gmu.cs.aeon.trepn.control.actions.ExecCommandRetvalInfo;

import java.util.List;

/*     */

/*     */
/*     */ public class TrepnInfoHelper
/*     */ {
    /*     */   private static final String ON_DEVICE_TREPN_STATE_FILEPATH = "/mnt/sdcard/trepn/trepn_state";
    /*  15 */   private static final String[] COMMAND_ARRAY_CAT_TREPN_STATE = {"shell", "cat", "/mnt/sdcard/trepn/trepn_state"};
    /*  16 */   private static final String[] COMMAND_ARRAY_REMOVE_TREPN_STATE = {"shell", "rm", "/mnt/sdcard/trepn/trepn_state"};
    /*     */   private static final String ERROR_MESSAGE_NEED_TO_ADD_ADB_TO_PATH = "Please add 'adb' (Android Debug Bridge) to your path";
    /*     */   private static final String PROFILING_STATE = "Profiling";
    /*     */   private static final String TREPN_STATE_VERSION = "Version:";
    /*     */   private static final String TREPN_STATE_STATE = "State:";
    /*     */   private static final String TREPN_STATE_SENSORS = "Sensors:";
    /*  23 */   private static String trepnVersion = "";
    /*  24 */   private static String supportedSensorIDs = "";
    /*  25 */   private static String trepnState = "";

    /*     */
/*     */
    public static ExecCommandRetvalInfo removeTrepnState(String device)
/*     */ {
/*  32 */
        ExecCommandRetvalInfo retvalInfo = AdbCommandLineHelper.runAdbCommandLineCommand(COMMAND_ARRAY_REMOVE_TREPN_STATE,
/*  33 */       device, 
/*  34 */       null, 
/*  35 */       true, 
/*  36 */       null);
/*     */     
/*  38 */
        return retvalInfo;
/*     */
    }

    /*     */
/*     */
    public static boolean isStoppedState(DeviceManager supplier)
/*     */ {
/*  42 */
        boolean result = true;
/*  43 */
        String trepnState = getTrepnState(supplier);
/*  45 */
        if ((trepnState != null) && (trepnState.matches("Profiling"))) {
/*  46 */
            result = false;
/*     */
        }
/*  48 */
        return result;
/*     */
    }

    /*     */
/*     */
    public static String getTrepnState(DeviceManager supplier)
/*     */ {
/*  54 */
        ExecCommandRetvalInfo retvalInfo = AdbCommandLineHelper.runAdbCommandLineCommand(COMMAND_ARRAY_CAT_TREPN_STATE,
/*  55 */       supplier.getSelectedDeviceString(), 
/*  56 */       "Please add 'adb' (Android Debug Bridge) to your path", 
/*  57 */       true, 
/*  58 */       null);
/*  60 */
        if (retvalInfo.hasError()) {
/*  61 */
            return null;
/*     */
        }
/*  64 */
        trepnState = "";
/*  65 */
        trepnVersion = "";
/*  66 */
        supportedSensorIDs = "";
/*     */     
/*  68 */
        List<String> outputLines = retvalInfo.getOutputLines();
/*  69 */
        for (String line : outputLines) {
/*  70 */
            if (line.startsWith("State:"))
/*     */ {
/*  71 */
                line.trim();
/*  72 */
                String[] parts = line.split("\\s");
/*  73 */
                if ((parts != null) && (parts.length == 2)) {
/*  74 */
                    trepnState = parts[1];
/*     */
                }
/*     */
            }
/*  78 */
            else if (line.startsWith("Version:"))
/*     */ {
/*  79 */
                line.trim();
/*     */         
/*     */ 
/*  82 */
                String[] parts = line.split("\\s");
/*  84 */
                if ((parts != null) && (parts.length >= 2))
/*     */ {
/*  85 */
                    trepnVersion = parts[1];
/*  88 */
                    if ((trepnVersion.contains("[")) || (trepnVersion.contains("]")))
/*     */ {
/*  89 */
                        int startIndex = trepnVersion.indexOf("[");
/*  90 */
                        int endIndex = trepnVersion.indexOf("]");
/*  91 */
                        String toReplace = trepnVersion.substring(startIndex, endIndex + 1);
/*  92 */
                        trepnVersion = trepnVersion.replace(toReplace, "");
/*     */
                    }
/*     */
                }
/*     */
            }
/*  98 */
            else if (line.startsWith("Sensors:"))
/*     */ {
/*  99 */
                line.trim();
/* 100 */
                String[] versionParts = line.split(":\\s");
/* 101 */
                if ((versionParts != null) && (versionParts.length == 2)) {
/* 102 */
                    supportedSensorIDs = versionParts[1];
/*     */
                }
/*     */
            }
/*     */
        }
/* 109 */  //   Settings.setSensorIDs(supportedSensorIDs);
/* 110 */
        return trepnState;
/*     */
    }

    /*     */
/*     */
    public static String getVersion()
/*     */ {
/* 114 */
        return trepnVersion;
/*     */
    }

    /*     */
/*     */
    public static String getSensorIDs()
/*     */ {
/* 118 */
        return supportedSensorIDs;
/*     */
    }

    /*     */
/*     */
    public static void clearInfo()
/*     */ {
/* 122 */
        trepnState = "";
/* 123 */
        trepnVersion = "";
/*     */     
/* 125 */
        supportedSensorIDs = "";
/*     */     
/* 127 */    // Settings.setSensorIDs(supportedSensorIDs);
/*     */
    }
/*     */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.util.TrepnInfoHelper
 * JD-Core Version:    0.7.0.1
 */