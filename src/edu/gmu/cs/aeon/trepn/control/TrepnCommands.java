package edu.gmu.cs.aeon.trepn.control;
/*     */ 
/*     */

import edu.gmu.cs.aeon.trepn.common.ControlStatus;
import edu.gmu.cs.aeon.trepn.common.ControlStatus.ControlState;
import edu.gmu.cs.aeon.trepn.control.actions.ControlAction;
import edu.gmu.cs.aeon.trepn.control.actions.ExecCommandRetvalInfo;

/*     */
/*     */ 
/*     */ public class TrepnCommands
/*     */ {
    /*  14 */   private static final String[] COMMAND_ARRAY_GET_ANDROID_VERSION = {"shell", "getprop",
/*  15 */     "ro.build.version.sdk"};
    /*  16 */   private static final String[] COMMAND_ARRAY_LEGACY_START_TREPN_APP = {"shell", "am", "startservice",
/*  17 */     "-n", "com.quicinc.trepn/.TrepnService"};
    /*  16 */   private static final String[] COMMAND_ARRAY_LEGACY_STOP_TREPN_APP = {"shell", "am", "stopservice",
/*  17 */     "-n", "com.quicinc.trepn/.TrepnService"};
    /*  18 */   private static final String[] COMMAND_ARRAY_START_TREPN_APP = {"shell", "am", "startservice", "--user",
/*  19 */     "0", "-n", "com.quicinc.trepn/.TrepnService"};
    /*  18 */   private static final String[] COMMAND_ARRAY_STOP_TREPN_APP = {"shell", "am", "stopservice", "--user",
/*  19 */     "0", "-n", "com.quicinc.trepn/.TrepnService"};
    /*  20 */   private static final String[] COMMAND_ARRAY_CHECK_ANDROID_BOOTED = {"shell", "getprop", "dev.bootcomplete"};
    /*     */   private static final String ANDROID_BOOTED = "1";
    /*     */   private static final int ICS = 15;
    /*     */   private static final int RETRY_MAX = 3;
    /*     */   private static int retry;

    /*     */
/*     */

    private static ExecCommandRetvalInfo startServiceCmd(String device, ControlAction controlAction)
/*     */ {
/*  73 */
        String[] startTrepnString = COMMAND_ARRAY_START_TREPN_APP;
/*     */
/*     */
/*  76 */
        ExecCommandRetvalInfo retvalInfo = controlAction.runAdbCommandLineCommand(COMMAND_ARRAY_GET_ANDROID_VERSION, device);
/*  79 */
        if (!retvalInfo.hasError())
/*     */ {
/*  80 */
            if (Integer.parseInt(retvalInfo.getLastNonEmptyOutputLine()) <= 15) {
/*  81 */
                startTrepnString = COMMAND_ARRAY_LEGACY_START_TREPN_APP;
/*     */
            }
/*  83 */
            retvalInfo = controlAction.runAdbCommandLineCommand(startTrepnString, device);
/*  84 */
            if (!retvalInfo.hasError()) {
/*  85 */
                ControlStatus.getCurrent().updateStatus(ControlStatus.ControlState.READY);
/*     */
            }
/*     */
        }
/*     */
        else
/*     */ {
/*  89 */
            retry += 1;
/*  92 */
            if (retry < 3) {
/*  93 */
                retvalInfo = startServiceCmd(device, controlAction);
/*     */
            }
/*     */
        }
/*  97 */
        return retvalInfo;
/*     */
    }

    /*     */
    /*     */
    private static ExecCommandRetvalInfo stopServiceCmd(String device, ControlAction controlAction)
/*     */ {
/*  73 */
        String[] stopTrepnString = COMMAND_ARRAY_STOP_TREPN_APP;
/*     */
/*     */
/*  76 */
        ExecCommandRetvalInfo retvalInfo = controlAction.runAdbCommandLineCommand(COMMAND_ARRAY_GET_ANDROID_VERSION, device);
/*  79 */
        if (!retvalInfo.hasError())
/*     */ {
/*  80 */
            if (Integer.parseInt(retvalInfo.getLastNonEmptyOutputLine()) <= 15) {
/*  81 */
                stopTrepnString = COMMAND_ARRAY_LEGACY_STOP_TREPN_APP;
/*     */
            }
/*  83 */
            retvalInfo = controlAction.runAdbCommandLineCommand(stopTrepnString, device);
/*  84 */
            if (!retvalInfo.hasError()) {
/*  85 */
                ControlStatus.getCurrent().updateStatus(ControlState.STOPPING_SERVICE);
/*     */
            }
/*     */
        }
/*     */
        else
/*     */ {
/*  89 */
            retry += 1;
/*  92 */
            if (retry < 3) {
/*  93 */
                retvalInfo = startServiceCmd(device, controlAction);
/*     */
            }
/*     */
        }
/*  97 */
        return retvalInfo;
/*     */
    }

    /*     */
    private static boolean checkAndroidSystemBootedCmd(String device, ControlAction controlAction)
/*     */ {
/* 103 */
        ExecCommandRetvalInfo retvalInfo = controlAction.runAdbCommandLineCommand(COMMAND_ARRAY_CHECK_ANDROID_BOOTED, device);
/* 106 */
        if (!retvalInfo.hasError())
/*     */ {
/* 107 */
            String outputLine = retvalInfo.getLastNonEmptyOutputLine();
/* 108 */
            if ((outputLine != null) && (outputLine.equals("1"))) {
/* 109 */
                return true;
/*     */
            }
/*     */
        }
/* 115 */
        return false;
/*     */
    }

    /*     */
/*     */
    private static ExecCommandRetvalInfo startCmd(Action action, String path, String device, ControlAction controlAction, boolean displayError)
/*     */ {
/* 120 */
        String cmdIntent = null;
        String cmdDesc = null;
        String cmdPath = null;
/*     */
/* 122 */
        ExecCommandRetvalInfo retvalInfo = null;
/* 124 */
        switch (action)
/*     */ {
/*     */
            case LOAD_PREFS:
/* 126 */
                cmdIntent = "load_preferences";
/* 127 */
                cmdDesc = "load_preferences_file";
/* 128 */
                cmdPath = "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref";
/* 129 */
                break;
/*     */
            case SAVE_PREFS:
/* 131 */
                cmdIntent = "save_preferences";
/* 132 */
                cmdDesc = "save_preferences_file";
/* 133 */
                cmdPath = "/mnt/sdcard/trepn/saved_preferences/trepn_eclipse_plugin.pref";
/* 134 */
                break;
/*     */
            case START_PROFILING:
/* 137 */
                cmdIntent = "start_profiling";
/* 138 */
                cmdDesc = "database_file";
/* 139 */
                cmdPath = path;
/* 140 */
                break;
            case UPDATE_GENERATED_FILENAME:
/* 137 */
                cmdIntent = "NEW_GENERATED_FILENAME";
/* 138 */
                cmdDesc = "GENERATED_FILENAME";
/* 139 */
                cmdPath = path;
/* 140 */
                break;
/*     */
            default:
/* 144 */
                retvalInfo = new ExecCommandRetvalInfo(-1);
/* 145 */
                retvalInfo.setLastNonEmptyErrorLine("Unrecognized Trepn command");
/*     */
        }
/* 150 */
        if ((cmdIntent != null) && (cmdDesc != null) && (cmdPath != null)) {
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + device);
/* 151 */
            retvalInfo = controlAction.runAdbCommandLineCommand(
/* 152 */         new String[]{
/* 153 */         "shell", "am", "broadcast", "-a", "com.quicinc.trepn." + cmdIntent, "-e",
/* 154 */         "com.quicinc.trepn." + cmdDesc, cmdPath},
/*     */
/* 156 */         device);
/*     */
        }
/* 159 */
        return retvalInfo;
/*     */
    }

    /*     */
/*     */
    public static ExecCommandRetvalInfo startCommand(Action action, String path, String device, ControlAction controlAction, boolean displayError)
/*     */ {
/* 164 */
        ExecCommandRetvalInfo retvalInfo = null;
/* 166 */
        switch (action)
/*     */ {
/*     */
            case START_SERVICE:
/* 168 */
                retry = 0;
/* 169 */
                retvalInfo = startServiceCmd(device, controlAction);
/* 170 */
                break;
/*     */
            case STOP_SERVICE:
/* 168 */
                retry = 0;
/* 169 */
                retvalInfo = stopServiceCmd(device, controlAction);
/* 170 */
                break;
/*     */
            case LOAD_PREFS:
/*     */
            case SAVE_PREFS:
/*     */
            case START_PROFILING:
/* 175 */
                retvalInfo = startCmd(action, path, device, controlAction, displayError);
/* 176 */
                break;
            case UPDATE_GENERATED_FILENAME:
/* 175 */
                retvalInfo = startCmd(action, path, device, controlAction, displayError);
/* 176 */
                break;

/*     */
            default:
/* 180 */
                retvalInfo = new ExecCommandRetvalInfo(-1);
/* 181 */
                retvalInfo.setLastNonEmptyErrorLine("Unrecognized Trepn command");
/*     */
        }
/* 186 */
        return retvalInfo;
/*     */
    }

    /*     */
/*     */   public static enum Action
/*     */ {
        /*  29 */     LOAD_PREFS, SAVE_PREFS, START_PROFILING, START_SERVICE, STOP_SERVICE, UPDATE_GENERATED_FILENAME;
/*     */
    }
/*     */
}


/* Location:           C:\Users\DavidIgnacio\Desktop\adt-bundle-windows-x86-20131030\eclipse\plugins\com.qualcomm.trepn.ide.eclipse.control_1.1.0.201406060127.jar
 * Qualified Name:     com.qualcomm.trepn.ide.eclipse.control.actions.TrepnCommands
 * JD-Core Version:    0.7.0.1
 */