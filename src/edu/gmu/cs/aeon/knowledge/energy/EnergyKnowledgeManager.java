package edu.gmu.cs.aeon.knowledge.energy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by DavidIgnacio on 4/5/2015.
 */
public class EnergyKnowledgeManager {
    public static Map<String, Double> energyGreedyAPIs = new HashMap<String, Double>();
    //todo change hard path
    static String greedyAPiPath = "C:\\Users\\DavidIgnacio\\Downloads\\patterns_length1_score.csv";

    static {
        energyGreedyAPIs = new EnergyKnowledgeManager().processAPIFile(false);
    }

    private Map<String, Double> processAPIFile(boolean hasMultiple) {
        InputStream in = getClass().getResourceAsStream("/patterns_length1_score.csv");
        ClassLoader classLoader = getClass().getClassLoader();

        Map<String, java.util.List<Double[]>> tmp = new HashMap<String, java.util.List<Double[]>>();
        try {
            File file = new File(classLoader.getResource("/data/patterns_length1_score.csv").getFile());
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line = reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] apiVal = line.split(",");
                String api = apiVal[0];
                if (!hasMultiple) {
                    // System.out.println(api + "," + apiVal[1]);
                    energyGreedyAPIs.put(api, Double.valueOf(apiVal[1]));
                } else {
                    if (api.contains("(")) {
                        String formattedAPI = api.substring(0, api.indexOf("("));
                        java.util.List<Double[]> avgValues = tmp.get(formattedAPI);
                        if (avgValues == null) {
                            avgValues = new ArrayList<Double[]>();
                            tmp.put(formattedAPI, avgValues);
                        }
                        avgValues.add(new Double[]{Double.valueOf(apiVal[1]), Double.valueOf(apiVal[2])});
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (hasMultiple) {
            for (String key : tmp.keySet()) {
                java.util.List<Double[]> list = tmp.get(key);
                double sumPrd = 0;
                double sum = 0;
                for (Double[] values : list) {
                    sumPrd += values[0] * values[1];
                    sum += values[0];
                }
                energyGreedyAPIs.put(key, sum == 0 ? 0 : (sumPrd / sum));
            }
            for (String key : energyGreedyAPIs.keySet()) {
                System.out.println(key + "," + energyGreedyAPIs.get(key));
            }
        }
        return energyGreedyAPIs;
    }

    private Map<String, Double> processGreenDroidFile(boolean hasMultiple) {
        InputStream in = getClass().getResourceAsStream("/patterns_length1_score.csv");
        ClassLoader classLoader = getClass().getClassLoader();

        Map<String, java.util.List<Double[]>> tmp = new HashMap<String, java.util.List<Double[]>>();
        try {

            File file = new File(classLoader.getResource("/data/modeled_android_apis.txt").getFile());
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] apiVal = line.split(",");
                String api = apiVal[0];
                if (!hasMultiple) {
                    // System.out.println(api + "," + apiVal[1]);
                    energyGreedyAPIs.put(api, Double.valueOf(apiVal[1]));
                } else {
                    if (api.contains("(")) {
                        String formattedAPI = api.substring(0, api.indexOf("("));
                        java.util.List<Double[]> avgValues = tmp.get(formattedAPI);
                        if (avgValues == null) {
                            avgValues = new ArrayList<Double[]>();
                            tmp.put(formattedAPI, avgValues);
                        }
                        avgValues.add(new Double[]{Double.valueOf(apiVal[1]), Double.valueOf(apiVal[2])});
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (hasMultiple) {
            for (String key : tmp.keySet()) {
                java.util.List<Double[]> list = tmp.get(key);
                double sumPrd = 0;
                double sum = 0;
                for (Double[] values : list) {
                    sumPrd += values[0] * values[1];
                    sum += values[0];
                }
                energyGreedyAPIs.put(key, sum == 0 ? 0 : (sumPrd / sum));
            }
            for (String key : energyGreedyAPIs.keySet()) {
                System.out.println(key + "," + energyGreedyAPIs.get(key));
            }
        }
        return energyGreedyAPIs;
    }
}
